# java-org.se



#### 软件架构
Java JDK 8 +  Maven 3.6.3 

#### 介绍
Java SE 学习
File、IO 流练习
HuTool 工具框架练习
Log4j2 日志框架练习

http://localhost:8080/ ：演示验证码，也是用户登录页面
http://localhost:8080/qrCode.html ：演示二维码

http://localhost:8080/props/settingInit  ：演示 Setting 通过构造器初始化
http://localhost:8080/props/settingRead  : Setting 读取配置属性

http://localhost:8080/cron/start ：开始定时任务，非守护线程模式，如果已经开启了，重复调用时会抛个异常
http://localhost:8080/cron/stopCron ：停止定时任务，调用后会将定时器进程立即结束，如果为守护线程模式，则正在执行的作业也会自动结束，否则作业线程将在执行完成后结束。

http://localhost:8080/cors/cors_jsonp.html  ：演示 jsonp 实现跨域请求
http://localhost:8080/cors/cors_filter.html  ：演示 CorsFilter 实现跨域请求
http://localhost:8080/cors/cross_origin.html  ：演示 @CrossOrigin 实现跨域请求

http://localhost:8080/user/getCode ：获取登陆界面的验证码
http://localhost:8080/user/login?loginName=zhangSan&pwd=123456&code=xxx ：用户登陆


http://localhost:8080/excel/read1 : excel 文件读取1

swagger 在线文档：
    http://localhost:8080/swagger-ui/index.html 