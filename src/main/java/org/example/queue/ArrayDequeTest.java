package org.example.queue;

import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * 双向数组 ArrayDeque 类， 实现了 Deque接口 ‑ 可用于双端队列。
 * 由数组组成的双端队列。
 * 没有容量限制，根据需要扩容。
 * 不是线程安全的。
 * 禁止插入null元素。
 * 当用作栈时，比栈速度快，当用作队列时，速度比LinkList快。
 * 大部分方法的算法时间复杂度为O(1)。
 * remove、removeFirstOccurrence、removeLastOccurrence、contains、remove 和批量操作的算法时间复杂度O(n)
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/8/1 16:09
 */
@SuppressWarnings("all")
public class ArrayDequeTest {

    @Test
    public void testArrayDeque1() {
        Deque<Integer> arrayDeque = new ArrayDeque<Integer>(10);
        for (int i = 0; i < 20; i++) {
            arrayDeque.add(i);
        }
        //[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
        System.out.println(arrayDeque);

        while (!arrayDeque.isEmpty()) {
            //19,	18,	17,	16,	15,	14,	13,	12,	11,	10,	9,	8,	7,	6,	5,	4,	3,	2,	1,	0,
            System.out.print(arrayDeque.pollLast() + ",\t");
        }
    }

    @Test
    public void testArrayDeque2() {
        Deque<Integer> arrayDeque = new ArrayDeque<Integer>(10);
        for (int i = 0; i < 20; i++) {
            arrayDeque.add(i);
        }
        //[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
        System.out.println(arrayDeque);

        /**
         * E peek()
         * 检索但不删除此 deque 表示的队列头（此deque的第一个元素），如果此deque为空，则返回 null
         */
        while (arrayDeque.peek() != null) {
            //0,	1,	2,	3,	4,	5,	6,	7,	8,	9,	10,	11,	12,	13,	14,	15,	16,	17,	18,	19,
            System.out.print(arrayDeque.pollFirst() + ",\t");
        }
    }


}
