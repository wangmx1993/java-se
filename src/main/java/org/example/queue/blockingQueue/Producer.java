package org.example.queue.blockingQueue;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2019/3/10 0010.
 * 生产者 线程任务
 */
public class Producer extends Thread {
    /**
     * blockingQueue：阻塞队列
     * random：用于产生随机整数
     */
    private BlockingQueue<String> blockingQueue;
    private Random random;

    public Producer(BlockingQueue blockingQueue) {
        this.blockingQueue = blockingQueue;
        this.random = new Random();
    }

    @Override
    public void run() {
        try {
            /** 循环随机产生3个字符串*/
            for (int i = 0; i < 3; i++) {
                String info = "中国制造 China2019889866" + (i + 1);
                /**往链式队列中添加元素，如果队列中的元素个数以已经达到队列容量，则put方法会阻塞
                 * 直到里面有元素被移除时，才能再次添加
                 * 链式队列添加元素与消费者是否已经消费无关，只要队列有容量，则可以一直添加*/
                blockingQueue.put(info);
                System.out.println(Thread.currentThread().getName() + " 生产了消息：" + info);
                TimeUnit.SECONDS.sleep(1 + random.nextInt(5));
            }
            /**这里往队列中插入一个标识"exit"，告诉它生产已经完毕，本任务即将结束*/
            blockingQueue.put("exit");
            System.out.println(Thread.currentThread().getName() + " 发送任务结束标识：exit");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}