package org.example.lombok;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Java Bean
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/3/25 9:26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private String p_id;
    private String p_name;
    private Integer age;
    private Date birthday;

}
