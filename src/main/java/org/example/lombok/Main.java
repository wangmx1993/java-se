package org.example.lombok;

import cn.hutool.core.date.DateUtil;

/**
 * lombok 测试
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2023/9/17 13:45
 */
public class Main {
    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test2() {
        Person person = new Person("9528", "华富", 33, DateUtil.parse("2008/08/05"));
        System.out.println(person);
    }

    private static void test1() {
        Person person = new Person();
        person.setAge(18);
        person.setBirthday(DateUtil.parse("1993/08/25"));
        person.setP_id("9527");
        person.setP_name("华安");
        System.out.println(person);
    }

}
