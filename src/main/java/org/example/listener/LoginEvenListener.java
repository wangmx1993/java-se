package org.example.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 为自定义用户登录事件{@link LoginEven} 设置应用监听器
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/10/24 9:54
 */
@Component
public class LoginEvenListener implements ApplicationListener<LoginEven> {

    /**
     * 处理应用程序事件。
     * ApplicationContext.publishEvent(ApplicationEvent event) 每调用发布一次事件，本方法就会触发一次。
     *
     * @param loginEven ：自定义用户登录事件
     */
    @Override
    public void onApplicationEvent(LoginEven loginEven) {
        /**
         * long getTimestamp()：返回事件发生时的系统时间（以毫秒为单位）
         */
        long timestamp = loginEven.getTimestamp();
        System.out.println("发生用户登录事件(org.example.listener.LoginEven)：" + timestamp + "\t" + loginEven);
    }

}
