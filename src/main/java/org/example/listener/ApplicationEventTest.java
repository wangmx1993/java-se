package org.example.listener;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * ApplicationEvent 事件测试
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/10/24 10:37
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ApplicationEventTest {

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * 测试{@link ContextRefreshedEventListener} 监听器
     * 应用启动就会自动注入 bean 到 Spring 容器会自动初始化，然后自动会触发 {@link ContextRefreshedEventListener} 监听器
     */
    @Test
    public void testContextRefreshedEvent() {

    }

    /**
     * 测试自定义事件 {@link LoginEven}
     * void publishEvent(ApplicationEvent event)：将应用程序事件通知所有注册到此应用程序的匹配的侦听器。
     * * 事件可以是框架事件（例如ContextRefreshedEvent）或特定于应用程序的自定义事件。
     */
    @Test
    public void testLoginEven() {
        String loginName = "hangSan";
        LoginEven loginEven = new LoginEven("loginEven", null, loginName);
        applicationContext.publishEvent(loginEven);

        loginName = "LiSi";
        loginEven = new LoginEven("loginEven", null, loginName);
        applicationContext.publishEvent(loginEven);
    }
}
