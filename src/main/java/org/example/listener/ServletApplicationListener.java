package org.example.listener;

import cn.hutool.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.ServletRequestHandledEvent;

/**
 * RequestHandledEvent 是一个 web-specific 事件，只能应用于使用 DispatcherServlet 的 Web 应用。
 * 在使用 Spring 作为前端的 MVC 控制器时，当 Spring 处理用户请求结束后，系统会自动触发该事件。
 * 它的实现类是 ServletRequestHandledEvent。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/4 15:31
 */
@Component
public class ServletApplicationListener implements ApplicationListener<ServletRequestHandledEvent> {

    private static final Logger log = LoggerFactory.getLogger(ServletApplicationListener.class);

    @Override
    public void onApplicationEvent(ServletRequestHandledEvent event) {
        JSONObject jsonObject = new JSONObject();
        // 请求相关信息
        jsonObject.set("description", event.getDescription());
        // 访问路径
        jsonObject.set("requestUrl", event.getRequestUrl());
        // 请求系统响应花费时间
        jsonObject.set("processingTimeMillis", event.getProcessingTimeMillis());
        // 请求发生时间
        jsonObject.set("timestamp", event.getTimestamp());
        // 请求失败原因异常
        jsonObject.set("failureCause", event.getFailureCause());
        // 请求响应HTTP状态值
        jsonObject.set("statusCode", event.getStatusCode());

        // {
        //     "description": "url=[/value/get]; client=[127.0.0.1]; method=[GET]; servlet=[dispatcherServlet]; session=[null]; user=[null]; time=[6ms]; status=[OK]",
        //     "processingTimeMillis": 6,
        //     "requestUrl": "/value/get",
        //     "timestamp": 1670140786484,
        //     "statusCode": 200
        // }
        log.info(jsonObject.toStringPretty());
    }

}
