package org.example.listener;

import org.springframework.context.ApplicationEvent;

import java.util.UUID;

/**
 * 自定义用户登录事件，继承 {@link ApplicationEvent} 事件抽象类
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/10/24 9:43
 */
public class LoginEven extends ApplicationEvent {
    private String id = "0";
    private String message = "";

    /**
     * 创建一个新的 ApplicationEvent
     * 这个构造器是默认且必须创建的，必须将事件最初发生的对象 source 传递上去。
     *
     * @param source：事件最初发生的对象或与事件关联的对象。值不能为 null，但是值也没有强制规定。
     *                                       可以参阅 {@link org.springframework.context.event.ContextRefreshedEvent}
     *                                       可以参阅 {@link org.springframework.context.event.ContextClosedEvent}
     */
    public LoginEven(Object source) {
        super(source);
    }

    /**
     * 构造器传参
     *
     * @param source  ：事件最初发生的对象或与事件关联的对象。值不能为 null，但是值也没有强制规定。
     *                可以参阅 {@link org.springframework.context.event.ContextRefreshedEvent}
     *                可以参阅 {@link org.springframework.context.event.ContextClosedEvent}
     * @param id      ：事件ID
     * @param message ：事件消息
     */
    public LoginEven(Object source, String id, String message) {
        super(source);
        id = id == null || "".equals(id) ? UUID.randomUUID().toString().replace("-", "") : id;
        this.id = id;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "LoginEven{" +
                "id='" + id + '\'' +
                ", message='" + message + '\'' +
                ", source=" + source +
                '}';
    }
}
