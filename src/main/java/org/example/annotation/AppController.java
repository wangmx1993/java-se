package org.example.annotation;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 控制层接口
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/8/25 9:24
 */
@RestController
@ClassOperate(desc = "AppController控制层", value = {"A1", "B2"})
@MultipleOperate(value = {"x11", "x22", "x33"}, desc = "AppController")
@SuppressWarnings("all")
public class AppController {
    /**
     * http://localhost:8080/annotation/method1
     *
     * @return
     */
    @GetMapping("/annotation/method1")
    @MethodOperate(desc = "查询方法.")
    public Map<String, Object> method1() {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("code", 200);
        dataMap.put("msg", "success");
        dataMap.put("data", "method1");
        return dataMap;
    }

    /**
     * http://localhost:8080/annotation/method2
     *
     * @return
     */
    @GetMapping("/annotation/method2")
    public Map<String, Object> method2() {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("code", 200);
        dataMap.put("msg", "success");
        dataMap.put("data", "method1");
        return dataMap;
    }

    /**
     * http://localhost:8080/annotation/method3
     *
     * @return
     */
    @GetMapping("/annotation/method3")
    @MultipleOperate(value = {"x1", "x2", "x3"}, desc = "method3")
    public Map<String, Object> method3() {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("code", 200);
        dataMap.put("msg", "success");
        dataMap.put("data", "method1");
        return dataMap;
    }

}
