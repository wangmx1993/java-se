package org.example.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 1: @Target: 表示注解使用的目标位置, 常用的有:
 * * TYPE(类,接口,注解,枚举), FIELD(成员变量), METHOD(方法), PARAMETER(形式参数), CONSTRUCTOR(构造器)
 * 2: @Retention: 表示注解生命范围, 类似 maven pom.xml 文件的 scope 属性, 可选值有:
 * * SOURCE(编译器将丢弃注解)
 * * CLASS(注解将由编译器记录在类文件中，但不需要在运行时由VM保留,这是默认行为),
 * * RUNTIME(注解将由编译器记录在类文件中，并在运行时由VM保留，因此可以反射地读取它们)
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/8/24 20:37
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ClassOperate {

    /**
     * 自定义属性值
     *
     * @return
     */
    String[] value() default {};

    /**
     * 描述
     *
     * @return
     */
    String desc() default "";
}
