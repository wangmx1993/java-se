package org.example.enums;

import org.junit.Test;

/**
 * 枚举常量 & values 遍历
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/4/29 10:51
 */
public class WeekEnum1Test {
    public static void main(String[] args) {
        //类似静态类，也是 枚举名.值 直接获取，每个值都是 Enum 实例
        WeekEnum1 sundayEnum = WeekEnum1.Sunday;

        //因为 Enum 重写了 toString 方法，方法中返回 name 属性
        //输出 Sunday
        System.out.println("WeekEnum1.Sunday=" + sundayEnum.toString());

        //返回此枚举常量的名称，与其枚举声明中声明的完全相同，看源码一目了然
        //输出 Sunday
        System.out.println("weekEnum.name()=" + sundayEnum.name());

        //每个枚举类都有一个 values 方法，可以遍历枚举类的所有 Enum 实例.
        WeekEnum1[] weekEnum1s = WeekEnum1.values();

        for (WeekEnum1 weekEnum_1_loop : weekEnum1s) {
            //ordinal()：返回此枚举常数的序号（就是在枚举声明中的位置，从0开始）。
            System.out.print(weekEnum_1_loop.ordinal() + ":" + weekEnum_1_loop.name() + "  ");
        }

        /**
         * compareTo：看源码一目了然，就是根据序号 ordinal 对比同类型的枚举值是否相等
         * 自己的序号减去对比项的序号，如果为0则相等，否则就是正负数。注意只能对比相同枚举类型
         */
        WeekEnum1 mondayEnum = WeekEnum1.Monday;
        int flag1 = sundayEnum.compareTo(mondayEnum);
        int flag2 = sundayEnum.compareTo(WeekEnum1.Sunday);
        //输出 6、0
        System.out.println("\n" + flag1 + "、" + flag2);
    }

    /**
     * 每个枚举类都有一个 values 方法，该方法可以遍历枚举类的所有实例.
     * 1、可以直接通过类名调用 values 方法，小哥哥都是一样的。
     * 2、也可以通过枚举常量调用 values 方法，小哥哥都是一样的。
     */
    @Test
    public void testValues() {
        WeekEnum1[] values = WeekEnum1.values();
        for (WeekEnum1 weekEnum1 : values) {
            // Monday	Tuesday	Wednesday	Thursday	Friday	Saturday	Sunday
            System.out.print(weekEnum1 + "\t");
        }
        System.out.println();

        WeekEnum1[] values1 = WeekEnum1.Sunday.values();
        for (WeekEnum1 weekEnum1 : values1) {
            // Monday	Tuesday	Wednesday	Thursday	Friday	Saturday	Sunday
            System.out.print(weekEnum1 + "\t");
        }
    }


}
