package org.example.enums;

import cn.hutool.core.util.StrUtil;

import java.util.HashSet;
import java.util.Set;

/**
 * 星期，一周的枚举-------枚举属性、方法、参数
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/4/29 11:03
 */
public enum WeekEnum2 {
    /**
     * 枚举值写在最前面，有其他内容时，必须以分号结尾。
     * 因为枚举只能通过常量获取枚举实例，所以枚举参数必须与构造器中的参数对应
     * 匹配两个参数的构造器创建 1-6 实例
     * 匹配三个参数的构造器创建 Sunday 实例
     */
    Monday(1, "星期一"),
    Tuesday(2, "星期二"),
    Wednesday(3, "星期三"),
    Thursday(4, "星期四"),
    Friday(5, "星期五"),
    Saturday(6, "星期六"),
    Sunday(7, "星期天", "周末");

    /**
     * 属性名称可以随便取，只需要构造器中的参数类型必须与上面的枚举参数类型对应
     */
    private final Integer code;
    private final String name;
    private String ext;
    /**
     * 支持定义静态属性
     */
    public static String summary;

    /**
     * 构造器默认是 private ，外部无法 new 枚举对象，只能通过上面的常量获取实例，必须与枚举参数对应
     *
     * @param code
     * @param name
     */
    WeekEnum2(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    WeekEnum2(Integer code, String name, String ext) {
        this.code = code;
        this.name = name;
        this.ext = ext;
    }

    /**
     * 因为枚举常量的参数都是通过内部私有的构造器赋值的，无法通过外部赋值，所以没必要提供 setter方法
     * 但是可以提供 getter 方法来获取枚举参数值
     *
     * @return
     */
    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getExt() {
        return ext;
    }

    /**
     * 获取工作日 周一到周五-----支持静态方法
     *
     * @return
     */
    public static Set<WeekEnum2> getWorkDays() {
        Set<WeekEnum2> workDays = new HashSet<>(5);
        workDays.add(WeekEnum2.Monday);
        workDays.add(WeekEnum2.Tuesday);
        workDays.add(WeekEnum2.Wednesday);
        workDays.add(WeekEnum2.Thursday);
        workDays.add(WeekEnum2.Friday);
        return workDays;
    }

    /**
     * 根据 星期数字获取对应的星期中文
     *
     * @param code 星期几 [1,7]
     * @return ：返回中文，星期一、星期二、...
     */
    public static WeekEnum2 getWorkDayByCode(Integer code) {
        WeekEnum2[] values = WeekEnum2.values();
        for (WeekEnum2 value : values) {
            if (StrUtil.equals(String.valueOf(value.code), String.valueOf(code))) {
                return value;
            }
        }
        return null;
    }


}