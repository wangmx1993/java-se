package org.example.enums;

import org.junit.Test;

import java.util.List;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/4/29 12:21
 */
public class WeekEnum4Test {
    public static void main(String[] args) {
        //枚举名.值 获取实例，这些值之所以叫枚举常量，因为它的值就如同常量一样一开始就顶死了的，无法修改
        WeekEnum4 mondayEnum = WeekEnum4.Monday;
        WeekEnum4 tuesdayEnum = WeekEnum4.Tuesday;
        // 输出：1 - 星期一
        System.out.println(mondayEnum.getCode() + " - " + mondayEnum.getName());
        // 输出：2 - 星期二
        System.out.println(tuesdayEnum.getCode() + " - " + tuesdayEnum.getName());
        // 输出：1,星期一
        System.out.println(mondayEnum.toSplitString());
        // 输出：2,星期二
        System.out.println(tuesdayEnum.toSplitString());
    }

    /**
     * 静态方法测试
     */
    @Test
    public void testPublicFun() {
        WeekEnum4 weekEnum1 = WeekEnum4.of(1);
        WeekEnum4 weekEnum7 = WeekEnum4.of(7);

        // 1,星期一
        System.out.println(weekEnum1.toSplitString());
        // 7,星期二
        System.out.println(weekEnum7.toSplitString());

        List<WeekEnum4> weekEnum4s = WeekEnum4.list();
        // [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]
        System.out.println(weekEnum4s);
    }


}