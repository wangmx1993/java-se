package org.example.enums;

import java.util.Map;

/**
 * 提供枚举实现的接口
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/4/29 12:05
 */
public interface WeekEnum3Interface {
    /**
     * 获取编码
     */
    Integer getCode();

    /**
     * 获取名称
     */
    String getName();

    /**
     * 获取全部枚举数据
     */
    Map<String, Object> getDataMap();
}