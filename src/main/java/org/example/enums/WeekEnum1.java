package org.example.enums;

/**
 * 枚举常量————星期，一周的枚举
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/4/29 10:49
 */
public enum WeekEnum1 {
    /**
     * 最简单的枚举形式，使用逗号分隔，结尾分号此时可以省略
     * 每一个值都代表当前的枚举实例，通过 枚举名.值 获取
     */
    Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
}