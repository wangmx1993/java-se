package org.example.enums;

import cn.hutool.core.util.StrUtil;

/**
 * 供养信息上报常量统一维护与管理
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/9 14:12
 */
public class EscalationConsts {

    /**
     * escalationType(上报类型) 1区划；2单位；3人员
     */
    public enum Type {
        MOF(1, "区划"),
        AGENCY(2, "单位"),
        PERSON(3, "人员");

        private final int code;
        private final String name;

        Type(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getIntCode() {
            return code;
        }

        public String getStrCode() {
            return String.valueOf(code);
        }

        public String getName() {
            return name;
        }

        /**
         * 根据 code 查询上报类型
         *
         * @param code
         * @return
         */
        public static Type getTypeByCode(String code) {
            Type[] values = Type.values();
            for (Type type : values) {
                if (StrUtil.equals(type.getStrCode(), code)) {
                    return type;
                }
            }
            return null;
        }
    }

    /**
     * 上报岗位状态：1-乡镇/街道;2-区县;3-市级;4-省级;5-已完成
     */
    public enum Status {
        STREET(1, "乡镇/街道"),
        COUNTY(2, "区县"),
        CITY(3, "市级"),
        PROVINCE(4, "省级"),
        FINISH(5, "已完成");

        private final int code;
        private final String name;

        Status(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getIntCode() {
            return code;
        }

        public String getStrCode() {
            return String.valueOf(code);
        }

        public String getName() {
            return name;
        }

        /**
         * 根据 code 查询上报岗位状态
         *
         * @param code
         * @return
         */
        public static Status getStatusByCode(String code) {
            Status[] values = Status.values();
            for (Status status : values) {
                if (StrUtil.equals(status.getStrCode(), code)) {
                    return status;
                }
            }
            return null;
        }

    }
}
