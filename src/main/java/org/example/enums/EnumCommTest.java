package org.example.enums;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/4/29 12:33
 */
public class EnumCommTest {
    public static void main(String[] args) {
        EnumComm.AreaEnum huNan = EnumComm.AreaEnum.HuNan;
        // 43:湖南
        System.out.println(huNan.getCode() + ":" + huNan.getName());

        EnumComm.ColorEnum yellow = EnumComm.ColorEnum.Yellow;
        // yellow:黄色
        System.out.println(yellow.getCode() + ":" + yellow.getName());
    }
}
