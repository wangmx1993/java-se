package org.example.enums;

import org.junit.Test;

import java.util.Set;

/**
 * 星期，一周的枚举-------枚举属性、方法、参数
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/4/29 11:20
 */
public class WeekEnum2Test {
    public static void main(String[] args) {
        // 枚举名.值 获取实例，这些值之所以叫枚举常量，因为它的值就如同常量一样一开始就顶死了的，无法修改
        WeekEnum2 sundayEnum = WeekEnum2.Sunday;
        WeekEnum2 mondayEnum = WeekEnum2.Monday;
        //上面的实例包含了自己枚举参数，所以可以通过 getter 方法获取
        // 7->星期天->周末
        System.out.println(sundayEnum.getCode() + "->" + sundayEnum.getName() + "->" + sundayEnum.getExt());
        // 1->星期一->null  ,因为星期一没有提供 ext 参数，所以为 null
        System.out.println(mondayEnum.getCode() + "->" + mondayEnum.getName() + "->" + mondayEnum.getExt());

        Set<WeekEnum2> workDays = WeekEnum2.getWorkDays();
        // [Tuesday, Friday, Wednesday, Thursday, Monday]
        System.out.println(workDays);

        WeekEnum2.summary = "蚩尤后裔";
        // 蚩尤后裔
        System.out.println(WeekEnum2.summary);

        // Monday
        System.out.println(WeekEnum2.getWorkDayByCode(1));
        // Wednesday
        System.out.println(WeekEnum2.getWorkDayByCode(3));
        // Thursday
        System.out.println(WeekEnum2.getWorkDayByCode(4));
        // Sunday
        System.out.println(WeekEnum2.getWorkDayByCode(7));
        // null
        System.out.println(WeekEnum2.getWorkDayByCode(9));

    }

    /**
     * 枚举作为 switch (xxx) 参数时，case 中的值必须是枚举常量的非限定名称。
     * 不能是 枚举.常量 的方式，否则编译不通过：
     * An enum switch case label must be the unqualified name of an enumeration constant.
     */
    @Test
    public void testSwitchCase() {
        WeekEnum2 workDayByCode = WeekEnum2.getWorkDayByCode(9);
        switch (workDayByCode) {
            // 这种方式是编译不通过的，必须是非限定名称
            // case WeekEnum2.Monday:
            case Monday:
                System.out.println(workDayByCode.getCode());
                break;
            case Saturday:
                System.out.println(workDayByCode.getName());
                break;
            case Sunday:
                System.out.println(workDayByCode.getExt());
                break;
            default:
                System.out.println(workDayByCode);
                break;
        }
    }

    @Test
    public void valueOfTest() {
        WeekEnum2 monday = WeekEnum2.valueOf("Monday");
        // Monday 1 星期一 null
        System.out.println(monday + " " + monday.getCode() + " " + monday.getName() + " " + monday.getExt());

        WeekEnum2 sunday = Enum.valueOf(WeekEnum2.class, "Sunday");
        // Sunday 7 星期天 周末
        System.out.println(sunday + " " + sunday.getCode() + " " + sunday.getName() + " " + sunday.getExt());

        // true
        System.out.println(monday == WeekEnum2.Monday);
        // true
        System.out.println(monday.equals(WeekEnum2.Monday));
    }

}