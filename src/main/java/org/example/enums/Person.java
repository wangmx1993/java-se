package org.example.enums;

/**
 * 枚举创建单例
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/16 14:30
 */
public class Person {
    private String agencyCode;
    private String idenNo;
    private String idenType;
    private String perName;
    private String sexCode;
    private String control;

    /**
     * 私有化构造函数
     */
    private Person() {
    }

    private enum SingletonEnum {
        /**
         * 枚举类内部定义的枚举值就是该类的实例，必须在第一行定义，当类初始化时，这些枚举值会被实例化.
         * 创建一个枚举对象，该对象天生为单例。
         * 枚举INSTANCE会在类加载初始化的时候创建,而Java类的加载和初始化过程都是线程安全的。
         */
        INSTANCE;
        /**
         * 枚举是一个特殊的类，可以定义自己的 field(包括静态属性)、方法(包括静态方法)、可以实现接口，也可以定义自己的构造器。
         */
        private final Person person;

        /**
         * 枚举类的构造方法默认用 private 修饰的，且不能出现 public 构造方法，因此无法 new 一个枚举对象。
         */
        SingletonEnum() {
            person = new Person();
        }

        public Person getInstance() {
            return person;
        }
    }

    /**
     * 对外暴露一个获取单例对象的静态方法
     *
     * @return
     */
    public static Person getInstance() {
        return SingletonEnum.INSTANCE.getInstance();
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getIdenNo() {
        return idenNo;
    }

    public void setIdenNo(String idenNo) {
        this.idenNo = idenNo;
    }

    public String getIdenType() {
        return idenType;
    }

    public void setIdenType(String idenType) {
        this.idenType = idenType;
    }

    public String getPerName() {
        return perName;
    }

    public void setPerName(String perName) {
        this.perName = perName;
    }

    public String getSexCode() {
        return sexCode;
    }

    public void setSexCode(String sexCode) {
        this.sexCode = sexCode;
    }

    public String getControl() {
        return control;
    }

    public void setControl(String control) {
        this.control = control;
    }

    @Override
    public String toString() {
        return "Person{" +
                "agencyCode='" + agencyCode + '\'' +
                ", idenNo='" + idenNo + '\'' +
                ", idenType='" + idenType + '\'' +
                ", perName='" + perName + '\'' +
                ", sexCode='" + sexCode + '\'' +
                ", control='" + control + '\'' +
                '}';
    }
}
