package org.example.enums;

import com.google.common.collect.Lists;

import java.util.*;

/**
 * 星期，一周的枚举-----------枚举 enum 定义 抽象方法、静态方法
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/4/29 12:19
 */
public enum WeekEnum4 {
    /**
     * 枚举值写在最前面，有其他内容时，必须以分号结尾。
     * 因为枚举只能通过常量获取枚举实例，所以枚举参数必须与构造器中的参数对应
     */
    Monday(1, "星期一") {
        @Override
        public String toSplitString() {
            return Monday.code + "," + Monday.name;
        }
    },
    Tuesday(2, "星期二") {
        @Override
        public String toSplitString() {
            return Tuesday.getCode() + "," + Tuesday.getName();
        }
    },
    Wednesday(3, "星期三") {
        @Override
        public String toSplitString() {
            return Wednesday.getCode() + "," + Tuesday.getName();
        }
    },
    Thursday(4, "星期四") {
        @Override
        public String toSplitString() {
            return Thursday.getCode() + "," + Tuesday.getName();
        }
    },
    Friday(5, "星期五") {
        @Override
        public String toSplitString() {
            return Friday.getCode() + "," + Tuesday.getName();
        }
    },
    Saturday(6, "星期六") {
        @Override
        public String toSplitString() {
            return Saturday.getCode() + "," + Tuesday.getName();
        }
    },
    Sunday(7, "星期天") {
        @Override
        public String toSplitString() {
            return Sunday.getCode() + "," + Tuesday.getName();
        }
    };

    /**
     * 属性名称可以随便取，但是构造器中的参数类型必须与上面的枚举参数类型对应
     */
    private final Integer code;
    private final String name;

    /**
     * 全部枚举值放入到常量中
     * 在静态代码块中为其赋值
     */
    private static final Map<Integer, WeekEnum4> weekEnum4Map = new HashMap<>();

    static {
        for (WeekEnum4 weekEnum4 : values()) {
            weekEnum4Map.put(weekEnum4.getCode(), weekEnum4);
        }
    }

    /**
     * 构造器默认是 private ，外部无法 new 枚举对象，只能通过上面的常量获取实例，必须与枚举参数对应
     *
     * @param code
     * @param name
     */
    WeekEnum4(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    /**
     * 枚举中可以有抽象方法，且由枚举值/枚举常量提供方法实现
     * 方法要是 public 修饰外部才能调用。
     *
     * @return
     */
    public abstract String toSplitString();

    /**
     * 根据数字获取星期枚举
     * 静态方法，方便通过类名调用。
     *
     * @param index
     * @return
     */
    public static WeekEnum4 of(Integer index) {
        return weekEnum4Map.get(index);
    }

    /**
     * 获取全部枚举项
     *
     * @return
     */
    public static List<WeekEnum4> list() {
        WeekEnum4[] values1 = WeekEnum4.values();
        ArrayList<WeekEnum4> weekEnum4s = Lists.newArrayList(values1);
        return weekEnum4s;
    }

}