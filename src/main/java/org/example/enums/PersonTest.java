package org.example.enums;

import org.junit.Test;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 枚举创建单例
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2023/7/8 10:18
 */
public class PersonTest {

    /**
     * 1147525854 Person{agencyCode='null', idenNo='null', idenType='null', perName='null', sexCode='null', control='null'}
     * 1147525854 Person{agencyCode='null', idenNo='null', idenType='null', perName='null', sexCode='null', control='null'}
     * 1147525854 Person{agencyCode='null', idenNo='null', idenType='null', perName='null', sexCode='null', control='null'}
     * 1147525854 Person{agencyCode='null', idenNo='null', idenType='null', perName='null', sexCode='null', control='null'}
     * 1147525854 Person{agencyCode='null', idenNo='null', idenType='null', perName='null', sexCode='null', control='null'}
     * 1147525854 Person{agencyCode='null', idenNo='null', idenType='null', perName='null', sexCode='null', control='null'}
     * 1147525854 Person{agencyCode='null', idenNo='null', idenType='null', perName='null', sexCode='null', control='null'}
     * 1147525854 Person{agencyCode='null', idenNo='null', idenType='null', perName='null', sexCode='null', control='null'}
     * 1147525854 Person{agencyCode='null', idenNo='null', idenType='null', perName='null', sexCode='null', control='null'}
     * 1147525854 Person{agencyCode='null', idenNo='null', idenType='null', perName='null', sexCode='null', control='null'}
     *
     * @throws InterruptedException
     */
    @Test
    public void test1() throws InterruptedException {
        int count = 10;
        CountDownLatch countDownLatch = new CountDownLatch(count);
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < count; i++) {
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    Person person = Person.getInstance();
                    System.out.println(person.hashCode() + " " + person);
                    try {
                        TimeUnit.MILLISECONDS.sleep(1000 + new Random().nextInt(3000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await();
    }

    public static void main(String[] args) {

    }
}
