package org.example.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 星期，一周的枚举-------枚举实现接口
 * 枚举 enum 默认已经继承了 Enum 类，所以无法再继承，但是可以实现接口。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/4/29 12:09
 */
public enum WeekEnum3 implements WeekEnum3Interface {
    //枚举值写在最前面，有其他内容时，必须以分号结尾。
    //因为枚举只能通过常量获取枚举实例，所以枚举参数必须与构造器中的参数对应
    //匹配两个参数的构造器创建实例
    Monday(1, "星期一"),
    Tuesday(2, "星期二"),
    Wednesday(3, "星期三"),
    Thursday(4, "星期四"),
    Friday(5, "星期五"),
    Saturday(6, "星期六"),
    Sunday(7, "星期天");

    /**
     * 属性名称可以随便取，但是构造器中的参数类型必须与上面的枚举参数类型对应
     */
    private final Integer code;
    private final String name;

    /**
     * 构造器默认是 private ，外部无法 new 枚举对象，只能通过上面的常量获取实例，必须与枚举参数对应
     *
     * @param code
     * @param name
     */
    WeekEnum3(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getName() {
        return this.name;
    }

    /**
     * 将整个枚举参数转为 Map 结构
     *
     * @return
     */
    @Override
    public Map<String, Object> getDataMap() {
        Map<String, Object> dataMap = new HashMap<>(WeekEnum3.values().length);
        for (WeekEnum3 weekEnum : WeekEnum3.values()) {
            dataMap.put(weekEnum.code + "", weekEnum.name);
        }
        return dataMap;
    }
}
