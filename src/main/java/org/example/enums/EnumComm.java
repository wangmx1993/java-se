package org.example.enums;

/**
 * 在一个类的内部维护多个枚举
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/4/29 12:26
 */
public class EnumComm {

    /**
     * 红绿灯颜色枚举
     */
    public enum ColorEnum {
        Red("red", "红色"),
        Yellow("yellow", "黄色"),
        Green("green", "绿色");

        private final String code;
        private final String name;

        ColorEnum(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

    /**
     * 系统上线省份枚举
     */
    public enum AreaEnum {
        HuNan("43", "湖南"),
        HeNan("41", "河南"),
        LiaoNiang("21", "辽宁");
        private final String code;
        private final String name;

        AreaEnum(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

}
