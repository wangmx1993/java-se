package org.example.enums;

import java.util.Map;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/4/29 12:12
 */
public class WeekEnum3Test {
    public static void main(String[] args) {
        // 枚举名.值 获取实例，这些值之所以叫枚举常量，因为它的值就如同常量一样一开始就顶死了的，无法修改
        WeekEnum3 sundayEnum = WeekEnum3.Sunday;
        // 7 - 星期天
        System.out.println(sundayEnum.getCode() + " - " + sundayEnum.getName());

        Map<String, Object> dataMap = sundayEnum.getDataMap();
        // {1=星期一, 2=星期二, 3=星期三, 4=星期四, 5=星期五, 6=星期六, 7=星期天}
        System.out.println(dataMap);
    }
}