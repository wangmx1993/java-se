package org.example.hutool;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import org.example.uitls.ResultData;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * HuTool ReUtil 正则表达式练习
 * HuTool 常用正则：src/test/java/org/example/hutool/other/ValidatorTest.java
 * 原生正则PAI：src/main/java/org/example/uitls/PatternTest.java
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2025/1/1 9:36
 */
@RestController
public class ReUtilController {
    /**
     * 默认情况下，.字符不会匹配换行符，但在Pattern.DOTALL模式下，.将会匹配任意字符，包括换行符。
     * 在处理包含换行的文本时，使用Pattern.DOTALL模式可以确保正则表达式能够匹配整个文本，而不仅仅是第一行。
     * 所以单行注释时不能使用DOTALL模式，而多行注释时，必须使用DOTALL模式。
     */
    private static final Pattern COMMENT_PATTERN_LINE1 = Pattern.compile("#.*");
    private static final Pattern COMMENT_PATTERN_LINE2 = Pattern.compile("--.*");
    private static final Pattern COMMENT_PATTERN_BLOCK = Pattern.compile("/\\*.*?\\*/", Pattern.DOTALL);

    /**
     * 清除SQL文本中的注释
     * 需求：从页面传入、或者从文件读取的sql内容中有注释信息，需要移除掉注释，方便后续程序执行干净的sql语句，防止因为存在注释而执行sql报错。
     * 支持注释类型：
     * * 单行注释1：# 注释文字
     * * 单行注释2：-- 注释文字
     * * 多行注释：/* 注释文字 *╱
     * <p>
     * http://localhost:8080/huTool/reUtil/delSqlComments
     * sql注释测试内容：static\sql\sqlCommentsTest.sql
     * <p>
     *
     * @param bodyMap ：sqlText 属性传入原生sql文本，请使用base64转码后传入。
     *                base64在线转码：https://tool.chinaz.com/tools/base64.aspx
     *                比如：{"sqlText": "LS0gc3Fs5rOo6YeK5rWL6K+V5paH5Lu2CgoKc2VsZWN0ICogZnJvbSBlbXAgdDsKCnNlbGVjdCAqIGZyb20gZW1wIHQ7LS3mn6Xor6Llhajpg6jlkZjlt6Xkv6Hmga8KCnNlbGVjdCAqIGZyb20gZW1wIHQ7LS0g5p+l6K+i5YWo6YOo5ZGY5bel5L+h5oGvLS0gLi0gLSA9PT0KCi0tICPmn6Xor6Llhajpg6jlkZjlt6Xkv6Hmga8tLSAuLSAtID09PS8qd214Ki8Kc2VsZWN0ICogZnJvbSBlbXAgdCB3aGVyZSB0LmVtcG5vID0gJzEwJy0tYXJncz1bMTBdOwoKLyoK5p+l6K+i6YOo6Zeo5L+h5oGvCiovCnNlbGVjdAogdC5pZCwgLS0g6YOo6ZeoaWQKIHQuY29kZSwgLS3pg6jpl6jnvJbnoIEvKuWUr+S4gCovCiB0Lm5hbWUsIC8q6YOo6Zeo5ZCN56ewKi8KIHQuY3JlYXRlX2RhdGUgLyrliJvlu7rml6XmnJ8KIC0t5pe26Ze0Ki8KIGZyb20gZGVwdCB0LS0vKumDqOmXqOihqCovCiB3aGVyZSBpc19kZWxldGVkPTI7LS3lj6rmn6Xor6LmnInmlYjnmoTmlbDmja4KCg=="}
     * @return
     */
    @PostMapping("huTool/reUtil/delSqlComments")
    public ResultData<String> delSqlComments(@RequestBody Map<String, Object> bodyMap) {
        String sqlText = MapUtil.getStr(bodyMap, "sqlText");
        if (StrUtil.isBlank(sqlText)) {
            return ResultData.success(sqlText);
        }
        sqlText = Base64.decodeStr(sqlText);//base64解码
        // 删除匹配的全部多行注释内容，因为多行注释可以包含单位注释符号，所以必须放在它前面;
        sqlText = ReUtil.delAll(COMMENT_PATTERN_BLOCK, sqlText);
        sqlText = ReUtil.delAll(COMMENT_PATTERN_LINE1, sqlText);
        sqlText = ReUtil.delAll(COMMENT_PATTERN_LINE2, sqlText);
        return ResultData.success(sqlText);
    }

}
