package org.example.hutool;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/16 14:30
 */
public class Person {
    private String agencyCode;
    private String idenNo;
    private String idenType;
    private String perName;
    private String sexCode;
    private String control;

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getIdenNo() {
        return idenNo;
    }

    public void setIdenNo(String idenNo) {
        this.idenNo = idenNo;
    }

    public String getIdenType() {
        return idenType;
    }

    public void setIdenType(String idenType) {
        this.idenType = idenType;
    }

    public String getPerName() {
        return perName;
    }

    public void setPerName(String perName) {
        this.perName = perName;
    }

    public String getSexCode() {
        return sexCode;
    }

    public void setSexCode(String sexCode) {
        this.sexCode = sexCode;
    }

    public String getControl() {
        return control;
    }

    public void setControl(String control) {
        this.control = control;
    }

    @Override
    public String toString() {
        return "Person{" +
                "agencyCode='" + agencyCode + '\'' +
                ", idenNo='" + idenNo + '\'' +
                ", idenType='" + idenType + '\'' +
                ", perName='" + perName + '\'' +
                ", sexCode='" + sexCode + '\'' +
                ", control='" + control + '\'' +
                '}';
    }
}
