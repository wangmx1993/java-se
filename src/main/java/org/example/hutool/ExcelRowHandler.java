package org.example.hutool;

import cn.hutool.core.lang.Console;
import cn.hutool.poi.excel.sax.handler.RowHandler;

import java.util.List;

/**
 * 在标准的ExcelReader中，如果数据量较大，读取Excel会非常缓慢，并有可能造成内存溢出。
 * 因此针对大数据量的Excel，Hutool封装了event模式的读取方式。
 * <p>
 * 实现一下RowHandler接口，这个接口是Sax读取的核心，
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/16 15:03
 */
public class ExcelRowHandler implements RowHandler {

    private List<List<Object>> totalRowList;

    public ExcelRowHandler(List<List<Object>> totalRowList) {
        this.totalRowList = totalRowList;
    }

    /**
     * 通过实现handle方法编写我们要对每行数据的操作方式（比如按照行入库，入List或者写出到文件等）
     *
     * @param sheetIndex ：sheet 页索引
     * @param rowIndex   ：行索引
     * @param rowList    ：行数据
     *                   注意这种方式读取时，对于合并的单元格，不会自动拆分赋值。
     *                   比如第1行 2，3，4列合并了，值是 xx，则读取后第一个list的第2个元素值是 xxx，而第3，4个元素是null。
     */
    @Override
    public void handle(int sheetIndex, long rowIndex, List<Object> rowList) {
        Console.log("[{}] [{}] {}", sheetIndex, rowIndex, rowList);
        totalRowList.add(rowList);
    }

}
