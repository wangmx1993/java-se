package org.example;

import org.example.netty.springBoot.NettyServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * ServletComponentScan //对 servlet 注解进行扫描，如 @WebServlet、@WebFilter、@WebListener
 * Hello world!
 */
@SpringBootApplication
@ServletComponentScan
public class JavaseApplication {

    private static final Logger log = LoggerFactory.getLogger(JavaseApplication.class);

    public static void main(String[] args) {

        SpringApplication.run(JavaseApplication.class, args);

        //开启 Netty 服务
        NettyServer nettyServer = new NettyServer();
        nettyServer.start();
    }

}
