package org.example.log4j2;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 面向 slf4j 编码，这样将来即使换成了其他实现，如  Logback ，也不用修改代码
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/6/6 16:36
 */
public class Log4j2Test {

    private static final Logger LOGGER = LoggerFactory.getLogger(Log4j2Test.class);

    public static void main(String[] args) {
        LOGGER.trace("I am trace!");
        LOGGER.debug("I am debug!");
        LOGGER.info("I am info!");
        LOGGER.warn("I am {}!", "warn");
        LOGGER.error("I am {}!", "error");
    }
}
