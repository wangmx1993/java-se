package org.example.type1;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

/**
 * 自动化 API 剪切板测试，可以自动将内容放入到系统剪切板中
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/1/6 19:31
 */
public class MyClipboard {
    public static void main(String[] args) throws InterruptedException {
        //创建字符串选择器，并将需要放入剪切板的内容传给它
        String content = "我是 Java 自动化代码放入到系统剪切板中的内容.";
        StringSelection stringSelection = new StringSelection(content);
        //创建系统剪贴板，并设置内容，相当于把内容放入系统的剪切板中，用户也可以手动 ctrl + v 找他
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
        for (int i = 3; i > 0; i--) {
            System.out.println(i + " 秒后自动粘贴内容.");
            TimeUnit.SECONDS.sleep(1);
        }
        //最后按 Ctrl + v 快捷键，将系统剪切板中得内容粘贴出来.
        pressMultipleKeyByNumber(KeyEvent.VK_CONTROL, KeyEvent.VK_V);
    }

    /**
     * 按下组合键，如 ctrl + c、ctrl + v、alt + tab 等等
     *
     * @param keycode：组合健数组，如 {KeyEvent.VK_CONTROL,KeyEvent.VK_V}
     */
    public static void pressMultipleKeyByNumber(int... keycode) {
        try {
            Robot robot = new Robot();
            //按顺序按下健
            for (int i = 0; i < keycode.length; i++) {
                robot.keyPress(keycode[i]);
                robot.delay(50);
            }

            //按反序松开健
            for (int i = keycode.length - 1; i >= 0; i--) {
                robot.keyRelease(keycode[i]);
                robot.delay(50);
            }
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }


}
