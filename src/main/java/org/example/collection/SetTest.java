package org.example.collection;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 集合测试
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/9/27 20:52
 */
@SuppressWarnings("Duplicates")
public class SetTest {

    @Test
    public void toStringTest() {
        Set<String> errMsgSet = new HashSet<>();
        errMsgSet.add("资产编码不允许为空！");
        errMsgSet.add("【入账状态】为已入账时，【记账日期】必须有值。");
        errMsgSet.add("单位编码的长度必须为 3、6、9、12、15、18、21 位。");

        String toString = errMsgSet.toString();

        // [资产编码不允许为空！, 【入账状态】为已入账时，【记账日期】必须有值。, 单位编码的长度必须为 3、6、9、12、15、18、21 位.]
        System.out.println(toString);
        // 资产编码不允许为空！, 【入账状态】为已入账时，【记账日期】必须有值。, 单位编码的长度必须为 3、6、9、12、15、18、21 位。
        System.out.println(toString.replaceAll("[\\[\\]]", ""));
        // 资产编码不允许为空！<br/>【入账状态】为已入账时，【记账日期】必须有值。<br/>单位编码的长度必须为 3、6、9、12、15、18、21 位。
        System.out.println(StringUtils.join(errMsgSet, "<br/>"));
        // 资产编码不允许为空！
        // 【入账状态】为已入账时，【记账日期】必须有值。
        // 单位编码的长度必须为 3、6、9、12、15、18、21 位。
        System.out.println(StringUtils.join(errMsgSet, "\n"));
    }

    /**
     * HashSet、LinkedHashSet 可以添加空、null 元素
     */
    @Test
    public void linkedHashSetTest1() {
        Set<String> msgSet = new LinkedHashSet<>(16);
        msgSet.add("成功");
        msgSet.add("");
        msgSet.add("失败");
        msgSet.add("");
        msgSet.add(" ");
        msgSet.add(null);
        msgSet.add("  ");
        msgSet.add("");
        msgSet.add(null);
        msgSet.add("结束");

        // 7：[成功, , 失败,  , null,   , 结束]
        System.out.println(msgSet.size() + "：" + msgSet);
        msgSet = msgSet.stream().filter(item -> StringUtils.isNotBlank(item)).collect(Collectors.toSet());
        // [成功, 结束, 失败]
        System.out.println(msgSet);
    }


}
