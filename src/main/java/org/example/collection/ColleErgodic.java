package org.example.collection;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 集合遍历
 * 1: 集合添加,删除元素 必须使用 迭代器 Iterator
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/8/24 15:38
 */
@SuppressWarnings("all")
public class ColleErgodic {


    /**
     * 集合遍历方式 1
     * 优点： 较常见，易于理解
     * 缺点： 每次都要计算 list.size()
     */
    @Test
    public void testErgodic1() {
        List<String> list = new ArrayList<>();
        list.add("10");
        list.add("20");
        list.add("30");

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    /**
     * 集合遍历方式 2
     * 优点：不必每次都计算 ，变量的作用域遵循最小范围原则
     */
    @Test
    public void testErgodic2() {
        List<String> list = new ArrayList<>();
        list.add("10");
        list.add("20");
        list.add("30");

        for (int i = 0, n = list.size(); i < n; i++) {
            System.out.println(list.get(i));
        }
    }

    /**
     * 集合遍历方式 3 ----- 迭代器
     * 如果需要对集合中的元素进行新增或是删除, 则必须使用迭代器(iterator)
     * 普通的索引方式会引起 IndexOutOfBoundsException
     */
    @Test
    public void testIterator() {
        List<String> list = new ArrayList<>();
        list.add("10");
        list.add("20");
        list.add("30");

        for (Iterator<String> iterator = list.iterator(); iterator.hasNext(); ) {
            String next = iterator.next();
            if ("20".equals(next)) {
                iterator.remove();
            }
            System.out.println(next);
        }
        System.out.println(list);//[10, 30]
    }

    /**
     * boolean removeIf(Predicate<? super E> filter) 是 Java8 Collecttion 的一个默认方法。
     * 1、删除此集合中满足给定谓词的所有元素。迭代期间或谓词引发的错误或运行时异常将转发给调用方。
     * 2、filter 接口函数返回 true，则元素会被删除
     * 3、集合中有任意一个元素满足条件被删除，则整个 removeIf 方法返回 true.
     * 4、removeIf 方法内部也是使用的迭代器
     */
    @Test
    public void testRemoveIf1() {
        List<String> list = new ArrayList<>();
        list.add("10");
        list.add("20");
        list.add("30");

        boolean removeIf = list.removeIf(item -> "20".equals(item));
        System.out.println(removeIf + ":" + list);//true:[10, 30]
    }

    /**
     * 删除为 null, 或者大于 20 的元素.
     */
    @Test
    public void testRemoveIf2() {
        List<String> list = new ArrayList<>();
        list.add("10");
        list.add("20");
        list.add(null);
        list.add("30");

        list.removeIf(item -> {
            if (item == null || Integer.valueOf(item) > 20) {
                return true;
            }
            return false;
        });
        System.out.println(list);//[10, 20]
    }

    /**
     * 集合遍历方式 4 -----  jdk 1.5 开始的写法
     */
    @Test
    public void testErgodic4() {
        List<String> list = new ArrayList<>();
        list.add("10");
        list.add("20");
        list.add("30");

        for (String s : list) {
            System.out.println(s);
        }
    }

    /**
     * 集合遍历方式 5 -----  jdk 1.8 开始的写法
     */
    @Test
    public void testErgodic5() {
        List<String> list = new ArrayList<>();
        list.add("10");
        list.add("20");
        list.add("30");

        list.stream().forEach(item -> System.out.println(item));
    }

    /**
     * 循环嵌套: 遵循外小内大原则
     * 异常处理写在循环外面
     */
    @Test
    public void testErgodic6() {
        try {
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10000; j++) {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
