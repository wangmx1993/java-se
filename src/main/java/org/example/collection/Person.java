package org.example.collection;

import java.util.Date;
import java.util.Objects;

/**
 * Java Bean
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/3/25 9:26
 */
public class Person {
    
    private String pid;
    private String pname;
    private Integer age;
    private Date birthday;

    public Person() {
    }

    public Person(String pid, String pname, Integer age, Date birthday) {
        this.pid = pid;
        this.pname = pname;
        this.age = age;
        this.birthday = birthday;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Person person = (Person) o;
        return Objects.equals(pid, person.pid) &&
                Objects.equals(pname, person.pname) &&
                Objects.equals(age, person.age) &&
                Objects.equals(birthday, person.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pid, pname, age, birthday);
    }

    @Override
    public String toString() {
        return "Person{" +
                "pid='" + pid + '\'' +
                ", pname='" + pname + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                '}';
    }
}
