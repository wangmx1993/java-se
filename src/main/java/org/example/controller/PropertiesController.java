package org.example.controller;

import cn.hutool.core.util.StrUtil;
import org.example.uitls.PropertiesUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Properties 工具类 web 访问.
 * 即便是打包成 .jar、.war 包部署到生产环境，照样读取正常。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2023/3/25 9:01
 */
@RestController
public class PropertiesController {

    /**
     * 查询类路径下的某个 .properties 文件中的所有键值数据
     * * http://localhost:8080/properties/findAllByClassPath
     * * http://localhost:8080/properties/findAllByClassPath?classPath=config/cron.setting
     * 查询类路径下的某个属性文件中指定的属性值，不存在时返回空
     * * http://localhost:8080/properties/findAllByClassPath?propertyName=publishTime
     * * http://localhost:8080/properties/findAllByClassPath?classPath=config/cron.setting&propertyName=org.example.cron.LogClearTimer.run1
     *
     * @param classPath ：类路径下的资源文件
     * @return
     */
    @GetMapping("properties/findAllByClassPath")
    public Map<String, String> findAllByClassPath(String classPath, String propertyName) {
        classPath = StrUtil.isBlank(classPath) ? "config/config.properties" : classPath;
        Map<String, String> stringMap = new HashMap<>();
        if (StrUtil.isNotBlank(propertyName)) {
            String propertyValue = PropertiesUtils.getOneToClassPath(classPath, propertyName);
            stringMap.put(propertyName, propertyValue);
        } else {
            stringMap = PropertiesUtils.findAllByClassPath(classPath);
        }

        // {"version":"1.1","author":"wangMaoXiong","publishTime":"2021-04-2"}
        return stringMap;
    }

    /**
     * 向类路径下的某个属性文件添加/修改单个属性值
     * * http://localhost:8080/properties/addPropertyToClassPath?propertyName=age&propertyValue=33
     *
     * @param classPath
     * @param propertyName
     * @param propertyValue
     * @return
     */
    @GetMapping("properties/addPropertyToClassPath")
    public Map<String, String> addPropertyToClassPath(String classPath,
                                                      @RequestParam String propertyName,
                                                      @RequestParam String propertyValue) {
        classPath = StrUtil.isBlank(classPath) ? "config/config.properties" : classPath;
        PropertiesUtils.addPropertyToClassPath(classPath, propertyName.trim(), propertyValue.trim());
        Map<String, String> stringMap = PropertiesUtils.findAllByClassPath(classPath);
        return stringMap;
    }

    /**
     * 向类路径下的某个属性文件删除指定的属性值
     * * http://localhost:8080/properties/deleteOneToClassPath?propertyName=age
     *
     * @param classPath
     * @param propertyName
     * @return
     */
    @GetMapping("properties/deleteOneToClassPath")
    public Map<String, String> deleteOneToClassPath(String classPath, @RequestParam String propertyName) {
        classPath = StrUtil.isBlank(classPath) ? "config/config.properties" : classPath;
        PropertiesUtils.deleteOneToClassPath(classPath, propertyName.trim());
        Map<String, String> stringMap = PropertiesUtils.findAllByClassPath(classPath);
        return stringMap;
    }

}
