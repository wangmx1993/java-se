package org.example.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import org.example.uitls.ResultCode;
import org.example.uitls.ResultData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 用户登陆控制器 · 练习使用验证码功能
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/3/13 11:21
 */
@RestController
public class LoginController {

    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    /**
     * 用户登陆请求
     * <p>
     * http://localhost:8080/user/login?loginName=zhangSan&pwd=123456&code=xxx
     *
     * @param loginName ：用户名
     * @param pwd       ：密码
     * @param code      ：验证码
     * @param session   ：Session
     * @return
     */
    @PostMapping("/user/login")
    public ResultData login(@RequestParam String loginName,
                            @RequestParam String pwd,
                            @RequestParam String code,
                            HttpSession session) {
        System.out.printf("sessionId=%s,loginName=%s, pwd=%s, code=%s %n", session.getId(), loginName, pwd, code);

        //获得存储在session中的验证码
        String sessionCheckCode = (String) session.getAttribute("code");
        //判断验证码是否正确，暂时不对账户和密码进行校验。验证码忽略大小写。
        if (code != null && sessionCheckCode != null && sessionCheckCode.equalsIgnoreCase(code)) {
            //登录成功，返回 json 的提示。
            return ResultData.success();
        } else {
            //登陆失败，提示验证码不正确！
            return ResultData.error(ResultCode.USER_VERIFI_CODE_ERROR);
        }
    }

    /**
     * 得到登陆验证码
     * http://localhost:8080/user/getCode
     *
     * @param response
     * @param session
     * @throws
     */
    @RequestMapping("/user/getCode")
    public void getCode(HttpServletResponse response, HttpSession session) {
        ServletOutputStream outputStream = null;
        try {
            //HuTool 定义图形验证码的长和宽,验证码的位数，干扰线的条数
            LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(116, 36, 4, 30);
            //取得验证码
            String captchaCode = lineCaptcha.getCode();
            System.out.printf("sessionId=%s,captchaCode=%s%n", session.getId(), captchaCode);

            //将验证码图片输出到输出流
            outputStream = response.getOutputStream();
            lineCaptcha.write(outputStream);

            //将验证码放入session，隔绝不同的用户
            session.setAttribute("code", captchaCode);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
