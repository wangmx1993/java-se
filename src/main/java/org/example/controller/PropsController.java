package org.example.controller;

import cn.hutool.setting.Setting;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.Map;

/**
 * 配置文件(HuTool-setting）- 替代 Jdk 的 Properties
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/3/27 15:52
 */
@RestController
@SuppressWarnings("all")
public class PropsController {

    /**
     * Setting 通过构造器初始化
     * <p>
     * Setting(String path)：path 可以是相对路径或绝对路径，如果文件不存在，则抛异常
     * Setting(String path, boolean isUseVariable)：isUseVariable：配置文件中使用是否使用变量 ${key}
     * Setting(String path, Charset charset, boolean isUseVariable)：charset：指定编码
     * Setting(File configFile, Charset charset, boolean isUseVariable)：configFile: 配置文件
     * Setting(URL url, Charset charset, boolean isUseVariable): 配置文件 url
     *
     * @return
     */
    @GetMapping("/props/settingInit")
    public String settingInit() throws MalformedURLException {
        //读取 classpath 下的配置文件，默认不将配置文件中的 ${key} 作为变量
        Setting setting1 = new Setting("config/wang1.setting");

        //{={driver=com.mysql.jdbc.Driver, url=jdbc:mysql://fedora.vmware:3306/extractor, user=root, pass=123456, author=${user}-汪茂雄, version=2021-03-17 15:00:00}}
        System.out.println(setting1.toString());

        //读取 classpath 下的 config 目录下的 配置文件，将配置文件中的 ${key} 作为变量
        Setting setting2 = new Setting("config/wang1.setting", true);

        //{={driver=com.mysql.jdbc.Driver, url=jdbc:mysql://fedora.vmware:3306/extractor, user=root, pass=123456, author=root-汪茂雄, version=2021-03-17 15:00:00}}
        System.out.println(setting2);

        //读取绝对路径下的配置文件
        //Setting setting3 = new Setting("D:\\project\\脚本\\wang1.setting");
        //System.out.println(setting3);

        return setting1.toString();
    }

    /**
     * Setting 读取配置参数
     * String get(Object key): 获取指定 key 对应的值，key 不存在时，返回 null.
     * String getStr(String key, String defaultValue): 获得字符串类型值, 当获取的值为空（null或者空白字符时，包括多个空格），返回默认值
     * boolean containsKey(Object key): 是否包含指定的 key
     * boolean containsValue(Object value)：是否包含指定的 value
     * Set<Entry<String, String>> entrySet(): 获取所有的键值对列表
     * Set<String> keySet()：获取所有键列表
     * int size()：获取键值总数
     * Collection<String> values(): 获取默认分组（空分组）中的所有值列表
     * Date getDate(K key): 获取 Date 型属性值， 无值或获取错误返回 null
     *
     * @return
     * @throws MalformedURLException
     */
    @GetMapping("/props/settingRead")
    public String settingRead() throws MalformedURLException {
        //读取 classpath 下的配置文件，将配置文件中的 ${key} 作为变量。文件不存在时，抛出异常
        Setting setting = new Setting("config/wang1.setting", true);

        System.out.println("" + setting.size());

        String driver = setting.get("driver");
        System.out.println(driver);//com.mysql.jdbc.Driver

        String url = setting.getStr("driver2", "oracle.jdbc.driver.OracleDriver");
        System.out.println(url);//oracle.jdbc.driver.OracleDriver

        System.out.println(setting.containsKey("abc"));//false
        System.out.println(setting.containsValue("root"));//true

        for (Map.Entry<String, String> entry : setting.entrySet()) {
            System.out.println("1--" + entry.getKey() + "=" + entry.getValue());
        }

        for (String key : setting.keySet()) {
            System.out.println("2··" + key);
        }

        for (String value : setting.values()) {
            System.out.println("3--" + value);
        }

        Date version = setting.getDate("version");
        System.out.println("version=" + version);//Wed Mar 17 15:00:00 CST 2021

        BigInteger pass = setting.getBigInteger("pass");
        System.out.println("pass=" + pass);//123456

        return setting.toString();
    }
}
