package org.example.cron;

import cn.hutool.cron.CronUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 全局定时任务启动与关闭-CronUtil
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/3/21 15:49
 */
@RestController
public class CronController {

    /**
     * 启动全局定时任务
     * http://localhost:8080/cron/start
     *
     * @return
     */
    @GetMapping("/cron/start")
    public String startCron() {
        CronUtil.start();
        return "Success";
    }

    /**
     * 关闭全局定时任务
     * http://localhost:8080/cron/stopCron
     *
     * @return
     */
    @GetMapping("/cron/stopCron")
    public String stopCron() {
        CronUtil.stop();
        return "Success";
    }
}
