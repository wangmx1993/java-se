package org.example.cron;

import cn.hutool.core.date.DateUtil;

import javax.sound.midi.SoundbankResource;
import java.util.Date;

/**
 * Cron 定时任务执行的业务方法
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/3/21 15:46
 */
public class LogClearTimer {

    public void run1() {
        System.out.println(new Date());
    }

    public void run2() {
        System.out.println(DateUtil.now());
    }
}
