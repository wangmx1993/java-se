package org.example;

/**
 * 自定义运行时异常
 *
 * @author zkjn
 */
public class SysException extends RuntimeException {

    /**
     * 表示异常信息是 json 对象数组字符串，如 List<Map<String, Object>>
     */
    public static final Integer EC_11001 = 11001;

    /**
     * 异常编码
     */
    private Integer errorCode;

    public SysException() {
    }

    public SysException(String message) {
        super(message);
    }

    public SysException(String message, Integer errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public SysException(String message, Throwable cause) {
        super(message, cause);
    }

    public SysException(String message, Throwable cause, Integer errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public SysException(Throwable cause) {
        super(cause);
    }

    public Integer getErrorCode() {
        return errorCode;
    }

}