package org.example.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 标准的 Servlet 实现 HttpServlet；重写其 doGet 、doPost 方法
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/27 17:17
 */
public class BookServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(":com.lct.servlet.BookServlet:" + req.getRequestURL());
        // 讲求转发
        req.getRequestDispatcher("/props/settingRead").forward(req, resp);
    }

}