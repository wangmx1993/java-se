package org.example.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * 标准 Servlet 监听器，实现 javax.servlet.ServletContextListener 接口
 * 然后实现方法
 * ServletContextListener：属于 Servlet 应用启动关闭监听器，监听容器初始化与销毁
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/27 17:29
 */
public class SystemListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("org.example.servlet.SystemListener::服务器启动.....");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("org.example.servlet.SystemListener::服务器关闭.....");
    }

}