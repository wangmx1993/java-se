
package org.example.webservice.user;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.example.webservice.user package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListUserByIds_QNAME = new QName("http://webService.web_app.wmx.com/", "listUserByIds");
    private final static QName _ListUserByIdsResponse_QNAME = new QName("http://webService.web_app.wmx.com/", "listUserByIdsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.example.webservice.user
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListUserByIds }
     * 
     */
    public ListUserByIds createListUserByIds() {
        return new ListUserByIds();
    }

    /**
     * Create an instance of {@link ListUserByIdsResponse }
     * 
     */
    public ListUserByIdsResponse createListUserByIdsResponse() {
        return new ListUserByIdsResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListUserByIds }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ListUserByIds }{@code >}
     */
    @XmlElementDecl(namespace = "http://webService.web_app.wmx.com/", name = "listUserByIds")
    public JAXBElement<ListUserByIds> createListUserByIds(ListUserByIds value) {
        return new JAXBElement<ListUserByIds>(_ListUserByIds_QNAME, ListUserByIds.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListUserByIdsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ListUserByIdsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://webService.web_app.wmx.com/", name = "listUserByIdsResponse")
    public JAXBElement<ListUserByIdsResponse> createListUserByIdsResponse(ListUserByIdsResponse value) {
        return new JAXBElement<ListUserByIdsResponse>(_ListUserByIdsResponse_QNAME, ListUserByIdsResponse.class, null, value);
    }

}
