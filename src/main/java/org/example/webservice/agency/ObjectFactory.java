
package org.example.webservice.agency;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.example.webservice.agency package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SyncAgencyStatInfo_QNAME = new QName("http://webService.web_app.wmx.com/", "syncAgencyStatInfo");
    private final static QName _QueryAgencyStatInfo_QNAME = new QName("http://webService.web_app.wmx.com/", "queryAgencyStatInfo");
    private final static QName _QueryAgencyStatInfoResponse_QNAME = new QName("http://webService.web_app.wmx.com/", "queryAgencyStatInfoResponse");
    private final static QName _SyncAgencyStatInfoResponse_QNAME = new QName("http://webService.web_app.wmx.com/", "syncAgencyStatInfoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.example.webservice.agency
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SyncAgencyStatInfo }
     * 
     */
    public SyncAgencyStatInfo createSyncAgencyStatInfo() {
        return new SyncAgencyStatInfo();
    }

    /**
     * Create an instance of {@link QueryAgencyStatInfo }
     * 
     */
    public QueryAgencyStatInfo createQueryAgencyStatInfo() {
        return new QueryAgencyStatInfo();
    }

    /**
     * Create an instance of {@link QueryAgencyStatInfoResponse }
     * 
     */
    public QueryAgencyStatInfoResponse createQueryAgencyStatInfoResponse() {
        return new QueryAgencyStatInfoResponse();
    }

    /**
     * Create an instance of {@link SyncAgencyStatInfoResponse }
     * 
     */
    public SyncAgencyStatInfoResponse createSyncAgencyStatInfoResponse() {
        return new SyncAgencyStatInfoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SyncAgencyStatInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.web_app.wmx.com/", name = "syncAgencyStatInfo")
    public JAXBElement<SyncAgencyStatInfo> createSyncAgencyStatInfo(SyncAgencyStatInfo value) {
        return new JAXBElement<SyncAgencyStatInfo>(_SyncAgencyStatInfo_QNAME, SyncAgencyStatInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryAgencyStatInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.web_app.wmx.com/", name = "queryAgencyStatInfo")
    public JAXBElement<QueryAgencyStatInfo> createQueryAgencyStatInfo(QueryAgencyStatInfo value) {
        return new JAXBElement<QueryAgencyStatInfo>(_QueryAgencyStatInfo_QNAME, QueryAgencyStatInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryAgencyStatInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.web_app.wmx.com/", name = "queryAgencyStatInfoResponse")
    public JAXBElement<QueryAgencyStatInfoResponse> createQueryAgencyStatInfoResponse(QueryAgencyStatInfoResponse value) {
        return new JAXBElement<QueryAgencyStatInfoResponse>(_QueryAgencyStatInfoResponse_QNAME, QueryAgencyStatInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SyncAgencyStatInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webService.web_app.wmx.com/", name = "syncAgencyStatInfoResponse")
    public JAXBElement<SyncAgencyStatInfoResponse> createSyncAgencyStatInfoResponse(SyncAgencyStatInfoResponse value) {
        return new JAXBElement<SyncAgencyStatInfoResponse>(_SyncAgencyStatInfoResponse_QNAME, SyncAgencyStatInfoResponse.class, null, value);
    }

}
