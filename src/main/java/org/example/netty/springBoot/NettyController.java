package org.example.netty.springBoot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Netty 框架发送消息
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/8/9 15:30
 */
@RestController
public class NettyController {

    /**
     * http://localhost:8080/netty/greet?msg=你好！
     *
     * @param msg ：待发送的消息
     * @return
     */
    @GetMapping("/netty/greet")
    public Map<String, Object> helloNetty(@RequestParam String msg) {
        return NettyClient.helloNetty(msg);
    }


}
