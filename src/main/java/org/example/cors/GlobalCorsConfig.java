package org.example.cors;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * CorsFilter 跨域请求过滤器，用于全局设置外部哪些主机可以访问本应用下的哪些接口
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/6/5 17:08
 */
@Configuration
public class GlobalCorsConfig {

    @Bean
    public CorsFilter corsFilter() {
        //1. 添加 CORS 配置信息
        CorsConfiguration config = new CorsConfiguration();
        //放行哪些原始域，即哪些域名可以访问咋们的接口；* 表示所有，不限制；多个时反复添加即可；
        //注意 127.0.0.1 与 localhost 也会仍然不同的地址.
        config.addAllowedOrigin("http://localhost:63342");
        config.addAllowedOrigin("http://127.0.0.1:63342");
        //是否发送 Cookie
        config.setAllowCredentials(true);
        //放行哪些请求方式，GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
        config.addAllowedMethod("*");

        //放行哪些原始请求头部信息，不能设置 *: config.addAllowedHeader();
        //暴露哪些头部信息，不能设置 *: config.addExposedHeader();

        //2. 添加映射路径，即允许对方可以访问本应用下的哪些请求接口，/** 表示所有请求
        UrlBasedCorsConfigurationSource corsConfigurationSource = new UrlBasedCorsConfigurationSource();
        corsConfigurationSource.registerCorsConfiguration("/**", config);

        //3. 返回新的 CorsFilter
        return new CorsFilter(corsConfigurationSource);
    }

}
