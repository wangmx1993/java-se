package org.example.cors;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * {@CrossOrigin} 如果定义在类上，则对类中的所有方法生效，定义在方法上则对方法生效
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/6/6 8:37
 */
@RestController
public class CrossOriginController {

    /**
     * @CrossOrigin 如果定义在类上，则对类中的所有方法生效，定义在方法上则对方法生效，优先级是采用就近原则,属性如下：
     * String[] origins() default {}：允许哪些请求源可以访问，默认为 * 所有源都可以访问
     * String[] value() default {}：等同于 origins
     * String[] allowedHeaders() default {}：允许的请求头列表，默认为 * ，允许所有
     * String[] exposedHeaders() default {}：对外暴露的头信息，如 Cache-Control，Content-Language，等等，默认不会暴露任何信息
     * RequestMethod[] methods() default {}：允许的请求方式，默认为 get、post、head
     */
    @GetMapping("/crossOrigin/getInfo")
    @CrossOrigin(origins = "*")
    public Map<String, Object> getInfo() {
        Map<String, Object> dataMap = new HashMap<>(4);

        dataMap.put("code", 200);
        dataMap.put("msg", "success");
        dataMap.put("data", null);
        dataMap.put("times", System.currentTimeMillis());
        return dataMap;
    }

}
