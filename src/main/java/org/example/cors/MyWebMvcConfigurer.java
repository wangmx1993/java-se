package org.example.cors;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 实现 WebMvcConfigurer(全局跨域) 重写 addCorsMappings 方法设置跨域映射
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/6/6 8:08
 */
@Configuration
public class MyWebMvcConfigurer implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //CorsRegistration addMapping(String pathPattern): 添加路径映射，如 /admin/info，或者 /admin/**
        registry.addMapping("/**")
                //是否发送Cookie
                .allowCredentials(true)
                //放行哪些原始域, * 表示所有
                .allowedOrigins(new String[]{"http://127.0.0.1:63342","http://localhost:63342"})
                //放行哪些请求方式，GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
                .allowedMethods(new String[]{"GET", "POST", "PUT", "DELETE"})
                //放行哪些原始请求头部信息
                .allowedHeaders("*");
                //暴露哪些头部信息，不能设置为 * : .exposedHeaders();
    }
}