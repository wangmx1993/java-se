package org.example.cors;


import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/6/5 16:38
 */
@Controller
public class InfoController {

    /**
     * Jsonp 实现跨域请求
     * get请求：http://localhost:8080/jsonp/getInfo
     *
     * @param request
     * @return
     * @ResponseBody ：表示内容直接返回给页面
     */
    @GetMapping("/jsonp/getInfo")
    @ResponseBody
    public String getInfo(HttpServletRequest request) {
        //根据前端$.ajax({jsonp:"callback"}) 设置的参数值进行取值,callback 的值是 JQuery 随机生成的
        String callback = request.getParameter("callback");
        System.out.println("callback=" + callback);

        //返回的参数必须是 json 格式，这里只是为了演示简单，实际中推荐使用 Gson、Fastjson、jackson 等库来操作 json
        String message = "{\"message\":\"I love You 西北的雪\",\"time\":" + System.currentTimeMillis() + "}";
        if (!StringUtils.isEmpty(callback)) {
            /**
             * callback 不为空，表示此时是 jsonp 跨域请求,后台返回的 json 数据必须放在 request.getParameter("callback") 的参数值中，如 callback(xxx) ,
             * 前端只会取其中的 xxx 纯数据
             * callback 为空时，表示是普通的 get 请求，此时不应该带上 callback(xxx) 中的前缀
             */
            message = callback + "(" + message + ")";
        }
        return message;
    }

    /**
     * HttpServletResponse 设置响应头跨域
     * post 请求：http://localhost:8080/response/postInfo
     *
     * @param response
     * @param info
     * @return
     */
    @PostMapping("response/postInfo")
    @ResponseBody
    public String postInfo(String info, HttpServletResponse response, HttpServletRequest request) {
        /**
         * response.setHeader("Access-Control-Allow-Origin", "*")：表示允许所有 域名 的脚本进行访问
         * response.setHeader("Access-Control-Allow-Origin", "http://localhost:55555")：表示允许指定的 协议://域名:端口 中的应用中的脚本访问
         * 注意 协议://域名:端口 必须完全一致，比如 localhost 与 127.0.0.1 虽然都是指向本机，但是也会认为是跨域
         *
         * response.setHeader 设置允许的源时，不能使用逗号分隔，也不能多次设置，否则后面的覆盖前面的，推荐方式：
         * 设置 IP 白名单如下，这里只是演示方便才写死，实际中应该从数据库或者配置文件中读取
         */
        Set<String> witchIpSet = new HashSet<>();
        witchIpSet.add("http://localhost:55555");
        witchIpSet.add("http://192.168.1.20:55555");

        //获取请求头信息中的源,如 http://localhost:63342
        String origin = request.getHeader("Origin");

        System.out.println("origin=" + origin);
        if (origin != null && witchIpSet.contains(origin)) {
            System.out.println("白名单之列，放行....");
            response.setHeader("Access-Control-Allow-Origin", origin);
        }
        System.out.println("info=" + info);
        return info;
    }

    @GetMapping("/filter/getInfo")
    @ResponseBody
    public Map<String, Object> getInfo() {
        Map<String, Object> dataMap = new HashMap<>(5);

        dataMap.put("code", 200);
        dataMap.put("msg", "success");
        dataMap.put("times", System.currentTimeMillis());
        dataMap.put("data", null);
        return dataMap;
    }
}