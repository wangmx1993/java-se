package org.example.uitls;

import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicLong;

/**
 * JDK1.5 之后的 java.util.concurrent.atomic 包里， 多了一批原子处理类。主 要用于在高并发环境下的高效程序处理。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/2/25 19:06
 */
public class AtomicTest {

    /**
     * AtomicInteger 一个提供原子操作的 Integer 的类。
     * 1、在 Java 语言中， ++i 和 i++ 操作并不是线程安全的，在使用的时候，不可避免的会用到 synchronized 关键字，
     * 而 AtomicInteger 则通过了一种线程安全的加减操作接口。
     * 2、可以避免多线程的优先级倒置和死锁情况的发生， 提升高并发下的性能
     */
    @Test
    @SuppressWarnings("all")
    public void testAtomicInteger() {
        AtomicInteger atomicInteger = new AtomicInteger(100);
        //100
        System.out.println(atomicInteger.getAndAdd(2));
        //100+2=102
        System.out.println(atomicInteger.get());
        //102+5=107
        System.out.println(atomicInteger.addAndGet(5));
        //107--=107
        System.out.println(atomicInteger.getAndDecrement());
        //106++=106
        System.out.println(atomicInteger.getAndIncrement());
        //--107=106
        System.out.println(atomicInteger.decrementAndGet());
        //++106=107
        System.out.println(atomicInteger.incrementAndGet());
        //107.0
        System.out.println(atomicInteger.floatValue());

        // 重置为0
        atomicInteger.set(0);
        // 0
        System.out.println(atomicInteger.getAndIncrement());
        // 1
        System.out.println(atomicInteger.getAndIncrement());

        // 2147483647
        System.out.println(Integer.MAX_VALUE);
    }

    /**
     * https://www.matools.com/api/java8
     * AtomicLong  与 AtomicInteger   完全同理。
     */
    @Test
    @SuppressWarnings("all")
    public void testAtomicLong() {
        AtomicLong atomicInteger = new AtomicLong(100);
        //100
        System.out.println(atomicInteger.getAndAdd(2));
        //100+2=102
        System.out.println(atomicInteger.get());
        //102+5=107
        System.out.println(atomicInteger.addAndGet(5));
        //107--=107
        System.out.println(atomicInteger.getAndDecrement());
        //106++=106
        System.out.println(atomicInteger.getAndIncrement());
        //--107=106
        System.out.println(atomicInteger.decrementAndGet());
        //++106=107
        System.out.println(atomicInteger.incrementAndGet());
        //107.0
        System.out.println(atomicInteger.floatValue());
    }

    /**
     * 一个 boolean 值可以用原子更新，一个AtomicBoolean用于诸如原子更新标志的应用程序。
     */
    @Test
    public void testAtomicBoolean1() {
        AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        boolean weakCompareAndSet1 = atomicBoolean.compareAndSet(false, false);
        //false=true
        System.out.println(weakCompareAndSet1 + "=" + atomicBoolean.get());

        boolean weakCompareAndSet2 = atomicBoolean.compareAndSet(true, false);
        //true=false
        System.out.println(weakCompareAndSet2 + "=" + atomicBoolean.get());
    }

    @Test
    public void testAtomicBoolean2() {
        AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        boolean weakCompareAndSet1 = atomicBoolean.weakCompareAndSet(false, false);
        //false=true
        System.out.println(weakCompareAndSet1 + "=" + atomicBoolean.get());

        boolean weakCompareAndSet2 = atomicBoolean.weakCompareAndSet(true, false);
        //true=false
        System.out.println(weakCompareAndSet2 + "=" + atomicBoolean.get());
    }

    @Test
    public void testAtomicIntegerArray() {
        AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(10);
        for (int i = 0; i < atomicIntegerArray.length(); i++) {
            //0
            System.out.println(atomicIntegerArray.get(i));
        }
    }


}
