package org.example.uitls;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import com.google.common.collect.Lists;
import org.se.exception.BasicException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

/**
 * 地方评价系统校验规则校验工具类
 */
@SuppressWarnings("All")
public class BasicDataRuleUtil {

    private static final Logger LOG = LoggerFactory.getLogger(BasicDataRuleUtil.class);

    /**
     * -------------------------------------单位校验-------------------------------------
     */

    /**
     * [单位信息]行政单位的经费供给方式和单位类别检查 & 事业单位的经费供给方式和单位类别检查
     * 错误：[单位信息]请检查行政单位的“经费供给方式”和“单位类别”：行政单位的经费供给方式必须填写；行政单位的单位类别只能选择11或12类。
     * 错误：[单位信息]请检查事业单位的“经费供给方式”和“单位类别”：事业单位的经费供给方式应该不为空；且单位类别不可以选择11或12类。
     * <p>
     * 【阻止】
     *
     * @param agencyMap
     */
    public static void agencyTypeErrorRule(Map<String, Object> agencyMap) {
        if (ObjectUtils.isEmpty(agencyMap)) {
            return;
        }
        Object agencyTypeCode = agencyMap.get("agency_type_code");
        if (ObjectUtil.isNotNull(agencyTypeCode)) {
            if (agencyTypeCode.toString().startsWith("1")) {
                if (ObjectUtil.isNull(agencyMap.get("fund_guar_code"))) {
                    throw new BasicException("行政单位的经费供给方式必须填写！");
                }
                if (ObjectUtil.isNotNull(agencyMap.get("agency_class_code")) &&
                        (!agencyMap.get("agency_class_code").toString().startsWith("11") && !agencyMap.get("agency_class_code").toString().startsWith("12"))) {
                    throw new BasicException("行政单位的单位类别只能选择11或12类！");
                }
            } else if (agencyTypeCode.toString().startsWith("2")) {
                if (ObjectUtil.isNull(agencyMap.get("fund_guar_code"))) {
                    throw new BasicException("事业单位的经费供给方式必须填写！");
                }
                if (ObjectUtil.isNotNull(agencyMap.get("agency_class_code")) &&
                        (agencyMap.get("agency_class_code").toString().startsWith("11") || agencyMap.get("agency_class_code").toString().startsWith("12"))) {
                    throw new BasicException("事业单位的单位类别不可以选择11或12类！");
                }
            }
        }
    }

    /**
     * [单位数字表]事业单位填写行政和政法编制数，
     * 提示：[单位基本数字表]请检查单位的行政和政法编制数，该单位为事业单位。
     * <p>
     * 【提示】
     *
     * @param agencyMap
     */
    public static String agencyPersonNumNoticeRule(Map<String, Object> personMap) {
        StringBuilder noticeMsg = new StringBuilder();
        if (ObjectUtils.isEmpty(personMap)) {
            return noticeMsg.toString();
        }
        Object agencyTypeCode = personMap.get("agency_type_code");
        if (ObjectUtil.isNotNull(agencyTypeCode)) {
            if (agencyTypeCode.toString().startsWith("2")) {
                if (ObjectUtil.isNotNull(personMap.get("admin_staf_num")) && Integer.parseInt(personMap.get("admin_staf_num").toString()) > 0) {
                    noticeMsg.append("请检查单位的行政和政法编制数，该单位为事业单位！");
                }
            }
        }
        personMap.put("noticeMsg", MapUtil.getStr(personMap, "noticeMsg", "") + noticeMsg.toString());
        return noticeMsg.toString();
    }

    /**
     * [人员信息]在职人员来源，错误：请检查“在职人员来源”。在职人员的在职人员来源必须填写；离退休人员不需要填写。
     * [错误]
     *
     * @param personMap
     * @return
     */
    public static String perOnWorkErrorRule(Map<String, Object> personMap) {
        StringBuilder noticeMsg = new StringBuilder();
        if (ObjectUtils.isEmpty(personMap)) {
            return noticeMsg.toString();
        }
        Object perStaCode = personMap.get("per_sta_code");
        if (ObjectUtil.isNotNull(perStaCode)) {
            //在职
            if ("3".equals(perStaCode.toString())) {
                if (ObjectUtil.isNull(personMap.get("per_sou_code"))) {
                    noticeMsg.append("在职人员的在职人员来源必须填写！");
                }
            }
        }
        personMap.put("noticeMsg", MapUtil.getStr(personMap, "noticeMsg", "") + noticeMsg.toString());
        return noticeMsg.toString();
    }

    /**
     * [人员信息]参加工作时间，错误：请检查参加工作时间。参加工作时间必须在1920年和统计年份之间,而且日期格式必须为YYYY-MM-DD,月份和日期必须两位，不够两位请补零。
     * [错误]
     *
     * @param personMap
     * @return
     */
    public static String perEnterAgencyDateErrorRule(Map<String, Object> personMap) {
        StringBuilder noticeMsg = new StringBuilder();
        if (ObjectUtils.isEmpty(personMap)) {
            return noticeMsg.toString();
        }
        Object enterAgencyDate = personMap.get("enter_agency_date");
        if (ObjectUtil.isNotNull(enterAgencyDate)) {
            try {
                DateTime parse = DateUtil.parse(enterAgencyDate.toString(), "yyyy-MM-dd");
                int year = parse.getField(DateField.YEAR);
                int curYear = Calendar.getInstance().get(Calendar.YEAR);
                if (year < 1920 || year > curYear) {
                    noticeMsg.append("参加工作时间必须在1920年和" + curYear + "之间！");
                }
            } catch (Exception e) {
                LOG.error("perEnterAgencyDateErrorRule--参加工作时间" + enterAgencyDate + "格式转换失败！");
                noticeMsg.append("请检查参加工作时间格式,例：1990-01-01！");
            }
        }
        personMap.put("noticeMsg", MapUtil.getStr(personMap, "noticeMsg", "") + noticeMsg.toString());
        return noticeMsg.toString();
    }

    /**
     * 人员扩展信息--性别（SEX_CODE）
     * 校验内容：{1=是, 2=否}
     *
     * @param personMap
     * @return
     */
    private String privateCheckPersonSexCode(Map<String, Object> personMap) {
        StringBuilder noticeMsg = new StringBuilder();
        if (ObjectUtils.isEmpty(personMap)) {
            return noticeMsg.toString();
        }
        Object seCode = personMap.get("sex_code");
        ArrayList<String> correctList = Lists.newArrayList("1", "2");
        if (seCode != null && !correctList.contains(String.valueOf(seCode))) {
            noticeMsg.append("人员性别编码为1或2！");
        }
        personMap.put("noticeMsg", MapUtil.getStr(personMap, "noticeMsg", "") + noticeMsg.toString());
        return noticeMsg.toString();
    }
}
