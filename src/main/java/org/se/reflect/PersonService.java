package org.se.reflect;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/12/9 17:41
 */
public class PersonService {

    public void deleteAll() {
        System.out.println("delete all data");
    }

    public Person findById(Integer id) {
        Person person = new Person(id, "阿苏勒_" + id);
        return person;
    }

    //特意写成 private 修饰
    private void deleteById(Integer id) {
        System.out.println("deletes id = " + id);
    }

}
