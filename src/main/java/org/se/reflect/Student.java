package org.se.reflect;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/12/9 18:04
 */
public class Student {
    //学号
    private Integer studentID;
    //班号
    private Integer classNumber;
    //家庭住址
    private String homeAddress;

    @Override
    public String toString() {
        return "Student{" +
                "studentID=" + studentID +
                ", classNumber=" + classNumber +
                ", homeAddress='" + homeAddress + '\'' +
                '}';
    }
}