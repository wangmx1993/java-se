package org.se.reflect;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * 人员实体
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/11/13 10:56
 */
@Data
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class Person implements Serializable {
    public Integer pId;
    public String pName;

}
