package org.se.exception;

import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * 异常举例演示
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/1/26 19:05
 */
public class ExceptionTest {
    /**
     * 数组下标越界异常 ArrayIndexOutOfBoundsException
     * 因为是运行时异常（非受检查异常），所以编码时不会强制要求进行异常处理
     * 输出：1 2   3，同时抛出异常：java.lang.ArrayIndexOutOfBoundsException: 3
     */
    @Test
    public void test1() {
        Integer[] integers = new Integer[]{1, 2, 3};
        for (int i = 0; i < 4; i++) {
            System.out.print(integers[i] + "\t");
        }
    }

    /**
     * 空指针异常 java.lang.NullPointerException
     * 因为是运行时异常（非受检查异常），所以编码时不会强制要求进行异常处理
     */
    @Test
    public void test2() {
        Date date = null;
        System.out.println(date.getTime());
    }

    /**
     * 类转换异常 ClassCastException
     * 因为是运行时异常（非受检查异常），所以编码时不会强制要求进行异常处理
     */
    @Test
    public void test3() {
        List<String> stringList = new ArrayList<>();
        //输出：[]
        System.out.println(stringList);
        //抛出异常：java.lang.ClassCastException: java.util.ArrayList cannot be cast to java.util.LinkedList
        LinkedList arrayList = (LinkedList) stringList;
        System.out.println(arrayList);
    }

    /**
     * 编译时异常/受检查异常 FileNotFoundException、IOException
     * 编译时异常是编码时必须处理的异常，必须捕获处理，或者 throws 往外抛
     */
    @Test
    public void test4() {
        File file = FileSystemView.getFileSystemView().getHomeDirectory();
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            System.out.println(fileInputStream.read());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
