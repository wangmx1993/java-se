package org.se.exception;

/**
 * 基础库自定义运行时异常
 * <p>
 * 1、直接继承 Exception 是受检查异常，如果不想调用者必须处理，则可以继承 RuntimeException 作为运行时异常即可
 * 2、运行时异常无需显示的指定回滚类型，默认回滚运行时异常：@Transactional
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/1/26 15:10
 */
public class BasicException extends RuntimeException {

    /**
     * 自定义属性'异常编码'(不需要时可以去掉)
     */
    private Integer errorCode;

    /**
     * java.lang.Throwable 实现了 java.io.Serializable 序列化接口
     * serialVersionUID 用于控制序列化版本，如果将来不需要做序列化，则可以不用加
     */
    private static final long serialVersionUID = 2425124454542714153L;

    /**
     * 不包含详细信息的构造器
     */
    public BasicException() {
        super();
    }

    /**
     * 用指定的详细错误信息的构造器
     *
     * @param message：异常消息
     */
    public BasicException(String message) {
        super(message);
    }

    public BasicException(String message, Integer errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * 使用指定的详细信息和原因构造新异常.
     * 与 cause 关联的异常信息不会自动合并到新异常中，而是使用 message。
     *
     * @param message ：异常详细信息
     * @param cause   ：原始异常对象，在它的基础上再包装一层，不会让原始异常信息丢失。
     */
    public BasicException(String message, Throwable cause) {
        super(message, cause);
    }

    public BasicException(String message, Throwable cause, Integer errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    /**
     * 构造具有指定原因和详细信息的新异常
     *
     * @param cause ：原始的异常对象，在它的基础上再包装一层，不会让原始异常信息丢失。
     */
    public BasicException(Throwable cause) {
        super(cause);
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }
}
