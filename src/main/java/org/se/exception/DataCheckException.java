package org.se.exception;

/**
 * 自定义数据校验编译异常
 * 1、java.lang.Exception 属于异常体系的超类，继承它之后，当 throw new DataCheckException() 抛出异常的时候
 * 调用者必须 try-catch 处理或者继续往外抛 throws。
 * 2、继承 Exception 的子类相当于编译时异常/受检查异常
 * 3、编译异常必须在显示的指定回滚类型，否则默认不回滚事务：@Transactional(rollbackFor = Exception.class)
 * 4、对于编译时异常，try-catch 后又直接往外抛原始异常对象时，相当于自己没有处理，此时方法上面必须使用 throws 继续往外抛
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/1/26 14:41
 */
public class DataCheckException extends Exception {
    /**
     * java.lang.Throwable 实现了 java.io.Serializable 序列化接口
     * serialVersionUID 用于控制序列化版本，如果将来不需要做序列化，则可以不用加
     */
    private static final long serialVersionUID = 2425120855442714153L;

    /**
     * 不包含详细信息的构造器
     */
    public DataCheckException() {
        super();
    }

    /**
     * 用指定的详细错误信息的构造器
     *
     * @param message：异常消息
     */
    public DataCheckException(String message) {
        super(message);
    }

    /**
     * 构造具有指定原因和详细信息的新异常
     *
     * @param cause ：原始的异常对象，在它的基础上再包装一层，不会让原始异常信息丢失。
     */
    public DataCheckException(Throwable cause) {
        super(cause);
    }

    /**
     * 使用指定的详细信息和原因构造新异常.
     * 与 cause 关联的异常信息不会自动合并到新异常中，而是使用 message。
     *
     * @param message ：异常详细信息
     * @param cause   ：原始异常对象，在它的基础上再包装一层，不会让原始异常信息丢失。
     */
    public DataCheckException(String message, Throwable cause) {
        super(message, cause);
    }
}