package org.se.exception;

import org.junit.Test;

/**
 * {@link DataCheckException} 测试类
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/1/26 14:41
 */
public class DataCheckExceptionTest {

    /**
     * 因为自定义的 DataCheckException 属于编译时异常，所以 throw 抛出之后，自己也必须处理或者继续往外抛
     *
     * @param name
     * @param age
     * @throws DataCheckException
     */
    public void register1(String name, Integer age) throws DataCheckException {
        if (age == null || age <= 0) {
            throw new DataCheckException(age + "");
        }
        System.out.println("name = " + name + ", age=" + age + " 校验通过.");
    }

    public void register2(String name, Integer age) throws DataCheckException {
        if (age == null || age <= 0) {
            throw new DataCheckException();
        }
        System.out.println("name = " + name + ", age=" + age + " 校验通过.");
    }

    public void register3(String name, Integer age) throws DataCheckException {
        try {
            if (age == null || age <= 0) {
                throw new RuntimeException("年龄不能小于0：" + age);
            }
        } catch (RuntimeException e) {
            //注意此时抛出的异常信息为：org.se.exception.DataCheckException: java.lang.RuntimeException: 年龄不能小于0：-10
            //当 e.getMessage() 的时候，得到的是：java.lang.RuntimeException: 年龄不能小于0：-10
            throw new DataCheckException(e);
        }

        System.out.println("name = " + name + ", age=" + age + " 校验通过.");
    }

    public void register4(String name, Integer age) throws DataCheckException {
        try {
            if (age == null || age <= 0) {
                throw new RuntimeException("年龄不能小于0：" + age);
            }
        } catch (RuntimeException e) {
            throw new DataCheckException("年龄不能为负数。", e);
        }

        System.out.println("name = " + name + ", age=" + age + " 校验通过.");
    }

    @Test
    public void testRegister1() {
        try {
            DataCheckExceptionTest test = new DataCheckExceptionTest();
            //正常输出：name = 张三, age=33 校验通过.
            test.register1("张三", 33);
            //抛出异常
            //org.se.exception.DataCheckException: -10
            //at org.se.exception.DataCheckExceptionTest.register1(DataCheckExceptionTest.java:23)
            test.register1("张三", -10);
        } catch (DataCheckException e) {
            e.printStackTrace();
            //对于编译时异常，直接往外抛原始对象时，相当于自己没有处理，此时方法上面必须使用 throws 继续往外抛
            //throw e;
        }
    }

    @Test
    public void testRegister2() {
        try {
            DataCheckExceptionTest test = new DataCheckExceptionTest();
            //正常输出：name = 张三, age=34 校验通过.
            test.register2("张三", 34);
            //抛出异常
            //org.se.exception.DataCheckException
            //at org.se.exception.DataCheckExceptionTest.register2(DataCheckExceptionTest.java:30)
            test.register2("张三", -10);
        } catch (DataCheckException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testRegister3() {
        try {
            DataCheckExceptionTest test = new DataCheckExceptionTest();
            //正常输出：name = 张三, age=34 校验通过.
            test.register3("张三", 34);
            //抛出异常
            //org.se.exception.DataCheckException: java.lang.RuntimeException: 年龄不能小于0：-10
            //at org.se.exception.DataCheckExceptionTest.register3(DataCheckExceptionTest.java:41)
            //Caused by: java.lang.RuntimeException: 年龄不能小于0：-10
            //at org.se.exception.DataCheckExceptionTest.register3(DataCheckExceptionTest.java:38)
            test.register3("张三", -10);
        } catch (DataCheckException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testRegister4() {
        try {
            DataCheckExceptionTest test = new DataCheckExceptionTest();
            //正常输出：name = 张三, age=34 校验通过.
            test.register4("张三", 34);
            //抛出异常
            //org.se.exception.DataCheckException: 年龄不能为负数。
            //at org.se.exception.DataCheckExceptionTest.register4(DataCheckExceptionTest.java:53)
            //Caused by: java.lang.RuntimeException: 年龄不能小于0：-10
            //at org.se.exception.DataCheckExceptionTest.register4(DataCheckExceptionTest.java:50)
            test.register4("张三", -10);
        } catch (DataCheckException e) {
            e.printStackTrace();
        }
    }

}
