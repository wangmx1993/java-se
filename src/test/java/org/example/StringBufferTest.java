package org.example;

import org.junit.Test;

/**
 * StringBuffer 线程安全的可变序列缓冲字符串
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/12/29 15:20
 */
public class StringBufferTest {

    /**
     * synchronized StringBuffer insert(int offset, String str)：将字符串(str)插入此字符序列的指定位置(offset)
     * offset 必须大于等于0，且小于等于源字符串的长度，否则异常
     */
    @Test
    public void insert1() {
        StringBuffer stringBuffer1 = new StringBuffer("123");
        StringBuffer stringBuffer2 = new StringBuffer("123");
        StringBuffer stringBuffer3 = new StringBuffer("123");
        StringBuffer stringBuffer4 = new StringBuffer("123");

        stringBuffer1.insert(0, "A");
        stringBuffer2.insert(1, "A");
        stringBuffer3.insert(2, "A");
        stringBuffer4.insert(3, "A");

        System.out.println(stringBuffer1);//A123
        System.out.println(stringBuffer2);//1A23
        System.out.println(stringBuffer3);//12A3
        System.out.println(stringBuffer4);//123A
    }

    /**
     * delete(int start, int end) 删除此序列的子字符串中的字符
     * 删除之后，源字符串缓冲区也会被清空
     */
    @Test
    public void testDelete() {
        StringBuffer stringBuffer1 = new StringBuffer("123");

        StringBuffer delete = stringBuffer1.delete(0, stringBuffer1.length());
        System.out.println("1=" + stringBuffer1);//1=
        System.out.println("2=" + delete);//2=

        delete.append("888");
        System.out.println("3=" + delete);//3=888
    }


}
