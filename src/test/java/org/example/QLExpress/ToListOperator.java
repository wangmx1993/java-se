package org.example.QLExpress;

import java.util.ArrayList;
import java.util.List;

/**
 * 在 QLExpress 中，可以通过自定义操作符{@link com.ql.util.express.Operator}来扩展语言的功能。
 * 然后可以被通过addFunction或者addOperator的方式注入到ExpressRunner中。
 * 如果使用Operator的基类OperatorBase.java将获得更强大的能力，基本能够满足所有的要求。
 * <p>
 * 自定义操作符"toList"，实现将多个元素转为List集合
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/11/9 10:31
 */
public class ToListOperator extends com.ql.util.express.Operator {

    /**
     * 自定义操作符号的优先级与 "*"相同，语法形式也是 data1 name data2 name data3,...
     *
     * @param list ：每次是两个元素，比如第一次是 data1 与 data2，第二次是第一次的结果 与 data3
     * @return /
     * @throws Exception /
     */
    @Override
    public Object executeInner(Object[] list) throws Exception {
        Object opData1 = list[0];
        Object opData2 = list[1];
        if (opData1 instanceof List) {
            ((List) opData1).add(opData2);
            return opData1;
        }
        List result = new ArrayList();
        for (Object opData : list) {
            result.add(opData);
        }
        return result;
    }

}