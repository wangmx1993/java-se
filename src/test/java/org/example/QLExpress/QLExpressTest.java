package org.example.QLExpress;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import com.google.common.collect.Lists;
import com.ql.util.express.DefaultContext;
import com.ql.util.express.DynamicParamsUtil;
import com.ql.util.express.ExpressRunner;
import com.ql.util.express.InstructionSet;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * QLExpress 脚本规则引擎测试
 * <a href="https://gitee.com/alibaba/QLExpress">...</a>
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/11/8 下午3:26
 */
public class QLExpressTest {
    /**
     * 执行一段文本
     * Object execute(String expressString, IExpressContext<String, Object> context, List<String> errorList, boolean isCache, boolean isTrace, long timeoutMillis)
     * * expressString：表达式文本
     * * context：执行上下文，可以扩展为包含ApplicationContext;它是脚本执行时的环境，包含变量、函数等信息。
     * * 上下文中的变量值如果在计算中被改变，则后续的执行中，如果用到了，也会被影响。可以理解为线程局部变量;
     * * 还可以直接通过get方法来获取其中的变量值;
     * * errorList：输出的错误信息List
     * * isCache：是否使用Cache中的指令集,建议为true
     * * isTrace：是否输出所有的跟踪调试信息，同时还需要log级别是DEBUG级别
     * * timeoutMillis：设置脚本的运行超时时间，防止死循环。毫秒。
     *
     * @throws Exception /
     */
    @Test
    public void testHelloWorld() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();
        context.put("a", 1);
        context.put("b", 2);
        context.put("c", 6);
        context.put("d", 3);
        // 自动先乘除后加减;没有空格隔开也没有关系;
        String express = "a + b * c / d";
        Object r = runner.execute(express, context, null, true, false, 3 * 60 * 1000);
        // 1 + 2 * 6 / 3 = 5
        System.out.println(r);
    }


    /**
     * 编译脚本，在QLExpress中，编译脚本并查询外部需要定义的变量和函数可以通过 ExpressRunner 提供的一些方法来实现。
     *
     * @throws Exception /
     */
    @Test
    public void testExpressRunner1() throws Exception {
        // isPrecise:是否需要高精度计算支持;高精度计算在会计财务中非常重要；
        // isTrace:是否跟踪执行指令的过程;主要是是否输出脚本的编译解析过程，一般对于业务系统来说关闭之后会提高性能。
        ExpressRunner runner = new ExpressRunner(true, true);
        // 定义脚本
        String express = "a + b * c / d";
        // 获取一个表达式需要的外部变量名称列表
        String[] variableNames = runner.getOutVarNames(express);
        System.out.println("==============================外部需要定义的变量==============================");
        System.out.println(ArrayUtil.toString(variableNames));// [a, b, c, d]

        // 获取一个表达式需要的外部变量名称列表
        // String[] functionNames = runner.getOutFunctionNames(express);
    }

    @Test
    public void testExpressRunner2() throws Exception {
        // 注意以下脚本int和没有int的区别
        String express = "int 平均分 = (语文 + 数学 + 英语 + 综合考试.科目2) / 4.0; return 平均分";
        ExpressRunner runner = new ExpressRunner(true, true);
        String[] names = runner.getOutVarNames(express);

        // 有int时：[平均分, 数学, 综合考试, 英语, 语文]
        // 无int时：[数学, 综合考试, 英语, 语文]
        System.out.println("=====================names====================");
        System.out.println(ArrayUtil.toString(names));
        try {
            // 脚本语法是否正确，可以通过ExpressRunner编译指令集的接口来完成。
            // 如果调用过程不出现异常，指令集instructionSet就是可以被加载运行（execute）了！
            InstructionSet instructionSet = runner.parseInstructionSet(express);
            System.out.println("=====================instructionSet====================");
            System.out.println(instructionSet.toString());
        } catch (Exception e) {
            throw new RuntimeException("脚本语法错误.", e);
        }
    }

    /**
     * Java对象操作
     * 基本的 Java 语法和对象操作在 QLExpress 中同样适用;
     * <pre>
     * import com.ql.util.express.test.OrderQuery; //系统自动会import java.lang.*,import java.util.*;
     * query = new OrderQuery();           // 创建class实例，自动补全类路径
     * query.setCreateDate(new Date());    // 设置属性
     * query.buyer = "张三";                // 调用属性，默认会转化为setBuyer("张三")
     * result = bizOrderDAO.query(query);  // 调用bean对象的方法
     * System.out.println(result.getId()); // 调用静态方法
     * </pre>
     *
     * @throws Exception /
     */
    @Test
    public void testJavaCode1() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();

        Person person = new Person(1001, "张三", DateUtil.parse("1993/08/25"), 13500.88F);
        context.put("person", person);

        // 1001
        System.out.println(runner.execute("person.getId()", context, null, true, false));
        // 1993-08-25 00:00:00
        System.out.println(runner.execute("person.getBirthday()", context, null, true, false));
        // 14500.88
        System.out.println(runner.execute("person.getSalary() + 1000", context, null, true, false));
        // 李四
        System.out.println(runner.execute("person.name = '李四'", context, null, true, false));
        // 李四
        System.out.println(runner.execute("person.getName()", context, null, true, false));
        // 王五Joni
        System.out.println(runner.execute("person.setName('王五Joni');person.getName()", context, null, true, false));
        // 王五JONI
        System.out.println(runner.execute("person.getName().toUpperCase()", context, null, true, false));
        // 有钱
        System.out.println(runner.execute("cn.hutool.core.util.ObjUtil.defaultIfNull(person.getSalary(), '0') > 10000 ? '有钱' : '一般'", context, null, true, false));
        // 有钱
        System.out.println(runner.execute("if person.getSalary() > 10000 then '有钱' else '一般'", context, null, true, false));
    }

    /**
     * 弱类型语言，请不要定义类型声明,更不要用Template（Map<String, List>之类的）
     * <p>
     * java语法：对象类型声明：FocFulfillDecisionReqDTO reqDTO = param.getReqDTO();
     * ql写法：reqDTO = param.getReqDTO();
     * <pre>
     *     和java语法相比，要避免的一些ql写法错误:
     *     不支持try{}catch{}
     *     注释目前只支持 /** ＊＊/，不支持单行注释 //
     *     不支持java8的lambda表达式
     *     不支持for循环集合操作for(Item item :list)
     *     弱类型语言，请不要定义类型声明,更不要用Template（Map<String, List> 之类的）
     *     array的声明不一样
     *     min,max,round,print,println,like,in 都是系统默认函数的关键字，请不要作为变量名
     * </pre>
     *
     * @throws Exception /
     */
    @Test
    public void testNewMap1() throws Exception {
        ExpressRunner runner1 = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();
        context.put("#personMap", MapUtil.builder().put("id", "1201A").put("code", "001001").build());

        String expressString = "keys = new ArrayList();\n" +
                "        keys.add(1);\n" +
                "        keys.add(2);\n" +
                "        keys.add(3);\n" +
                "        return keys;";
        Object result1 = runner1.execute(expressString, context, null, false, false);
        System.out.println(result1); // [1, 2, 3]

        String expressString2 = "dataMap = new HashMap();\n" +
                "        dataMap.putAll(#personMap);\n" +
                "        return dataMap;";
        Object result2 = runner1.execute(expressString2, context, null, false, false);
        System.out.println(result2);// {code=001001, id=1201A}
    }

    /**
     * 集合的快捷写法
     * 在QLExpress中，可以使用一些快捷的语法来操作集合。
     *
     * @throws Exception /
     */
    @Test
    public void testNewMap2() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();

        // 使用NewMap关键字创建Map
        String expressMap = "abc = NewMap(1:1, 2:2); return abc.get(1) + abc.get(2);";
        Object resultMap = runner.execute(expressMap, context, null, false, false);
        System.out.println("NewMap Result: " + resultMap);// 3

        // 使用NewList关键字创建List
        String expressList = "abc = NewList(1, 2, 3); return abc.get(1) + abc.get(2);";
        Object resultList = runner.execute(expressList, context, null, false, false);
        System.out.println("NewList Result: " + resultList);// 5

        // 使用方括号[]创建List
        String expressSquareBrackets = "abc = [1, 2, 3]; return abc[1] + abc[2];";
        Object resultSquareBrackets = runner.execute(expressSquareBrackets, context, null, false, false);
        System.out.println("Square Brackets Result: " + resultSquareBrackets);// 5
    }

    /**
     * 集合的遍历
     * 类似java的语法，只是ql不支持for(obj:list){}的语法，只能通过下标访问。
     */
    @Test
    public void testForEachMap() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();

        // 创建一个Map
        Map<String, String> map = new HashMap<>();
        map.put("a", "a_value");
        map.put("b", "b_value");

        // 将Map放入上下文中
        context.put("map", map);

        System.out.println("==============map.keySet=====遍历Map===========");
        String express1 = "keySet = map.keySet();\n" + "objArr = keySet.toArray();\n" + "for (i = 0; i < objArr.length; i++) {\n" + "    key = objArr[i];\n" + "    System.out.println(map.get(key));\n" + "}";
        runner.execute(express1, context, null, false, false);

        System.out.println("==============map.forEach=====遍历Map===========");
        String express2 = "map.forEach((key, value) -> {\n" + "            System.out.println(key + \"=\" + value);\n" + "        })";
        runner.execute(express2, context, null, false, false);

        System.out.println("==============map.entrySet().stream().forEach========遍历Map========");
        String express3 = "map.entrySet().stream().forEach(entry -> {\n" + "            System.out.println(entry.getKey() + \"=\" + entry.getValue());\n" + "        })";
        runner.execute(express3, context, null, false, false);
    }

    @Test
    public void testForEachList() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();
        context.put("dataList", Lists.newArrayList("1", "a", "哈", "你好"));

        System.out.println("==============dataList.stream().forEach=========遍历List==========");
        String express1 = "dataList.stream().forEach(item -> System.out.println(item))";
        runner.execute(express1, context, null, false, false);

        System.out.println("==============for (int i = 0; i < dataList.size(); i++)=========遍历List==========");
        String express2 = "for (int i = 0; i < dataList.size(); i++) {\n" + "            System.out.println(dataList.get(i));\n" + "        }";
        runner.execute(express2, context, null, false, false);

        System.out.println("==============dataList.forEach=========遍历List==========");
        String express3 = "StringBuffer result = new StringBuffer(\"(\");\n" + "        dataList.forEach(item -> result.append(\"'\").append(item).append(\"',\"));\n" + "        result.deleteCharAt(result.lastIndexOf(\",\")).append(\")\");" + "        return result";
        Object execute3 = runner.execute(express3, context, null, false, false);
        System.out.println(execute3);// ('1','a','哈','你好')
    }

    /**
     * 支持操作符：
     * * +,-,*,/,<,>,<=,>=,==,!=,<>【等同于!=】,%,mod【取模等同于%】,++,--,in【类似sql】,like【sql语法】,&&【等同and】,||【等同or】,!,等操作符
     * 逻辑三元操作：a > b ? a : b;
     * * 注意：关键字必须小写，不能大写;
     * <p>
     * 支持 for，break、continue、if then else 等标准的程序控制逻辑
     * <p>
     *
     * @throws Exception /
     */
    @Test
    public void testOperator1() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();
        context.put("a", 1);
        context.put("b", 2);
        context.put("c", 6);
        context.put("d", 3);
        context.put("f", 5);

        System.out.println("==================算数运算=================");
        System.out.println(runner.execute("f / 3", context, null, true, false));// 1
        System.out.println(runner.execute("f / 3.0", context, null, true, false));// 1.6666666666666667
        System.out.println(runner.execute("(b + c - a) * 1.5 / 2 ", context, null, true, false));// 5.25
        System.out.println(runner.execute("(c*d) mod f", context, null, true, false));// 3
        System.out.println(runner.execute("(c * d) % f", context, null, true, false));// %》》3

        System.out.println("==================逻辑运算=================");
        System.out.println(runner.execute("b * d > c", context, null, true, false));// false
        System.out.println(runner.execute("a == 1", context, null, true, false));// true
        System.out.println(runner.execute("a2 == null", context, null, true, false));// true
        System.out.println(runner.execute("a2 != null", context, null, true, false)); // false
        System.out.println(runner.execute("b * d > f && c <= 10", context, null, true, false));// true

        System.out.println("==================三元运算=================");
        System.out.println(runner.execute("b * d > f ? a : c", context, null, true, false));// 1

        System.out.println("==================in【类似sql】==================");
        System.out.println(runner.execute("b in (1,2,3)", context, null, true, false));// true
        System.out.println(runner.execute("b in (1,23,3)", context, null, true, false));// false

        System.out.println("==================like【类似sql】==================");
        System.out.println(runner.execute("'zhangSan' like 'zhang%'", context, null, true, false));// true
        System.out.println(runner.execute("'liSi' like 'zhang%'", context, null, true, false));// false
        System.out.println(runner.execute("!('liSi' like 'zhang%')", context, null, true, false));// true

        System.out.println("==================修改变量值==================");
        System.out.println(runner.execute("a = b + c;a = a * c;a * 2", context, null, true, false));// 96
        System.out.println(context.get("a"));// 48

        System.out.println("==================if-then-else==================");
        System.out.println(runner.execute("if f > 3 then '有钱' else '一般'", context, null, true, false));
    }

    /**
     * 扩展操作符：Operator
     * 在 QLExpress 中，可以通过自定义操作符{@link com.ql.util.express.Operator}来扩展语言的功能。
     */
    @Test
    public void testOperator2() throws Exception {
        // 示例 1：替换 if then else 关键字
        ExpressRunner runner1 = new ExpressRunner();
        DefaultContext<String, Object> context1 = new DefaultContext<>();
        context1.put("语文", 120);
        context1.put("数学", 123);
        context1.put("english", 55);
        // 添加操作符和关键字的别名，同时对操作符可以指定错误信息。例如：addOperatorWithAlias("加","+",null)
        runner1.addOperatorWithAlias("如果", "if", null);
        runner1.addOperatorWithAlias("则", "then", null);
        runner1.addOperatorWithAlias("否则", "else", null);

        String express1 = "如果 (语文 + 数学 + english > 270) 则 {return 1;} 否则 {return 0;}";
        // String express1 = "如果 (语文 + 数学 + 英语 > 270) 则 1 否则 0";
        Object result1 = runner1.execute(express1, context1, null, false, false, 100L);
        System.out.println("Result 1: " + result1); // 输出结果 1
    }

    /**
     * 自定义Operator
     * 在 QLExpress 中，可以通过自定义操作符{@link com.ql.util.express.Operator}来扩展语言的功能。
     */
    @Test
    public void testOperator3() throws Exception {
        DefaultContext<String, Object> context2 = new DefaultContext<>();

        // 示例 2.1：addOperator - 添加自定义操作符号，此操作符号的优先级与 "*"相同，语法形式也是  data name data
        ExpressRunner runner2 = new ExpressRunner();
        runner2.addOperator("join", new ToListOperator());
        Object result2_1 = runner2.execute("1 join 2 join 3", context2, null, false, false);
        System.out.println("Result 2.1: " + result2_1); // 输出结果 [1, 2, 3]

        // 示例 2.2：replaceOperator - 替换操作符处理
        ExpressRunner runner2_2 = new ExpressRunner();
        runner2_2.replaceOperator("+", new ToListOperator());
        Object result2_2 = runner2_2.execute("1 + 2+3", context2, null, false, false);
        System.out.println("Result 2.2: " + result2_2); // 输出结果 [1, 2, 3]

        // 示例 2.3：addFunction - 添加函数定义
        ExpressRunner runner2_3 = new ExpressRunner();
        runner2_3.addFunction("toList", new ToListOperator());
        Object result2_3 = runner2_3.execute("toList(1, 2, 3)", context2, null, false, false);
        System.out.println("Result 2.3: " + result2_3); // 输出结果 [1, 2, 3]
    }

    /**
     * 脚本中定义function
     * 在QLExpress中，可以通过 function 关键字来定义函数，像JS一样，函数不需要指定返回值类型
     *
     * @throws Exception /
     */
    @Test
    public void testFunction1() throws Exception {
        final String express = "function add(int a, int b) { return a + b;}\n" + "function sub(int a, int b) { return a - b;}\n" + "a = 10;\n" + // 可以省略数据类型
                "result = add(a, 4) + sub(a, 9);\n" + "return result;";
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();
        // 执行脚本
        Object result = runner.execute(express, context, null, true, false);
        System.out.println("Result: " + result);// 15
        // 输出函数调用过程中的参数和返回值：call: a + 4 + a - 9 = 10 + 4 + 10 - 9 = 15
        System.out.println("call: a + 4 + a - 9 = " + context.get("a") + " + 4 + " + context.get("a") + " - 9 = " + context.get("result"));
    }

    @Test
    public void testFunction2() throws Exception {
        final String express = "function isBlank(CharSequence str) {\n" + "int length;\n" + "if ((str == null) || ((length = str.length()) == 0)) {return true;}\n" + "for (int i = 0; i < length; i++) {\n" + "if (false == cn.hutool.core.util.CharUtil.isBlankChar(str.charAt(i))) {return false;}\n" + "}\n" + "return true;\n" + "}\n" + "return isBlank(#str)";
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();

        context.put("#str", "");
        Object result = runner.execute(express, context, null, true, false);
        System.out.println("Result: " + result);// true

        context.put("#str", "  ");
        System.out.println(runner.execute(express, context, null, true, false));// true

        context.put("#str", null);
        System.out.println(runner.execute(express, context, null, true, false));// true

        context.put("#str", " Ok ");
        System.out.println(runner.execute(express, context, null, true, false));// false
    }

    /**
     * 在 QLExpress 中，可使用 addFunctionOfClassMethod 和 addFunctionOfServiceMethod 方法来绑定 Java 类或对象的方法
     * OperatorBase getFunction(String name); // 通过name获取function的定义
     * <p>
     * addFunctionOfClassMethod(String name, String className, String functionName, String[] parameterTypes, String errorInfo)
     * 添加一个类的函数定义，例如：Math.abs(double) 映射为表达式中的 "取绝对值(-5.0)"
     * <p>
     * addFunctionOfServiceMethod(String name, Object serviceObject, String functionName, String[] parameterTypes, String errorInfo)
     * 用于将一个用户自己定义的对象(例如Spring对象)方法转换为一个表达式计算的函数
     * <p>
     * addFunctionOfServiceMethod(String name, Object serviceObject, String functionName, Class<?>[] parameterClassTypes, String errorInfo)
     * 用于将一个用户自己定义的对象(例如Spring对象)方法转换为一个表达式计算的函数
     *
     * @throws Exception /
     */
    @Test
    public void testFunction4() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();

        // addFunction - 添加函数定义
        runner.addFunction("toList", new ToListOperator());
        Object result2_3 = runner.execute("toList(1, 2, 3)", context, null, false, false);
        System.out.println("Result: " + result2_3); // 输出结果 [1, 2, 3]

        // 绑定 java.lang.Math 类的 public static double abs(double a) 方法
        runner.addFunctionOfClassMethod("取绝对值", Math.class.getName(), "abs", new String[]{"double"}, null);
        System.out.println(runner.execute("取绝对值(-100.90)", context, null, false, false));// 100.9
        System.out.println(runner.execute("取绝对值(0.90)", context, null, false, false));// 0.9
        System.out.println(runner.execute("取绝对值(0)", context, null, false, false));// 0.0

        // 绑定 org.apache.commons.lang3.StringUtils 的 public static String upperCase(final String str) 方法
        runner.addFunctionOfClassMethod("toUpperCase", StringUtils.class.getName(), "upperCase", new String[]{"String"}, null);
        // HELLO WORLD 100.9
        System.out.println(runner.execute("toUpperCase('Hello World ' + 取绝对值(-100.90))", context, null, false, false));

        // 绑定 System.out(PrintStream) 的 public void println(String x) 方法
        runner.addFunctionOfServiceMethod("printStr", System.out, "println", new String[]{"String"}, null);
        System.out.println(runner.execute("printStr('日志')", context, null, false, false));

        // 绑定 org.example.QLExpress.Person 对象的 public String toString() 方法
        runner.addFunctionOfServiceMethod("toPString", new Person(1001, "张三", DateUtil.parse("1993/08/25"), 13500.88F), "toString", new Class[]{}, null);
        // Person{id=1001, name='张三', birthday=1993-08-25 00:00:00, salary=13500.88}
        System.out.println(runner.execute("toPString();", context, null, false, false));
    }

    /**
     * 在QLExpress中，可以通过使用不定参数（动态参数）来处理方法的参数。
     *
     * @throws Exception /
     */
    @Test
    public void testFunction5() throws Exception {
        // 创建 ExpressRunner 实例
        ExpressRunner runner = new ExpressRunner(true, false);
        // 创建 DefaultContext 实例
        DefaultContext<String, Object> context = new DefaultContext<>();
        context.put("#source", "1，2，3");

        // 在 runner 中添加一个函数，使用不定参数:public static boolean cn.hutool.core.util.StrUtil.containsAny(CharSequence str, CharSequence... testStrs)
        String className = cn.hutool.core.util.StrUtil.class.getName();
        runner.addFunctionOfClassMethod("containsAnyStr", className, "containsAny", new Class[]{CharSequence.class, CharSequence[].class}, null);
        // (1)不定参数默认使用数组进行传递
        System.out.println(runner.execute("containsAnyStr(#source, [\"1\"]);", context, null, false, false));// true
        System.out.println(runner.execute("containsAnyStr(#source, [\"12\"]);", context, null, false, false));// false
        System.out.println(runner.execute("containsAnyStr(#source, [\"12\",\"3\"]);", context, null, false, false));// true

        //(2)像java一样,支持函数动态参数调用,需要打开以下全局开关,否则以下调用会失败
        DynamicParamsUtil.supportDynamicParams = true;
        System.out.println(runner.execute("containsAnyStr(#source, \"12\",\"3\");", context, null, false, false));// true
        System.out.println(runner.execute("containsAnyStr(#source, \"12\",\"30\");", context, null, false, false));// false
    }

    /**
     * macro 宏定义
     * 在QLExpress中，宏定义（macro）允许将一个表达式片段命名为宏，并在其他地方引用这个宏。
     * 当需要在多个地方使用相同的复杂表达式时非常有用，可以提高代码的可读性和维护性。
     */
    @Test
    public void testMacro1() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        DefaultContext<String, Object> context = new DefaultContext<>();

        // 定义宏
        runner.addMacro("计算平均成绩", "(语文+数学+英语)/3.0");
        runner.addMacro("是否优秀", "计算平均成绩>90");

        // 设置变量值
        context.put("语文", 88);
        context.put("数学", 99);
        context.put("英语", 95);

        System.out.println(runner.execute("计算平均成绩", context, null, false, false));// 94.0
        Object result = runner.execute("是否优秀", context, null, false, false);
        System.out.println("Result: " + result);// true
    }

}
