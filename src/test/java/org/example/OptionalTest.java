package org.example;

import org.junit.Test;

import java.util.*;

/**
 * 1、Optional 类是一个可以为 null 的容器对象，它可以保存类型T的值，或者仅仅保存 null。
 * 2、如果值存在，则 isPresent() 方法返回 true，调用 get() 方法会返回该对象。
 * 3、Optional 类的引入很好的解决空指针异常。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/6/2 9:09
 */
public class OptionalTest {

    /**
     * Optional<T> of(T value) : 返回具有指定的当前非空值的 Optional，value 是要显示的值，必须为非空。如果 value 为 null，则 NullPointerException
     * Optional<T> ofNullable(T value)：如果 value 为 null，则返回空的 Optional。
     * boolean isPresent()：如果存在值，则返回 true，否则返回 false
     * T orElse(T other)：如果值存在，则返回，否则返回 other
     * T get()：如果存在值，则返回，否则抛出异常 NoSuchElementException
     */
    @Test
    public void testOptional1() {
        Optional<String> optional1 = Optional.of("蚩尤后裔");
        Optional<Object> optional2 = Optional.ofNullable(null);

        System.out.println(optional1.isPresent());//true
        System.out.println(optional2.isPresent());//false

        System.out.println(optional1.orElse("x"));//蚩尤后裔
        System.out.println(optional2.orElse("x"));//x

        String s = optional1.get();
        System.out.println(s);//蚩尤后裔
    }

    /**
     * void ifPresent(Consumer<? super T> consumer)：如果存在值，则使用该值调用 consumer 函数，否则不执行任何操作。
     * 如果值存在且为 null，则抛出 NullPointerException
     */
    @Test
    public void testIfPresent() {
        Map<String, Object> dataMap1 = new HashMap<>();
        dataMap1.put("agency_code", "100010");

        Optional.ofNullable(dataMap1).ifPresent(item -> {
            item.put("msg", "蚩尤后裔");
        });

        System.out.println(dataMap1);//{msg=蚩尤后裔, agency_code=100010}

    }


    /**
     * T orElseGet(Supplier<? extends T> other): 如果有值，则直接返回，否则使用 Supplier 接口的实现用来生成默认值。
     */
    @Test
    public void testOrElseGet() {
        Map<String, Object> dataMap = null;
        Optional<Object> o1 = Optional.ofNullable(dataMap);

        Map<String, Object> resultMap = (Map<String, Object>) o1.orElseGet(() -> {
            Map<String, Object> initMap = new HashMap<>();
            initMap.put("msg", "默认初始化");
            return initMap;
        });

        System.out.println(resultMap);//{msg=默认初始化}
    }

    /**
     * T orElseThrow(Supplier<? extends X> exceptionSupplier) :  如果有值，则直接返回，否则使用 Supplier 接口的实现用来生成抛出异常
     */
    @Test
    public void testOrElseThrow() {
        Optional<Object> o1 = Optional.ofNullable(null);

        Object orElseThrow = o1.orElseThrow(() -> new RuntimeException("值不存在！"));
        System.out.println(orElseThrow);
    }

    /**
     * Optional<U> map(Function<? super T, ? extends U> mapper): 如果值等于 null，则返回一个空的 Optional，否则调用 mapper 映射函数得到新的值。
     */
    @Test
    public void testMap() {

        Optional<Object> o1 = Optional.ofNullable("王重阳 Love！");
        String orElse = o1.map(item -> item.toString().toUpperCase()).orElse("谢晓峰");
        System.out.println(orElse);//王重阳 LOVE！

        Optional<Object> o2 = Optional.ofNullable(null);
        String orElse2 = o2.map(item -> item.toString().toUpperCase()).orElse("谢晓峰");
        System.out.println(orElse2);//谢晓峰
    }

    /**
     * Optional<T> filter(Predicate<? super T> predicate)：如果存在存在值，则使用 predicate 进行过滤，否则返回空的选项。
     */
    @Test
    public void testFilter() {
        Optional<Object> o1 = Optional.ofNullable(null);

        Object anElse = o1.filter(item -> item.toString().contains("o")).orElse("谢晓峰");
        System.out.println(anElse);
    }


}
