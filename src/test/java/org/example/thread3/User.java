package org.example.thread3;

/**
 * 需要在线程间共享的数据可以封装成POJO实体
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/10/31 16:26
 */
public class User {
    /**
     * 用户信用卡账户余额
     */
    private Integer accountBalance;

    public Integer getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Integer accountBalance) {
        this.accountBalance = accountBalance;
    }
}