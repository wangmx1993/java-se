package org.example;

import org.junit.Test;

import java.util.*;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/10/31 10:01
 */
public class IteratorTest {
    /**
     * Iterator 接口可用于遍历任何 Collection 接口，可以从一个 Collection 中使用迭代器方法来获取迭代器实例。
     */
    @Test
    public void iteratorTest1() {
        String[] strings = {"万里长城", "今犹在", "我", "不见当年", "秦始皇。"};
        List<String> list = new ArrayList<>(Arrays.asList(strings));
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            if ("我".equals(iterator.next())) {
                iterator.remove();
            }
        }
        //[万里长城, 今犹在, 不见当年, 秦始皇。]
        System.out.println(list);
    }

    @Test
    public void iteratorTest2() {
        String[] strings = {"万里长城", "今犹在", "我", "不见当年", "秦始皇。"};
        List<String> list = new ArrayList<>(Arrays.asList(strings));

        ListIterator<String> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            int nextIndex = listIterator.nextIndex();
            String next = listIterator.next();
            //0:万里长城
            //1:今犹在
            //2:我
            //3:不见当年
            //4:秦始皇。
            System.out.println(nextIndex + ":" + next);
            if ("我".equals(next)) {
                listIterator.set("我们");
            }
        }
        //[万里长城, 今犹在, 我们, 不见当年, 秦始皇。]
        System.out.println(list);
    }
}
