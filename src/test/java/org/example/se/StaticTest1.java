package org.example.se;

/**
 * static 静态代码块使用
 * 本类运行后输出结果如下：
 * <p>
 * static1：3
 * 我是 f 方法.
 * static1：1000
 * static2：4
 * static3：5
 * main：5
 * main：0
 * </p>
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/8/11 8:31
 */
public class StaticTest1 {

    private static int a;

    private int b;

    static {
        StaticTest1.a = 3;
        System.out.println("static1：" + a);
        StaticTest1 t = new StaticTest1();
        t.f();
        t.b = 1000;
        System.out.println("static1：" + t.b);
    }

    static {
        StaticTest1.a = 4;
        System.out.println("static2：" + a);
    }

    public static void main(String[] args) {
        System.out.println("main：" + StaticTest1.a);
        System.out.println("main：" + new StaticTest1().b);
    }

    static {
        StaticTest1.a = 5;
        System.out.println("static3：" + a);
    }

    public void f() {
        System.out.println("我是 f 方法.");
    }
}
