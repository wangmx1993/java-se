package org.example.se;

import java.util.ArrayList;
import java.util.List;

/**
 * final 修饰的类，不能被继承
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/8/21 9:34
 */
public final class FinalClazz {

    //final 修饰的成员变量，可以被继承，不能被修改，声明的同时初始化
    private final int size = 33;

    //先声明，然后通过构造器初始
    private final String path;

    public FinalClazz(String path) {
        this.path = path;
    }

    private static final List<String> colors;

    static {
        colors = new ArrayList<>();
        colors.add("red");
        colors.add("yellow");
        colors.add("blue");
    }

    //final 修饰的方法，可以被继承，不能被重写
    public final void show() {
        System.out.println("I am show");
    }

    public static void main(String[] args) {
         //  33
         //  a/bc/
         //  [red, yellow, blue]
         //  I am show
        System.out.println(new FinalClazz("a/bc/").size);
        System.out.println(new FinalClazz("a/bc/").path);
        System.out.println(FinalClazz.colors);
        new FinalClazz("a/bc/").show();

        // 常量是包装类型时，对象的引用不能改变，但是对象的值可以变；常量是基本类型时，值不能变.
        FinalClazz.colors.add("green");
        // [red, yellow, blue, green]
        System.out.println(FinalClazz.colors);
    }

}
