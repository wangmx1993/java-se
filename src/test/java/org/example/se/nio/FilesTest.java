package org.example.se.nio;

import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.nio.file.attribute.UserPrincipal;
import java.util.ArrayList;
import java.util.List;

/**
 * 1、public final class Files extends Object ：该类只包含对文件，目录或其他类型文件进行操作的静态方法。
 * 2、在大多数情况下，这里定义的方法将委托给相关的文件系统提供程序来执行文件操作。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/10/30 20:26
 */
public class FilesTest {

    /**
     * copy(InputStream in, Path target, CopyOption... options) 将输入流中的所有字节复制到文件
     * copy(Path source, OutputStream out) 将文件中的所有字节复制到输出流。
     * copy(Path source, Path target, CopyOption... options) 将文件复制到目标文件。
     * 文件复制
     */
    @Test
    public void copyTest1() {
        try {
            //已经存在的源文件
            File fileFrom = new File("C:\\Users\\A\\Desktop\\logs\\2020-10-30.log");
            FileInputStream inputStream = new FileInputStream(fileFrom);
            //目标文件，不存在时，自动新建
            File fileTarget = new File("C:\\Users\\A\\Desktop\\logs\\2020-10-30(backup).log");
            //文件复制：将输入流复制到目标路径
            Files.copy(inputStream, fileTarget.toPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * createDirectories(Path dir, FileAttribute<?>... attrs) 级联创建目录。
     */
    @Test
    public void createDirectoriesTest() {
        Path path = new File(FileSystemView.getFileSystemView().getHomeDirectory() + "/a/b/c").toPath();
        try {
            Files.createDirectories(path);
            System.out.println("创建目录：" + path.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * createTempDirectory(String prefix, FileAttribute<?>... attrs) 在默认临时文件目录中创建一个新目录，使用给定的前缀生成其名称。不能级联创建
     * createTempFile(String prefix, String suffix, FileAttribute<?>... attrs) 在默认临时文件目录中创建一个空文件，使用给定的前缀和后缀生成其名称。
     * createTempFile(Path dir, String prefix, String suffix, FileAttribute<?>... attrs) 在指定的目录中创建一个新的空文件，使用给定的前缀和后缀字符串生成其名称。
     */
    @Test
    public void createTempFileTest() {
        try {
            Path tempPath = Files.createTempDirectory("wmx");
            Path tempFile = Files.createTempFile(tempPath, "app", ".log");
            //C:\Users\A\AppData\Local\Temp\wmx334054469682945199
            System.out.println("创建目录：" + tempPath.toString());
            //创建文件：C:\Users\A\AppData\Local\Temp\wmx1210100186779004310\app5528144522589796024.log
            System.out.println("创建文件：" + tempFile.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * delete(Path path) 删除文件。不存在时抛出异常
     * deleteIfExists(Path path) 删除文件（如果不存在，则不影响）。
     * exists(Path path, LinkOption... options) 测试文件是否存在。
     */
    @Test
    public void getLastModifiedTimeTest() {
        try {
            Path path = new File(FileSystemView.getFileSystemView().getHomeDirectory() + "/a/b/c.log").toPath();
            Files.deleteIfExists(path);

            Path path2 = new File("E:\\git_project\\baoAn\\2321.txt").toPath();

            UserPrincipal owner = Files.getOwner(path2);
            //WANGMAOXIONG\A (User)
            System.out.println(owner);

            FileTime lastModifiedTime = Files.getLastModifiedTime(path2);
            //2020-10-17T08:18:32.376552Z
            System.out.println(lastModifiedTime);

            System.out.println(Files.isExecutable(path2));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * move(Path source, Path target, CopyOption... options) 将文件/目录移动或重命名为目标文件/目录。
     * 源文件或者目录 soutce 必须存在，目标文件或者目录不能事先存在，但其父目录必须存在.
     * 当源文件与目标文件位于同一目录下时，则是重命名，否则是移动。类似 Linux 的 mv 命令。
     */
    @Test
    public void moveTest() {
        try {
            Path pathFrom = new File("C:\\Users\\A\\Desktop\\logs\\bb").toPath();
            Path pathTarget = new File("C:\\Users\\A\\Desktop\\logs\\cc\\dd").toPath();
            Files.move(pathFrom, pathTarget);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * newBufferedReader(Path path) 打开一个文件进行阅读，返回一个 BufferedReader以高效的方式从文件读取文本。
     * newBufferedReader(Path path, Charset cs) 打开一个文件进行阅读，返回一个 BufferedReader ，可以用来以有效的方式从文件读取文本。
     */
    @Test
    public void newBufferedReaderTest() {
        BufferedReader bufferedReader = null;
        try {
            Path pathFrom = new File("C:\\Users\\A\\Desktop\\logs\\cc\\dd\\2020-10-30(backup).log").toPath();
            bufferedReader = Files.newBufferedReader(pathFrom);
            String readLine = bufferedReader.readLine();
            int count = 1;
            while (readLine != null) {
                //12020-10-30 20:33:21 [info]：你好，中国！I Love You! 1
                //22020-10-30 20:33:21 [error]： 你好，中国！I Love You!
                System.out.println((count++) + readLine);
                readLine = bufferedReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * newBufferedWriter(Path path, Charset cs, OpenOption... options) 打开或创建一个写入文件，返回一个 BufferedWriter ，可以用来以有效的方式将文本写入文件。
     * 会覆盖源文件中的内容
     */
    @Test
    public void newBufferedWriterTest() {
        BufferedWriter bufferedWriter = null;
        try {
            Path pathFrom = new File("C:\\Users\\A\\Desktop\\logs\\cc\\dd\\2020-10-30(backup).log").toPath();
            bufferedWriter = Files.newBufferedWriter(pathFrom);
            bufferedWriter.append("万里长城今犹在，");
            bufferedWriter.newLine();
            bufferedWriter.append("不见当年秦始皇。");
            System.out.println(pathFrom.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * readAllBytes(Path path) 读取文件中的所有字节。
     * readAllLines(Path path) 从文件中读取所有行。
     * readAllLines(Path path, Charset cs) 从文件中读取所有行。
     */
    @Test
    public void readAllBytesTest() {
        try {
            Path path = new File("C:\\Users\\A\\Desktop\\logs\\cc\\dd\\2020-10-30(backup).log").toPath();
            byte[] bytes = Files.readAllBytes(path);
            //万里长城今犹在，
            //不见当年秦始皇。
            System.out.println(new String(bytes));

            List<String> allLines = Files.readAllLines(path);
            int count = 1;
            for (String line : allLines) {
                //1：万里长城今犹在，
                //2：不见当年秦始皇。
                System.out.println((count++) + "：" + line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * write(Path path, byte[] bytes, OpenOption... options) 将字节写入文件。
     * write(Path path, Iterable<? extends CharSequence> lines, Charset cs, OpenOption... options) 将文本行写入文件。
     * write(Path path, Iterable<? extends CharSequence> lines, OpenOption... options) 将文本行写入文件。
     * 源文件内容会被覆盖
     */
    @Test
    public void writeTest() {
        try {
            Path path = new File("C:\\Users\\A\\Desktop\\logs\\cc\\dd\\2020-10-30(backup).log").toPath();
            List<String> lines = new ArrayList<>();
            lines.add("当年王谢堂前燕");
            lines.add("飞入寻常百姓家");
            Files.write(path, lines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
