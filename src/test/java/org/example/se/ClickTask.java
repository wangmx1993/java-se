package org.example.se;

import java.awt.*;
import java.awt.event.InputEvent;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Administrator on 2018/6/23 0023.
 * 创建模拟用户单击的定时器任务
 * 可以创建一次执行或重复执行的任务
 * TimeTask 类似 Thread 类
 */
public class ClickTask extends TimerTask {
    //用户单击计数
    private AtomicInteger atomicInteger = new AtomicInteger(0);

    //同样实现run方法
    @Override
    public void run() {
        //单数次与偶数次分别单击桌面两个不同的位置
        if (atomicInteger.addAndGet(1) % 2 == 0) {
            clickScreenByXY(100, 300);
        } else {
            clickScreenByXY(1200, 300);
        }
        System.out.println(new Date() + " click " + atomicInteger.get() + " time ,Thread name is :" + Thread.currentThread().getName());
    }

    /**
     * 模拟用户单击屏幕指定区域,默认单击屏幕最中央
     *
     * @param x：x坐标
     * @param y：y坐标
     */
    public static final void clickScreenByXY(Integer x, Integer y) {
        try {
            /**创建工具包对象*/
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            /**创建自动化对象*/
            Robot robot = new Robot();
            /**利用工具包对象获取屏幕分辨率*/
            if (x == null) {
                x = toolkit.getScreenSize().width / 2;
            }
            if (y == null) {
                y = toolkit.getScreenSize().height / 2;
            }
            /**
             * 移动鼠标到指定位置
             * 然后按下鼠标左键，再松开，模拟单击操作
             */
            robot.mouseMove(x, y);
            robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
            robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
            robot.delay(100);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // 创建定时任务
        TimerTask timerTask = new ClickTask();
        //创建定时器对象
        Timer timer = new Timer();
        /**相当于新开一个线程去执行定时器任务
         * 定时器执行定时器任务，3 秒后开始执行任务
         * 任务执行完成后，间隔 5 秒再次执行，循环往复
         */
        timer.schedule(timerTask, 3000, 5000);
    }
}