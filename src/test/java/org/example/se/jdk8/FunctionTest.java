package org.example.se.jdk8;

import cn.hutool.core.date.DateUtil;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;
import org.se.jdk8.Person;

import java.util.Date;
import java.util.List;
import java.util.function.Function;

/**
 * Java 8 函数式接口 - Function
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/11/11 14:06
 */
public class FunctionTest {

    // =================testFunctionA_1 ——> testFunctionA_4 的效果完全一样，由繁到简==========./start=============
    @Test
    public void testFunctionA_1() {
        Function<Person, String> personFunction = new Function<Person, String>() {
            @Override
            public String apply(Person person) {
                String connectionSymbol = "&";
                Date birthday = person.getBirthday();
                String yyyyMMdd = DateUtil.format(birthday, "yyyyMMdd");
                return person.getId() + connectionSymbol + person.getName() + connectionSymbol + yyyyMMdd + connectionSymbol + person.getSalary();
            }
        };
        String apply = personFunction.apply(new Person(1, "张三", new Date(), 1000f));
        // 1&张三&20241101&1000.0
        System.out.println(apply);
    }

    @Test
    public void testFunctionA_2() {
        Function<Person, String> personFunction = (Person person) -> {
            String connectionSymbol = "&";
            Date birthday = person.getBirthday();
            String yyyyMMdd = DateUtil.format(birthday, "yyyyMMdd");
            return person.getId() + connectionSymbol + person.getName() + connectionSymbol + yyyyMMdd + connectionSymbol + person.getSalary();
        };
        String apply = personFunction.apply(new Person(1, "张三", new Date(), 1000f));
        // 1&张三&20241101&1000.0
        System.out.println(apply);
    }

    @Test
    public void testFunctionA_3() {
        Function<Person, String> personFunction = person -> {
            String connectionSymbol = "&";
            Date birthday = person.getBirthday();
            String yyyyMMdd = DateUtil.format(birthday, "yyyyMMdd");
            return person.getId() + connectionSymbol + person.getName() + connectionSymbol + yyyyMMdd + connectionSymbol + person.getSalary();
        };
        String apply = personFunction.apply(new Person(1, "张三", new Date(), 1000f));
        // 1&张三&20241101&1000.0
        System.out.println(apply);
    }

    @Test
    public void testFunctionA_4() {
        Function<Person, String> personFunction = p -> p.getId() + "&" + p.getName() + "&" + DateUtil.format(p.getBirthday(), "yyyyMMdd") + "&" + p.getSalary();
        String apply = personFunction.apply(new Person(1, "张三", new Date(), 1000f));
        // 1&张三&20241101&1000.0
        System.out.println(apply);
    }
    // =================testFunctionA_1 ——> testFunctionA_4 的效果完全一样，由繁到简==========./end=============

    @Test
    public void testFunction2() {
        Function<List<String>, List<String>> process = inList -> {
            List<String> outList = Lists.newArrayList();
            if (inList != null && !inList.isEmpty()) {
                long lineNum = 1;
                for (String string : inList) {
                    outList.add((lineNum++) + ":" + string);
                }
            }
            return outList;
        };
        List<String> collect = Lists.newArrayList("张三", "李四", "王五");
        List<String> apply = process.apply(collect);
        // 1:张三
        // 2:李四
        // 3:王五
        apply.forEach(System.out::println);
    }

    @Test
    public void testFunction3() {
        Person person = new Person(1, "张三(Joni)", new Date(), 1000f);

        Function<Person, String> getNameFunction = Person::getName;
        Function<String, String> toUpperCaseFunction = String::toUpperCase;
        Function<String, String> toLowerCaseFunction = String::toLowerCase;

        // 获取大写的名字
        // andThen：先执行左侧，返回String结果，再给右侧的函数用，注意不能为null，否则NPE异常;
        Function<Person, String> getNameToUpperCase = getNameFunction.andThen(toUpperCaseFunction);
        String nameToUpperCase = getNameToUpperCase.apply(person);
        System.out.println(nameToUpperCase);// 张三(JONI)

        // 获取小写的名字
        // compose：先执行右侧，返回String结果，再给左侧侧的函数用，注意不能为null，否则NPE异常;
        Function<Person, String> getNameToLowerCase = toLowerCaseFunction.compose(getNameFunction);
        String nameToLowerCase = getNameToLowerCase.apply(person);
        System.out.println(nameToLowerCase); // 张三(joni)

        // 简写
        String nameToUpperCase2 = getNameFunction.andThen(toUpperCaseFunction).apply(person);
        String nameToLowerCase2 = toLowerCaseFunction.compose(getNameFunction).apply(person);

        System.out.println(nameToUpperCase2); // 张三(JONI)
        System.out.println(nameToLowerCase2); // 张三(joni)
    }

}
