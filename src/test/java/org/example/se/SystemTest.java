package org.example.se;

import org.junit.jupiter.api.Test;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.Scanner;

/**
 * java.lang.System 类包含一些有用的类字段和方法。它不能被实例化
 * 在 System 类提供的设施中，有标准输入、标准输出和错误输出流；对外部定义的属性和环境变量的访问；加载文件和库的方法；还有快速复制数组的一部分的实用方法。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/9/12 7:33
 */
public class SystemTest {

    /**
     * 使用键盘标准输入流(System.in)构建 Scanner 对象
     */
    @Test
    public void testScanner() {
        //使用键盘标准输入流(System.in)构建 Scanner 对象
        Scanner scan = new Scanner(System.in);
        System.out.println("nextLine 方式接收数据，# 号健退出：");
        boolean flag = true;
        while (flag) {
            //nextLine()：阻塞方法，接收用户输入的单行数据.
            //以Enter为结束符,也就是说 nextLine()方法返回的是输入回车之前的所有字符。 可以获得空白。
            String nextLine = scan.nextLine();
            System.out.println("输入数据行为：" + nextLine);
            if ("#".equals(nextLine)) {
                flag = false;
            }
        }
        scan.close();
    }

    /**
     * 当复制大量数据时，建议使用 System.arraycopy()
     * arraycopy(Object src, int srcPos, Object dest, int destPos, int length)
     * 将指定源数组中的数组从指定位置复制到目标数组的指定位置。
     * src：源数组
     * srcPos：源数组中复制的起始位置，从0开始，
     * dest：目标数组
     * destPos：目标数组中覆盖的起始位置，从0开始
     * length：复制的长度
     */
    @Test
    public void testArraycopy() {
        String[] arr1 = new String[]{"中", "大秦", "大汉"};
        String[] arr2 = new String[arr1.length];

        System.arraycopy(arr1, 0, arr2, 0, arr1.length);
        arr1[arr1.length - 1] = "大宋";
        System.out.println(Arrays.asList(arr1));//[中, 大秦, 大宋]
        System.out.println(Arrays.asList(arr2));//[中, 大秦, 大汉]
    }

    /**
     * http://127.0.0.1:8080/app/setSystemProperty?key=token&value=898989TT6767AA
     * 设置的 value 值相当于一个静态的系统全局变量,存储在内存中,任何用户都可以在应用的任何地方通过System.getProperty(key)获取
     *
     * @param key   ：系统属性 key
     * @param value ：系统属性值
     * @return
     */
    @GetMapping("app/setSystemProperty")
    public String setSystemProperty(@RequestParam String key, @RequestParam String value) {
        System.setProperty(key, value);
        return "Success";
    }

    /**
     * 获取 JVM 自带的系统属性
     */
    @Test
    public void testProperty() {
        String userName = System.getProperty("user.name");
        String userDir = System.getProperty("user.dir");
        String userHome = System.getProperty("user.home");
        String userTmpDir = System.getProperty("java.io.tmpdir");
        String javaHome = System.getProperty("java.home");

        System.out.println("userName=" + userName);//userName=Think
        System.out.println("userDir=" + userDir);//userDir=C:\wmx\project\spring-cloud-server-micros
        System.out.println("userHome=" + userHome);//userHome=C:\Users\Think
        System.out.println("userTmpDir=" + userTmpDir);//userTmpDir=C:\Users\Think\AppData\Local\Temp\
        System.out.println("javaHome=" + javaHome);//javaHome=C:\wmx\software\Java\jdk1.8.0_131\jre
    }
}
