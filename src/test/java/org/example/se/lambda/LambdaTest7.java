package org.example.se.lambda;

/**
 * 有参数、有返回值
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/3/3 16:01
 */
public class LambdaTest7 {
    public static void main(String[] args) {
        Monkey7 monkey_1 = (a, b) -> {
            Integer c = a - b;
            return a + " - " + b + " = " + c;
        };
        //当Lambda 体只有一条语句时，return 与大括号可以省略
        Monkey7 monkey_2 = (a, b) -> a + " - " + b + " = " + (a - b);

        String result_1 = monkey_1.subtraction(9527, 527);
        String result_2 = monkey_2.subtraction(9527, 527);
        //输出：9527 - 527 = 9000
        System.out.println(result_1);
        //输出：9527 - 527 = 9000
        System.out.println(result_2);
    }
}

interface Monkey7 {
    String subtraction(Integer a, Integer b);
}
