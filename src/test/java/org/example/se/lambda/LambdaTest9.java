package org.example.se.lambda;

import org.junit.Test;

import java.util.*;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/3/3 17:11
 */
public class LambdaTest9 {

    /**
     * 一般方式
     */
    @Test
    public void test1() {
        Integer[] playerScore = {89, 100, 77, 90, 86};
        // 使用匿名内部类根据 分数从低到高进行排序
        Arrays.sort(playerScore, new Comparator<Integer>() {
            /**
             *
             * @param o1 o1是后一个数，第一次比较就是100
             * @param o2 o2是前一个数，第一次比较就是89
             * @return 比较的返回值，第一次就是100.compareTo(89),返回值是1
             */
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        });
        //输出结果：[77, 86, 89, 90, 100]
        System.out.println(Arrays.asList(playerScore));
    }

    /**
     * Lambda 表达式
     */
    @Test
    public void test2() {
        Integer[] playerScore = {89, 100, 77, 90, 86};
        Arrays.sort(playerScore, (o1, o2) -> o1.compareTo(o2));
        //[77, 86, 89, 90, 100]
        System.out.println(Arrays.asList(playerScore));
    }

    /**
     * 其中 Integer::intValue（方法引用使用一对冒号 ::），就是Integer类中的方法intValue：
     */
    @Test
    public void test3() {
        Integer[] playerScore = {89, 100, 77, 90, 86};
        Arrays.sort(playerScore, Comparator.comparing(Integer::intValue));

        //[77, 86, 89, 90, 100]
        System.out.println(Arrays.asList(playerScore));

        Arrays.sort(playerScore, Comparator.comparing(Integer::intValue).reversed());
        //[100, 90, 89, 86, 77]
        System.out.println(Arrays.asList(playerScore));
    }

    /**
     * List遍历
     */
    @Test
    public void testList() {
        List<String> languages = Arrays.asList("java", "php", "python");
        //java8 之前
        for (String str : languages) {
            System.out.print(str + ",");
        }
        System.out.println();
        //java8之后
        languages.forEach(language -> System.out.print(language + ","));
    }

    /**
     * Map遍历
     */
    @Test
    public void testMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("author", "蚩尤后裔");
        map.put("age", "18");
        map.put("blog", "https://wangmaoxiong.blog.csdn.net/article/details/104439248");
        map.put("hobby", "美女");

        //java8之前的方式：方法一 在for-each循环中使用entries来遍历（常用）
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "=" + entry.getValue());
        }
        System.out.println();

        //java8之后的方式
        map.forEach((key, value) -> System.out.println(key + "=" + value));
    }


}
