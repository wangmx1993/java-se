package org.example.se.lambda;

/**
 * 无参、无返回值，Lambda 体有多条语句
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/3/3 15:57
 */
public class LambdaTest4 {
    public static void main(String[] args) {
        Monkey4 monkey = () -> {
            int count = 1000;
            System.out.println("Monkey show " + count);
        };
        //输出：Monkey show 1000
        monkey.show();
    }
}

interface Monkey4 {
    void show();
}
