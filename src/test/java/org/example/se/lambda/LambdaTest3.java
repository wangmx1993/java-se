package org.example.se.lambda;

/**
 * 无参、无返回值，Lambda 体只有一条语句
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/3/3 15:55
 */
public class LambdaTest3 {
    public static void main(String[] args) {
        Monkey3 monkey = () -> System.out.println("Monkey show...");
        //输出：Monkey show...
        monkey.show();
    }
}

interface Monkey3 {
    void show();
}
