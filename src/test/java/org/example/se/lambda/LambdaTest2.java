package org.example.se.lambda;

/**
 * 匿名内部类作为参数
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/3/3 15:47
 */
public class LambdaTest2 {

    public static void show(Monkey monkey) {
        System.out.println(" I am show ..." + monkey.print(1000));
    }

    public static void main(String[] args) {
        //传统写法.输出：I am show ...8000
        show(new Monkey() {
            @Override
            public Integer print(Integer a) {
                return a * 8;
            }
        });
        //Lambda 表达式写法。输出：I am show ...6000
        show(a -> a * 6);
    }
}

interface Monkey {
    Integer print(Integer a);
}
