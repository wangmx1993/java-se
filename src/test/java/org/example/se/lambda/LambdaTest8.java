package org.example.se.lambda;

/**
 * 参数类型可以省略，编译器会自动进行类型判断
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/3/3 16:02
 */
public class LambdaTest8 {
    public static void main(String[] args) {
        Monkey8 monkey_1 = (Integer a) -> (a * 100);
        Monkey8 monkey_2 = (a) -> (a * 200);
        //输出 800
        System.out.println(monkey_1.print(8));
        //输出 1200
        System.out.println(monkey_2.print(6));
    }
}

interface Monkey8 {
    Integer print(Integer a);
}
