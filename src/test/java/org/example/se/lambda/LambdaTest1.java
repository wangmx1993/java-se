package org.example.se.lambda;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 匿名内部类 Lambda 表达式新旧写法
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/3/3 15:46
 */
public class LambdaTest1 {

    public static void main(String[] args) {
        //传统写法
        Runnable runnable_1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("我是子线程 " + Thread.currentThread().getName());
            }
        };
        //Lambda 写法
        Runnable runnable_2 = () -> System.out.println("I am a sub-thread " + Thread.currentThread().getName());

        //执行任务
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(runnable_1);//输出：我是子线程 pool-1-thread-1
        executorService.execute(runnable_2);//输出：I am a sub-thread pool-1-thread-2
    }
}