package org.example.se.lambda;

/**
 * 1个参数、无返回值，Lambda 体只有一条语句
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/3/3 15:58
 */
public class LambdaTest5 {
    public static void main(String[] args) {
        //1个参数时，-> 左侧的 "()" 可以省略
        Monkey5 monkey1 = (info) -> System.out.println("Monkey " + info);
        Monkey5 monkey2 = info -> System.out.println("Monkey " + info);

        //输出：Monkey 美猴王
        monkey1.show("美猴王");
        //Monkey 六阿弥猴
        monkey2.show("六阿弥猴");
    }
}

interface Monkey5 {
    void show(String info);
}
