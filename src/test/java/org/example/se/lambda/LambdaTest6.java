package org.example.se.lambda;

/**
 * 多个参数、无返回值，Lambda 体有多条语句
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/3/3 15:59
 */
public class LambdaTest6 {
    public static void main(String[] args) {
        Monkey6 monkey = (id, info) -> {
            id = id == null ? 0 : id;
            System.out.println("Monkey " + id + " " + info);
        };
        //输出：Monkey 9527 六阿弥猴
        monkey.show(9527, "六阿弥猴");
    }
}

interface Monkey6 {
    void show(Integer id, String info);
}
