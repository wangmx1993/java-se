package org.example.se.threadLocal;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * ThreadLocal(线程局部变量) 依托于线程的生命周期而存在，贯穿于整个线程，解决了线程前后值传递的问题。
 * 为了更好的避免使用 ThreadLocal 发生内存泄露，使用 ThreadLocal 时遵守以下两个小原则:
 * 1、ThreadLocal申明为 private static final
 * * Private与final 尽可能不让他人修改变更引用，
 * * Static 表示为类属性，只有在程序结束才会被回收。
 * 2、ThreadLocal 使用后务必调用 remove 方法
 * * 最简单有效的方法是使用后将其移除。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/9/12 8:45
 */
public class ThreadLocalTest {

    private static final AtomicInteger atomicInteger = new AtomicInteger();
    /**
     * ThreadLocal 申明为 private static final
     * * Private 与 final 尽可能不让他人修改变更引用，
     * * Static 表示为类属性，只有在程序结束才会被回收。
     */
    private static final ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>() {
        @Override
        protected Integer initialValue() {
            return atomicInteger.getAndIncrement();
        }
    };

    /**
     * @param args
     */
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        for (int j = 0; j < 5; j++) {
                            System.out.println(Thread.currentThread() + ":" + threadLocal.get());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        //ThreadLocal 使用后务必调用 remove 方法进行移除，防止内存泄露.
                        threadLocal.remove();
                    }
                }
            }).start();
        }
    }
}
