package org.example.se.concurrent;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * Semaphore 计数信号量限制资源访问的线程数目
 * Semaphore 只对可用许可的号码进行计数，并采取相应的行动，信号量常常用于多线程的代码中，限制访问某些资源（物理或逻辑的）的线程数目，比如数据库连接池。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/9/19 16:26
 */
public class SemaphoreTest {

    public static void main(String[] args) {
        //创建具有给定的许可数和非公平的公平设置的 Semaphore（计数信号量）
        Semaphore semaphore = new Semaphore(3);
        //使用线程池创建多个线程执行
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 6; i++) {
            executorService.execute(new MyThread4(semaphore));//计数信号量作为参数传入
        }
    }
}

class MyThread4 extends Thread {
    private Semaphore semaphore;//计数信号量作为参数传入

    public MyThread4(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            System.out.println("线程 " + currentThread().getName() + " 准备获取许可证数，当前共有：" + semaphore.availablePermits());
            //acquire：从此计数信号量中获取一个许可证，在获得许可之前，此线程将一直阻塞，或者线程被中断。
            //获取许可证后，semaphore 中可用的许可数减会 1
            semaphore.acquire();
            System.out.println("线程 " + currentThread().getName() + " ···获得许可证,剩余许可证数：" + semaphore.availablePermits());
            Thread.sleep(2000 + new Random().nextInt(3000));//模拟线程在执行任务
            // release：当前线程释放许可证，将其返回给计数信号量
            // 释放许可证后，semaphore 中可用的许可数增加 1。如果任意线程试图获取许可，则选中一个线程并将刚刚释放的许可给予它。然后针对线程安排目的启用（或再启用）该线程。
            // 线程执行完成后一定要记得归还许可证，否则 Semaphore 的许可证用完之后，其它线程会一直阻塞在 acquire()
            semaphore.release();
            System.out.println("线程 " + currentThread().getName() + " ·········执行完毕，归还许可证,剩余许可证数：" + semaphore.availablePermits());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
//---------------------控制台输出示例-------------------
//线程 pool-1-thread-2 准备获取许可证数，当前共有：3
//线程 pool-1-thread-5 准备获取许可证数，当前共有：3
//线程 pool-1-thread-1 准备获取许可证数，当前共有：3
//线程 pool-1-thread-1 ···获得许可证,剩余许可证数：0
//线程 pool-1-thread-4 准备获取许可证数，当前共有：3
//线程 pool-1-thread-3 准备获取许可证数，当前共有：3
//线程 pool-1-thread-5 ···获得许可证,剩余许可证数：1
//线程 pool-1-thread-6 准备获取许可证数，当前共有：2
//线程 pool-1-thread-2 ···获得许可证,剩余许可证数：2
//线程 pool-1-thread-5 ·········执行完毕，归还许可证,剩余许可证数：0
//线程 pool-1-thread-4 ···获得许可证,剩余许可证数：0
//线程 pool-1-thread-1 ·········执行完毕，归还许可证,剩余许可证数：1
//线程 pool-1-thread-3 ···获得许可证,剩余许可证数：0
//线程 pool-1-thread-2 ·········执行完毕，归还许可证,剩余许可证数：1
//线程 pool-1-thread-6 ···获得许可证,剩余许可证数：0
//线程 pool-1-thread-4 ·········执行完毕，归还许可证,剩余许可证数：1
//线程 pool-1-thread-3 ·········执行完毕，归还许可证,剩余许可证数：2
//线程 pool-1-thread-6 ·········执行完毕，归还许可证,剩余许可证数：3