package org.example.se.concurrent;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Exchanger;

/**
 * Exchanger,俗称交换器,用于在线程之间交换数据,但是比较受限,因为只能两个线程之间交换数据
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/9/19 16:38
 */
public class ExchangeTest {
    public static void main(String[] args) {
        Exchanger<Set<String>> exchanger = new Exchanger<>();

        new Thread(() -> {
            Thread.currentThread().setName("线程1");
            Set<String> aSet = new HashSet<>();
            aSet.add("大唐");
            aSet.add("大宋");
            aSet.add("大明");
            try {
                Set<String> changeB = exchanger.exchange(aSet);
                for (String s : changeB) {
                    Thread.sleep(500 + new Random().nextInt(3000));//模拟线程在执行任务
                    System.out.println(Thread.currentThread().getName() + "：" + s);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {
            Thread.currentThread().setName("线程2");
            Set<String> bSet = new HashSet<>();
            bSet.add("1");
            bSet.add("2");
            bSet.add("3");
            try {
                Set<String> changeA = exchanger.exchange(bSet);
                for (String s : changeA) {
                    Thread.sleep(500 + new Random().nextInt(3000));//模拟线程在执行任务
                    System.out.println(Thread.currentThread().getName() + "：" + s);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}


//-----------------------控制台输出示例---------------------
//线程1：1
//线程2：大唐
//线程1：2
//线程2：大明
//线程2：大宋
//线程1：3