package org.example.se.concurrent;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * CountDownLatch 倒计数锁存器
 * 1、java.util.concurrent.CountDownLatch 是一个同步的辅助类，允许一个或多个线程，等待其他一组线程完成操作，再继续执行。可以理解为倒计数锁
 * 2、再比如：主线程中开4个线程A、B、C、D去执行任务，分别是计算上一年1、2、3、4四个季度的某某数据，主线程一直等待到 A、B、C、D 执行完毕(数据计算完毕)后再进行数据汇总。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/9/19 15:14
 */
public class CountDownLatchTest {
    public static void main(String[] args) {
        try {
            int threadCount = 5;
            //注意倒计数锁存器等待线程的个数要与实际一致
            CountDownLatch countDownLatch = new CountDownLatch(threadCount);
            ExecutorService executorService = Executors.newCachedThreadPool();//无界线程池
            for (int i = 0; i < threadCount; i++) {
                //假如实际开的线程小于 CountDownLatch 构造器中的值，那么主线程会永远休眠
                //假如实际开的线程大于 CountDownLatch 构造器中的值，那么当锁存器计数减到 0 时，主线程会提交运行
                executorService.execute(new MyThread(countDownLatch, i + ""));
            }
            System.out.println("主线程开始等待两个子线程执行任务...");
            countDownLatch.await();//主线程阻塞等待
            System.out.println("两个子线程任务全部执行完毕，主线程开始关闭线程池...");
            executorService.shutdown();//关闭线程池
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

/**
 * 线程 任务
 */
class MyThread extends Thread {

    private CountDownLatch countDownLatch;//倒计数锁存器
    private String track;//赛道

    public MyThread(CountDownLatch countDownLatch, String track) {
        this.countDownLatch = countDownLatch;
        this.track = track;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(500 + new Random().nextInt(1000));//模拟线程在执行任务
            System.out.println(Thread.currentThread().getName() + "：" + track + " 道已就位.......");
            Thread.sleep(1000 + new Random().nextInt(3000));//模拟线程在执行任务
            System.out.println(Thread.currentThread().getName() + "："+ track + " 道已完成");
            //线程执行完毕，递减锁存器的计数，即 CountDownLatch 总数减1
            //如果忘了计数递减，那么主线程可能会因为计数器迟迟不到0而一致等待
            countDownLatch.countDown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

//----------------------控制台输出内容示例----------------------
//主线程开始等待两个子线程执行任务...
//pool-1-thread-1：0 道已就位.......
//pool-1-thread-3：2 道已就位.......
//pool-1-thread-5：4 道已就位.......
//pool-1-thread-4：3 道已就位.......
//pool-1-thread-2：1 道已就位.......
//pool-1-thread-5：4 道已完成
//pool-1-thread-4：3 道已完成
//pool-1-thread-2：1 道已完成
//pool-1-thread-1：0 道已完成
//pool-1-thread-3：2 道已完成
//两个子线程任务全部执行完毕，主线程开始关闭线程池...