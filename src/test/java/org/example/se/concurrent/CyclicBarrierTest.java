package org.example.se.concurrent;

import java.util.Random;
import java.util.concurrent.*;

/**
 * CyclicBarrier 循环栅栏锁
 * 1、java.util.concurrent.CyclicBarrier 是一个同步辅助类，允许一组线程相互之间等待，达到一个共同点，再继续执行。可以理解为循环栅栏锁
 * 2、相比与 CountDownLatch，CyclicBarrier 有一个 reset() 方法可以将将屏障重置为其初始状态，即恢复构造器中最开始设置的个数值
 * 3、比如：8个运动员(线程)参加赛跑，必须得所有人都准备好了，才能开始比赛。再比如：游戏一方5个人(代表5个线程)组成团队赛，必须大家都准备好了，比赛才能开始
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/9/19 15:27
 */
public class CyclicBarrierTest {
    public static void main(String[] args) {
        int count = 5;
        //与 CountDownLatch 一样，初始化将来要同步的线程数要与实际的相等
        CyclicBarrier cyclicBarrier = new CyclicBarrier(count);
        ExecutorService executorService = Executors.newFixedThreadPool(count);
        System.out.println("主线程调用开始...");
        for (int i = 0; i < count; i++) {
            executorService.execute(new MyThread2(cyclicBarrier, i + ""));
        }
        System.out.println("主线程调用完毕...");
    }
}

class MyThread2 implements Runnable {
    private CyclicBarrier cyclicBarrier;
    private String id;

    /**
     * 参数传入
     */
    public MyThread2(CyclicBarrier cyclicBarrier, String id) {
        this.cyclicBarrier = cyclicBarrier;
        this.id = id;
    }

    @Override
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(1 + new Random().nextInt(3));
            System.out.println(Thread.currentThread().getName() + "：玩家 " + id + " 加入游戏,开始等待其它玩家...");
            //到达栅栏时，等待其它线程，一直到调用 await 方法的线程个数达到 new CyclicBarrier(3) 初始化的个数后
            //所有的线程才会接着向后运行，否则一直阻塞在这里
            cyclicBarrier.await();
            System.out.println(Thread.currentThread().getName() + "：所有玩家已准备完毕,开始游戏...");
            TimeUnit.SECONDS.sleep(1 + new Random().nextInt(3));
            System.out.println(Thread.currentThread().getName() + "玩家 " + id + " 正在大杀特杀...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

//-----------------------------控制台输出示例----------------------------
//主线程调用开始...
//主线程调用完毕...
//pool-1-thread-3：玩家 2 加入游戏,开始等待其它玩家...
//pool-1-thread-2：玩家 1 加入游戏,开始等待其它玩家...
//pool-1-thread-4：玩家 3 加入游戏,开始等待其它玩家...
//pool-1-thread-1：玩家 0 加入游戏,开始等待其它玩家...
//pool-1-thread-5：玩家 4 加入游戏,开始等待其它玩家...
//pool-1-thread-5：所有玩家已准备完毕,开始游戏...
//pool-1-thread-3：所有玩家已准备完毕,开始游戏...
//pool-1-thread-4：所有玩家已准备完毕,开始游戏...
//pool-1-thread-2：所有玩家已准备完毕,开始游戏...
//pool-1-thread-1：所有玩家已准备完毕,开始游戏...
//pool-1-thread-1玩家 0 正在大杀特杀...
//pool-1-thread-3玩家 2 正在大杀特杀...
//pool-1-thread-4玩家 3 正在大杀特杀...
//pool-1-thread-5玩家 4 正在大杀特杀...
//pool-1-thread-2玩家 1 正在大杀特杀...