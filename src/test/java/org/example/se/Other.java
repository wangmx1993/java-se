package org.example.se;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/8/21 9:29
 */
public class Other {

    @Test
    public void testAge() {
        DateTime birthday = DateUtil.parse("2024-01-27 01:11:00");
        DateTime nowDay = DateUtil.date();
        long betweenDay = DateUtil.betweenDay(birthday, nowDay, true);
        System.out.println("出生日期：" + birthday);
        System.out.println("当前日期：" + nowDay);
        System.out.println("成长天数：" + betweenDay);
    }

    /**
     * 1、将一类对象的共同特征总结出来构造类/接口的过程叫抽象
     * 2、提供继承信息的类被称为父类（超类、基类），同理可以设计成抽象类或者接口
     * 3、从已有类(接口)得到继承信息创建新类(接口)的过程叫继承，得到继承信息的类被称为子类（派生类）
     * 4、多态两个要点：方法重写/实现，父类的引用指向子类的生成。
     */
    @Test
    public void testPolymorphic() {
        int[] batchExcuteSql = new int[]{0};
        ArrayList<int[]> arrayList = ListUtil.toList(batchExcuteSql);
        List<int[]> list = ListUtil.of(batchExcuteSql);
        System.out.println(list);
        System.out.println(arrayList);
    }

    /**
     * JarFile 读取 Jar 包中的文件信息
     *
     * @throws IOException
     */
    @Test
    public void testJarFile1() throws IOException {
        String jarFilePath = "D:\\project\\IDEA_project\\java-se\\target\\java-se-1.0-SNAPSHOT.jar";
        JarFile jarFile = null;
        try {
            jarFile = new JarFile(jarFilePath);
            Enumeration<JarEntry> entries = jarFile.entries();
            while (entries.hasMoreElements()) {
                JarEntry entry = entries.nextElement();
                if (!entry.isDirectory()) {
                    String filePath = entry.getName();
                    long size = entry.getSize();
                    long time = entry.getTime();
                    // BOOT-INF/classes/org/example/servlet/MyMvcConfig.class size=2115 2024-08-09 10:48:48
                    // BOOT-INF/classes/static/cors/cors_jsonp.html size=1638 2021-06-05 17:02:16
                    // BOOT-INF/lib/spring-boot-starter-validation-2.3.5.RELEASE.jar size=4758 2020-10-29 17:19:08
                    System.out.println(filePath + " size=" + size + " " + DateUtil.date(time));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jarFile != null) {
                jarFile.close();
            }
        }
    }



}
