package org.example.se.chain;

import java.io.Serializable;
import java.util.Date;

/**
 * 链式编程-方式 1
 * 温馨提示：如果链式编程的参数过多，建议使用 builder 模式
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/11/12 下午2:14
 * @see java.lang.StringBuffer
 * @see java.lang.StringBuilder
 */
public class Chain1Bean implements Serializable {
    private Integer id;
    private String name;
    private Date birthday;
    private Float salary;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Float getSalary() {
        return salary;
    }

    public Chain1Bean setId(Integer id) {
        this.id = id;
        return this;
    }

    public Chain1Bean setName(String name) {
        this.name = name;
        return this;
    }

    public Chain1Bean setBirthday(Date birthday) {
        this.birthday = birthday;
        return this;
    }

    public Chain1Bean setSalary(Float salary) {
        this.salary = salary;
        return this;
    }

    public static Chain1Bean create() {
        return new Chain1Bean();
    }

    @Override
    public String toString() {
        return "PersonBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", salary=" + salary +
                '}';
    }
}

