package org.example.se.chain;

import org.junit.jupiter.api.Test;

import java.util.Date;

/**
 * {@link Chain1Bean} 链式编程 测试类
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/11/12 下午2:18
 */
public class ChainTest {

    /**
     * {@link Chain1Bean} 链式编程 测试类
     */
    @Test
    public void testChain1Bean() {
        Chain1Bean chain1Bean1 = Chain1Bean.create().setId(1).setName("张三").setBirthday(new Date(2024, 11, 12)).setSalary(10000.0f);
        // PersonBean{id=1, name='张三', birthday=Fri Dec 12 00:00:00 CST 3924, salary=10000.0}
        System.out.println(chain1Bean1);

        Chain1Bean chain1Bean2 = Chain1Bean.create().setId(2).setName("李四").setBirthday(new Date(2024, 10, 12)).setSalary(20000.0f);
        // PersonBean{id=2, name='李四', birthday=Wed Nov 12 00:00:00 CST 3924, salary=20000.0}
        System.out.println(chain1Bean2);
    }

    @Test
    public void testChain2Bean() {
        Chain2Bean chain2Bean1 = Chain2Bean.builder().id(1).name("张三").salary(10000.0f).birthday(new Date(2024, 11, 12)).build();
        // PersonBean{id=1, name='张三', birthday=Fri Dec 12 00:00:00 CST 3924, salary=10000.0}
        System.out.println(chain2Bean1);

        Chain2Bean chain2Bean2 = Chain2Bean.builder().id(2).name("李四").salary(20000.0f).birthday(new Date(2024, 10, 12)).build();
        // PersonBean{id=2, name='李四', birthday=Wed Nov 12 00:00:00 CST 3924, salary=20000.0}
        System.out.println(chain2Bean2);
    }

    @Test
    public void testChain3Bean() {
        Chain3Bean chain3Bean1 = Chain3Bean.builder().id(1).name("张三").salary(10000.0f).birthday(new Date(2024, 11, 12)).build();
        // PersonBean{id=1, name='张三', birthday=Fri Dec 12 00:00:00 CST 3924, salary=10000.0}
        System.out.println(chain3Bean1);

        Chain3Bean chain3Bean2 = Chain3Bean.builder().id(2).name("李四").salary(20000.0f).birthday(new Date(2024, 10, 12)).build();
        // PersonBean{id=2, name='李四', birthday=Wed Nov 12 00:00:00 CST 3924, salary=20000.0}
        System.out.println(chain3Bean2);

    }


    @Test
    public void testChain4Bean() {
        Chain4Bean chain4Bean1 = Chain4Bean.builder().id(1).name("张三").salary(10000.0f).birthday(new Date(2024, 11, 12)).build();
        // PersonBean{id=1, name='张三', birthday=Fri Dec 12 00:00:00 CST 3924, salary=10000.0}
        System.out.println(chain4Bean1);

        Chain4Bean chain4Bean2 = Chain4Bean.builder().id(2).name("李四").salary(20000.0f).birthday(new Date(2024, 10, 12)).build();
        // PersonBean{id=2, name='李四', birthday=Wed Nov 12 00:00:00 CST 3924, salary=20000.0}
        System.out.println(chain4Bean2);

    }

    @Test
    public void testChain5Bean() {
        Chain5Bean chain5Bean1 = Chain5Bean.builder().id(1).name("张三").salary(10000.0f).birthday(new Date(2024, 11, 12)).build();
        // Chain5Bean(id=1, name=张三, birthday=Fri Dec 12 00:00:00 CST 3924, salary=10000.0)
        System.out.println(chain5Bean1);

        Chain5Bean chain5Bean2 = Chain5Bean.builder().id(2).name("李四").salary(20000.0f).birthday(new Date(2024, 10, 12)).build();
        // Chain5Bean(id=2, name=李四, birthday=Wed Nov 12 00:00:00 CST 3924, salary=20000.0)
        System.out.println(chain5Bean2);
    }

    /**
     * Java 8 带来了函数式接口编程，可以借助它进行灵活的构建对象，可以参考 {@link cn.hutool.core.builder}
     * 任何带有默认构造器和 set 方法的类都可以使用这个通用的 Builder 模式了
     */
    @Test
    public void testChin6() {
        Chain1Bean chain1Bean = cn.hutool.core.builder.GenericBuilder.of(Chain1Bean::new)
                .with(Chain1Bean::setId, 1)
                .with(Chain1Bean::setName, "张三")
                .with(Chain1Bean::setSalary, 10000.0f)
                .with(Chain1Bean::setBirthday, new Date(2024, 11, 12))
                .build();
        // PersonBean{id=1, name='张三', birthday=Fri Dec 12 00:00:00 CST 3924, salary=10000.0}
        System.out.println(chain1Bean);
    }

}
