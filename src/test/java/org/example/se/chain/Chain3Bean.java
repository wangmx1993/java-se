package org.example.se.chain;

import java.io.Serializable;
import java.util.Date;

/**
 * 链式编程-方式 3 - 使用 Builder 模式
 * 温馨提示：如果想要限制创建好的对象后期无法修改属性，则可以不对外提供 JavaBean 属性的 setter 方法，或者是设置为 private 私有。
 * 相反如果后期想要修改对象的属性值，则可以对外提供 JavaBean 属性的 setter 方法。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/11/12 下午2:14
 * @see cn.hutool.core.net.url.UrlBuilder
 * @see cn.hutool.db.sql.SqlBuilder
 * @see org.springframework.boot.builder.SpringApplicationBuilder
 */
public class Chain3Bean implements Serializable {
    private final Integer id;
    private final String name;
    private final Date birthday;
    private final Float salary;


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Float getSalary() {
        return salary;
    }

    private Chain3Bean(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.birthday = builder.birthday;
        this.salary = builder.salary;
    }

    @Override
    public String toString() {
        return "PersonBean{" + "id=" + id + ", name='" + name + '\'' + ", birthday=" + birthday + ", salary=" + salary + '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String name;
        private Date birthday;
        private Float salary;

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder birthday(Date birthday) {
            this.birthday = birthday;
            return this;
        }

        public Builder salary(Float salary) {
            this.salary = salary;
            return this;
        }

        public Chain3Bean build() {
            return new Chain3Bean(this);
        }

    }
}

