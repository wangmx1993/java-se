package org.example.se.chain;


import java.io.Serializable;
import java.util.Date;

/**
 * 链式编程-方式 2 - 使用 Builder 模式
 * 温馨提示：如果想要限制创建好的对象后期无法修改属性，则可以不对外提供 JavaBean 属性的 setter 方法，或者是设置为 private 私有。
 * 相反如果后期想要修改对象的属性值，则可以对外提供 JavaBean 属性的 setter 方法。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/11/12 下午2:14
 * @see cn.hutool.core.net.url.UrlBuilder
 * @see cn.hutool.db.sql.SqlBuilder
 * @see org.springframework.boot.builder.SpringApplicationBuilder
 */
public class Chain2Bean implements Serializable {
    private Integer id;
    private String name;
    private Date birthday;
    private Float salary;

    private Chain2Bean() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "PersonBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", salary=" + salary +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String name;
        private Date birthday;
        private Float salary;

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder birthday(Date birthday) {
            this.birthday = birthday;
            return this;
        }

        public Builder salary(Float salary) {
            this.salary = salary;
            return this;
        }

        public Chain2Bean build() {
            Chain2Bean chain2Bean = new Chain2Bean();
            chain2Bean.setId(id);
            chain2Bean.setName(name);
            chain2Bean.setBirthday(birthday);
            chain2Bean.setSalary(salary);
            return chain2Bean;
        }
    }
}

