package org.example.se.chain;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 链式编程-方式 5 ：使用 lombok 的 @Builder 注解
 * 温馨提示：如果想要限制创建好的对象后期无法修改属性，则可以不对外提供 JavaBean 属性的 setter 方法，或者是设置为 private 私有。
 * 相反如果后期想要修改对象的属性值，则可以对外提供 JavaBean 属性的 setter 方法。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/11/12 下午2:14
 */
@Builder
@ToString
@Getter
public class Chain5Bean implements Serializable {
    private Integer id;
    private String name;
    private Date birthday;
    private Float salary;

}

