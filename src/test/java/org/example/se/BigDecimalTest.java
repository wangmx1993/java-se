package org.example.se;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

/**
 * BigDecimal 不可变任意精度小数
 * BigDecimal 所有方法操作后，都不会改变原对象的值，只会返回新的结果值
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/7/12 9:38
 */
@SuppressWarnings("all")
public class BigDecimalTest {

    /**
     * 演示金额计算时为什么不能使用浮点数 fouat 与 double，而应该使用 BigDecimal
     * 1、比如预期应该是 0.1+0.2=0.3，9.9F*100.0F=990.00F
     */
    @Test
    public void bigDecimalTest1() {
        double d1 = 0.1;
        double d2 = 0.2;
        // 0.30000000000000004  ——> 显示这样显示是不对的.
        System.out.println(d1 + d2);

        float f1 = 9.9F;
        float f2 = 100.0F;
        // 989.99994 ——理论上应该等于 990 才是对的.
        System.out.println(f1 * f2);

        // 加法
        BigDecimal d3 = BigDecimal.valueOf(d1).add(BigDecimal.valueOf(d2));
        // 乘法
        BigDecimal multiply = BigDecimal.valueOf(f1).multiply(BigDecimal.valueOf(f2));
        // 四舍五入保留2位小数
        BigDecimal f3 = multiply.setScale(2, RoundingMode.HALF_UP);
        // 0.3
        System.out.println(d3);
        // 990.00
        System.out.println(f3);
    }

    /**
     * 演示 BigDecimal 常量
     * <p>
     * static BigDecimal 	ZERO 值为0，标度为0。
     * static BigDecimal 	ONE 值1，标度为0。
     * static BigDecimal 	TEN 值为10，标度为0。
     * static int	ROUND_CEILING 圆形模式向正无穷大转弯。
     * static int	ROUND_DOWN 舍入模式向零舍入。
     * static int	ROUND_FLOOR 舍入模式向负无穷大转弯。
     * static int	ROUND_HALF_DOWN 四舍五入模式向“最近邻居”转弯，除非这两个邻居都是等距离的，在这种情况下，这是倒圆的。
     * static int	ROUND_HALF_EVEN 四舍五入模式向“最近邻居”转弯，除非两个邻居都是等距离的，在这种情况下，向着邻居方向转移。
     * static int	ROUND_HALF_UP 四舍五入模式向“最近邻居”转弯，除非两个邻居都是等距的，在这种情况下是圆括弧的。
     * static int	ROUND_UNNECESSARY 舍入模式来确定所请求的操作具有精确的结果，因此不需要舍入。
     * static int	ROUND_UP 舍入模式从零开始。
     * ​
     */
    @Test
    public void bigDecimalTest2() {
        BigDecimal zero = BigDecimal.ZERO;
        BigDecimal one = BigDecimal.ONE;
        BigDecimal ten = BigDecimal.TEN;
        // 0、1、10
        System.out.printf("%s、%s、%s %n", zero.longValue(), one.longValue(), ten);
        System.out.println();

        // ROUND_UP=0
        // ROUND_DOWN=1
        // ROUND_CEILING=2
        // ROUND_FLOOR=3
        // ROUND_HALF_UP=4
        // ROUND_HALF_DOWN=5
        // ROUND_HALF_EVEN=6
        // ROUND_UNNECESSARY=7
        System.out.println("ROUND_UP=" + BigDecimal.ROUND_UP);
        System.out.println("ROUND_DOWN=" + BigDecimal.ROUND_DOWN);
        System.out.println("ROUND_CEILING=" + BigDecimal.ROUND_CEILING);
        System.out.println("ROUND_FLOOR=" + BigDecimal.ROUND_FLOOR);
        System.out.println("ROUND_HALF_UP=" + BigDecimal.ROUND_HALF_UP);
        System.out.println("ROUND_HALF_DOWN=" + BigDecimal.ROUND_HALF_DOWN);
        System.out.println("ROUND_HALF_EVEN=" + BigDecimal.ROUND_HALF_EVEN);
        System.out.println("ROUND_UNNECESSARY=" + BigDecimal.ROUND_UNNECESSARY);
    }

    /**
     * 演示 BigDecimal 构造器
     * BigDecimal(double val) 不推荐方式
     * BigDecimal valueOf(double val) 不推荐方式
     * * 1、flout 与 double 类型不推荐使用构造器方式，因为结果是不可预测的
     * * 2、flout 类型也不推荐使用 valueOf(double val) 方法，应用精度提升也会导致结果不可预测.
     * * 3、double 类型可以使用 valueOf(double val) 方法，flout 不推荐。
     * BigDecimal(String val) 推荐方式
     */
    @Test
    public void bigDecimalTest3() {
        // 以下方式不推荐
        BigDecimal bigDecimal3 = new BigDecimal(3.14F);
        BigDecimal bigDecimal4 = new BigDecimal(3.14159);
        BigDecimal bigDecimal5 = BigDecimal.valueOf(3.14F);
        // bigDecimal3=3.1400001049041748046875
        System.out.println("bigDecimal3=" + bigDecimal3);
        // bigDecimal4=3.14158999999999988261834005243144929409027099609375
        System.out.println("bigDecimal4=" + bigDecimal4);
        System.out.println("bigDecimal5=" + bigDecimal5);//bigDecimal5=3.140000104904175

        // 以下方式推荐
        // 1、double 类型可以使用 valueOf(double val) 方法，flout 不推荐
        // 2、最推荐的方式是一律转成 String 类型操作
        BigDecimal bigDecimal7 = new BigDecimal(String.valueOf(3.14F));
        BigDecimal bigDecimal8 = new BigDecimal(String.valueOf(3.14159));

        // bigDecimal6=3.14159
        System.out.println("bigDecimal6=" + BigDecimal.valueOf(3.14159));
        // bigDecimal7=3.14
        System.out.println("bigDecimal7=" + bigDecimal7);
        // bigDecimal8=3.14159
        System.out.println("bigDecimal8=" + bigDecimal8);
    }

    /**
     * 演示 加减乘除运算、求余数
     * 1、BigDecimal 所有方法操作后，都不会改变原对象的值，只会返回新的结果值。
     * 2、除法运算时必须指定舍入模式，否则会抛异常，否则一旦无法整除就会报错：
     * * java.lang.ArithmeticException: Non-terminating decimal expansion; no exact representable decimal result.
     * * BigDecimal divide(BigDecimal divisor, int scale, RoundingMode roundingMode)
     */
    @Test
    public void bigDecimalTest4() {
        BigDecimal bigDecimal = new BigDecimal("-3.14159");
        BigDecimal abs = bigDecimal.abs();//求绝对值
        BigDecimal add = bigDecimal.add(BigDecimal.valueOf(300.45));//加法
        BigDecimal subtract = bigDecimal.subtract(BigDecimal.valueOf(-1000.14159));//减法
        BigDecimal multiply = bigDecimal.multiply(new BigDecimal("-202"));//乘法
        // 除法，当结果有可能为无穷无尽的小数时，必须指定舍入模式，否则会抛异常。HALF_UP是四舍五入，同时保留 4 位小数
        BigDecimal divide = bigDecimal.divide(new BigDecimal("-0.2025"), 4, RoundingMode.HALF_UP);
        BigDecimal remainder = bigDecimal.remainder(BigDecimal.valueOf(3));//求余数

        System.out.println("bigDecimal=" + bigDecimal);//bigDecimal=-3.14159
        System.out.println("abs=" + abs);//3.14159
        System.out.println("add=" + add);//297.30841
        System.out.println("subtract=" + subtract);//997.00000
        System.out.println("multiply=" + multiply);//634.60118
        System.out.println("divide=" + divide);//15.51402
        System.out.println("remainder=" + remainder);//-0.14159

        // 下面因为无法整除就会报错：java.lang.ArithmeticException: Non-terminating decimal expansion; no exact representable decimal result.
        System.out.println(new BigDecimal(10).divide(new BigDecimal("3")));
    }

    /**
     * 演示  求绝对值、最大/小值、相反数、判断正负数、指数运算
     */
    @Test
    public void bigDecimalTest5() {
        BigDecimal bigDecimal = BigDecimal.valueOf(3.1415926);
        BigDecimal negate = bigDecimal.negate();//求自己的负数
        BigDecimal abs = negate.abs();//求绝对值
        int signum = negate.signum();//正数返回1、负数返回-1，0返回0
        int signum1 = abs.signum();
        BigDecimal max = bigDecimal.max(BigDecimal.TEN);//求最大值
        BigDecimal min = bigDecimal.min(BigDecimal.ZERO);//求最小值
        BigDecimal pow = BigDecimal.valueOf(2).pow(10);//指数运算

        System.out.println("bigDecimal=" + bigDecimal);//bigDecimal=3.1415926
        System.out.println("negate=" + negate);//-3.1415926
        System.out.println("abs=" + abs);//3.1415926
        System.out.println(signum + "," + signum1 + "," + BigDecimal.valueOf(0.00).signum());//-1,1,0
        System.out.println("max=" + max);//10
        System.out.println("min=" + min);//0
        System.out.println(pow);//1024
    }

    /**
     * 演示 大小比较,是否相等
     */
    @Test
    public void testCompareTo() {
        // 0
        System.out.println(BigDecimal.valueOf(0).compareTo(BigDecimal.ZERO));
        // 1
        System.out.println(BigDecimal.valueOf(10).compareTo(BigDecimal.ZERO));
        // -1
        System.out.println(BigDecimal.valueOf(-10).compareTo(BigDecimal.ZERO));
        // true
        System.out.println(new BigDecimal("10").equals(BigDecimal.TEN));
        // true
        System.out.println(BigDecimal.valueOf(10).equals(BigDecimal.TEN));
        // false
        System.out.println(BigDecimal.valueOf(10).equals(BigDecimal.ONE));
    }

    /**
     * 演示设置小数位数与舍入模式
     * 1、只有除法运算可以直接设置结果保留的小数位数，以及舍入模式，其它操作需要使用 setScale 进行设置
     * 2、设置小数位数与舍入模式是非常有必要的
     * BigDecimal setScale(int newScale, RoundingMode roundingMode)：
     * * newScale：保留的小数位数，0 表示不保留小数。
     * * roundingMode：舍入模式，比如四舍五入，向上舍入，向下舍入等等.
     */
    @Test
    public void bigDecimalTest7() {
        // 假设为某人的薪水
        BigDecimal salary = new BigDecimal("12898.9988");
        // 假设税率为 0.1535
        BigDecimal tax = salary.multiply(BigDecimal.valueOf(0.1535));
        // salary(工资)=12898.9988
        System.out.println("salary(工资)=" + salary);
        // 扣税：1979.99631580
        System.out.println("扣税：" + tax);

        // 实际中并不需要这么长的精度，通常保留2位或者4位就狗了
        //  HALF_UP：四舍五入
        //  UP：向上舍入,比如 1.1 ——>2、5.5 ——6，-1.1 ——> -2、-2.5——>-3
        //  DOWN：向下舍入，比如 1.1 ——>1、5.5 ——5，-1.1 ——> -1、-2.5——>-2
        BigDecimal half_up = tax.setScale(2, RoundingMode.HALF_UP);
        BigDecimal up = tax.setScale(2, RoundingMode.UP);
        BigDecimal down = tax.setScale(2, RoundingMode.DOWN);
        // 1980.00,1980.00,1979.99
        System.out.println(half_up + "," + up + "," + down);
        // 1980
        System.out.println(tax.setScale(0, RoundingMode.HALF_UP));
    }

    /**
     * BigDecimal 不可变任意精度小数 转其他 数据类型
     * int intValue(): BigDecimal 转换为 int，只留整数部分，小数部分全部舍弃，不做任何舍入模式。
     * * 类似于 double 缩小到 short，此转换可能会丢失有关此 BigDecimal 值的总体大小和精度的信息，并返回带有相反符号的结果.
     * long longValue(): BigDecimal 转换为 long，只留整数部分，小数部分全部舍弃，不做任何舍入模式。
     * * 类似于 double 缩小到 short，此转换可能会丢失有关此 BigDecimal 值的总体大小和精度的信息，并返回带有相反符号的结果.
     * float floatValue()：BigDecimal 转换为 float，类似 double 缩小到 float，可能会丢失 BigDecimal 精度信息。
     * double doubleValue() ：BigDecimal 转换为 double，类似 double 缩小到 float，可能会丢失 BigDecimal 精度信息。
     * BigInteger toBigInteger() ：BigDecimal 转换为 BigInteger,任何小数部分都将被丢弃,不做任何舍入模式.
     */
    @Test
    public void bigDecimalTest8() {
        BigDecimal salary = new BigDecimal("178454547498.895676");
        int intValue = salary.intValue();
        long longValue = salary.longValue();
        float floatValue = salary.floatValue();
        double doubleValue = salary.doubleValue();
        BigInteger bigInteger = salary.toBigInteger();

        // -1934078934
        System.out.println(intValue);
        // 178454547
        System.out.println(new BigDecimal("178454547.895676").intValue());
        // 178454547498
        System.out.println(longValue);
        // 1.78454544E11
        System.out.println(floatValue);
        // 1.784545474988957E11
        System.out.println(doubleValue);
        // 178454547498
        System.out.println(bigInteger);
        // 178454547498
        System.out.println(new BigDecimal("178454547498.000000").toBigInteger());
        // 178454547498
        System.out.println(new BigDecimal("178454547498").toBigInteger());
        // 0
        System.out.println(new BigDecimal("0").toBigInteger());
    }

    /**
     * 柠檬影业投资效益计算(利滚利的方式)
     */
    @Test
    public void lemonA1() {
        float principal = 5000F;//本金
        float scale = 0.022F;//佣金比例
        int day = 30;//投资天数
        float income = 0;//收益
        System.out.println("本金=" + principal + ",抽成比例=" + scale + ",连续利滚利天数=" + day);

        for (int i = 1; i <= day; i++) {
            float dayIncome = (principal + income) * scale;
            income += dayIncome;
            System.out.println("第" + i + "天，当天收益：" + dayIncome + "，总收益：" + income);
        }
    }

    /**
     * 柠檬影业投资效益计算(利滚利的方式)
     * 使用 BigDecimal API 实现上面 lemon1 同样的效果.
     */
    @Test
    public void lemonB1() {
        BigDecimal principal = new BigDecimal("5000");//本金
        BigDecimal scale = new BigDecimal("0.022");//佣金抽成比例
        int day = 30;//投资天数
        BigDecimal income = new BigDecimal("0");//收益
        System.out.println("本金=" + principal + ",抽成比例=" + scale + ",连续利滚利天数=" + day);

        for (int i = 1; i <= day; i++) {
            BigDecimal dayIncome = principal.add(income).multiply(scale).setScale(4, RoundingMode.HALF_UP);
            income = income.add(dayIncome).setScale(4, RoundingMode.HALF_UP);
            BigDecimal total = principal.add(income).setScale(4, RoundingMode.HALF_UP);
            System.out.println("第" + i + "天，当天收益：" + dayIncome + "，总收益=" + income + "，加本金=" + total);
        }
    }

    /**
     * 利滚利的方式（按具体日期进行输出）
     */
    @Test
    public void lemonB2() {
        BigDecimal principal = new BigDecimal("7000");//本金
        BigDecimal scale = new BigDecimal("0.026");//佣金抽成比例
        DateTime dateTime = DateUtil.parse("2021-09-12");//开始投资日期
        int day = 19;//连续投资天数
        BigDecimal income = new BigDecimal("0");//收益
        System.out.println("本金：" + principal + ", 抽成比例：" + scale + ", 利滚利投资天数：" + day + ", 开始投资日期：" + dateTime.toDateStr());
        for (int i = 1; i <= day; i++) {
            BigDecimal dayIncome = principal.add(income).multiply(scale).setScale(4, RoundingMode.HALF_UP);
            income = income.add(dayIncome).setScale(4, RoundingMode.HALF_UP);
            BigDecimal total = principal.add(income).setScale(4, RoundingMode.HALF_UP);
            System.out.println(dateTime.toDateStr() + "，当天收益=" + dayIncome + "，总收益=" + income + ", 本金加收益=" + total);
            dateTime = DateUtil.offsetDay(dateTime, 1);
        }
    }

    /**
     * 当天收益全部提现，只留本金滚动的方式
     */
    @Test
    public void lemonB3() {
        BigDecimal principal = new BigDecimal("5000");//本金
        BigDecimal scale = new BigDecimal("0.026");//佣金抽成比例
        DateTime dateTime = DateUtil.parse("2021-09-12");//开始投资日期
        int day = 19;//连续投资天数
        BigDecimal income = new BigDecimal("0");//收益
        System.out.println("本金：" + principal + ", 抽成比例：" + scale + ", 本金投资天数：" + day + ", 开始投资日期：" + dateTime.toDateStr());
        for (int i = 1; i <= day; i++) {
            BigDecimal dayIncome = principal.multiply(scale).setScale(4, RoundingMode.HALF_UP);
            income = income.add(dayIncome).setScale(4, RoundingMode.HALF_UP);
            BigDecimal total = principal.add(income).setScale(4, RoundingMode.HALF_UP);
            System.out.println(dateTime.toDateStr() + "，当天收益=" + dayIncome + "，总收益=" + income + ", 本金加收益=" + total);
            dateTime = DateUtil.offsetDay(dateTime, 1);
        }
    }

}