package org.example.se.io;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.util.Assert;

import java.io.*;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * BufferedReader、BufferedWriter 带缓冲的字符流
 * 1、BufferedReader 从字符输入流读取文本，缓冲字符，以提供字符，数组和行的高效读取。
 * 2、BufferedWriter 将文本写入字符输出流，缓冲字符，以提供单个字符，数组和字符串的高效写入。
 * 3、BufferedReader、BufferedWriter 分别继承自 Reader，Writer。可以指定缓冲区大小，或者可以使用默认大小，默认值足够大，可用于大多数用途。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/11/12 10:06
 */
public class BufferedReaderTest {

    /**
     * 使用 BufferedReader + FileReader 高效读取文件内容
     *
     * @param srcFile ：被读取的源文件
     * @return
     */
    public static String readFileByBufferedReader(File srcFile) {
        Assert.isTrue(srcFile != null && srcFile.exists(), srcFile + "文件不存在！");
        Assert.isTrue(srcFile.isFile(), srcFile + "不是文件！");
        StringBuilder stringBuilder = new StringBuilder();
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        try {
            fileReader = new FileReader(srcFile);
            bufferedReader = new BufferedReader(fileReader);
            String readLine;
            while ((readLine = bufferedReader.readLine()) != null) {
                stringBuilder.append(readLine).append("\r\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return stringBuilder.toString();
    }

    /**
     * 使用 BufferedReader + InputStreamReader 高效读取文件内容，支持设置读取时的字符编码
     *
     * @param srcFile ：被读取的源文件
     * @param charset ：使用指定的字符编码读取，可以为空.
     * @return
     */
    public static String readFileByBufferedReader(File srcFile, String charset) {
        Assert.isTrue(srcFile != null && srcFile.exists(), srcFile + "文件不存在！");
        Assert.isTrue(srcFile.isFile(), srcFile + "不是文件！");
        StringBuilder stringBuilder = new StringBuilder();
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        try {
            inputStreamReader = StringUtils.isBlank(charset) ? new InputStreamReader(new FileInputStream(srcFile)) : new InputStreamReader(new FileInputStream(srcFile), charset);
            bufferedReader = new BufferedReader(inputStreamReader);
            String readLine;
            while ((readLine = bufferedReader.readLine()) != null) {
                stringBuilder.append(readLine).append("\r\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStreamReader != null) {
                    inputStreamReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return stringBuilder.toString();
    }

    /**
     * 使用 BufferedWriter + FileWriter 文件字符输出流写入文件，需要指定字符编码的，可以使用 OutputStreamWriter
     *
     * @param textList ：被写入的内容
     * @param destFile ：被写入的文件
     * @param append   ：是否追加内容
     */
    public static void writeFileByFileWriter(List<String> textList, File destFile, boolean append) {
        Assert.isTrue(textList != null && !textList.isEmpty(), "被写入的内容不能为空！");
        Assert.notNull(destFile, "目标文件不能为空！");
        BufferedWriter bufferedWriter = null;
        try {
            if (!destFile.getParentFile().exists()) {
                destFile.getParentFile().mkdirs();
            }
            if (!destFile.exists()) {
                destFile.createNewFile();
            }
            bufferedWriter = new BufferedWriter(new FileWriter(destFile, append));
            for (String text : textList) {
                bufferedWriter.append(text);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void testReadFileByFileReader1() throws IOException {
        String classPath = "data/备忘录-数据库常用表.sql";
        String path = FileInputStreamTest.class.getClassLoader().getResource(classPath).getPath();
        path = URLDecoder.decode(path, "utf-8");
        File file = new File(path);
        String text = readFileByBufferedReader(file);
        System.out.println(text);
    }

    @Test
    public void testReadFileByFileReader2() throws IOException {
        //String classPath = "data/备忘录-数据库常用表.sql";
        String classPath = "data/config.properties";
        String path = FileInputStreamTest.class.getClassLoader().getResource(classPath).getPath();
        path = URLDecoder.decode(path, "utf-8");
        File file = new File(path);
        String text = readFileByBufferedReader(file, "gbk");
        System.out.println(text);
    }

    @Test
    public void testWriteFileByFileWriter() {
        String classPath = "data/备忘录-数据库常用表.sql";

        List<String> textList = new ArrayList<>();
        textList.add("过秦论");
        textList.add("万里长城今尤在，");
        textList.add("不见当年秦始皇。");

        File destFile = new File("D:/" + classPath);
        writeFileByFileWriter(textList, destFile, true);
        System.out.println("生成文件：" + destFile.getPath());
    }

}
