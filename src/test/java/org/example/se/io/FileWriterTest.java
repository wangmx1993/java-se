package org.example.se.io;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.springframework.util.Assert;

import java.io.*;
import java.net.URLDecoder;

/**
 * FileReader、FileWriter 文件字符流
 * 1、public class FileWriter extends OutputStreamWriter：用来写入字符文件的便捷类。
 * 2、FileWriter 用于写入字符流，要写入原始字节流，请考虑使用 FileOutputStream。
 * 3、建议使用'org.apache.commons.io.FileUtils'，Apache已经封装的很好了
 * 4、FileWriter 虽然使用方便，但是写文件时是以系统默认的编码格式输出的，因此对于一些不同编码格式容易产生乱码，尤其对于中文字符， 在使用时应格外留意。为了解决这个问题，可以使用OutputStreamWrite
 * 5、字节流是一个一个字节读取，字符流是一个一个字符读取，比如 '1'、'a'、'读'、'写'、'\r'、'\n'
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/10/30 20:26
 */
public class FileWriterTest {

    /**
     * 使用 FileReader 读取文件内容
     *
     * @param srcFile
     * @return
     * @throws IOException
     */
    public static String readFileByFileReader(File srcFile) {
        Assert.isTrue(srcFile != null && srcFile.exists(), srcFile + "文件不存在！");
        Assert.isTrue(srcFile.isFile(), srcFile + "不是文件！");
        //每次读取的字符个数
        int size;
        //存储每次读取的字符内容
        char[] chars = new char[1024];
        //汇总整个读取的字符内容
        char[] finalChars = {};
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(srcFile);
            while ((size = fileReader.read(chars)) != -1) {
                char[] subarray = ArrayUtils.subarray(chars, 0, size);
                finalChars = ArrayUtils.addAll(finalChars, subarray);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new String(finalChars);
    }

    /**
     * 使用 FileWriter 文件字符输出流写入文件
     *
     * @param text     ：被写入的内容
     * @param destFile ：被写入的文件
     * @param append   ：是否追加内容
     */
    public static void writeFileByFileWriter(String text, File destFile, boolean append) {
        Assert.hasLength(text, "被写入的内容不能为空！");
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(destFile, append);
            fileWriter.append(text);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileWriter != null) {
                    fileWriter.flush();
                    fileWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void testReadFileByFileReader() throws IOException {
        String classPath = "data/备忘录-数据库常用表.sql";
        String path = FileInputStreamTest.class.getClassLoader().getResource(classPath).getPath();
        path = URLDecoder.decode(path, "utf-8");
        File file = new File(path);
        String text = readFileByFileReader(file);
        System.out.println(text);
    }

    @Test
    public void testWriteFileByFileWriter() throws UnsupportedEncodingException {
        String classPath = "data/备忘录-数据库常用表.sql";
        String path = FileInputStreamTest.class.getClassLoader().getResource(classPath).getPath();
        path = URLDecoder.decode(path, "utf-8");
        File file = new File(path);
        String text = readFileByFileReader(file);

        File destFile = new File("D:/" + classPath);
        writeFileByFileWriter(text, destFile, true);
        System.out.println("生成文件：" + destFile.getPath());
    }


}
