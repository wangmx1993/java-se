package org.example.se.io;

import org.example.se.io.beans.Person;
import org.example.se.io.beans.User;
import org.junit.Test;
import org.springframework.util.Assert;

import java.io.*;
import java.util.*;

/**
 * ObjectOutputStream、ObjectInputStream 对象输入输出流，用于序列化/反序列化、对象克隆等
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/11/13 17:39
 */
public class ObjectInputStreamTest {

    /**
     * Java bean 对象序列化
     *
     * @param t        ：被序列化的 java bean 对象，必须实现 Serializable 接口
     * @param destFile ：序列化数据存放的位置，父目录必须存在，文件不存在时会自动新建.
     * @throws IOException
     */
    public static <T> void justSerialize(T t, File destFile) throws IOException {
        Assert.notNull(t, "被序列化对象不能为 Null！");
        Assert.notNull(destFile, "序列化后存储的路径不能为空！");
        ObjectOutputStream objectOutputStream = null;
        try {
            if (!destFile.getParentFile().exists()) {
                destFile.getParentFile().mkdirs();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(destFile);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(t);
        } catch (IOException e) {
            throw e;
        } finally {
            try {
                if (objectOutputStream != null) {
                    objectOutputStream.flush();
                    objectOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 从文件反序列化
     *
     * @param srcFile ：序列化对象后保持的数据
     */
    public static Object deserializeFromFile(File srcFile) throws IOException, ClassNotFoundException {
        Assert.isTrue(srcFile != null && srcFile.exists(), "源文件不能为空！");
        Object object = null;
        ObjectInputStream objectInputStream = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(srcFile);
            objectInputStream = new ObjectInputStream(fileInputStream);
            object = objectInputStream.readObject();
            objectInputStream.close();

        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (objectInputStream != null) {
                    objectInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return object;
    }

    /**
     * 对象深度克隆，使用泛型，支持任意对象。可以替代上面两个方法.
     *
     * @param src ：被克隆的对象，如 java bean，Set、List、Map 等等
     * @param <E> ：支持任意类型，如果是 java Bean，则必须实现 {@link Serializable} 接口实现序列化，否则报错
     * @return ：失败时返回 null.
     */
    public static <E> E deepCloneObject(E src) throws IOException, ClassNotFoundException {
        Assert.notNull(src, "被克隆对象不能为空！");
        E dest = null;
        ObjectOutputStream out = null;
        ObjectInputStream in = null;
        try {
            ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
            out = new ObjectOutputStream(byteOut);
            out.writeObject(src);
            ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
            in = new ObjectInputStream(byteIn);
            dest = (E) in.readObject();
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dest;
    }

    @Test
    public void testJustSerialize() throws IOException {
        User user = new User();
        user.setId(9527);
        user.setName("华安");
        user.setBirthday(new Date());
        User.setAddress("USA");

        //序列化保存在本地的位置
        String path = "D:/serialize/user.txt";
        File destFile = new File(path);
        ObjectInputStreamTest.justSerialize(user, destFile);
        System.out.println("对象序列化成功，存储路径：" + destFile.getPath());
    }

    @Test
    public void testDeserializeFromFile() throws IOException, ClassNotFoundException {
        String path = "D:/serialize/user.txt";
        File file = new File(path);
        User user = (User) ObjectInputStreamTest.deserializeFromFile(file);
        //User{id=9527, name='华安', birthday=Sun Nov 14 08:49:22 CST 2021}
        System.out.println(user);
    }

    @Test
    public void deepCopyMapTest1() throws IOException, ClassNotFoundException {
        Map<String, Object> map1 = new HashMap<>(2);
        map1.put("id", 3000);
        map1.put("name", "华安");

        Map<String, Object> deepCopy2 = deepCloneObject(map1);
        deepCopy2.put("marry", false);

        //{name=华安, id=3000}
        System.out.println(map1);
        //{name=华安, marry=false, id=3000}
        System.out.println(deepCopy2);
    }

    @Test
    public void deepCopyMapTest2() throws IOException, ClassNotFoundException {
        Person person = new Person(11, "无忌", new Date(), 7878.89F);
        Map<String, Person> map1 = new HashMap<>(2);
        map1.put("11", person);

        Map<String, Person> cloneObject = deepCloneObject(map1);
        cloneObject.get("11").setId(20002);

        //{11=Person{id=11, name='无忌', birthday=Tue Aug 11 14:57:06 CST 2020, salary=7878.89}}
        System.out.println(map1);
        //{11=Person{id=20002, name='无忌', birthday=Tue Aug 11 14:57:06 CST 2020, salary=7878.89}}
        System.out.println(cloneObject);
    }

    @Test
    public void deepCopyListTest1() throws IOException, ClassNotFoundException {
        List<String> list = new ArrayList<>(Arrays.asList("中", "会话", "品牌"));
        List<String> copyList = deepCloneObject(list);
        copyList.add("环境");

        //[中, 会话, 品牌]
        System.out.println(list);
        //[中, 会话, 品牌, 环境]
        System.out.println(copyList);
    }

    @Test
    public void deepCopyListTest2() throws IOException, ClassNotFoundException {
        List<Map<String, Object>> mapList = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>(2);
        map1.put("id", 3000);
        map1.put("name", "华安");
        mapList.add(map1);

        List<Map<String, Object>> cloneList = deepCloneObject(mapList);
        cloneList.get(0).put("id", "2000");

        //[{name=华安, id=3000}]
        System.out.println(mapList);
        //[{name=华安, id=2000}]
        System.out.println(cloneList);
    }

    @Test
    public void deepCopyListTest3() throws IOException, ClassNotFoundException {
        List<Person> personList = new ArrayList<>(8);
        personList.add(new Person(11, "无忌", new Date(), 7878.89F));

        List<Person> cloneObject = deepCloneObject(personList);
        cloneObject.get(0).setSalary(556576.67F);

        //[Person{id=11, name='无忌', birthday=Tue Aug 11 14:59:32 CST 2020, salary=7878.89}]
        System.out.println(personList);
        //[Person{id=11, name='无忌', birthday=Tue Aug 11 14:59:32 CST 2020, salary=556576.7}]
        System.out.println(cloneObject);
    }
}
