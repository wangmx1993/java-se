package org.example.se.io;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.stream.Stream;

/**
 * FileInputStream、FileOutputStream 文件输入输出字节流，最常见的IO操作流之一。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/11/11 10:09
 */
public class FileInputStreamTest {

    private static final Logger log = LoggerFactory.getLogger(FileInputStreamTest.class);

    /**
     * 根据文件路径读取文件内容
     *
     * @param srcPath     ：文件地址，如：D:/a.txt
     * @param charsetName ：文件内容编码，如 utf-8、gbk，可以为空
     * @return
     */
    public static String readPathToString(String srcPath, String charsetName) throws UnsupportedEncodingException, FileNotFoundException {
        Assert.hasText(srcPath, "源文件路径不能为空！");
        File srcFile = new File(srcPath);
        return readFileToString(srcFile, charsetName);
    }

    /**
     * 根据文件读取文件内容
     *
     * @param srcFile     ：源文件
     * @param charsetName ：文件内容编码，如 utf-8、gbk，可以为空
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String readFileToString(File srcFile, String charsetName) throws UnsupportedEncodingException, FileNotFoundException {
        Assert.isTrue(srcFile.exists(), srcFile + " 文件不存在！");
        Assert.isTrue(srcFile.isFile(), srcFile + " 不是文件！");
        InputStream inputStream = new FileInputStream(srcFile);
        return readInputStreamToString(inputStream, charsetName);
    }

    /**
     * 根据文件输入流读取文件内容
     *
     * @param inputStream ：源文件字节输入流
     * @param charsetName ：文件内容编码，如 utf-8、gbk，可以为空
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String readInputStreamToString(InputStream inputStream, String charsetName) throws UnsupportedEncodingException {
        Assert.notNull(inputStream, "源文件 InputStream 输入流不能为空！");
        //循环读取的大小
        int size;
        //存储每次读取的字节内容
        byte[] bytes = new byte[1024];
        //将每次读取的内容进行汇总
        byte[] finalBytes = {};
        try {
            while ((size = inputStream.read(bytes)) != -1) {
                byte[] subarray = ArrayUtils.subarray(bytes, 0, size);
                finalBytes = ArrayUtils.addAll(finalBytes, subarray);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        String text = StringUtils.isBlank(charsetName) ? new String(finalBytes) : new String(finalBytes, charsetName);
        return text;
    }

    /**
     * 复制文件
     *
     * @param srcFile      ：源文件
     * @param outputStream ：目标文件输出流
     * @throws FileNotFoundException
     */
    public static void copyFileFromFile(File srcFile, OutputStream outputStream) throws FileNotFoundException {
        Assert.isTrue(srcFile != null && srcFile.exists(), srcFile + " 源文件不能存在！");
        Assert.notNull(outputStream, "目标文件 OutputStream 输出流不能为空！");
        InputStream inputStream = new FileInputStream(srcFile);
        copyFileFromInputStream(inputStream, outputStream);
    }

    /**
     * 复制文件
     *
     * @param inputStream  ：源文件字节输入流
     * @param outputStream ：目标文件字节输出流
     */
    public static void copyFileFromInputStream(InputStream inputStream, OutputStream outputStream) {
        Assert.notNull(inputStream, "源文件 InputStream 输入流不能为空！");
        Assert.notNull(outputStream, "目标文件 OutputStream 输出流不能为空！");
        //循环读取的大小
        int size;
        //存储每次读取的字节内容
        byte[] bytes = new byte[1024];
        try {
            while ((size = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, size);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            try {
                inputStream.close();
                outputStream.flush();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //--------------------------------------方法测试--------------------------------------------
    @Test
    public void testReadPathToString() throws Exception {
        String classPath = "data/备忘录-数据库常用表.sql";
        String path = FileInputStreamTest.class.getClassLoader().getResource(classPath).getPath();
        path = URLDecoder.decode(path, "utf-8");

        String string = readPathToString(path, "utf-8");
        System.out.println("读取文件内容：\n" + string);
    }

    @Test
    public void testReadFileToString() throws Exception {
        String classPath = "data/备忘录-数据库常用表.sql";
        String path = FileInputStreamTest.class.getClassLoader().getResource(classPath).getPath();
        path = URLDecoder.decode(path, "utf-8");
        File file = new File(path);
        String string = readFileToString(file, "utf-8");
        System.out.println("读取文件内容：\n" + string);
    }

    @Test
    public void testReadInputStreamToString() throws Exception {
        String classPath = "data/备忘录-数据库常用表.sql";
        InputStream inputStream = FileInputStreamTest.class.getClassLoader().getResourceAsStream(classPath);
        String string = readInputStreamToString(inputStream, "utf-8");
        System.out.println("读取文件内容：\n" + string);
    }

    @Test
    public void testCopyFileFromInputStream() throws FileNotFoundException {
        String srcClassPath = "data/images/韩团美女.jpeg";
        InputStream inputStream = FileInputStreamTest.class.getClassLoader().getResourceAsStream(srcClassPath);
        File destFile = new File(FileSystemView.getFileSystemView().getHomeDirectory(), srcClassPath);
        if (!destFile.getParentFile().exists()) {
            destFile.getParentFile().mkdirs();
        }
        FileOutputStream outputStream = new FileOutputStream(destFile);
        copyFileFromInputStream(inputStream, outputStream);
        System.out.println("复制文件：" + srcClassPath + " -> " + destFile);
    }

    @Test
    public void testCopyFileFromFile() throws FileNotFoundException, UnsupportedEncodingException {
        String srcClassPath = "data/备忘录-数据库常用表.sql";
        String path = FileInputStreamTest.class.getClassLoader().getResource(srcClassPath).getPath();
        path = URLDecoder.decode(path, "utf-8");
        File file = new File(path);

        File destFile = new File(FileSystemView.getFileSystemView().getHomeDirectory(), srcClassPath);
        if (!destFile.getParentFile().exists()) {
            destFile.getParentFile().mkdirs();
        }
        FileOutputStream outputStream = new FileOutputStream(destFile);
        copyFileFromFile(file, outputStream);
        System.out.println("复制文件：" + srcClassPath + " -> " + destFile);
    }

    /**
     * 1、从 Jdk 1.7 开始，try-with-source 语法糖支持自动关闭输入输出流。但是资源释放的类需要实现 java.io.Closeable 接口(其中只有一个 close() 方法)。
     * 2、比如常见的 java.io.FileInputStream、java.io.FileOutputStream 流都实现了 Closeable 接口，所以使用 try-with-source 语法糖能自动关闭输入输出流。
     */
    @Test
    public void testTryWithSource() {
        try (InputStream inputStream = FileInputStreamTest.class.getClassLoader().getResourceAsStream("data/备忘录-数据库常用表.sql");
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("utf-8"));
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
            Stream<String> lines = bufferedReader.lines();
            lines.forEach(item -> System.out.println(item));
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
    }

}
