package org.example.se.io.randomAccessFile;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.example.se.io.FileInputStreamTest;
import org.junit.jupiter.api.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * randomAccessFile-随机访问文件
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/1/28 14:42
 */
public class RandomAccessFileTest {

    /**
     * 读取文件内容
     *
     * @param file        ：文件位置
     * @param charsetName ：内容编码，如 utf-8、gbk，可以为空
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String readFileToString(File file, String charsetName) throws IOException {
        //循环读取的大小
        int size;
        //存储每次读取的字节内容
        byte[] bytes = new byte[1024];
        //将每次读取的内容进行汇总
        byte[] finalBytes = {};
        RandomAccessFile randomAccessFile = null;
        try {
            randomAccessFile = new RandomAccessFile(file, "r");
            while ((size = randomAccessFile.read(bytes)) != -1) {
                byte[] subarray = ArrayUtils.subarray(bytes, 0, size);
                finalBytes = ArrayUtils.addAll(finalBytes, subarray);
            }
        } finally {
            try {
                if (randomAccessFile != null) {
                    randomAccessFile.close();
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        String text = StringUtils.isBlank(charsetName) ? new String(finalBytes) : new String(finalBytes, charsetName);
        return text;
    }

    /**
     * 复制文件
     *
     * @param sourceFile ：被复制的源文件
     * @param destFile   ：复制到的新目标文件路径，目标文件不存在时，自动新建，但是它的父目录必须存在，否则找不到路径。
     */
    public static void copyFile(File sourceFile, File destFile) throws IOException {
        //循环读取的大小
        int size;
        //存储每次读取的字节内容
        byte[] bytes = new byte[1024];
        RandomAccessFile inRandomAccessFile = null;
        RandomAccessFile outRandomAccessFile = null;
        try {
            if (!destFile.getParentFile().exists()) {
                destFile.getParentFile().mkdirs();
            }
            inRandomAccessFile = new RandomAccessFile(sourceFile, "r");
            outRandomAccessFile = new RandomAccessFile(destFile, "rw");
            while ((size = inRandomAccessFile.read(bytes)) != -1) {
                outRandomAccessFile.write(bytes, 0, size);
            }
        } finally {
            try {
                if (inRandomAccessFile != null) {
                    inRandomAccessFile.close();
                }
                if (outRandomAccessFile != null) {
                    outRandomAccessFile.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 使用 RandomAccessFile 写入文件内容，当文件已经存在内容时，默认追加到结尾。
     *
     * @param destFile ：待写入的目标文件
     * @param content  ：待写入的内容
     * @throws IOException
     */
    public static void writeToFile(File destFile, String content) throws IOException {
        RandomAccessFile randomAccessFile = null;
        try {
            randomAccessFile = new RandomAccessFile(destFile, "rw");
            long length = randomAccessFile.length();
            randomAccessFile.seek(length);
            //必须指定编码，否则写入的中文打开文件后会乱码
            randomAccessFile.write(content.getBytes("utf-8"));
        } finally {
            try {
                if (randomAccessFile != null) {
                    randomAccessFile.close();
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Test
    public void testReadFileToString() throws IOException {
        String classPath = "data/备忘录-数据库常用表.sql";
        String path = FileInputStreamTest.class.getClassLoader().getResource(classPath).getPath();
        path = URLDecoder.decode(path, "utf-8");

        String string = readFileToString(new File(path), null);
        System.out.println("读取文件内容：\n" + string);
    }

    @Test
    public void testCopyFile() throws IOException {
        String srcClassPath = "data/images/韩团美女.jpeg";
        String path = FileInputStreamTest.class.getClassLoader().getResource(srcClassPath).getPath();
        path = URLDecoder.decode(path, "utf-8");

        File destFile = new File(FileSystemView.getFileSystemView().getHomeDirectory(), srcClassPath);

        copyFile(new File(path), destFile);
        System.out.println("复制文件：" + destFile);
    }

    @Test
    public void testWriteToFile() throws IOException {
        File destFile = new File(FileSystemView.getFileSystemView().getHomeDirectory(), "1.txt");
        writeToFile(destFile, "\n++123……*（78dhjdhjf京东方合肥京东方和!@#@#@$%#%$^jhjHJHJH很久很久");
        System.out.println("写入文件：" + destFile);
    }


}
