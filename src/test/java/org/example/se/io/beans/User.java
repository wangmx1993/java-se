package org.example.se.io.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/11/14 8:17
 */
public class User implements Serializable {
    /**
     * 具体数值自己定义，序列化之后如果修改了这个uid，则反序列化时会报错.
     */
    private static final long serialVersionUID = 1749243243491717954L;
    private Integer id;
    private String name;
    private Date birthday;
    /**
     * 默认情况下，静态属性不会被序列化
     */
    private static String address = "China";

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getAddress() {
        return address;
    }

    public static void setAddress(String address) {
        User.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
