package org.example.se.io;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.util.Assert;

import javax.swing.filechooser.FileSystemView;
import java.io.*;

/**
 * InputStreamReader、OutputStreamWriter 所有输入输出字符流的基类
 * 1、InputStreamReader 是从字节流到字符流的桥，它读取字节，并使用指定的 charset 将其解码为字符，它使用的字符集可以由名称指定，也可以被明确指定，或者可以接受平台的默认字符集。
 * 2、字符流只适用于字符型，对图片、音视频等是不适合的。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/11/11 16:31
 */
public class InputStreamReaderTest {

    /**
     * 使用字符流读取文件内容，适合读取文本内容，其它的如图片、视频 等是不适合的。
     *
     * @param inputStream ：文件字节输入流
     * @param charsetName ：将字节解码为字符流，可以为空
     * @return
     * @throws IOException
     */
    public static String readFileByInputStreamReader(InputStream inputStream, String charsetName) throws IOException {
        Assert.notNull(inputStream, "输入流 InputStream 不能为空！");
        // InputStreamReader(InputStream in):创建使用默认字符集的 InputStreamReader
        // InputStreamReader(InputStream in, String charsetName)：创建使指定字符集的 InputStreamReader
        InputStreamReader inputStreamReader = StringUtils.isBlank(charsetName) ? new InputStreamReader(inputStream) : new InputStreamReader(inputStream, charsetName);
        //每次的读取的字符数组大小
        int size;
        //存放每次读取的字段内容
        char[] chars = new char[1024];
        //存放整个源文件的内容
        char[] finalChars = {};
        while ((size = inputStreamReader.read(chars)) != -1) {
            char[] subarray = ArrayUtils.subarray(chars, 0, size);
            finalChars = ArrayUtils.addAll(finalChars, subarray);
        }
        return new String(finalChars);
    }

    /**
     * 复制文件——————适合读取字符类型的文件，比如文本内容，其它的如图片、视频 等是不适合的。
     *
     * @param inputStream  ：源文件字节输入流
     * @param outputStream ：目标文件字节输出流
     * @param charsetName  ：将字节解码为字符流，可以为空
     */
    public static void copyFileByOutputStreamWriter(InputStream inputStream, OutputStream outputStream, String charsetName) {
        Assert.notNull(inputStream, "源文件 InputStream 输入流不能为空！");
        Assert.notNull(outputStream, "目标文件 OutputStream 输出流不能为空！");
        //循环读取的大小
        int size;
        //存储每次读取的字节内容
        char[] bytes = new char[1024];
        InputStreamReader inputStreamReader = null;
        OutputStreamWriter outputStreamWriter = null;
        try {
            // InputStreamReader(InputStream in):创建使用默认字符集的 InputStreamReader
            // InputStreamReader(InputStream in, String charsetName)：创建使指定字符集的 InputStreamReader
            inputStreamReader = StringUtils.isBlank(charsetName) ? new InputStreamReader(inputStream) : new InputStreamReader(inputStream, charsetName);
            //OutputStreamWriter(OutputStream out) ：创建使用默认字符编码的 OutputStreamWriter
            //OutputStreamWriter(OutputStream out, String charsetName): 创建使用指定字符编码的 OutputStreamWriter
            outputStreamWriter = StringUtils.isBlank(charsetName) ? new OutputStreamWriter(outputStream) : new OutputStreamWriter(outputStream, charsetName);
            while ((size = inputStreamReader.read(bytes)) != -1) {
                outputStreamWriter.write(bytes, 0, size);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            try {
                if (outputStreamWriter != null) {
                    outputStreamWriter.flush();
                    outputStreamWriter.close();
                }
                outputStream.flush();
                outputStream.close();
                if (inputStreamReader != null) {
                    inputStream.close();
                }
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Test
    public void testReadFileFromInputStream() throws IOException {
        //String classPath = "data/备忘录-数据库常用表.sql";
        String classPath = "data/config.properties";
        InputStream inputStream = FileInputStreamTest.class.getClassLoader().getResourceAsStream(classPath);
        String fromInputStream = readFileByInputStreamReader(inputStream, "gbk");
        System.out.println(fromInputStream);
    }

    @Test
    public void testCopyFileFromInputStream() throws FileNotFoundException {
        String srcClassPath = "data/备忘录-数据库常用表.sql";
        InputStream inputStream = FileInputStreamTest.class.getClassLoader().getResourceAsStream(srcClassPath);
        File destFile = new File(FileSystemView.getFileSystemView().getHomeDirectory(), srcClassPath);
        if (!destFile.getParentFile().exists()) {
            destFile.getParentFile().mkdirs();
        }
        FileOutputStream outputStream = new FileOutputStream(destFile);
        copyFileByOutputStreamWriter(inputStream, outputStream, "");
        System.out.println("复制文件：" + srcClassPath + " -> " + destFile);
    }

}
