package org.example.se.io;


import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.util.Assert;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.net.URLDecoder;

/**
 * BufferedInputStream、BufferedOutputStream 带缓存的字节输入输出流
 * BufferedInputStream 继承于 FilterInputStream，默认缓冲区大小是 8M，能够减少访问磁盘的次数，提高文件读取性能。
 * BufferedOutputStream 继承于 FilterOutputStream，能够提高文件的写入效率。
 * 当创建 BufferedInputStream 时，将创建一个内部缓冲区数组。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/11/11 15:43
 */
public class BufferedInputStreamTest {


    /**
     * 根据文件路径读取文件内容
     *
     * @param srcPath     ：文件地址，如：D:/a.txt
     * @param charsetName ：文件内容编码，如 utf-8、gbk，可以为空
     * @return
     */
    public static String readPathToString(String srcPath, String charsetName) throws UnsupportedEncodingException, FileNotFoundException {
        Assert.hasText(srcPath, "源文件路径不能为空！");
        File srcFile = new File(srcPath);
        return readFileToString(srcFile, charsetName);
    }

    /**
     * 根据文件读取文件内容
     *
     * @param srcFile     ：源文件
     * @param charsetName ：文件内容编码，如 utf-8、gbk，可以为空
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String readFileToString(File srcFile, String charsetName) throws UnsupportedEncodingException, FileNotFoundException {
        Assert.isTrue(srcFile.exists(), srcFile + " 文件不存在！");
        Assert.isTrue(srcFile.isFile(), srcFile + " 不是文件！");
        InputStream inputStream = new FileInputStream(srcFile);
        return readInputStreamToString(inputStream, charsetName);
    }

    /**
     * 根据文件输入流读取文件内容
     *
     * @param inputStream ：源文件字节输入流
     * @param charsetName ：文件内容编码，如 utf-8、gbk，可以为空
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String readInputStreamToString(InputStream inputStream, String charsetName) throws UnsupportedEncodingException {
        Assert.notNull(inputStream, "源文件 InputStream 输入流不能为空！");
        //循环读取的大小
        int size;
        //存储每次读取的字节内容
        byte[] bytes = new byte[1024];
        //将每次读取的内容进行汇总
        byte[] finalBytes = {};
        BufferedInputStream bufferedInputStream = null;
        try {
            bufferedInputStream = new BufferedInputStream(inputStream);
            while ((size = bufferedInputStream.read(bytes)) != -1) {
                byte[] subarray = ArrayUtils.subarray(bytes, 0, size);
                finalBytes = ArrayUtils.addAll(finalBytes, subarray);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            try {
                inputStream.close();
                if (bufferedInputStream != null) {
                    bufferedInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String text = StringUtils.isBlank(charsetName) ? new String(finalBytes) : new String(finalBytes, charsetName);
        return text;
    }

    /**
     * 复制文件
     *
     * @param srcFile      ：源文件
     * @param outputStream ：目标文件输出流
     * @throws FileNotFoundException
     */
    public static void copyFileFromFile(File srcFile, OutputStream outputStream) throws FileNotFoundException {
        Assert.isTrue(srcFile != null && srcFile.exists(), srcFile + " 源文件不能存在！");
        Assert.notNull(outputStream, "目标文件 OutputStream 输出流不能为空！");
        InputStream inputStream = new FileInputStream(srcFile);
        copyFileFromInputStream(inputStream, outputStream);
    }

    /**
     * 复制文件
     *
     * @param inputStream  ：源文件字节输入流
     * @param outputStream ：目标文件字节输出流
     */
    public static void copyFileFromInputStream(InputStream inputStream, OutputStream outputStream) {
        Assert.notNull(inputStream, "源文件 InputStream 输入流不能为空！");
        Assert.notNull(outputStream, "目标文件 OutputStream 输出流不能为空！");
        //循环读取的大小
        int size;
        //存储每次读取的字节内容
        byte[] bytes = new byte[1024];
        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutputStream = null;
        try {
            bufferedInputStream = new BufferedInputStream(inputStream);
            bufferedOutputStream = new BufferedOutputStream(outputStream);
            while ((size = bufferedInputStream.read(bytes)) != -1) {
                bufferedOutputStream.write(bytes, 0, size);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            try {
                if (bufferedOutputStream != null) {
                    bufferedOutputStream.flush();
                    bufferedOutputStream.close();
                }
                outputStream.flush();
                outputStream.close();
                inputStream.close();
                if (bufferedInputStream != null) {
                    bufferedInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //--------------------------------------方法测试--------------------------------------------
    @Test
    public void testReadPathToString() throws Exception {
        String classPath = "data/备忘录-数据库常用表.sql";
        String path = FileInputStreamTest.class.getClassLoader().getResource(classPath).getPath();
        path = URLDecoder.decode(path, "utf-8");

        String string = readPathToString(path, "utf-8");
        System.out.println("读取文件内容：\n" + string);
    }

    @Test
    public void testReadFileToString() throws Exception {
        String classPath = "data/备忘录-数据库常用表.sql";
        String path = FileInputStreamTest.class.getClassLoader().getResource(classPath).getPath();
        path = URLDecoder.decode(path, "utf-8");
        File file = new File(path);
        String string = readFileToString(file, "utf-8");
        System.out.println("读取文件内容：\n" + string);
    }

    @Test
    public void testReadInputStreamToString() throws Exception {
        String classPath = "data/备忘录-数据库常用表.sql";
        InputStream inputStream = FileInputStreamTest.class.getClassLoader().getResourceAsStream(classPath);
        String string = readInputStreamToString(inputStream, "utf-8");
        System.out.println("读取文件内容：\n" + string);
    }

    @Test
    public void testCopyFileFromInputStream() throws FileNotFoundException {
        String srcClassPath = "data/images/韩团美女.jpeg";
        InputStream inputStream = FileInputStreamTest.class.getClassLoader().getResourceAsStream(srcClassPath);
        File destFile = new File(FileSystemView.getFileSystemView().getHomeDirectory(), srcClassPath);
        if (!destFile.getParentFile().exists()) {
            destFile.getParentFile().mkdirs();
        }
        FileOutputStream outputStream = new FileOutputStream(destFile);
        copyFileFromInputStream(inputStream, outputStream);
        System.out.println("复制文件：" + srcClassPath + " -> " + destFile);
    }

    @Test
    public void testCopyFileFromFile() throws FileNotFoundException, UnsupportedEncodingException {
        String srcClassPath = "data/备忘录-数据库常用表.sql";
        String path = FileInputStreamTest.class.getClassLoader().getResource(srcClassPath).getPath();
        path = URLDecoder.decode(path, "utf-8");
        File file = new File(path);

        File destFile = new File(FileSystemView.getFileSystemView().getHomeDirectory(), srcClassPath);
        if (!destFile.getParentFile().exists()) {
            destFile.getParentFile().mkdirs();
        }
        FileOutputStream outputStream = new FileOutputStream(destFile);
        copyFileFromFile(file, outputStream);
        System.out.println("复制文件：" + srcClassPath + " -> " + destFile);
    }

}
