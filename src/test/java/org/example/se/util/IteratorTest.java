package org.example.se.util;

import org.junit.Test;

import java.util.*;

/**
 * 1、Iterator 接口可用于遍历任何 Collection 接口，可以从一个 Collection 中使用迭代器方法来获取迭代器实例。
 * 2、迭代器取代了 Java 集合框架中的 Enumeration，迭代器允许调用者在迭代过程中移除元素。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/1/27 10:06
 */
public class IteratorTest {

    /**
     * 实现了 Iterable  接口的子类都可以转成 Iterator  迭代器操作元素。
     */
    @Test
    public void testIterator1() {
        Set<String> set = new HashSet<>();
        set.add("中国");
        set.add("美利坚");
        set.add("大不列颠");

        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            //美利坚
            //中国
            //大不列颠
            System.out.println(iterator.next());
        }
    }

    /**
     * 迭代器取代了 Java 集合框架中的 Enumeration，迭代器允许调用者在迭代过程中移除元素。
     * 集合删除元素必须借助迭代器进行删除。
     */
    @Test
    public void testIterator2() {
        List<Map<String, Object>> billNoList = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>();
        map1.put("agency_code", "1000");

        Map<String, Object> map2 = new HashMap<>();
        map2.put("agency_code", "1001");

        Map<String, Object> map3 = new HashMap<>();
        map3.put("agency_code", "1002");

        billNoList.add(map1);
        billNoList.add(map2);
        billNoList.add(map3);

        Iterator<Map<String, Object>> iterator = billNoList.iterator();
        while (iterator.hasNext()) {
            Map<String, Object> next = iterator.next();
            if ("1002".equals(next.get("agency_code"))) {
                iterator.remove();
            }
        }
        //[{agency_code=1000}, {agency_code=1001}]
        System.out.println(billNoList);
    }

    /**
     * 实现 Iterable<T> 接口的对象都可以转成迭代器 Iterator 操作
     */
    @Test
    public void testIterator3() {
        Set<String> set = new HashSet<>();
        set.add("中国");
        set.add("美利坚");
        set.add("大不列颠");

        //美利坚
        //中国
        //大不列颠
        set.forEach((item) -> System.out.println(item));
    }

}
