package org.example.se.util;

import org.junit.Test;

import java.lang.reflect.Array;
import java.util.*;

/**
 * 1、instanceof 关键字用于判断一个引用类型变量所指向的对象是否是一个类（或接口、抽象类、父类）的实例
 * * 用法：result = object instanceof class
 * * 参数：
 * * result：布尔类型
 * * object：必选项。任意对象表达式，不能是基本类型。如果为 null，那么将返回 false。
 * * class：必选项。表示一个类或者一个接口
 * 2、编译器会检查 object 是否能转换成右边的 class 类型，如果不能转换则直接编译报错，如果不能确定类型，则通过编译，具体看运行时定
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/1/5 9:45
 */
public class InstanceofTest {

    /**
     * 对象类型判断 1
     */
    @Test
    public void testInstanceof1() {
        Integer integer = 898989;
        String string = "哈哈";
        String address = null;
        int[] ints = {1, 2, 3, 4, 5};
        //true
        System.out.println(integer instanceof Integer);
        //true
        System.out.println(string instanceof String);
        //true
        System.out.println(string instanceof CharSequence);
        //false
        System.out.println(address instanceof String);
        //true
        System.out.println(ints instanceof int[]);

    }

    /**
     * 对象类型判断 2
     */
    @Test
    public void testInstanceof2() {
        Map<String, Object> hashMap = new HashMap<>();
        HashMap linkedHashMap = new LinkedHashMap();
        List<String> arrayList = new ArrayList<>();

        //true
        System.out.println(hashMap instanceof Map);
        //true
        System.out.println(hashMap instanceof HashMap);
        //false
        System.out.println(hashMap instanceof LinkedHashMap);
        //true
        System.out.println(linkedHashMap instanceof HashMap);
        //true
        System.out.println(linkedHashMap instanceof LinkedHashMap);
        //true
        System.out.println(arrayList instanceof Collection);
    }

    /**
     * 判断对象是否为 null，或者为空
     *
     * @param object
     * @return
     */
    public static boolean isEmpty(final Object object) {
        if (object == null) {
            return true;
        }
        if (object instanceof CharSequence) {
            return ((CharSequence) object).length() == 0;
        }
        //数组有点特殊，无法用 instanceof 指定具体的类型，如 int[],String[] ...
        if (object.getClass().isArray()) {
            return Array.getLength(object) == 0;
        }
        if (object instanceof Iterable<?>) {
            Iterable iterable = (Iterable<?>) object;
            return false == iterable.iterator().hasNext();
        }
        if (object instanceof Map<?, ?>) {
            return ((Map<?, ?>) object).isEmpty();
        }
        if (object instanceof Iterator) {
            Iterator iterator = (Iterator) object;
            return false == iterator.hasNext();
        }
        return false;
    }

    @Test
    public void testIsEmpty() {
        //true
        System.out.println(isEmpty(null));
        //true
        System.out.println(isEmpty(new HashMap<>()));
        //true
        System.out.println(isEmpty(new int[]{}));
        //false
        System.out.println(isEmpty(new int[]{1, 2, 3}));
        //true
        System.out.println(isEmpty(new ArrayList<>()));
        //true
        System.out.println(isEmpty(new ArrayList<>().iterator()));
    }

}
