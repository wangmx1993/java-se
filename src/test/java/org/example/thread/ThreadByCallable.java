package org.example.thread;

import cn.hutool.core.thread.ThreadUtil;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Java 中如何实现线程？
 * Callable 接口从 JDK 1.5 开始新增，Runnable 从 JDK1.0 开始就有，区别在于 Callable 的 call 方法有返回值，且可以抛出异常，
 * 而 Runnable 的 run 方法没有返回值。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/10/31 13:13
 */
public class ThreadByCallable implements Callable {
    @Override
    public Object call() throws Exception {
        System.out.println("我是线程：" + Thread.currentThread().getName());
        ThreadUtil.safeSleep(1000);
        return "success";
    }

    public static void main(String[] args) {
        //使用线程池性能更优
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 3; i++) {
            executorService.submit(new ThreadByCallable());
        }
        System.out.println(Thread.currentThread().getName() + "：3秒后关闭线程池");
        ThreadUtil.safeSleep(3000);
        executorService.shutdownNow();
        System.out.println(Thread.currentThread().getName() + "：关闭线程池");
//        依次输出：
// 我是线程：pool-1-thread-1
// 我是线程：pool-1-thread-2
// 我是线程：pool-1-thread-3
// main：5秒后关闭线程池
// main：关闭线程池
    }
}
