package org.example.thread;

/**
 * Java 中如何实现线程？
 * 实现 Runable 接口，实现 run 方法
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/10/31 13:07
 */
public class ThreadByRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("我是线程：" + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            new Thread(new ThreadByRunnable()).start();
        }
        //依次输出：
        //我是线程：Thread-0
        //我是线程：Thread-1
        //我是线程：Thread-2
    }
}