package org.example.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Java 中如何实现线程？
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/10/31 13:11
 */
public class ThreadByPoolExecutor implements Runnable {
    @Override
    public void run() {
        System.out.println("My name is 线程：" + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        //使用线程池性能更优
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 3; i++) {
            //execute(Runnable command)
            executorService.execute(new ThreadByPoolExecutor());
        }
        //        依次输出：
        //My name is 线程：pool-1-thread-1
        //My name is 线程：pool-1-thread-2
        //My name is 线程：pool-1-thread-3
    }
}
