package org.example.thread;

/**
 * Java 中如何实现线程？
 * 继承 Thread 类，重写 run 方法
 * Thread 类本身也是实现 Runnable 即可：class Thread implements Runnable
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/10/31 13:09
 */
public class ThreadByThread extends Thread {
    @Override
    public void run() {
        System.out.println("I an 线程：" + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            new ThreadByThread().start();
        }
        //依次输出：
        //I an 线程：Thread-0
        //I an 线程：Thread-1
        //I an 线程：Thread-2
    }
}