package org.example;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期测试
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/3/4 17:30
 */
public class DateTest {

    /**
     * 计算工龄，向上取整
     *
     * @param nowTime  : 当前时间
     * @param workTime ： 开始工作时间
     * @return /
     */
    public static int getWorkAge(Date nowTime, Date workTime) {
        // 当前时间的年月日
        Calendar cal = Calendar.getInstance();
        cal.setTime(nowTime);
        int nowYear = cal.get(Calendar.YEAR);
        int nowMonth = cal.get(Calendar.MONTH);
        int nowDay = cal.get(Calendar.DAY_OF_MONTH);

        // 开始工作时间的年月日
        cal.setTime(workTime);
        int workYear = cal.get(Calendar.YEAR);
        int workMonth = cal.get(Calendar.MONTH);
        int workDay = cal.get(Calendar.DAY_OF_MONTH);

        // 得到工龄
        int year = nowYear - workYear; // 得到年差
        // 若目前月数大于开始工作时间的月数，年差+1
        if (nowMonth > workMonth) {
            year = year + 1;
        } else if (nowMonth == workMonth) {
            // 当月数相等时，判断日数，若当月的日数大于开始工作时间的日数，年差-1
            if (nowDay > workDay) {
                year = year + 1;
            }
        }
        return year;
    }

    /**
     * 获取日历信息，年月份，时分秒等
     */
    @Test
    public void testCalendar1() {
        Date dateNow = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateNow);
        System.out.println("年份：" + calendar.get(Calendar.YEAR));

        /***默认取值为[0,11]，其中0表示1月*/
        System.out.println("月份：" + (calendar.get(Calendar.MONTH) + 1));
        System.out.println("月中第几天：" + calendar.get(Calendar.DATE));
        System.out.println("月中第几天：" + calendar.get(Calendar.DAY_OF_MONTH));

        /**默认取值为[1,7]，其中1表示星期天，7表示星期六;西方以星期天为一个星期的开始*/
        System.out.println("星期中第几天：" + (calendar.get(Calendar.DAY_OF_WEEK) - 1));
        System.out.println("一年中第几天：" + calendar.get(Calendar.DAY_OF_YEAR));

        System.out.println("天中的小时：" + calendar.get(Calendar.HOUR));
        System.out.println("小时中的分：" + calendar.get(Calendar.MINUTE));
        System.out.println("分中的秒：" + calendar.get(Calendar.SECOND));
    }

    /**
     * 日期格式化不推荐使用 DateFormat.getDateTimeInstance().format
     * 原生的推荐使用 java.text.SimpleDateFormat 或者 java 8 的 java.time.format.DateTimeFormatter
     * 或者第三方的日期API库，比如 Apache DateFormatUtils、hutool DateUtil
     */
    @Test
    public void testDateFormat1() {
        // 获取指定日期
        Calendar calendar = Calendar.getInstance();
        calendar.set(1993, Calendar.SEPTEMBER, 8, 7, 5, 8);
        Date testDate = calendar.getTime();

        String batchNo = DateFormat.getDateTimeInstance().format(testDate);
        // 日期格式样式和时间格式样式默认的语言环境不同，所格式化的结果也不同，可能开发环境正常，测试环境或者正式环境不正常。
        // 比如开发环境的结果为：1993-9-8 7:05:08
        // 而亲测有一个到测试环境的结果为：Sep 8, 1993 7:05:08 AM
        System.out.println(batchNo);
    }

    @Test
    public void testSimpleDateFormat() {
        DateTime dateTime = DateUtil.parse("1993/08/25 13:08:08");
        SimpleDateFormat dtf = new SimpleDateFormat("yyyy‐MM‐dd HH:mm:ss");
        String format = dtf.format(dateTime);
        System.out.println(format);// 1993‐08‐25 13:08:08
    }

    /**
     * 日常开发，经常需要对日期格式化，不能使用大写的 YYYY。
     * 因为 YYYY 是基于周来计算年的，它指向当天所在周属于的年份，一周从周日开始算起，周六结束，只要本周跨年，
     * 那么这一周就算下一年的了。正确姿势是使用 yyyy 格式、
     */
    @Test
    public void testFormatYYYY() {
        // 获取指定日期
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, Calendar.DECEMBER, 31, 20, 30, 40);
        Date testDate = calendar.getTime();

        // java.util.Date 转为 java.time.LocalDateTime
        Instant instant = testDate.toInstant();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        System.out.println("格式化前：2021-12-31 20:30:40");

        //---------------------------YYYY-----------------------------------------------
        String pattern1 = "YYYY‐MM‐dd HH:mm:ss";
        SimpleDateFormat dtf = new SimpleDateFormat(pattern1);
        // 原生 SimpleDateFormat YYYY‐MM‐dd HH:mm:ss 格式化后：2022‐12‐31 20:30:40
        System.out.println("原生 SimpleDateFormat " + pattern1 + " 格式化后：" + dtf.format(testDate));

        String format = DateFormatUtils.format(testDate, pattern1);
        // Apache DateFormatUtils YYYY‐MM‐dd HH:mm:ss 格式化后：2022‐12‐31 20:30:40
        System.out.println("apache DateFormatUtils " + pattern1 + " 格式化后：" + format);

        String formatTime = DateUtil.format(testDate, pattern1);
        // hutool DateUtil YYYY‐MM‐dd HH:mm:ss 格式化后：2022‐12‐31 20:30:40
        System.out.println("hutool DateUtil " + pattern1 + " 格式化后：" + formatTime);

        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(pattern1);
        String localDateTimeFormat = localDateTime.format(timeFormatter);
        // DateTimeFormatter YYYY‐MM‐dd HH:mm:ss 格式化后：2022-12-31 20:30:40
        System.out.println("DateTimeFormatter " + pattern1 + " 格式化后：" + localDateTimeFormat);

        //---------------------------yyyy-----------------------------------------------
        String pattern2 = "yyyy‐MM‐dd HH:mm:ss";
        dtf = new SimpleDateFormat(pattern2);
        // 原生 SimpleDateFormat yyyy‐MM‐dd HH:mm:ss 格式化后：2021‐12‐31 20:30:40
        System.out.println("原生 SimpleDateFormat " + pattern2 + " 格式化后：" + dtf.format(testDate));

        format = DateFormatUtils.format(testDate, pattern2);
        // apache DateFormatUtils yyyy‐MM‐dd HH:mm:ss 格式化后：2021‐12‐31 20:30:40
        System.out.println("apache DateFormatUtils " + pattern2 + " 格式化后：" + format);

        formatTime = DateUtil.format(testDate, pattern2);
        // hutool DateUtil yyyy‐MM‐dd HH:mm:ss 格式化后：2021‐12‐31 20:30:40
        System.out.println("hutool DateUtil " + pattern2 + " 格式化后：" + formatTime);

        timeFormatter = DateTimeFormatter.ofPattern(pattern2);
        localDateTimeFormat = localDateTime.format(timeFormatter);
        // DateTimeFormatter yyyy‐MM‐dd HH:mm:ss 格式化后：2021‐12‐31 20:30:40
        System.out.println("DateTimeFormatter " + pattern2 + " 格式化后：" + localDateTimeFormat);
    }

    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(date.toString().substring(0, 10));
    }
}
