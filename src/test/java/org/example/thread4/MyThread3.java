package org.example.thread4;

/**
 * 1、isAlive 方法的功能是判断当前的线程是否处于活动状态，即线程已经启动且尚未终止，线程处于正在运行或准备开始运行的状态，就认为线程是"存活"的。
 * 2、sleep 方法的作用是在指定的毫秒数内让当前"正在执行的线程"休眠(暂停执行)。
 * 3、getId 方法的作用是取得线程的唯一标识。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/27 15:52
 */
public class MyThread3 extends Thread {

    public MyThread3(String threadName) {
        this.setName(threadName);
    }

    @Override
    public void run() {
        try {
            //run 方法中一律推荐使用 Thread.currentThread() 获取当前线程后再调用 getName、getId 等方法.
            System.out.println("[run] getName=" + Thread.currentThread().getName() + " , getId=" + Thread.currentThread().getId());
            System.out.println("[run] " + Thread.currentThread().getName() + " 执行任务开始...");
            Thread.sleep(2000);
            System.out.println("[run] " + Thread.currentThread().getName() + " 执行任务结束...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        MyThread3 myThread3 = new MyThread3("A");
        myThread3.start();
        int second = 60;
        for (int i = 0; i < second; i++) {
            Thread.sleep(1000);
            boolean alive = myThread3.isAlive();
            System.out.println("[main] 线程 A 当前是否存活：" + alive);
            if (!alive) {
                System.out.println("[main] 线程 A 运行结束.");
                break;
            }
        }
        System.out.println("[main] getName=" + Thread.currentThread().getName() + " , getId=" + Thread.currentThread().getId());

        // [run] getName=A , getId=12
        // [run] A 执行任务开始...
        // [main] 线程 A 当前是否存活：true
        // [run] A 执行任务结束...
        // [main] 线程 A 当前是否存活：false
        // [main] 线程 A 运行结束.
        // [main] getName=main , getId=1
    }
}