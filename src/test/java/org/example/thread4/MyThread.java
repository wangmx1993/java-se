package org.example.thread4;

import cn.hutool.core.thread.ThreadUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

/**
 * 普通线程调用 - 给线程命名
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/10/31 16:34
 */
public class MyThread extends Thread {

    private static final Logger log = LoggerFactory.getLogger(MyThread.class);

    @Override
    public synchronized void run() {
        log.info(" 任务开始: " + Thread.currentThread().getName());
        ThreadUtil.safeSleep(1000 + new Random().nextInt(3000));
        log.info(" 结束：" + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            MyThread myThread = new MyThread();
            // 直接使用Thread的setName方法即可命名
            myThread.setName("武当梯云纵线程" + i);
            myThread.start();
        }
        // [10:52:19:703] [INFO] - [武当梯云纵线程2] - org.example.thread4.MyThread.run(MyThread.java:22) -  任务开始: 武当梯云纵线程2
        // [10:52:19:703] [INFO] - [武当梯云纵线程4] - org.example.thread4.MyThread.run(MyThread.java:22) -  任务开始: 武当梯云纵线程4
        // [10:52:19:703] [INFO] - [武当梯云纵线程3] - org.example.thread4.MyThread.run(MyThread.java:22) -  任务开始: 武当梯云纵线程3
        // [10:52:19:703] [INFO] - [武当梯云纵线程1] - org.example.thread4.MyThread.run(MyThread.java:22) -  任务开始: 武当梯云纵线程1
        // [10:52:19:703] [INFO] - [武当梯云纵线程0] - org.example.thread4.MyThread.run(MyThread.java:22) -  任务开始: 武当梯云纵线程0
        // [10:52:22:089] [INFO] - [武当梯云纵线程4] - org.example.thread4.MyThread.run(MyThread.java:24) -  结束：武当梯云纵线程4
        // [10:52:22:593] [INFO] - [武当梯云纵线程2] - org.example.thread4.MyThread.run(MyThread.java:24) -  结束：武当梯云纵线程2
        // [10:52:22:613] [INFO] - [武当梯云纵线程0] - org.example.thread4.MyThread.run(MyThread.java:24) -  结束：武当梯云纵线程0
        // [10:52:22:683] [INFO] - [武当梯云纵线程1] - org.example.thread4.MyThread.run(MyThread.java:24) -  结束：武当梯云纵线程1
        // [10:52:22:949] [INFO] - [武当梯云纵线程3] - org.example.thread4.MyThread.run(MyThread.java:24) -  结束：武当梯云纵线程3
    }
}