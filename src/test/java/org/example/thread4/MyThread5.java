package org.example.thread4;

/**
 * 如果检测到线程中断后，还需要继续执行 for 循环后面的代码，则使用 break 跳出循环即可；
 * 如果不需要继续执行循环后面的代码，则可以使用 return 直接返回，或者采用抛异常的方式.
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/27 15:59
 */
public class MyThread5 extends Thread {

    private int count = 2000000;

    @Override
    public void run() {
        try {
            for (int i = 0; i < count; i++) {
                //判断当前线程是否已经中断，如果已经中断，则退出。也可以用：Thread.currentThread().isInterrupted() 测试线程是否中断
                if (Thread.interrupted()) {
                    //如果检测到线程中断后，还需要继续执行 for 循环后面的代码，则使用 break; 跳出循环即可
                    //如果不需要继续执行循环后面的代码，则可以使用 return 直接返回，或者采用抛异常的方式.
                    //如果需要将线程停止的状态向上级调用传播，则可以使用抛异常的方式.
                    throw new InterruptedException();
                    //break;
                    //return;
                }
                System.out.println("[" + Thread.currentThread().getName() + "] " + i);
            }
            System.out.println("[" + Thread.currentThread().getName() + "] 运行结束.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        MyThread5 myThread = new MyThread5();
        myThread.start();
        int sleep = 30;
        System.out.println("[" + Thread.currentThread().getName() + "] " + sleep + " 毫秒后停止线程.");
        Thread.sleep(sleep);
        myThread.interrupt();
        System.out.println("[" + Thread.currentThread().getName() + "] " + sleep + " 运行结束.");
    }
}
