package org.example;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * java.lang.String API 测试类
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/12/24 19:04
 */
public class StringTest {

    /**
     * String 类型基本知识
     */
    @Test
    public void basicTest1() {
        String s = "Hello";//jvm看到 "Hello ",在string池创建对象存储它，并将其引用返回给 s
        //jvm看到 "Java"，在string池创建新的string对象存储它，再把新建的string对象的引用返回给s。
        //而原先的 "Hello "仍然在string池内没有消失，等待垃圾回收，Hello 是不能被修改的。
        s = "Java";

        //因为常量池中已经有 Hello 对象，所以此时 s1 引用会直接指向它
        String s1 = "Hello";
        //首先 jvm 会为 xyz 字符串在 String 常量池中创建一个对象
        //然后 new String 又会在堆内存中创建对象（非常量池），所以一共是两个对象
        String x = new String("xyz");
    }

    /**
     * toString()、String.valueOf、(String)强转 的区别
     * <p>
     * toString() 可能会抛空指针异常
     * 1、因为 Object 类里已有 toString() 方法，所以 Java 对象都可以调用此方法，但必须保证 object 不是 null，否则抛出 NullPointerException。
     * 2、包装类型有 toString() 方法，可以使用 toString() 也可以使用  String.valueOf，但包封装类型也无法强转。
     * <p>
     * String.valueOf() 对象为 null 时，返回字符串'null'，推荐使用
     * 1、String.valueOf(Object obj)) 方法不会出现空指针异常，而且是静态的方法，直接通过 String调 用即可，当 obj 为 null 时，会返回字符串 'null'。
     * 2、基本类型没有 toString() 方法，也无法强转，所以推荐 String.valueOf 方法转为字符串类型。
     * <p>
     * (String)强转，不推荐使用
     * 1、(String)强转 不确定类型时，推荐使用 instanceof 做类型检查，否则容易抛出 ClassCastException 异常。
     * 2、(String)强转的对象为 null 时，也会强转成功，同一返回 null，比如：Object obj1 = null;String str1 = (String) obj1;
     */
    @Test
    public void testValueOf() {
        Object s1 = "88888";
        Object s2 = 77777;

        //88888
        System.out.println((String) s1);
        if (s2 instanceof String) {
            System.out.println((String) s2);
        } else {
            //77777 非字符串类型!
            System.out.println(s2 + " 非字符串类型!");
            String valueOf = String.valueOf(s2);
            //77777
            System.out.println(valueOf);
        }

        Integer integer = 9999;
        String string = integer.toString();
        String valueOf = String.valueOf(integer);
        //9999,9999
        System.out.println(string + "," + valueOf);

        Object obj1 = null;
        String str1 = (String) obj1;
        //null
        System.out.println(str1);
    }


    /**
     * replace(CharSequence target, CharSequence replacement)
     * 将与字面目标序列匹配的字符串的每个子字符串替换为指定的字面替换序列。
     */
    @Test
    public void replaceTest() {
        String s = "万里'长城'！";
        String replace = s.replace("'", "");
        System.out.println(s);//万里'长城'！
        System.out.println(replace);//万里长城！
    }

    /**
     * replaceAll(String regex, String replacement)
     * 用给定的字符串替换与给定的正则表达式匹配的此字符串的每个子字符串
     */
    @Test
    public void replaceAllTest() {
        String s = "万里'长城'！";
        String replace = s.replaceAll("['！]", "");
        System.out.println(s);//万里'长城'！
        System.out.println(replace);//万里长城
    }

    /**
     * replaceFirst(String regex, String replacement)
     * 用给定的替换替换与给定的 regular expression匹配的此字符串的第一个子字符串。
     */
    @Test
    public void replaceFirstAllTest() {
        String s = "万里'长城'！";
        String replace = s.replaceFirst("['！]", "");
        System.out.println(s);//万里'长城'！
        System.out.println(replace);//万里长城'！
    }

    /**
     * String[] split(String regex)
     * 1: 将此字符串分割为给定的 regular 表达式的匹配。
     * 2: 没有匹配时,返回长度为1的数组,元素为源内容
     * 3、结果中不会再保留 regex 分隔符
     */
    @Test
    public void testSplit1() {
        String s = "万里'长城'！长又长";
        String[] replace = s.split("['！]");
        System.out.println(Arrays.asList(replace));//[万里, 长城, , 长又长]
    }

    /**
     * split(String regex, int limit) 将这个字符串拆分为给定的 regular expression的匹配。
     */
    @Test
    public void testSplit2() {
        String s = "万里'长城'！长又长";
        String[] replace = s.split("['！]", 2);
        //[万里, 长城'！长又长]
        System.out.println(Arrays.asList(replace));
        System.out.println(Long.MAX_VALUE);
        System.out.println("9223372036854775807".length());
        System.out.println(new Date().getTime());
    }

    @Test
    public void testSplit3() {
        String s1 = "001001 测试单位-001001";
        String s2 = "001001";
        String s3 = "";
        String[] replace1 = s1.split(" ");
        String[] replace2 = s2.split(" ");
        String[] replace3 = s3.split(" ");

        // [001001, 测试单位-001001]
        System.out.println(Arrays.asList(replace1));
        // [001001],001001
        System.out.println(Arrays.asList(replace2) + "," + replace2[0]);
        // []
        System.out.println(Arrays.asList(replace3));
    }

    @Test
    public void testSplit4() {
        String reason = "";
        String text = "nested exception is java.sql.SQLIntegrityConstraintViolationException: ORA-01400: cannot insert NULL into (\"KF_ELEMENT\".\"BAS_AGENCY_INFO\".\"AGENCY_ID\")";
        String[] split = text.split("ORA[-][0-9]{5}:\\s*");
        if (split != null && split.length > 1) {
            reason = split[1];
        }
        // cannot insert NULL into ("KF_ELEMENT"."BAS_AGENCY_INFO"."AGENCY_ID")
        System.out.println(reason);
    }

    /**
     * String format(String format, Object... args)
     * %s ：格式化字符串（String）输出。因为可以将任意类型都作为字符串形式输出，素以参数可以是任意类型
     * %S ：表示将字符串以大写形式输出
     * %n：表示换行
     */
    @Test
    public void format1() {
        //3.14159
        System.out.println(String.format("%s", 3.14159));

        //可以支持多个参数，%s、%S 相当于参数的占位符，参数值按顺序填入
        Integer id = 990;
        String name = "张三丰WuDang";
        //id=990,name=张三丰WUDANG
        System.out.println(String.format("id=%s,name=%S", id, name));

        //可以在%s之间插入变量编号，1$表示第一个参数，2$表示第2个参数，以此类推
        //id=990，name=张三丰WuDang
        System.out.println(String.format("id=%2$s，name=%1$s", name, id));

        List<String> list = new ArrayList<>();
        list.add("中国");
        list.add("美国");
        //[中国, 美国]
        System.out.println(String.format("%s", list));

        String[] strings = {"秦", "时", "明月"};
        //秦时
        System.out.println(String.format("%s%s", strings));
        //秦时明月
        System.out.println(String.format("%s%s%s", strings));
    }

    /**
     * %b: 格式化 boolean 类型输出
     */
    @Test
    public void format2() {
        //）参数必须是布尔型
        boolean isLogin = false;
        //isLogin = false
        System.out.println(String.format("isLogin = %b", isLogin));
    }

    /**
     * %d 表示将整数格式化为10进制整数
     * %o 表示将整数格式化为8进制整数
     * %x 表示将整数格式化为16进制整数
     * %X 表示将整数格式化为16进制整数，并且字母变成大写形式
     */
    @Test
    public void format3() {
        //）注意参数必须是整形
        Integer[] integers = new Integer[]{20, 35};
        //20、35, 24、43, 14、23, 14、23
        System.out.println(String.format("%d、%d, ", integers[0], integers[1]));
        System.out.println(String.format("%o、%o, ", integers[0], integers[1]));
        System.out.println(String.format("%x、%x, ", integers[0], integers[1]));
        System.out.println(String.format("%X、%X", integers[0], integers[1]));

        System.out.println();
        //如下表示输出结果长度最低不能少于两位，否则前面用 0 补齐
        System.out.printf("%02d、%02d、%02d", 1, 22, 333);//01、22、333。
    }

    /**
     * static String format(String format, Object... args)	使用指定的格式字符串和参数返回格式化的字符串。
     * %e 表示以科学技术法输出浮点数
     * %E 表示以科学技术法输出浮点数，并且为大写形式
     * %f 表示以十进制格式化输出浮点数
     * %.1f 表示保留小数点1位，%.2f 表示保留小数点2位，以此类推
     * 注意参数必须是浮点型、如果是其它类型则报错
     */
    @Test
    public void format4() {
        Float salary = 9500.89456f;
        Double money = 999889.89656;

        //9.500895e+03、9.998899e+05
        System.out.println(String.format("%e、%e", salary, money));
        //9.500895E+03、9.998899E+05
        System.out.println(String.format("%E、%E", salary, money));
        //9500.89、999889.8966
        System.out.println(String.format("%.2f、%.4f", salary, money));

        //没有什么是字符串输出不了的：9500.895,999889.89656
        System.out.println(String.format("%s,%s", salary, money));
    }

    /**
     * %t 表示格式化日期/时间类型
     * %T 日期/时间的大写形式
     * <p>
     * %t、%T 之后用特定的字母表示不同的输出格式：
     * y 表示输出日期的年份（2位数的年，如2019的19）
     * Y 表示输出日期的年份（4位数的年，如 2019）
     * m 表示输出日期的月份，如 06
     * d 表示输出日期的日号，如 21
     * b 表示输出月份的中文名称，如 六月
     * H 表示输出时间的时（24进制）
     * I 表示输出时间的时（12进制）
     * M 表示输出时间的分
     * S 表示输出时间的秒
     * L 表示输出时间的毫秒
     * p 表示输出时间的上午或下午信息
     * A 表示星期几的全称
     * a 表示星期几的简称
     * c 表示日期时间的详细信息，如：星期日 六月 21 21:25:41 CST 2020
     * </p>
     *
     * <p>
     * 常见日期组合：
     * D 表示 "%tm/%td/%ty" 的组合
     * F 表示 "%tY-%tm-%td" 的组合（常用）
     * R 表示 "%tH:%tM" 的组合
     * T 表示 "%tH:%tM:%tS" 的组合（常用）
     * r 表示 "%tI:%tM:%tS %Tp" 的组合（常用）
     * </p>
     */
    @Test
    public void format5() {
        Date date = new Date();
        long dateTime = date.getTime();
        /**
         * 1、1$ 表示取后面的第一个参数，2$ 表示取后面的第二个参数，以此类推，放在 %t 的中间。
         * 2、因为格式中有6个 %t 占位符，后面的参数只有两个，所以必须指定 变量编号
         * 3、Date 类型和 long 类型的毫秒值都可以作为日期参数
         * 4、输出：20-12-26，2020/12/26
         */
        System.out.println(String.format("%1$ty-%1$tm-%1$td，%2$tY/%2$tm/%2$td", date, dateTime));

        //每一个 %t 都相当于一个占位符
        //输出：2020/六月/21
        System.out.println(String.format("%tY/%tB/%td", date, date, date));

        //输出：06/21/20、2020-06-21
        System.out.println(String.format("%tD、%tF", dateTime, date));

        //输出：2020-06-21 21:18:44.571(下午)
        System.out.println(String.format("%1$tF %1$tH:%1$tM:%1$tS.%1$tL(%1$tp)", date));

        //常用写法。输出：2020-12-26 15:00:19
        System.out.println(String.format("%1$tF %1$tT", date));

        //常用写法。输出：2020-06-21 21:24:59 星期日
        System.out.println(String.format("%1$tF %1$tT %1$tA", date));

        //常用写法：输出：2020-06-21 09:25:22 下午 星期日
        System.out.println(String.format("%1$tF %1$tr %1$ta", date));

    }
}
