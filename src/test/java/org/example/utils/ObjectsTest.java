package org.example.utils;

import org.junit.Test;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.Objects;

/**
 * java.util.Objects jdk 原生对象工具类
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/6/12 10:31
 */
public class ObjectsTest {

    /**
     * boolean equals(Object a, Object b): 比较两个对象是否相等,(a == b) || (a != null && a.equals(b))
     */
    @Test
    public void equals() {
        System.out.println(Objects.equals("哈哈", "哈哈"));//true
        System.out.println(Objects.equals(889000, 889000));//true
        System.out.println(Objects.equals(null, 889000));//false
        System.out.println(Objects.equals(null, null));//true
    }

    /**
     * boolean deepEquals(Object a, Object b): 在 equals 的基础上可以深度对比数组的各个元素是否相等
     */
    @Test
    public void deepEquals() {
        Integer[] integers1 = {1, 2, 3000, 8888};
        Integer[] integers2 = {1, 2, 3000, 8888};

        System.out.println(Objects.equals(integers1, integers2));//false
        System.out.println(Objects.deepEquals(integers1, integers2));//true
    }

    /**
     * int compare(T a, T b, Comparator<? super T> c)：如果参数相同返回 0，否则返回 c.compare(a, b) 。因此，如果两个参数都是{@code null}则返回0。
     * 注意，如果其中一个参数是 null ，则 NullPointerException 可能被抛出，也可能不被抛出，这取决于{@link Comparator}为 null 选择的排序策略
     */
    @Test
    public void compare1() {
        int compare1 = Objects.compare(100, 200, Comparator.naturalOrder());
        int compare2 = Objects.compare(300, 200, Comparator.naturalOrder());
        int compare3 = Objects.compare(null, 35, Comparator.nullsLast(Comparator.naturalOrder()));

        System.out.println(compare1);//-1
        System.out.println(compare2);//1
        System.out.println(compare3);//1
    }

    /**
     * 比较两个人谁的工资高
     */
    @Test
    public void compare2() {
        //Person12(String pid, String name, Integer age, Float salary, LocalDate birthday, Boolean isMarry)
        Person12 person1 = new Person12("11Q", "张三", 33, 15009F, LocalDate.now(), true);
        Person12 person2 = new Person12("21D", "李斯", 23, 7800.8F, LocalDate.now(), false);

        int compare = Objects.compare(person1, person2, new Comparator<Person12>() {
            @Override
            public int compare(Person12 o1, Person12 o2) {
                if (Objects.equals(o1.getSalary(), o2.getSalary())) {
                    return 0;
                }
                if (o1.getSalary() == null) {
                    return -1;
                }
                if (o2.getSalary() == null) {
                    return 1;
                }
                return o1.getSalary() - o2.getSalary() > 0 ? 1 : -1;
            }
        });
        System.out.println(compare);//1
    }

    /**
     * int hashCode(Object o)：返回非 null 的哈希代码，o 为 null 时，返回0。o != null ? o.hashCode() : 0
     * int hash(Object... values)：为多个对象生成 哈希值，Arrays.hashCode(values)。
     */
    @Test
    public void hashCodeTest() {
        String[] strings1 = {"a", "b", "c"};
        String[] strings2 = {"a", "b", "c"};

        int hash = Objects.hash(strings1, strings2);
        System.out.println(hash);//-1646592051

        System.out.println(Objects.hashCode(strings1));//1031980531
        System.out.println(Objects.hashCode(strings2));//721748895
    }

    /**
     * boolean isNull(Object obj): 如果对为 null ，则返回 true,否则返回 false。return obj == null;
     * boolean nonNull(Object obj)：如果对不为 null ，则返回 true,否则返回 false。return obj != null;
     * T requireNonNull(T obj)：检查指定的对象引用是否为 null，此方法主要用于在方法和构造函数中进行参数验证。
     * 不为 null 时，返回原对象，否则 throw new NullPointerException();
     * T requireNonNull(T obj, String message)：可以自定义空指针异常信息，throw new NullPointerException(message);
     */
    @Test
    public void requireNonNull() {
        String s1 = null;
        Objects.requireNonNull(s1, "主键不能为空！");
    }

    /**
     * T requireNonNull(T obj, Supplier<String> messageSupplier)：如果对象为 null，则从 Supplier 接口函数中获取空指针信息。
     */
    @Test
    public void requireNonNull2() {
        String code = null;
        Objects.requireNonNull(code, () -> {
            String errMsg = "编码【code】不能为空！";
            return errMsg;
        });
        System.out.println(code);
    }
}
