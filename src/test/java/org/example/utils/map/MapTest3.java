package org.example.utils.map;

import cn.hutool.json.JSONUtil;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Java 中 Map 根据 Key 排序
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2023/9/22 14:53
 */
public class MapTest3 {

    /**
     * 可以通过 TreeMap 对 Map 的 Key 进行排序。
     * TreeMap 是有序的键值对集合，它实现了 SortedMap 接口。
     */
    @Test
    public void test1_1() {
        Map<String, String> map = new TreeMap<>();
        map.put("d", "444");
        map.put("b", "222");
        map.put("a03TY01", "333");
        map.put("a01TY01", "111");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            // a01TY01=111
            // a03TY01=333
            // b=222
            // d=444
            System.out.println(entry.getKey() + "=" + entry.getValue());
        }
    }

    @Test
    public void test1_2() {
        Map<String, String> map = new HashMap<>();
        map.put("d", "444");
        map.put("b", "222");
        map.put("a03TY01", "333");
        map.put("a01TY01", "111");
        map.put("d02P98", "中国");
        map.put("da2P98", "中国人");

        TreeMap<String, String> treeMap = new TreeMap<>(map);

        // {a03TY01=333, b=222, d=444, a01TY01=111, d02P98=中国, da2P98=中国人}
        System.out.println(map);
        // {a01TY01=111, a03TY01=333, b=222, d=444, d02P98=中国, da2P98=中国人}
        System.out.println(treeMap);
    }

    /**
     * 可以通过 Collections 的 sort()方法对 Map 的 Key 集合进行排序。
     */
    @Test
    public void test2() {
        Map<String, String> map = new HashMap<>();
        map.put("d", "444");
        map.put("b", "222");
        map.put("a03TY01", "333");
        map.put("a01TY01", "111");
        map.put("d02P98", "中国");
        map.put("da2P98", "中国人");

        List<String> sortKeys = new ArrayList<>(map.keySet());
        Collections.sort(sortKeys);
        for (String key : sortKeys) {
            // a01TY01:111
            // a03TY01:333
            // b:222
            // d:444
            // d02P98:中国
            // da2P98:中国人
            System.out.println(key + ":" + map.get(key));
        }
    }

    /**
     * LinkedHashMap 按照插入顺序排序
     */
    @Test
    public void test3() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("d", "444");
        map.put("b", "222");
        map.put("a03TY01", "333");
        map.put("a01TY01", "111");
        map.put("d02P98", "中国");
        map.put("da2P98", "中国人");

        for (Map.Entry<String, String> entry : map.entrySet()) {
            // d=444
            // b=222
            // a03TY01=333
            // a01TY01=111
            // d02P98=中国
            // da2P98=中国人
            System.out.println(entry.getKey() + "=" + entry.getValue());
        }
    }

    /**
     * List<Map> 结构，list 元素根据 map 中的key进行排序。
     * 更多集合排序可以参考：https://gitee.com/wangmx1993/apache-kafka/blob/master/src/test/java/com/wmx/apachekafka/jdk/ComparatorTest.java
     */
    @Test
    public void test4() {
        Map<String, Object> map1 = new LinkedHashMap<>();
        Map<String, Object> map2 = new LinkedHashMap<>();
        Map<String, Object> map3 = new LinkedHashMap<>();

        map1.put("code", "001002");
        map1.put("name", "财政厅");

        map2.put("code", "001003");
        map2.put("name", "农业厅");

        map3.put("code", "001001");
        map3.put("name", "教育厅");

        List<Map<String, Object>> dataList = new ArrayList<>();
        dataList.add(map1);
        dataList.add(map2);
        dataList.add(map3);

        dataList = dataList.stream().sorted(Comparator.nullsLast(Comparator.comparing(item -> String.valueOf(item.get("code"))))).collect(Collectors.toList());

        // [
        //     {
        //         "code": "001001",
        //         "name": "教育厅"
        //     },
        //     {
        //         "code": "001002",
        //         "name": "财政厅"
        //     },
        //     {
        //         "code": "001003",
        //         "name": "农业厅"
        //     }
        // ]
        System.out.println(JSONUtil.toJsonPrettyStr(dataList));

    }


}
