package org.example.utils;

import java.time.LocalDate;
import java.util.Map;

/**
 * 原生方式对象克隆。
 * 1、自定义 Java bean，必须实现{@link Cloneable} 接口
 * 2、重写 Object 的 {@link Object#clone()} 方法，调用 super.clone();
 * 3、后期 Java bean 调用 clone() 方法即可克隆对象
 * 4、此种方式属于浅克隆，即只对基本数据类型有效，对 list、Map、Set、java bean 等对象无效，即克隆出来的新对象仍然与源对象公用这些引用类型。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/1/31 17:34
 */
public class Person12 implements Cloneable {
    private String pid;
    private String name;
    private Integer age;
    private Float salary;
    private LocalDate birthday;
    private Boolean isMarry;
    private Map<String, Object> extMap;

    public Person12() {
    }

    //Person12 person1 = new Person12("11Q", "张三", 33, 15009F, LocalDate.now(), true);
    public Person12(String pid, String name, Integer age, Float salary, LocalDate birthday, Boolean isMarry) {
        this.pid = pid;
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.birthday = birthday;
        this.isMarry = isMarry;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Boolean getMarry() {
        return isMarry;
    }

    public void setMarry(Boolean marry) {
        isMarry = marry;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Map<String, Object> getExtMap() {
        return extMap;
    }

    public void setExtMap(Map<String, Object> extMap) {
        this.extMap = extMap;
    }


    @Override
    public String toString() {
        return "Person1{" +
                "pid='" + pid + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                ", birthday=" + birthday +
                ", isMarry=" + isMarry +
                ", extMap=" + extMap +
                '}';
    }
}
