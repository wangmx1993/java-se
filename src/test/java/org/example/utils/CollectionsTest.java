package org.example.utils;

import org.junit.Test;

import java.util.*;

/**
 * Collections 集合工具类
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/10/22 14:50
 */
public class CollectionsTest {

    /**
     * 获取可序列化的空 Lie、Set、Map，返回的结果不可变，否则异常：java.lang.UnsupportedOperationException
     */
    @Test
    public void emptyTest() {
        List emptyList = Collections.EMPTY_LIST;
        // []
        System.out.println(emptyList);

        Map emptyMap = Collections.EMPTY_MAP;
        // {}
        System.out.println(emptyMap);

        Set emptySet = Collections.EMPTY_SET;
        // []
        System.out.println(emptySet);
    }

    /**
     * addAll(Collection<? super T> c, T... elements) 将所有指定的元素添加到指定的集合。
     */
    @Test
    public void addAllTest() {
        List<String> list1 = new ArrayList<>();
        list1.add("明清");
        list1.add("元");

        Collections.addAll(list1, "唐", "宋");
        //[明清, 元, 唐, 宋]
        System.out.println(list1);
    }

    /**
     * disjoint(Collection<?> c1, Collection<?> c2) 如果两个指定的集合没有共同的元素，则返回 true 。
     */
    @Test
    public void disjointTest() {
        List<String> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();
        list1.add("明清");
        list2.add("宋");
        //true
        System.out.println(Collections.disjoint(list1, list2));

        list2.clear();
        list2.add("明清");
        //false
        System.out.println(Collections.disjoint(list1, list2));
    }

    /**
     * fill(List<? super T> list, T obj) 用指定的元素代替指定列表的所有元素。
     */
    @Test
    public void fillTest() {
        List<String> list1 = new ArrayList<>();
        list1.add("唐");
        list1.add("宋");
        list1.add("唐");

        Collections.fill(list1, "大唐");
        //[大唐, 大唐, 大唐]
        System.out.println(list1);
    }

    /**
     * max(Collection<? extends T> coll) 根据其元素的 自然顺序返回给定集合的最大元素。
     * min(Collection<? extends T> coll) 根据其元素的 自然顺序返回给定集合的最小元素。
     * <p>
     * ["a", "h, "x"] -> max=x，min=a
     * ["afd", "ac", "x"] -> max=x，min=ac
     * ["b", "B", "x"] -> max=x，min=B
     * [45, 33, 78] -> max=78，min=33
     */
    @Test
    public void maxTest() {
        List<String> list1 = new ArrayList<>();
        list1.add("45");
        list1.add("33");
        list1.add("78");

        String max = Collections.max(list1);
        String min = Collections.min(list1);
        //78,33
        System.out.println(max + "," + min);
    }

    /**
     * max(Collection<? extends T> coll, Comparator<? super T> comp)
     * 根据指定的比较器引发的顺序返回给定集合的最大元素。
     * min(Collection<? extends T> coll, Comparator<? super T> comp) 根据指定的比较器引发的顺序返回给定集合的最小元素。
     */
    @Test
    public void maxTest2() {
        List<Map<String, Object>> data = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>();
        Map<String, Object> map2 = new HashMap<>();
        Map<String, Object> map3 = new HashMap<>();
        Map<String, Object> map4 = new HashMap<>();

        map1.put("code", null);
        map2.put("code", "22");
        map3.put("code", "0151");
        map4.put("code", "251");

        data.add(map1);
        data.add(map2);
        data.add(map3);
        data.add(map4);

        /**
         * java.util.Comparator 是一个函数式接口，需要实现 int compare(T o1, T o2) 方法
         * 1、比较两个参数（o1,o2）的顺序，当第一个参数小于、等于或大于第二个参数时，返回负整数、零或正整数
         * 2、compare 返回正整数，则认为o1大于o2，同时o1的值作为参数传递给下一次比较时的o2
         * 3、compare 返回负整数，则认为o1小于o2，同时o2的值作为参数传递给下一次比较时的o2
         */
        Map<String, Object> max = Collections.max(data, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                Object code1 = o1.get("code");
                Object code2 = o2.get("code");
                if (code1 == null) {
                    //返回-1，表示o2大
                    return -1;
                }
                if (code2 == null) {
                    //返回-1，表示o1大
                    return 1;
                }
                int i = Integer.parseInt(code1.toString()) - Integer.parseInt(code2.toString());
                return i;
            }
        });
        //{code=251}
        System.out.println(max);
    }

    /**
     * max：获取集合中的最大值，使用自定义的排序规则
     * min：获取集合中的最小值，使用自定义的排序规则
     */
    @Test
    @SuppressWarnings("all")
    public void maxTest3() {
        class Student {
            private String name;
            private Integer age;

            public Student(String name, int age) {
                this.name = name;
                this.age = age;
            }

            public Integer getAge() {
                return age;
            }

            @Override
            public String toString() {
                return "Student{name='" + name + ", age=" + age + "}";
            }
        }
        List<Student> studentList = new ArrayList<Student>();
        studentList.add(new Student("张三", 34));
        studentList.add(new Student("李四", 44));
        studentList.add(new Student("王五", 24));

        //Collections.min 也是同理，第二个参数是 java.util.Comparator 接口
        Student max = Collections.max(studentList, new Comparator<Student>() {
            //实现方法 compare，表示集合中前一个元素(o1) 与后一个元素(o2) 比较
            // o1 等于 o2 返回 0，o1 大于 o2 返回 正整数，o1 小于 o2 返回复整数
            @Override
            public int compare(Student o1, Student o2) {
                //Integer 实现的 compareTo 方法的底层就是 (x < y) ? -1 : ((x == y) ? 0 : 1)
                int flag = o1.getAge().compareTo(o2.getAge());
                //等价于：o1.getAge() < o2.getAge() ? -1 : o1.getAge() == o2.getAge() ? 0 : 1
                return flag;
            }
        });
        // Student{name='李四, age=44}
        System.out.println(max);
    }

    /**
     * replaceAll(List<T> list, T oldVal, T newVal) 将列表中一个指定值的所有出现替换为另一个
     * 替换成功返回 true，否则返回 false
     */
    @Test
    public void replaceAllTest1() {
        List<String> list1 = new ArrayList<>();
        list1.add("45");
        list1.add("33");
        list1.add("78");

        boolean b = Collections.replaceAll(list1, "33", "133");
        //true,[45, 133, 78]
        System.out.println(b + "," + list1);
    }

    @Test
    public void replaceAllTest2() {
        List<String> list1 = new ArrayList<>();
        list1.add("45");
        list1.add("33");
        list1.add("78");
        list1.add("33");
        list1.add("33");
        boolean b = Collections.replaceAll(list1, "33", "133");
        //true,[45, 133, 78, 133, 133]
        System.out.println(b + "," + list1);
    }

    /**
     * reverse(List<?> list) 反转指定列表中元素的顺序。
     */
    @Test
    public void reverseTest() {
        // List 的元素是 POJO 对象也是同理可以反序
        List<String> list1 = new ArrayList<>();
        list1.add("45");
        list1.add("033");
        list1.add("078");

        Collections.reverse(list1);
        //[078, 033, 45]
        System.out.println(list1);
    }

    /**
     * shuffle(List<?> list) 使用默认的随机源随机排列指定的列表。
     * 所有排列都以大致相等的可能性发生。洗牌
     */
    @Test
    public void shuffleTest() {
        List<String> list1 = new ArrayList<>();
        list1.add("45");
        list1.add("033");
        list1.add("078");
        list1.add("18");
        list1.add("1458");

        for (int i = 0; i < 10; i++) {
            Collections.shuffle(list1);
            // 每次输出的顺序都不一样
            // [18, 1458, 078, 45, 033]
            // [078, 1458, 18, 45, 033]
            // [45, 18, 033, 078, 1458]
            // [033, 45, 078, 18, 1458]
            // [45, 078, 033, 18, 1458]
            // [033, 1458, 078, 18, 45]
            // [45, 1458, 18, 078, 033]
            // [45, 033, 078, 18, 1458]
            // [033, 1458, 45, 078, 18]
            // [18, 1458, 033, 45, 078]
            System.out.println(list1);
        }
    }

    /**
     * shuffle(List<?> list, Random rnd) 使用指定的随机源随机排列指定的列表。
     */
    @Test
    public void shuffleTest2() {
        List<String> list1 = new ArrayList<>();
        list1.add("45");
        list1.add("033");
        list1.add("078");
        list1.add("18");
        list1.add("1458");

        //因为随机源一样，所以运行多次随机结果一样
        for (int i = 0; i < 10; i++) {
            Collections.shuffle(list1, new Random(100));
            System.out.println(list1);
        }
    }

    /**
     * sort(List<T> list) 根据其元素的natural ordering对指定的列表进行排序。
     */
    @Test
    public void sortTest1() {
        Integer[] integers = {45, 56, 33, 4545, 233};
        List<Integer> list = Arrays.asList(integers);
        Collections.sort(list);

        // [33, 45, 56, 233, 4545]
        System.out.println(list);

        // Comparator.reverseOrder()：返回一个与 自然排序相反的比较器。
        Collections.sort(list, Comparator.reverseOrder());
        // [4545, 233, 56, 45, 33]
        System.out.println(list);
    }

    @Test
    public void sortTest2() {
        List<String> fields = Arrays.asList("biz_key", "agency_id", "fiscal_year", "agency_code", "is_end", "mof_div_code", "zip");
        Collections.sort(fields);
        // [agency_code, agency_id, biz_key, fiscal_year, is_end, mof_div_code, zip]
        System.out.println(fields);
    }

    /**
     * sort(List<T> list, Comparator<? super T> c) 根据指定的比较器引起的顺序对指定的列表进行排序。
     * 1、完全类似上面的 max、min 的使用方式。
     * 2、下面演示顺序排列，null 值在最前面
     * 3、倒序也是同理，可以直接 反转列表中元素的顺序
     */
    @Test
    public void sortTest3() {
        //随机造数据，其中包含 null 元素
        List<Map<String, Object>> data = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Map<String, Object> map1 = new HashMap<>();
            int nextInt = new Random().nextInt(1000);
            map1.put("code", nextInt % 5 == 0 ? null : nextInt < 100 ? "0" + nextInt : nextInt + "");
            data.add(map1);
        }

        //[{code=837}, {code=247}, {code=933}, {code=322}, {code=781}, {code=551}, {code=511}, {code=017}, {code=null}, {code=058}]
        System.out.println(data);

        Collections.sort(data, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                Object code1 = o1.get("code");
                Object code2 = o2.get("code");
                if (code1 == null) {
                    //返回-1，表示o2大
                    return -1;
                }
                if (code2 == null) {
                    //返回-1，表示o1大
                    return 1;
                }
                int i = Integer.parseInt(code1.toString()) - Integer.parseInt(code2.toString());
                return i;
            }
        });

        //[{code=null}, {code=017}, {code=058}, {code=247}, {code=322}, {code=511}, {code=551}, {code=781}, {code=837}, {code=933}]
        System.out.println(data);

        //反转指定列表中元素的顺序
        Collections.reverse(data);

        //[{code=933}, {code=837}, {code=781}, {code=551}, {code=511}, {code=322}, {code=247}, {code=058}, {code=017}, {code=null}]
        System.out.println(data);
    }

    /**
     * <T> void sort(List<T> list,Comparator<? super T> c)：根据指定的比较器对指定的列表进行排序。
     */
    @Test
    public void sortTest4() {
        class Student {
            private String name;
            private Integer age;

            public Student(String name, int age) {
                this.name = name;
                this.age = age;
            }

            public Integer getAge() {
                return age;
            }

            @Override
            public String toString() {
                return "Student{name='" + name + ", age=" + age + "}";
            }
        }
        List<Student> studentList = new ArrayList<Student>();
        studentList.add(new Student("张三", 34));
        studentList.add(new Student("李四", 44));
        studentList.add(new Student("王五", 24));

        // [Student{name='张三, age=34}, Student{name='李四, age=44}, Student{name='王五, age=24}]
        System.out.println(studentList);
        // 按年龄从小到大排列
        Collections.sort(studentList, new Comparator<Student>() {
            public int compare(Student o1, Student o2) {
                return (o1.getAge() < o2.getAge()) ? -1 : ((o1.getAge() == o2.getAge()) ? 0 : 1);
            }
        });
        // [Student{name='王五, age=24}, Student{name='张三, age=34}, Student{name='李四, age=44}]
        System.out.println(studentList);
    }

    /**
     * void swap(List<?> list, int i, int j)： 交换指定列表中指定位置的元素。(如果指定的位置相等，调用此方法将保持不变）
     */
    @Test
    public void swapTest() {
        List<Long> longList = new ArrayList<Long>();
        longList.add(34L);
        longList.add(77L);
        longList.add(56L);
        longList.add(83L);
        //[34, 77, 56, 83]
        System.out.println(longList);

        //交换索引 1，2 元素的位置
        Collections.swap(longList, 1, 2);

        //[34, 56, 77, 83]
        System.out.println(longList);
    }

    /**
     * List<T> unmodifiableList(List<? extends T> list):返回指定列表不可修改的视图，即返回的 List 只能读，无法修改
     * Map<K,V> unmodifiableMap(Map<? extends K,? extends V> m)：返回不可修改的 Map
     */
    @Test
    public void testUnmodifiableList() {
        List<Long> longList = new ArrayList<Long>();
        longList.add(34L);
        longList.add(77L);
        longList.add(56L);
        longList.add(83L);

        List<Long> descList = new ArrayList<Long>(longList.size());
        //[34, 77, 56, 83]
        System.out.println(longList);

        descList = Collections.unmodifiableList(longList);
        //只能读.[34, 77, 56, 83], descList.get(0)=34
        System.out.println(descList + ", descList.get(0)=" + descList.get(0));

        //修改抛异常：java.lang.UnsupportedOperationException
        descList.add(98L);
    }

    // Collections 类返回的对象，如：emptyList() / singletonList(T o) 等都是 immutable list，不可对其进行添加或者删除元素的操作.
    // 如果查询无结果，返回 Collections.emptyList() 空集合对象，调用方一旦在返回的集合中进行了添加元素的操作，就会触发UnsupportedOperationException异常.
    @Test
    public void testEmptyList() {
        List<Object> emptyList = Collections.emptyList();
        // []
        System.out.println(emptyList);

        Set<String> singleton = Collections.singleton("1");
        // [1]
        System.out.println(singleton);
    }

    /**
     * List<T> synchronizedList(List<T> list):返回由指定列表支持的同步（线程安全）列表。为了保证串行访问，重要的是通过返回的列表完成对后台列表的所有访问。
     * 在迭代时，用户必须在返回的列表上手动同步 synchronized (list)
     * 同理还有：Map<K,V> synchronizedMap(Map<K,V> m)、<T> Set<T> synchronizedSet(Set<T> s)、Collection<T> synchronizedCollection(Collection<T> c)等
     */
    @Test
    @SuppressWarnings("all")
    public void testSynchronizedList() {
        List<Long> longList = new ArrayList<Long>();
        longList.add(34L);
        longList.add(77L);
        longList.add(56L);
        longList.add(83L);

        //[34, 77, 56, 83]
        System.out.println(longList);

        List<Long> syncLongList = Collections.synchronizedList(longList);
        synchronized (syncLongList) {
            Iterator<Long> iterator = syncLongList.iterator();
            while (iterator.hasNext()) {
                //34、77、56、83、
                System.out.print(iterator.next() + "、");
            }
        }
    }

}
