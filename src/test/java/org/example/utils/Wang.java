package org.example.utils;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.digest.DigestUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/2/16 10:30
 */
public class Wang {

    @Test
    public void test() {
        String s = "pos_sala + grad_sala + nation_reg_sub + local_reg_sub + merit_pay";
        String[] split = s.split(" ");
        System.out.println(Arrays.asList(split));

        List<String> integerList = Arrays.asList(split);
        String join = StringUtils.join(integerList, " ");
        System.out.println(join);
    }

    @Test
    public void test1() {
        LocalDate localDate = LocalDate.now();
        LocalDateTime localDateTime = LocalDateTime.now();

        System.out.println(localDate);
        System.out.println(localDateTime);
        System.out.println(localDateTime.getNano());
    }

    @Test
    public void test2() {
        Set<String> keySet = new HashSet<>();
        keySet.add("age");

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("id", 100);
        dataMap.put("name", "华安");
        dataMap.put("age", 33);

//        Iterator<String> iterator = dataMap.keySet().iterator();
//        while (iterator.hasNext()){
//            String key = iterator.next();
//            if(!keySet.contains(key)) {
//                dataMap.remove(key);
//            }
//        }
//
//        for(Map.Entry<String, Object> entry : dataMap.entrySet()) {
//            String key = entry.getKey();
//            if(!keySet.contains(key)) {
//                dataMap.remove(key);
//            }
//        }


        System.out.println(dataMap);
        System.out.println(dataMap.get("xx"));

    }

    @Test
    public void test3() {
        List<String> list = new ArrayList<>();
        list.add("YY1");
        list.add("YY2");
        list.add("YY3");

        System.out.println(list);
        Object[] objects = list.toArray();
        for (Object obj : objects) {
            System.out.println(obj);
        }
    }

    @Test
    public void test4() {

    }

    @Test
    public void test5() {
        List<Map<String, Object>> billNoList = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>();
        map1.put("agency_code", "1000");

        Map<String, Object> map2 = new HashMap<>();
        map2.put("agency_code", "1001");

        Map<String, Object> map3 = new HashMap<>();
        map3.put("agency_code", "1002");

        billNoList.add(map1);
        billNoList.add(map2);
        billNoList.add(map3);

        System.out.println(billNoList);

        billNoList.stream().forEach(item -> {
            item.put("agency_code", null);
        });
        System.out.println(billNoList);

    }

    @Test
    public void test6() {
        Integer intObj = new Integer(0);
        Class<Number> numberClass = Number.class;
        String s = "1";
        System.out.println(CharSequence.class.isAssignableFrom(s.getClass()));

    }


    public static boolean isIntNumber(String str) {
        // 判断正整数正则
        Pattern patternInteger = Pattern.compile("^\\d+(\\.\\d+)?$");
        return patternInteger.matcher(str).matches();
    }

    /**
     * 判断字符串是否为正整数或者浮点数 1.11 -> true 1 -> true -1 -> false 1a -> false
     *
     * @param str
     * @return
     */
    public static boolean isNumeric(String str) {
        // 判断浮点数正则
        Pattern patternFloat = Pattern.compile("^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*))$");
        return patternFloat.matcher(str).matches();
    }

    public static void main(String[] args) {
        String simpleUUID = IdUtil.fastSimpleUUID();
        System.out.println(simpleUUID);
        System.out.println(SecureUtil.md5(simpleUUID));
        System.out.println(DigestUtil.md5Hex(simpleUUID));

        System.out.println(cn.hutool.core.codec.Base64.encode(simpleUUID));
    }

}
