package org.example.thread2;

import cn.hutool.core.thread.ThreadUtil;

/**
 * 实例变量数据线程间共享
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/27 15:39
 */
public class MyThread extends Thread {
    private int count = 1000;

    /**
     * synchronized 为 run 方法添加对象锁
     */
    @Override
    public synchronized void run() {
        int total = 2;
        for (int i = 0; i < total && count > 0; i++) {
            count--;
            //取线程名称不要使用 this.getName();
            ThreadUtil.safeSleep(1000);
            System.out.println(Thread.currentThread().getName() + " 窗口售 1 张票，剩余：" + count);
        }
    }

    public static void main(String[] args) {
        // 1、多个线程执行同一个任务
        int count = 30;
        MyThread myThread = new MyThread();
        for (int i = 0; i < count; i++) {
            Thread thread = new Thread(myThread, "B" + i);
            thread.start();
        }
        // B2 窗口售 1 张票，剩余：999
        // B2 窗口售 1 张票，剩余：998
        // B1 窗口售 1 张票，剩余：997
        // B1 窗口售 1 张票，剩余：996
        // B0 窗口售 1 张票，剩余：995
        // B0 窗口售 1 张票，剩余：994
    }
}