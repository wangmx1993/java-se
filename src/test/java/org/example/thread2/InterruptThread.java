package org.example.thread2;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 使用Thread提供的interrupt()方法，因为该方法虽然不会中断一个正在运行的线程，但是它可以使一个被阻塞的线程抛出一个中断异常，
 * 从而使线程提前结束阻塞状态，退出堵塞代码。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2020/10/31 13:27
 */
public class InterruptThread extends Thread {
    @Override
    public void run() {
        try {
            for (int i = 0; i < 10; i++) {
                System.out.println((i) + ":" + Thread.currentThread().getName());
                //随机休眠 1-4秒，模拟此时在进行其它操作
                Thread.currentThread().sleep(1000 + new Random().nextInt(3000));
            }
        } catch (InterruptedException e) {
            //因为中断线程后会抛出 InterruptedException，所以就退出 for 循环，然后线程结束
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            InterruptThread interruptThread = new InterruptThread();
            interruptThread.start();
            //5秒之后，中断线程
            TimeUnit.SECONDS.sleep(5);
            interruptThread.interrupt();
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
