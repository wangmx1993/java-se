package org.example.thread2;

import cn.hutool.core.thread.ThreadUtil;

/**
 * 实例变量数据线程间不共享
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/27 15:36
 */
public class MyThread1 extends Thread {

    private int count = 1000;

    public MyThread1(String threadName) {
        this.setName(threadName);
    }

    @Override
    public void run() {
        int total = 2;
        for (int i = 0; i < total && count > 0; i++) {
            count--;
            // 取线程名称不要使用 this.getName();
            ThreadUtil.safeSleep(1000);
            System.out.println(Thread.currentThread().getName() + " 用户完成 1 个任务，剩余：" + count);
        }
    }

    public static void main(String[] args) {
        int count = 3;
        for (int i = 0; i < count; i++) {
            MyThread1 myThread = new MyThread1("A" + i);
            myThread.start();
        }
        // A2 用户完成 1 个任务，剩余：999
        // A0 用户完成 1 个任务，剩余：999
        // A1 用户完成 1 个任务，剩余：999
        // A0 用户完成 1 个任务，剩余：998
        // A2 用户完成 1 个任务，剩余：998
        // A1 用户完成 1 个任务，剩余：998
    }
}