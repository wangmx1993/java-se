package org.example.thread2.login;

/**
 * 使用两个独立的线程模拟两个用户登陆
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/27 15:48
 */
public class MyThread extends Thread {
    private String userName;
    private String password;

    public MyThread(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    @Override
    public void run() {
        // 两个不同的线程模拟两个不同的两个用户独立登陆
        LoginServlet.getInstance().doPost(userName, password);
    }

    public static void main(String[] args) {
        // 两个不同的线程模拟两个不同的两个用户独立登陆
        MyThread myThread1 = new MyThread("a", "123456");
        MyThread myThread2 = new MyThread("root", "admin");
        myThread1.start();
        myThread2.start();

        // a,123456	org.example.thread2.login.LoginServlet@5dd97d6e
        // root,admin	org.example.thread2.login.LoginServlet@5dd97d6e
        // root = admin
        // root = admin
    }
}