package org.example.hutool.utilTest;

import cn.hutool.core.date.DateField;
import cn.hutool.core.lang.Console;
import cn.hutool.core.util.RandomUtil;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 随机工具-RandomUtil
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/4 11:44
 */
public class RandomUtilTest {

    /**
     * BigDecimal randomBigDecimal(): 获得指定范围内的随机数[0, 1)
     * BigDecimal randomBigDecimal(BigDecimal limit): 获得指定范围内的随机数 [0,limit)
     * BigDecimal randomBigDecimal(BigDecimal min, BigDecimal max):获得指定范围[min,max)内的随机数
     * boolean randomBoolean() ：获得随机 Boolean 值
     * byte[] randomBytes(int length):随机 bytes
     * char randomChar()：随机字母或数字，小写
     * char randomChar(String baseString)：从 baseString 中随机选取一个字符
     * DateTime randomDate(Date baseDate, DateField dateField, int min, int max)：以给定日期为基准，随机产生一个日期
     * * baseDate  基准日期
     * * dateField 偏移的时间字段，例如时、分、秒等.{@link DateField}
     * * min       偏移最小量，可以为负数表示过去的时间（包含）
     * * max       偏移最大量，可以为负数表示过去的时间（不包含）
     * DateTime randomDay(int min, int max): 以当天为基准，随机产生一个日期
     * * min 偏移最小天，可以为负数表示过去的时间（包含）
     * * max 偏移最大天，可以为负数表示过去的时间（不包含）
     */
    @Test
    public void testRandomBigDecimal() {
        System.out.println(RandomUtil.randomBigDecimal());//如 0.7710627286213504
        System.out.println(RandomUtil.randomBigDecimal(new BigDecimal("1000")));//355.5710498295456
        //如 2846.478138290069
        System.out.println(RandomUtil.randomBigDecimal(new BigDecimal("1000"), new BigDecimal("10000")));
        System.out.println(RandomUtil.randomBoolean());//如 true
        System.out.println(RandomUtil.randomChar());// 如 x
        System.out.println(RandomUtil.randomChar("唐宋元明清"));
        System.out.println(RandomUtil.randomDate(new Date(), DateField.YEAR, 10, 30));// 如 2037-04-04 11:57:22
        Console.log("randomDay={}", RandomUtil.randomDay(-30, 30));//如 randomDay=2021-04-11 11:59:08
    }

    /**
     * double randomDouble() :获得随机数[0, 1)
     * double randomDouble(double limit)：获得指定范围内的随机数 [0,limit)
     * double randomDouble(double min, double max)： 获得指定范围[min,max)内的随机数
     * double randomDouble(int scale, RoundingMode roundingMode): 获得指定范围[0, 1)内的随机数
     * * scale        保留小数位数
     * * roundingMode 保留小数的模式 {@link RoundingMode},如 HALF_UP 四舍五入
     * double randomDouble(double min, double max, int scale, RoundingMode roundingMode):获得指定范围内的随机数，并指定小数保留位数
     * double randomDouble(double limit, int scale, RoundingMode roundingMode)：获得指定范围内的随机数，并指定小数保留位数
     */
    @Test
    public void testRandomDouble() {
        System.out.println(RandomUtil.randomDouble());//如 0.6790818271078333
        System.out.println(RandomUtil.randomDouble(10000));//如 3017.192892363888
        System.out.println(RandomUtil.randomDouble(100, 1000));// 如 129.58292389714913
        System.out.println(RandomUtil.randomDouble(2, RoundingMode.HALF_UP));// 如 0.01
        System.out.println(RandomUtil.randomDouble(1000, 10000, 2, RoundingMode.HALF_UP));//如 8572.42
    }

    /**
     * <T> T randomEle(List<T> list)：随机获得列表中的元素
     * <T> T randomEle(List<T> list, int limit)：随机获得列表中的元素
     * <T> T randomEle(T[] array): 随机获得数组中的元素
     * <T> T randomEle(T[] array, int limit): 随机获得数组中的元素
     * List<T> randomEleList(List<T> source, int count): 随机获得列表中的一定量的元素，返回List,不会获取重复位置的元素
     * List<T> randomEles(List<T> list, int count): 随机获得列表中的一定量元素,会获取重复位置的元素
     * Set<T> randomEleSet(Collection<T> collection, int count)：随机获得列表中的一定量的不重复元素，返回Set
     */
    @Test
    public void testRandomEle() {
        String[] arrs = {"秦汉", "隋唐", "明清", "魏晋", "南北朝"};
        List<String> asList = Arrays.asList(arrs);
        List<String> eleList = RandomUtil.randomEleList(asList, 3);
        List<String> randomEles = RandomUtil.randomEles(asList, 3);
        Set<String> eleSet = RandomUtil.randomEleSet(asList, 4);

        System.out.println(RandomUtil.randomEle(arrs));// 如 魏晋
        System.out.println(RandomUtil.randomEle(asList, 3));// 如 隋唐
        System.out.println(eleList);//如 [南北朝, 明清, 魏晋]
        System.out.println(randomEles);//如 [明清, 秦汉, 明清]
        System.out.println(eleSet);//如 [隋唐, 南北朝, 魏晋, 明清]
    }

    /**
     * int randomInt() :获得随机数int值
     * int randomInt(int limit)：获得指定范围内的随机数 [0,limit)
     * int randomInt(int min, int max)：获得指定范围[min,max)内的随机数，min 不能等于 max,否则异常
     * int[] randomInts(int length)：创建指定长度的随机索引
     */
    @Test
    public void testRandomInt() {
        Console.log(RandomUtil.randomInt());// 如 -117321989、823605703
        Console.log(RandomUtil.randomInt(1000));// 如 56
        Console.log(RandomUtil.randomInt(1000, 10000));// 如 5443

        int[] randomInts = RandomUtil.randomInts(10);
        Console.log(randomInts);//如 [5, 6, 3, 7, 2, 9, 1, 8, 0, 4]
    }

    /**
     * randomLong：随机生成整形数字
     * char randomNumber()：随机数字字符，数字为0~9单个数字
     * String randomNumbers(int length)：获得一个只包含数字的随机字符串
     * String randomString(int length)： 获得一个随机的字符串（只包含数字和小写字母）
     * String randomString(String baseString, int length)：从 baseString 中获得一个随机的字符串
     * String randomStringUpper(int length)：获得一个随机的字符串（只包含数字和大写字符）
     * String randomStringWithoutStr(int length, String elemData)：获得一个随机的字符串（只包含数字和字符） 并排除指定字符串(elemData)
     */
    @Test
    public void testRandomLong() {
        Console.log(RandomUtil.randomLong());// 如 2942198353087821396
        Console.log(RandomUtil.randomLong(1000));// 如 96
        Console.log(RandomUtil.randomLong(1000, 10000));// 如 6508
        System.out.println(RandomUtil.randomNumber());
        System.out.println(RandomUtil.randomNumbers(11));// 如 44371019982

        System.out.println(RandomUtil.randomString(20));//如 g03583qedh512kbmq9u7
        System.out.println(RandomUtil.randomString("随机工具-RandomUtil", 20));//如 n随Rt随RlU工-RiRooUna机m
        System.out.println(RandomUtil.randomStringUpper(20));// 如 8KFFD2NXVXAJFK76JD9X
    }

}
