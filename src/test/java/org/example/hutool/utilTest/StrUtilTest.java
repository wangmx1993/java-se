package org.example.hutool.utilTest;

import cn.hutool.core.util.StrUtil;
import org.junit.Test;

/**
 * 字符串工具-StrUtil
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/16 14:44
 */
public class StrUtilTest {

    /**
     * String addPrefixIfNot(CharSequence str, CharSequence prefix): 如果给定字符串不是以 prefix 开头的，在开头补充 prefix
     * String addSuffixIfNot(CharSequence str, CharSequence suffix): 如果给定字符串不是以 suffix 结尾的，在尾部补充 suffix
     */
    @Test
    public void testAddPrefixIfNot() {
        String addPrefixIfNot = StrUtil.addPrefixIfNot("1,2,3", "(");
        String addSuffixIfNot = StrUtil.addSuffixIfNot(addPrefixIfNot, ")");
        addSuffixIfNot = StrUtil.addSuffixIfNot(addSuffixIfNot, ")");

        System.out.println(addSuffixIfNot);//(1,2,3)
    }

    /**
     * String blankToDefault(CharSequence str, String defaultStr): 如果字符串是 null、或者为空、或者是空白字符串，则返回指定默认字符串，否则返回字符串本身。
     * String cleanBlank(CharSequence str)：清理空白字符，前后中间都会清除掉.
     */
    @Test
    public void testBlankToDefault() {
        System.out.println(StrUtil.blankToDefault("大唐", "T"));//大唐
        System.out.println(StrUtil.blankToDefault("      ", "T"));//T
        System.out.println(StrUtil.blankToDefault("", "T"));//T
        System.out.println(StrUtil.blankToDefault(null, "T"));//T

        System.out.println(StrUtil.cleanBlank(" 大 汉     "));//大汉
    }


    /**
     * String brief(CharSequence str, int maxLength): 将给定字符串，变成 "xxx...xxx" 形式的字符串
     * String center(CharSequence str, final int size): 居中字符串，两边补充空白字符串，如果指定长度小于字符串，则返回原字符串
     * String center(CharSequence str, final int size, char padChar)：当 str 长度不足 size 时，两边使用 padChar 字符补充
     * String center(CharSequence str, final int size, CharSequence padStr)
     */
    @Test
    public void testBrief() {
        System.out.println(StrUtil.brief("432524199308257451", 14));//4325241...7451
        System.out.println(StrUtil.center("蚩尤后裔", 10));//   蚩尤后裔
        System.out.println(StrUtil.center("蚩尤后裔", 10, '*'));//***蚩尤后裔***
        System.out.println(StrUtil.center("蚩尤后裔", 10, "VV"));//VVV蚩尤后裔VVV

        // 0000000000
        System.out.println(StrUtil.center("", 10, "0"));
    }

    /**
     * 补充字符串以满足最小长度
     * <p>
     * String padPre(CharSequence str, int minLength, CharSequence padStr)
     * * str       字符串
     * * minLength 最小长度
     * * padStr    补充的字符
     *
     * <pre>
     * StrUtil.padPre(null, *, *);//null
     * StrUtil.padPre("1", 3, "ABC");//"AB1"
     * StrUtil.padPre("123", 2, "ABC");//"12"
     * </pre>
     *
     * @return 补充后的字符串
     */
    @Test
    public void padPreTest() {
        // null
        System.out.println(StrUtil.padPre(null, 10, "*"));
        // AB1
        System.out.println(StrUtil.padPre("1", 3, "ABC"));
        // 12
        System.out.println(StrUtil.padPre("123", 2, "ABC"));
    }

    /**
     * String padAfter(CharSequence str, int minLength, CharSequence padStr)
     * 补充字符串以满足最小长度
     * * str       字符串，如果为{@code null}，直接返回null
     * * minLength 最小长度
     * * padStr    补充的字符
     *
     * <pre>
     * StrUtil.padAfter(null, *, *);//null
     * StrUtil.padAfter("1", 3, "ABC");//"1AB"
     * StrUtil.padAfter("123", 2, "ABC");//"23"
     * </pre>
     */
    @Test
    public void padAfterTest() {
        // null
        System.out.println(StrUtil.padAfter(null, 10, "*"));
        // 1AB
        System.out.println(StrUtil.padAfter("1", 3, "ABC"));
        // 23
        System.out.println(StrUtil.padAfter("123", 2, "ABC"));
        // 0000000000
        System.out.println(StrUtil.padAfter("", 10, "0"));
    }

    /**
     * StringBuilder builder(CharSequence... strs): 创建 StringBuilder 对象
     * String concat(boolean isNullToEmpty, CharSequence... strs)：连接多个字符串为一个
     * * isNullToEmpty 是否将 null 转为""
     * * strs          字符串数组
     * * 返回连接后的字符串
     */
    @Test
    public void testBuilder() {
        StringBuilder builder = StrUtil.builder("大唐帝国");
        builder.reverse();
        System.out.println(builder.toString());//国帝唐大


        String concat = StrUtil.concat(true, "select * from ", "VW_PERSON ");
        concat = StrUtil.concat(true, concat, "where ");
        concat = StrUtil.concat(true, concat, "per_id = '7787878TY6767=89' ");
        System.out.println(concat);//select * from VW_PERSON where per_id = '7787878TY6767=89'
    }

    /**
     * int compare(final CharSequence str1, final CharSequence str2, final boolean nullIsLess): 比较两个字符串，用于排序
     * int compareIgnoreCase(CharSequence str1, CharSequence str2, boolean nullIsLess): 忽略大小写
     * 1、str1 大于 str2 ，返回 >0；str1 小于 str2 ，返回 <0；str1 等于 str2 ，返回 =0；
     * 2、nullIsLess: null 是否小于非空值,true 表示 null 小于非空值
     */
    @Test
    public void testCompare() {
        System.out.println(StrUtil.compare("a", "x", true));
        System.out.println(StrUtil.compare("z", "a", true));
        System.out.println(StrUtil.compare("b", "b", true));
    }

    /**
     * boolean contains(CharSequence str, CharSequence searchStr) : 指定字符串是否在字符串中出现过
     * boolean containsIgnoreCase(CharSequence str, CharSequence testStr)：忽略大小写
     * boolean containsAny(CharSequence str, CharSequence... testStrs)：查找指定字符串是否包含指定字符串列表中的任意一个字符串
     * boolean containsAnyIgnoreCase(CharSequence str, CharSequence... testStrs)：忽略大小写
     * boolean containsBlank(CharSequence str)：给定字符串是否包含空白符（空白符包括空格、制表符、全角空格和不间断空格）
     */
    @Test
    public void testContainsAny() {
        System.out.println(StrUtil.containsAny("大唐荣耀", "大行", "唐"));//true
        System.out.println(StrUtil.containsBlank("大 提琴"));//true
    }

    /**
     * int count(CharSequence content, CharSequence strForSearch): 统计指定内容中包含指定字符串的数量
     * String[] cut(CharSequence str, int partLength): 将字符串切分为 N 等份
     */
    @Test
    public void testCount() {
        String source = "等哈就看和大家按时当今时代圣诞节活动就的大好机会和大家按时大家";
        System.out.println(StrUtil.count(source, "大家"));
    }

    /**
     * String format(CharSequence template, Object... params)
     * 格式化文本, {} 表示占位符<br>
     * 此方法只是简单将占位符 {} 按照顺序替换为参数<br>
     * 如果想输出 {} 使用 \\转义 { 即可，如果想输出 {} 之前的 \ 使用双转义符 \\\\ 即可<br>
     * 例：<br>
     * 通常使用：format("this is {} for {}", "a", "b") =》 this is a for b<br>
     * 转义{}： format("this is \\{} for {}", "a", "b") =》 this is \{} for a<br>
     * 转义\： format("this is \\\\{} for {}", "a", "b") =》 this is \a for b<br>
     * <p>
     * template 文本模板，被替换的部分用 {} 表示，如果模板为null，返回"null"
     */
    @Test
    public void formatTest() {
        String template = "{}爱{}，{}!";
        String str = StrUtil.format(template, "我", "你", 2022);
        // 我爱你，2022!
        System.out.println(str);
    }


}
