package org.example.hutool.utilTest;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import org.example.uitls.BasicDataRuleUtil;
import org.junit.Test;
import org.springframework.util.Assert;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * 反射工具-ReflectUtil
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/8 20:32
 */
public class ReflectUtilTest {

    /**
     * Method[] getMethods(Class<?> beanClass): 获得一个类中所有方法列表，包括其父类中的方法
     * 1、私有方法都能获取到
     */
    @Test
    public void getMethodsTest1() {
        Method[] methods = ReflectUtil.getMethods(Stack.class);
        for (Method method : methods) {
            System.out.println(method.getName());
        }
        System.out.println("--------------------------------------------------");
        // E push(E item)
        Method push = ReflectUtil.getMethod(Stack.class, "push", Object.class);
        //public java.lang.Object java.util.Stack.push(java.lang.Object)
        System.out.println(push == null ? null : push.toString());
        // push
        System.out.println(push == null ? null : push.getName());
    }

    /**
     * Method[] getMethods(Class<?> clazz, Filter<Method> filter)
     * 1、获得指定类过滤后的Public方法列表
     * * @param clazz  查找方法的类
     * * @param filter 过滤器
     * * @return 过滤后的方法列表
     */
    @Test
    public void getMethodsTest2() {
        Method[] methods = ReflectUtil.getMethods(BasicDataRuleUtil.class, method -> {
            if (StrUtil.containsIgnoreCase(method.getName(), "agency")) {
                return true;
            }
            return false;
        });
        for (Method method : methods) {
            // agencyPersonNumNoticeRule
            // agencyTypeErrorRule
            // perEnterAgencyDateErrorRule
            System.out.println(method.getName());
        }
    }

    /**
     * Method[] getMethodsDirectly(Class<?> beanClass, boolean withSuperClassMethods)
     * 1、获得一个类中所有方法列表，直接反射获取，无缓存，包括私有方法。
     * * beanClass             类
     * * withSuperClassMethods 是否包括父类的方法列表
     */
    @Test
    public void getMethodsDirectlyTest() {
        Method[] methods = ReflectUtil.getMethodsDirectly(BasicDataRuleUtil.class, false, false);
        for (Method method : methods) {
            // agencyPersonNumNoticeRule
            // perOnWorkErrorRule
            // agencyTypeErrorRule
            // perEnterAgencyDateErrorRule
            // privateCheckPersonSexCode
            System.out.println(method.getName());
        }
    }

    /**
     * Method[] getPublicMethods(Class<?> clazz)
     * 1、获得本类及其父类所有Public方法
     * Method getPublicMethod(Class<?> clazz, String methodName, Class<?>... paramTypes)1
     * 1、查找指定Public方法 如果找不到对应的方法或方法不为public的则返回{@code null}
     * * @param clazz      类
     * * @param methodName 方法名
     * * @param paramTypes 参数类型
     * List<Method> getPublicMethods(Class<?> clazz, Method... excludeMethods)
     * List<Method> getPublicMethods(Class<?> clazz, String... excludeMethodNames)
     * 1、获得指定类过滤后的Public方法列表
     * * @param clazz          查找方法的类
     * * @param excludeMethods 不包括的方法
     * * @param excludeMethodNames 不包括的方法名列表
     */
    @Test
    public void getPublicMethodsTest1() {
        Method[] methods = ReflectUtil.getPublicMethods(BasicDataRuleUtil.class);
        for (Method method : methods) {
            // agencyTypeErrorRule
            // perOnWorkErrorRule
            // agencyPersonNumNoticeRule
            // perEnterAgencyDateErrorRule
            // wait
            // wait
            // wait
            // equals
            // toString
            // hashCode
            // getClass
            // notify
            // notifyAll
            System.out.println(method.getName());
        }
    }

    /**
     * List<Method> getPublicMethods(Class<?> clazz, Filter<Method> filter)
     * 1、获得指定类过滤后的Public方法列表
     * * @param clazz  查找方法的类
     * * @param filter 过滤器，返回 true 的保留，否则舍弃。
     * * @return 过滤后的方法列表
     */
    @Test
    public void testGetPublicMethods2() {
        List<Method> methods = ReflectUtil.getPublicMethods(BasicDataRuleUtil.class, method -> {
            if (StrUtil.containsIgnoreCase(method.getName(), "agency")) {
                return true;
            }
            return false;
        });
        for (Method method : methods) {
            // agencyPersonNumNoticeRule
            // agencyTypeErrorRule
            // perEnterAgencyDateErrorRule
            System.out.println(method.getName());
        }
    }

    /**
     * Method getMethod(Class<?> clazz, String methodName, Class<?>... paramTypes):
     * 1、查找指定方法 如果找不到对应的方法则返回 null
     * 2、如果方法有参数，则必须指定参数类型。
     * 3、private 方法也能获取
     * Method getMethod(Class<?> clazz, boolean ignoreCase, String methodName, Class<?>... paramTypes)
     * * clazz      类，如果为{@code null}返回{@code null}
     * * methodName 方法名，如果为空字符串返回{@code null}
     * * paramTypes 参数类型，指定参数类型如果是方法的子类也算
     * * ignoreCase 是否忽略大小写
     * Method getMethodIgnoreCase(Class<?> clazz, String methodName, Class<?>... paramTypes)
     * 1、忽略大小写查找指定方法，如果找不到对应的方法则返回{@code null}
     * 2、此方法为精准获取方法名，即方法名和参数数量和类型必须一致，否则返回{@code null}。
     * * @param paramTypes 参数类型，指定参数类型如果是方法的子类也算
     */
    @Test
    public void getMethodTest1() {
        // E push(E item)
        Method push = ReflectUtil.getMethod(Stack.class, "push", Object.class);
        //public java.lang.Object java.util.Stack.push(java.lang.Object)
        System.out.println(push == null ? null : push.toString());
        // push
        System.out.println(push == null ? null : push.getName());
    }

    /**
     * 私有方法也能获取
     */
    @Test
    public void getMethodTest2() {
        // E push(E item)
        Method push = ReflectUtil.getMethod(BasicDataRuleUtil.class, "privateCheckPersonSexCode", Map.class);
        // private java.lang.String org.example.uitls.BasicDataRuleUtil.privateCheckPersonSexCode(java.util.Map)
        System.out.println(push == null ? null : push.toString());
        // privateCheckPersonSexCode
        System.out.println(push == null ? null : push.getName());
    }

    /**
     * Method getMethodByName(Class<?> clazz, boolean ignoreCase, String methodName)
     * 1、按照方法名查找指定方法名的方法，同名方法有重载时，只返回匹配到的第一个方法，如果找不到对应的方法则返回{@code null}
     * Method getMethodByName(Class<?> clazz, String methodName):
     * 1、按照方法名查找指定方法名的方法，同名方法有重载时，只返回匹配到的第一个方法，如果找不到对应的方法则返回{@code null}
     * Method getMethodByNameIgnoreCase(Class<?> clazz, String methodName)
     * 1、按照方法名查找指定方法名的方法(忽略大小写)，同名方法有重载时，只返回匹配到的第一个方法，如果找不到对应的方法则返回{@code null}
     * Set<String> getMethodNames(Class<?> clazz)：获得指定类中的 Public 方法名，去重重载的方法
     * 同理还有对应方法可以获取构造器以及字段
     */
    @Test
    public void getMethodByNameTest() {
        Method push = ReflectUtil.getMethodByName(Stack.class, "push");
        //public java.lang.Object java.util.Stack.push(java.lang.Object)
        System.out.println(push == null ? null : push.toString());
    }

    /**
     * T newInstance(String clazz)
     * T newInstance(Class<T> clazz, Object... params): 实例化对象
     * * clazz  类
     * * params 构造函数参数
     * T newInstanceIfPossible(Class<T> beanClass): 尝试遍历并调用此类的所有构造方法，直到构造成功并返回.
     * 对于某些特殊的接口，按照其默认实现实例化，例如：
     * * Map       -》 HashMap
     * * Collction -》 ArrayList
     * * List      -》 ArrayList
     * * Set       -》 HashSet
     */
    @Test
    public void newInstanceTest() {
        Stack stack = ReflectUtil.newInstance(Stack.class);
        Stack stack2 = ReflectUtil.newInstance("java.util.Stack");
        Map map = ReflectUtil.newInstanceIfPossible(Map.class);
        HashMap hashMap = ReflectUtil.newInstanceIfPossible(HashMap.class);

        stack.add(1);
        stack.add(2);
        stack.add(3);

        hashMap.put("code", "200");
        map.put("id", 1000);

        System.out.println(stack.pop());//3
        System.out.println(stack.pop());//2
        System.out.println(stack.pop());//1
        System.out.println(stack.isEmpty());//true
        System.out.println(stack2.size());//0

        System.out.println(map.toString());//{id=1000}
        System.out.println(hashMap);//{code=200}
    }

    /**
     * T invoke(Object obj, Method method, Object... args)：执行方法，对于用户传入参数会做必要检查，包括：
     * T invoke(Object obj, String methodName, Object... args)
     * T invokeStatic(Method method, Object... args)
     * * 1、忽略多余的参数
     * * 2、参数不够补齐默认值
     * * 3、传入参数为 null，但是目标参数类型为原始类型，做转换
     * * 4、目标方法没有返回值，强行接收的值也会为  null。
     * T setAccessible(T accessibleObject): 设置方法为可访问（私有方法可以被外部调用）
     * * T              AccessibleObject 的子类，比如 Class、Method、Field 等
     * * accessibleObject 可设置访问权限的对象，比如Class、Method、Field等
     */
    @Test
    public void invokeTest1() {
        Object object = ReflectUtil.newInstance("java.util.Stack");
        Method push = ReflectUtil.getMethodByName(Stack.class, "push");
        Method pop = ReflectUtil.getMethodByName(Stack.class, "pop");

        //对于私有方法，调用前必须设置访问权限，否则抛异常：IllegalAccessException
        ReflectUtil.setAccessible(push);
        ReflectUtil.setAccessible(pop);

        ReflectUtil.invoke(object, push, "1号已就位.");
        ReflectUtil.invoke(object, push, "2号已就位.");
        ReflectUtil.invoke(object, push, "3号已就位.");

        Object invoke1 = ReflectUtil.invoke(object, pop);
        Object invoke2 = ReflectUtil.invoke(object, pop);
        Object invoke3 = ReflectUtil.invoke(object, pop);

        System.out.println(invoke1);//3号已就位.
        System.out.println(invoke2);//2号已就位.
        System.out.println(invoke3);//1号已就位.
    }

    /**
     * 如果目标方法发生异常，ReflectUtil 也会抛出异常：UtilException 多种异常包装
     * 私有方法照样能调用
     */
    @Test
    public void invokeTest2() {
        Object object = ReflectUtil.newInstance("org.example.uitls.BasicDataRuleUtil");
        String methodName = "privateCheckPersonSexCode";
        Method privateCheckPersonSexCode = ReflectUtil.getMethod(BasicDataRuleUtil.class, methodName, Map.class);
        Assert.notNull(privateCheckPersonSexCode, methodName + " 方法未找到！");

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("agency_type_code", "100");
        dataMap.put("sex_code", "100");

        /**抛出异常：
         * cn.hutool.core.exceptions.UtilException: InvocationTargetException: null
         * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:922)
         * 	at cn.hutool.core.util.ReflectUtil.invokeStatic(ReflectUtil.java:832)
         * 	at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)
         * Caused by: java.lang.reflect.InvocationTargetException
         * 	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
         * 	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
         * 	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
         * 	at java.lang.reflect.Method.invoke(Method.java:498)
         * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:920)
         * 	... 27 more
         * Caused by: org.se.exception.BasicException: 行政单位的经费供给方式必须填写！
         * 	at org.example.uitls.BasicDataRuleUtil.agencyTypeErrorRule(BasicDataRuleUtil.java:45)
         * 	... 32 more
         */
        String invoke = ReflectUtil.invoke(object, privateCheckPersonSexCode, dataMap);
        // 人员性别编码为1或2！
        System.out.println(invoke);
        // {noticeMsg=人员性别编码为1或2！, agency_type_code=100, sex_code=100}
        System.out.println(dataMap);
    }

    @Test
    public void invokeTest3() {
        String methodName1 = "agencyPersonNumNoticeRule";
        String methodName2 = "perOnWorkErrorRule";
        String methodName3 = "perEnterAgencyDateErrorRule";

        Method method1 = ReflectUtil.getMethod(BasicDataRuleUtil.class, methodName1, Map.class);
        Method method2 = ReflectUtil.getMethod(BasicDataRuleUtil.class, methodName2, Map.class);
        Method method3 = ReflectUtil.getMethod(BasicDataRuleUtil.class, methodName3, Map.class);

        Assert.notNull(method1, methodName1 + " 方法未找到！");
        Assert.notNull(method2, methodName2 + " 方法未找到！");
        Assert.notNull(method3, methodName3 + " 方法未找到！");

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("enter_agency_date", "1919-01-05");
        dataMap.put("per_sta_code", "3");
        dataMap.put("agency_type_code", "2");
        dataMap.put("admin_staf_num", "2");

        Object invoke1 = ReflectUtil.invokeStatic(method1, dataMap);
        Object invoke2 = ReflectUtil.invokeStatic(method2, dataMap);
        Object invoke3 = ReflectUtil.invokeStatic(method3, dataMap);
        // 请检查单位的行政和政法编制数，该单位为事业单位！
        System.out.println(invoke1);
        // 在职人员的在职人员来源必须填写！
        System.out.println(invoke2);
        // 参加工作时间必须在1920年和2022之间！
        System.out.println(invoke3);
        // {noticeMsg=请检查单位的行政和政法编制数，该单位为事业单位！在职人员的在职人员来源必须填写！参加工作时间必须在1920年和2022之间！, enter_agency_date=1919-01-05, per_sta_code=3, agency_type_code=2, admin_staf_num=2}
        System.out.println(dataMap);
    }

}
