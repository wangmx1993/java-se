package org.example.hutool.utilTest;

import cn.hutool.core.lang.Console;
import cn.hutool.core.util.ArrayUtil;
import org.junit.Test;

import java.util.Map;

/**
 * 数组工具-ArrayUtil
 * 主要针对原始类型数组和泛型数组相关方案进行封装。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/5 12:58
 */
public class ArrayUtilTest {

    /**
     * boolean isEmpty(int[] array): 数组是否为空，8种基本类型都有
     * boolean isEmpty(T[] array): 数组是否为空
     * boolean hasNull(T... array): 是否包含{@code null}元素
     * boolean hasEmpty(Object... args): 是否存在{@code null}或空对象
     * int emptyCount(Object... args): 计算{@code null}或空元素对象的个数
     * boolean isNotEmpty(int[] array)：数组是否非空，8种基本类型都有
     * boolean isNotEmpty(T[] array)：数组是否非空
     * boolean isSorted(int[] array): 检查数组是否升序, 若传入空数组，则返回 false. 8种基本类型都有
     * boolean isSorted(T[] array)：检查数组是否升序
     * boolean isSortedDESC(T[] array)： 检查数组是否降序
     * <T extends Comparable<? super T>> boolean isSorted(T[] array)： 检查数组是否升序，即array[i].compareTo(array[i + 1]) 《= 0，若传入空数组，则返回false
     * boolean isSortedDESC(float[] array)：检查数组是否降序，即array[i] >= array[i+1]，若传入空数组，则返回 false
     * <T extends Comparable<? super T>> boolean isSortedDESC(T[] array): 检查数组是否降序
     */
    @Test
    public void testEmpty() {
        int[] a = {};
        int[] b = null;
        int[] c = {1, 2};
        int[] d = {11, 2};
        String[] strings = {"秦汉", null, "", "隋唐"};

        Console.log(ArrayUtil.isEmpty(a), ArrayUtil.isEmpty(b), ArrayUtil.isEmpty(c));//true true false
        //true false true false
        Console.log(ArrayUtil.isSorted(a), ArrayUtil.isSorted(b), ArrayUtil.isSorted(c), ArrayUtil.isSorted(d));

        Console.log(ArrayUtil.emptyCount(strings));//2
    }

    /**
     * int[] addAll(int[]... arrays): 将多个数组合并在一起, 忽略 null 的数组，返回合并后的数组。8种基本类型都有
     * T[] addAll(T[]... arrays): 将多个数组合并在一起, 忽略 null 的数组
     * boolean contains(int[] array, int value) ：数组中是否包含元素, 8种基本类型都有
     * boolean contains(T[] array, T value)：数组中是否包含元素
     * boolean containsAll(T[] array, T... values): 数组中是否包含指定元素中的全部
     * boolean containsAny(T[] array, T... values): 数组中是否包含指定元素中的任意一个
     * containsIgnoreCase(CharSequence[] array, CharSequence value): 数组中是否包含元素，忽略大小写
     * int indexOf(boolean[] array, boolean value): 返回数组中指定元素所在位置，未找到返回-1. 8种基本类型都有
     * int indexOf(T[] array, Object value): 返回数组中指定元素所在位置，未找到返回-1
     * int indexOfIgnoreCase(CharSequence[] array, CharSequence value)：返回数组中指定元素所在位置，忽略大小写，未找到返回
     * int lastIndexOf(boolean[] array, boolean value): 返回数组中指定元素所在最后的位置，未找到返回 -1. 支持8种基本类型
     * int lastIndexOf(T[] array, Object value)
     * Object append(Object array, T... newElements): 将新元素添加到已有数组中,添加新元素会生成一个新的数组，不影响原数组。元素不能是原始类型。
     * T[] append(T[] buffer, T... newElements)：同上.
     */
    @Test
    public void testAddAll() {
        int[] a = {1, 2, 3};
        int[] b = {11, 22, 3};
        Integer[] c = {11, 22, 3};

        int[] addAll = ArrayUtil.addAll(a, b);
        Console.log(addAll);//[1, 2, 3, 11, 22, 3]

        Console.log(ArrayUtil.contains(a, 2), ArrayUtil.contains(a, 22));//true false
        Console.log(ArrayUtil.indexOf(a, 3), ArrayUtil.indexOf(b, 36));//2 -1

        Object append = ArrayUtil.append(c, 4, 55, 66, 7);
        Console.log(append);//[11, 22, 3, 4, 55, 66, 7]

        Console.log("containsAny={}", ArrayUtil.containsAny(c, 22, 77, 88));//containsAny=true
    }

    /**
     * String join(short[] array, CharSequence conjunction): 以 conjunction 为分隔符将数组转换为字符串. 8 种基础类型都有
     * String join(Object array, CharSequence conjunction):以 conjunction 为分隔符将数组转换为字符串
     * String join(T[] array, CharSequence conjunction)：同上
     * String join(T[] array, CharSequence conjunction, String prefix, String suffix)：
     * * prefix      每个元素添加的前缀，null表示不添加
     * * suffix      每个元素添加的后缀，null表示不添加
     * long max(long... numberArray): 取最大值, 支持所有数值基本类型
     * T max(T[] numberArray)
     * int min(int... numberArray)：取最小值, 支持所有数值基本类型
     * T min(T[] numberArray)
     * int[] range(int excludedEnd):  生成一个从[0,excludedEnd)的数字列表
     * int[] range(int includedStart, int excludedEnd):生成一个[includedStart,excludedEnd)范围的数字列表
     * int[] range(int includedStart, int excludedEnd, int step): step 是步进
     */
    @Test
    public void testJoin() {
        int[] id = {1, 2, 3, 4, 5, 6, 7};
        String[] ids = {"100s", "pp998", "899yu", "8989f"};

        String join = ArrayUtil.join(id, ",");
        System.out.println(join);//1,2,3,4,5,6,7

        String join1 = ArrayUtil.join(ids, ",", "'", "'");
        Console.log(join1);//'100s','pp998','899yu','8989f'

        Console.log(ArrayUtil.min(id), ArrayUtil.max(id));//1 7

        int[] range = ArrayUtil.range(20);
        Console.log(range);//[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]

        int[] range1 = ArrayUtil.range(10, 20);
        Console.log(range1);//[10, 11, 12, 13, 14, 15, 16, 17, 18, 19]

        int[] range2 = ArrayUtil.range(10, 20, 2);
        Console.log(range2);//[10, 12, 14, 16, 18]
    }

    /**
     * int[] remove(int[] array, int index):移除数组中对应位置的元素, 如果 index 小于0或者大于长度，返回新数组，原数据组不变. 支持8中基本类型
     * int[] removeEle(int[] array, int element)：移除数组中匹配到的第一个元素. 支持8中基本类型
     * T[] removeEle(T[] array, T element)
     * T[] remove(T[] array, int index)
     * T[] removeBlank(T[] array)：去除{@code null}或者""或者空白字符串 元素
     * T[] removeEmpty(T[] array)： 去除{@code null}或者"" 元素
     * T[] removeNull(T[] array)：去除{@code null} 元素
     * T[] clone(T[] array): 克隆数组，如果非数组返回{@code null}
     * int length(Object array): 获取数组长度, 如果参数为{@code null}，返回0
     * T[] newArray(Class<?> componentType, int newSize): 新建一个空数组
     */
    @Test
    public void testRemove() {
        int[] a = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
        int[] b = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19};

        int[] remove = ArrayUtil.remove(a, 5);
        Console.log(remove);//[10, 11, 12, 13, 14, 16, 17, 18, 19]

        int[] removeEle = ArrayUtil.removeEle(a, 17);
        Console.log(removeEle);//[10, 11, 12, 13, 14, 15, 16, 18, 19]

        int[] clone = ArrayUtil.clone(b);
        int[] remove1 = ArrayUtil.remove(b, 0);
        Console.log("clone={}", clone);//clone= [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
        Console.log("remove1={}", remove1);//remove1= [11, 12, 13, 14, 15, 16, 17, 18, 19]
    }

    /**
     * Object resize(Object array, int newSize): 生成一个新的重新设置大小的数组,调整大小后拷贝原数组到新数组下。扩大则占位前N个位置，其它位置补充0，缩小则截断
     * T[] resize(T[] buffer, int newSize): 同上
     * double[] reverse(double[] array)：反转数组，会变更原数组. 支持8中基本类型
     * T[] reverse(T[] array)：反转数组，会变更原数组
     * String[] nullToEmpty(String[] array)：  数组元素中的null转换为""
     */
    @Test
    public void testResize() {
        int[] a = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19};

        Object resize1 = ArrayUtil.resize(a, 20);
        Console.log(resize1);//[10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        Object resize2 = ArrayUtil.resize(a, 5);
        Console.log(resize2);//[10, 11, 12, 13, 14]

        int[] reverse = ArrayUtil.reverse(a);
        Console.log(reverse);//[19, 18, 17, 16, 15, 14, 13, 12, 11, 10]
    }

    /**
     * boolean[] shuffle(boolean[] array): 随机打乱数组顺序，会变更原数组。支持8种基本类型。
     * T[] shuffle(T[] array)：打乱数组顺序，会变更原数组
     * float[] sub(float[] array, int start, int end)：获取[start,end) 范围的子数组. 支持8种基本类型。
     * T[] sub(T[] array, int start, int end)
     * float[] swap(float[] array, int index1, int index2): 交换数组中 index1与 index2 位置的值，会改变原数组的值. 支持8种基本类型。
     * swap(T[] array, int index1, int index2)
     * T[] setOrAppend(T[] buffer, int index, T value)：元素值设置为数组的某个位置，当给定的index大于数组长度，则追加
     */
    @Test
    public void testShuffle() {
        int[] a = {19, 18, 17, 16, 15, 14, 13, 12, 11, 10};
        int[] b = {19, 18, 17, 16, 15, 14, 13, 12, 11, 10};

        int[] shuffle = ArrayUtil.shuffle(a);
        Console.log(shuffle);//如 [19, 11, 13, 16, 12, 14, 10, 17, 15, 18]
        Console.log(a);//如 [19, 11, 13, 16, 12, 14, 10, 17, 15, 18]

        int[] sub = ArrayUtil.sub(b, 3, 6);
        Console.log(sub);//[16, 15, 14]

        int[] swap = ArrayUtil.swap(b, 0, b.length - 1);
        Console.log(swap);//[10, 18, 17, 16, 15, 14, 13, 12, 11, 19]
        Console.log(b);//[10, 18, 17, 16, 15, 14, 13, 12, 11, 19]
    }

    /**
     * float[] wrap(Float... values): 原始类型数组转为包装类数组，支持8种基本类型。
     * float[] unWrap(Float... values)： 包装类数组转为原始类型数组，支持8种基本类型。
     * Object[] wrap(Object obj): 包装数组对象, obj 可以是对象数组或者基本类型数组
     * T[] distinct(T[] array): 去重数组中的元素，去重后生成新的数组，原数组不变
     * boolean equals(Object array1, Object array2): 判断两个数组是否相等，判断依据包括数组长度和每个元素都相等。元素不能是基本类型。
     * T get(Object array, int index)： 获取数组对象中指定index的值，支持负数，例如-1表示倒数第一个值。如果数组下标越界，返回null
     */
    @Test
    public void testWrap() {
        int[] a = {19, 18, 17, 16, 15, 14, 13, 12, 11, 10};
        Integer[] b = {19, 18, 19, 16, 14, 14, 13, 12, 11, 10};

        Console.log(ArrayUtil.equals(ArrayUtil.wrap(a), b));//false

        Integer[] integers = ArrayUtil.wrap(a);
        Console.log(integers);//[19, 18, 17, 16, 15, 14, 13, 12, 11, 10]

        int[] ints = ArrayUtil.unWrap(integers);
        Console.log(ints);//[19, 18, 17, 16, 15, 14, 13, 12, 11, 10]

        Integer[] distinct = ArrayUtil.distinct(b);
        Console.log(distinct);//[19, 18, 16, 14, 13, 12, 11, 10]

        Console.log(ArrayUtil.get(a, -1));//10

        Console.log(b);
        Object[] any = ArrayUtil.getAny(b, 3);
        Console.log(any);
    }

    /**
     * T[] insert(T[] buffer, int index, T... newElements):将新元素插入到到已有数组中的某个位置,不影响原数组,如果插入位置为负数，从原数组从后向前计数，若大于原数组长度，则空白处用null填充.
     * boolean isArray(Object obj): 对象是否为数组对象
     * Map<K, V> zip(K[] keys, V[] values)：映射键值
     * Map<K, V> zip(K[] keys, V[] values, boolean isOrder): 映射键值,isOrder:是否有序
     */
    @Test
    public void testZip() {
        String[] ids = {"100s", "8989f"};
        String[] idArr = {"100s", "8989f"};

        String[] insert = ArrayUtil.insert(ids, ids.length, "pp998", "899yu");
        Console.log(insert);//[100s, 8989f, pp998, 899yu]

        String[] insert2 = ArrayUtil.insert(idArr, -1, "pp998", "899yu");
        Console.log(insert2);//[100s, pp998, 899yu, 8989f]

        String[] keys = {"id", "name", "age"};
        String[] values = {"98989UYH", "华安", "33", "xxx"};
        Map<String, String> zip = ArrayUtil.zip(keys, values);
        Console.log(zip);//{name=华安, id=98989UYH, age=33}

        Map<String, String> zip2 = ArrayUtil.zip(keys, values, true);
        Console.log(zip2);//{id=98989UYH, name=华安, age=33}
    }

    /**
     * String toString(Object obj): 数组或集合转 String，与集合转字符串格式相同
     * 如果 obj 为 null，则返回 null.
     */
    @Test
    public void testToString() {
        int[] a = {19, 18, 17, 16, 15, 14, 13, 12, 11, 10};
        Integer[] b = {19, 18, 19, 16, 14, 14, 13, 12, 11, 10};
        Object[] args = {10, "好汉", "Hi"};
        // [10, 好汉, Hi]
        System.out.println(ArrayUtil.toString(args));
        // [19, 18, 19, 16, 14, 14, 13, 12, 11, 10]
        System.out.println(ArrayUtil.toString(b));
        // [19, 18, 17, 16, 15, 14, 13, 12, 11, 10}
        System.out.println(ArrayUtil.toString(a));
    }


}
