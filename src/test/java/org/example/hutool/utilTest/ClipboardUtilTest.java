package org.example.hutool.utilTest;

import cn.hutool.core.swing.clipboard.ClipboardUtil;
import org.junit.Test;

import java.awt.datatransfer.Clipboard;

/**
 * 剪贴板工具-ClipboardUtil
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/8 20:11
 */
public class ClipboardUtilTest {

    /**
     * Clipboard getClipboard():获取系统剪贴板,{@link Clipboard}
     * setStr(String text)：设置字符串文本到系统剪贴板
     * String getStr()：从系统剪贴板获取文本
     * setImage 设置图片到剪贴板
     * getImage 从剪贴板获取图片
     */
    @Test
    public void testClipboardUtil() {
        Clipboard clipboard = ClipboardUtil.getClipboard();

        ClipboardUtil.setStr("蚩尤后裔！");
        System.out.println("往系统剪贴板成功设置内容，可以去任意地方粘贴(Ctrl+v)");

        String str = ClipboardUtil.getStr();
        System.out.println(str);
    }

}
