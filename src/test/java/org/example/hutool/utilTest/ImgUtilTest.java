package org.example.hutool.utilTest;

import cn.hutool.core.img.ImgUtil;
import org.junit.Before;
import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

/**
 * 图片工具-ImgUtil
 * 针对 awt 中图片处理进行封装，这些封装包括：缩放、裁剪、转为黑白、加水印等操作。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/2 20:43
 */
public class ImgUtilTest {

    private File srcImageFile;
    private File destImageFile;

    @Before
    public void before() {
        URL url = ImgUtilTest.class.getClassLoader().getResource("data/images/1.jpg");
        String path = url.getPath();
        srcImageFile = new File(path);

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        destImageFile = new File(homeDirectory, System.currentTimeMillis() + ".jpg");
    }

    /**
     * scale(File srcImageFile, File destImageFile, float scale)
     * 缩放图像（按比例缩放），目标文件的扩展名决定目标文件类型
     * srcImageFile  源图像文件
     * destImageFile 缩放后的图像文件，扩展名决定目标类型
     * scale         缩放比例。比例大于1时为放大，小于1大于0为缩小
     */
    @Test
    public void testScale() {
        ImgUtil.scale(srcImageFile, destImageFile, 0.5f);
        System.out.println(destImageFile.toPath());
    }

    /**
     * cut(File srcImgFile, File destImgFile, Rectangle rectangle)
     * 图像切割(按指定起点坐标和宽高切割)
     * srcImgFile  源图像文件
     * destImgFile 切片后的图像文件
     * rectangle   矩形对象，表示矩形区域的x，y，width，height
     */
    @Test
    public void testCut() {
        //裁剪的矩形区域
        Rectangle rectangle = new Rectangle(1000, 100, 300, 350);
        ImgUtil.cut(srcImageFile, destImageFile, rectangle);
        System.out.println(destImageFile.toPath());
    }

    /**
     * convert(File srcImageFile, File destImageFile):图像类型转换：GIF=》JPG、GIF=》PNG、PNG=》JPG、PNG=》GIF(X)、BMP=》PNG
     */
    @Test
    public void testConvert() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destImageFile = new File(homeDirectory, System.currentTimeMillis() + ".gif");
        ImgUtil.convert(srcImageFile, destImageFile);
        System.out.println(destImageFile.toPath());
    }

    /**
     * gray(File srcImageFile, File destImageFile) ：彩色转为黑白
     */
    @Test
    public void testGray() {
        ImgUtil.gray(srcImageFile, destImageFile);
        System.out.println(destImageFile.toPath());
    }

    /**
     * binary(File srcImageFile, File destImageFile)
     * 彩色转为黑白二值化图片，根据目标文件扩展名确定转换后的格式
     */
    @Test
    public void testBinary() {
        ImgUtil.binary(srcImageFile, destImageFile);
        System.out.println(destImageFile.toPath());
    }

    /**
     * 给图片添加文字水印
     * pressText(File imageFile, File destFile, String pressText, Color color, Font font, int x, int y, float alpha)
     * imageFile 源图像文件
     * destFile  目标图像文件
     * pressText 水印文字
     * color     水印的字体颜色
     * font      {@link Font} 字体相关信息，如果默认则为{@code null}
     * x         修正值。 0 表示默认在中间，正负偏移量相对于中间偏移
     * y         修正值。 0 表示默认在中间，正负偏移量相对于中间偏移
     * alpha     透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
     */
    @Test
    public void testPressText() {
        String text = "版权所有：汪茂雄";
        Font font = new Font("新宋体", Font.BOLD, 60);
        ImgUtil.pressText(srcImageFile, destImageFile, text, Color.BLACK, font, 0, 0, 0.6f);
        System.out.println(destImageFile.toPath());
    }

    /**
     * flip(File imageFile, File outFile)： 水平翻转图像
     */
    @Test
    public void testFlip() {
        ImgUtil.flip(srcImageFile, destImageFile);
        System.out.println(destImageFile.toPath());
    }

    /**
     * BufferedImage read(File imageFile) :  从文件中读取图片
     * write(Image image, File targetFile) ：将图像写出到目标文件
     */
    @Test
    public void testRead() {
        BufferedImage bufferedImage = ImgUtil.read(srcImageFile);

        //将下面的输出流换成 web 请求的输出流，即可将图片输出网页：https://gitee.com/wangmx1993/java-se/blob/master/src/main/java/org/example/controller/QRCodeController.java
        //ImageIO.write(bufferedImage,"jpg", OutputStream);

        ImgUtil.write(bufferedImage, destImageFile);
        System.out.println(destImageFile);
    }

    /**
     * String toBase64(Image image, String imageType): 将图片对象转换为 Base64 形式
     * imageType ：目标图片类型，可以是 gif、jpg、jpeg、bmp、png、psd
     */
    @Test
    public void testToBase64() {
        String base64 = ImgUtil.toBase64(ImgUtil.read(srcImageFile), "png");
        System.out.println(base64);
    }

    /**
     * String toBase64DataUri(Image image, String imageType)
     * 1、将图片对象转换为 Base64 的 Data URI 形式，格式为：data:image/[imageType];base64,[data]
     * 2、这个格式恰好是网页 <img src='xxx'/> 标签需要的格式，返回后可以直接 使用
     */
    @Test
    public void test() {
        String base64DataUri = ImgUtil.toBase64DataUri(ImgUtil.read(srcImageFile), "jpg");
        System.out.println(base64DataUri);
    }

}
