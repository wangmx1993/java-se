package org.example.hutool.utilTest;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.io.resource.FileResource;
import cn.hutool.core.io.resource.Resource;
import cn.hutool.core.io.resource.ResourceUtil;
import org.junit.Test;
import org.springframework.util.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

/**
 * ResourceUtil 工具提供了资源快捷读取封装。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/13 20:08
 */
public class ResourceUtilTest {

    /**
     * String readStr(String resource, Charset charset)
     * 读取Classpath下的资源为字符串
     * 1、resource 可以是绝对路径，也可以是相对路径（相对ClassPath）
     * 2、文件不存在时报错：FileNotFoundException、NoResourceException
     */
    @Test
    public void testReadFile0() {
        String resourceName = "data/config.json";
        // String resourceName = "D:/project/IDEA_project/java-org.se/target/test-classes/data/config.json";
        // String resourceName = "file:/D:/project/IDEA_project/java-org.se/target/test-classes/data/config.json";
        String config = ResourceUtil.readStr(resourceName, Charset.forName("UTF-8"));
        System.out.println(config);
    }

    /**
     * String readUtf8Str(String resource)
     * 读取Classpath下的资源为字符串，使用UTF-8编码
     * 1、resource 资源路径，使用相对ClassPath的路径
     * 2、文件不存在时报错：NoResourceException
     */
    @Test
    public void testReadFile1() {
        String config = ResourceUtil.readUtf8Str("data/config.json");
        System.out.println(config);
    }

    /**
     * BufferedReader getReader(String resource, Charset charset)
     * 1、从ClassPath资源中获取{@link BufferedReader}
     * 2、文件不存在时报错：NoResourceException
     * BufferedReader getUtf8Reader(String resource)
     * 1、使用 UTF-8 编码从ClassPath资源中获取{@link BufferedReader}
     * 2、文件不存在时报错：NoResourceException
     *
     * @throws IOException
     */
    @Test
    public void testReadFile2() throws IOException {
        // 读取Classpath下的资源为字符串，使用UTF-8编码
        // BufferedReader bufferedReader = ResourceUtil.getReader("data/config.json", Charset.forName("UTF-8"));
        BufferedReader bufferedReader = ResourceUtil.getUtf8Reader("data/config.json");

        StringBuilder sBuilder = new StringBuilder();
        String readLine = bufferedReader.readLine();
        while (readLine != null) {
            sBuilder.append(readLine).append("\n");
            readLine = bufferedReader.readLine();
        }
        System.out.println(sBuilder.toString());
    }

    /**
     * URL getResource(String resource)
     * 1、获得资源的URL<br>
     * 2、文件不存在时返回 null
     *
     * @throws IOException
     */
    @Test
    public void testReadFile3() {
        String resourceName = "data/config.json";
        URL url = ResourceUtil.getResource(resourceName);
        Assert.notNull(url, "资源文件不存在。");
        // file:/D:/project/IDEA_project/java-org.se/target/test-classes/data/config.json
        System.out.println(url);

        List<String> list = FileUtil.readUtf8Lines(url);
        list.stream().forEach(item -> System.out.println(item));
    }

    /**
     * Resource getResourceObj(String path)
     * 获取{@link Resource} 资源对象<br>
     * 1、如果提供路径为绝对路径或路径以file:开头，返回{@link FileResource}，否则返回{@link ClassPathResource}
     * 2、文件不存在时报错：FileNotFoundException、NoResourceException
     */
    @Test
    public void testReadFile4() {
        String resourceName = "data/config.json";
        // String resourceName = "file:/D:/project/IDEA_project/java-org.se/target/test-classes/data/config.json";
        // String resourceName = "D:/project/IDEA_project/java-org.se/target/test-classes/data/config.json";
        Resource resourceObj = ResourceUtil.getResourceObj(resourceName);
        byte[] bytes = resourceObj.readBytes();
        System.out.println(new String(bytes));
    }

    /**
     * byte[] readBytes(String resource)
     * 1、读取Classpath下的资源为byte[]
     * 2、resource 可以是绝对路径，也可以是相对路径（相对ClassPath）
     */
    @Test
    public void testReadFile5() {
        String resourceName = "data/config.json";
        // String resourceName = "file:/D:/project/IDEA_project/java-org.se/target/test-classes/data/config.json";
        // String resourceName = "D:/project/IDEA_project/java-org.se/target/test-classes/data/config.json";
        byte[] bytes = ResourceUtil.readBytes(resourceName);
        System.out.println(new String(bytes));
    }

}
