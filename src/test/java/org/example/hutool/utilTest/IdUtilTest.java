package org.example.hutool.utilTest;

import cn.hutool.core.lang.ObjectId;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import org.junit.Test;

/**
 * 唯一ID工具类 - IdUtil
 * 在分布式环境中，唯一ID生成应用十分广泛，生成方法也多种多样，Hutool针对一些常用生成策略做了简单封装。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/2/3 10:45
 */
public class IdUtilTest {

    /**
     * String randomUUID(): 获取随机UUID，带 -，36 位
     * String simpleUUID(): 简化的UUID，去掉了横线，34 位
     * String fastUUID()：获取随机UUID，使用性能更好的ThreadLocalRandom生成UUID，带 -，36 位
     * String fastSimpleUUID()：简化的UUID，去掉了横线，使用性能更好的ThreadLocalRandom生成UUID
     */
    @Test
    public void uuidTest() {
        String uuid = IdUtil.randomUUID();
        String simpleUUID = IdUtil.simpleUUID();

        String fastUUID = IdUtil.fastUUID();
        String fastSimpleUUID = IdUtil.fastSimpleUUID();

        //36	03541c97-c991-4c97-8d14-a2d3b684f7fd
        System.out.println(uuid.length() + "\t" + uuid);
        //32	3e1f45186e1a442f92d576dad73067db
        System.out.println(simpleUUID.length() + "\t" + simpleUUID);

        //36	7d03ad2c-9e98-48f6-b6f3-1c3c1802a8af
        System.out.println(fastUUID.length() + "\t" + fastUUID);
        //32	62c95ff6dcf8438b8d6fa8c8b370e270
        System.out.println(fastSimpleUUID.length() + "\t" + fastSimpleUUID);
    }


    /**
     * 创建MongoDB ID生成策略实现<br>
     * ObjectId由以下几部分组成：
     * <pre>
     * 1. Time 时间戳。
     * 2. Machine 所在主机的唯一标识符，一般是机器主机名的散列值。
     * 3. PID 进程ID。确保同一机器中不冲突
     * 4. INC 自增计数器。确保同一秒内产生objectId的唯一性。
     * </pre>
     * <p>
     * 参考：http://blog.csdn.net/qxc1281/article/details/54021882
     */
    @Test
    public void objectIdTest() {
        //类似：24	601a11b42fa40bc470caaac7
        for (int i = 0; i < 10; i++) {
            //底层是调用：String ObjectId.next()
            String objectId = IdUtil.objectId();
            System.out.println(objectId.length() + "\t" + objectId);
        }
        System.out.println("-----------------");

        //类似：26	601a11b4-2fa40bc4-70caaad2
        for (int i = 0; i < 10; i++) {
            //String next(boolean withHyphen): 获取一个objectId, withHyphen: 是否包含分隔符
            String objectId = ObjectId.next(true);
            System.out.println(objectId.length() + "\t" + objectId);
        }
    }

    /**
     * Snowflake getSnowflake(long workerId, long datacenterId) ：获取单例的Twitter的Snowflake 算法生成器对象<br>
     * workerId: 终端ID/机器id。[0-31]
     * datacenterId: 数据中心ID。[0-31]
     * synchronized long nextId(): 下一个ID
     */
    @Test
    public void test1() {
        Snowflake snowflake = IdUtil.getSnowflake(31, 31);
        for (int i = 0; i < 100; i++) {
            long id = snowflake.nextId();
            //类似：19	1356800425034125313
            System.out.println(String.valueOf(id).length() + "\t" + id);
        }
    }
}
