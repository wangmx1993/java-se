package org.example.hutool.utilTest;

import cn.hutool.core.lang.Console;
import cn.hutool.core.util.NumberUtil;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 数字工具-NumberUtil —— 对数学运算做工具性封装
 * <p>
 * 1、加减乘除运算都会将 double 转为 BigDecimal 后计算，解决 float 和 double 类型无法进行精确计算的问题。这些方法常用于商业计算。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/5 16:52
 */
public class NumberUtilTest {

    /**
     * 加法：支持所有数值类型
     * double add(double v1, double v2): 提供精确的加法运算
     * BigDecimal add(BigDecimal... values)：提供精确的加法运算
     * double add(float v1, double v2)
     * BigDecimal add(Number... values)
     * BigDecimal add(String... values)：提供精确的加法运算
     */
    @Test
    public void testAdd() {
        double a = 788677.7887;
        double b = 7487584.899;

        double add = NumberUtil.add(a, b);
        Console.log(add, a + b);//8276262.6877 8276262.687700001

        BigDecimal bigDecimal = NumberUtil.add("376273637.7878", "7676767.8978");
        Console.log(bigDecimal);//383950405.6856
    }

    /**
     * 减法：支持所有数值类型
     * double sub(double v1, double v2): 提供精确的加法运算
     * BigDecimal sub(BigDecimal... values)：提供精确的加法运算
     * double sub(float v1, double v2)
     * BigDecimal sub(Number... values)
     * BigDecimal sub(String... values)：提供精确的加法运算
     */
    @Test
    public void testSub() {
        double a = 788677.7887;
        double b = 7487584.8999;

        double add = NumberUtil.sub(a, b);
        Console.log(add, a - b);//-6698907.1112 -6698907.111199999

        BigDecimal bigDecimal = NumberUtil.sub("376273637.7878", "7676767.8978");
        Console.log(bigDecimal);//368596869.8900
    }

    /**
     * 乘法：支持所有数值类型
     * double mul(double v1, double v2): 提供精确的加法运算
     * BigDecimal mul(BigDecimal... values)：提供精确的加法运算
     * double mul(float v1, double v2)
     * BigDecimal mul(Number... values)
     * BigDecimal mul(String... values)：提供精确的加法运算
     */
    @Test
    public void testMul() {
        double a = 7887.787;
        double b = 7484.89;

        double add = NumberUtil.mul(a, b);
        Console.log(add, a * b);//5.903921803843E7 5.9039218038430005E7

        BigDecimal bigDecimal = NumberUtil.mul("376273637.7878", "7676767.8978");
        Console.log(bigDecimal);//2888565383357808.04848684
    }

    /**
     * 除法：支持所有数值类型
     * double div(double v1, double v2): 提供精确的加法运算
     * BigDecimal div(BigDecimal... values)：提供精确的加法运算
     * double div(float v1, double v2)
     * BigDecimal div(Number... values)
     * BigDecimal div(String... values)：提供精确的加法运算
     * BigDecimal div(BigDecimal v1, BigDecimal v2, int scale, RoundingMode roundingMode):roundingMode 保留小数的模式 {@link RoundingMode}
     */
    @Test
    public void testDiv() {
        double a = 7887.787;
        double b = 7484.89;

        double add = NumberUtil.div(a, b);
        Console.log(add, a / b);//1.0538280456 1.05382804556914

        BigDecimal bigDecimal = NumberUtil.div("376273637.7878", "7676767.8978");
        Console.log(bigDecimal);//49.0145908795
    }

    /**
     * long factorial(long n)： 阶乘，n! = n * (n-1) * ... * 2 * 1，n =(0,20)，超过20的阶乘会超过Long.MAX_VALUE
     * long sqrt(long x): 平方根
     * BigDecimal pow(Number number, int n): 提供精确的幂运算
     * int divisor(int m, int n): 最大公约数
     * int multiple(int m, int n): 最小公倍数
     * String getBinaryStr(Number number): 获得数字对应的二进制字符串
     * int binaryToInt(String binaryStr): 二进制转int
     * long binaryToLong(String binaryStr): 二进制转long
     * NumberUtil.compare: 比较两个值的大小
     * NumberUtil.toStr 数字转字符串，自动并去除尾小数点儿后多余的0
     */
    @Test
    public void testFactorial() {
        long factorial = NumberUtil.factorial(10);
        Console.log("10的阶乘={}", factorial);//10的阶乘=3628800

        long sqrt = NumberUtil.sqrt(64);
        Console.log("64的平方={}", sqrt);//64的平方=8

        Console.log("2的10次方={}", NumberUtil.pow(2, 10));//2的10次方=1024

        int divisor = NumberUtil.divisor(81, 72);
        Console.log("81和72的最大公约数={}", divisor);//81和72的最大公约数=9

        int multiple = NumberUtil.multiple(81, 72);
        Console.log("81和72的最小公倍数={}", multiple);//81和72的最小公倍数=648

        String binaryStr = NumberUtil.getBinaryStr(100);
        Console.log("100转为二进制={}", binaryStr);//8的转为二进制=100转为二进制=1100100
        int binaryToInt = NumberUtil.binaryToInt(binaryStr);
        Console.log("二进制{}转十进制={}", binaryStr, binaryToInt);//二进制1100100转十进制=100

        Console.log(NumberUtil.compare(20, 30));//-1
    }

    /**
     * 小数点位数保留
     * BigDecimal round(BigDecimal number, int scale): 采用四舍五入策略保留固定位数小数
     * BigDecimal round(BigDecimal number, int scale, RoundingMode roundingMode): 保留小数的模式 {@link RoundingMode}，如果传入null则默认四舍五入
     * BigDecimal round(BigDecimal double, int scale): 采用四舍五入策略保留固定位数小数
     * BigDecimal round(BigDecimal double, int scale, RoundingMode roundingMode): 保留小数的模式 {@link RoundingMode}，如果传入null则默认四舍五入
     * BigDecimal round(BigDecimal String, int scale): 采用四舍五入策略保留固定位数小数
     * BigDecimal round(BigDecimal String, int scale, RoundingMode roundingMode): 保留小数的模式 {@link RoundingMode}，如果传入null则默认四舍五入
     * BigDecimal roundDown(BigDecimal value, int scale)：保留固定小数位数，舍去多余位数
     * BigDecimal roundDown(Number number, int scale)：保留固定小数位数，舍去多余位数
     */
    @Test
    public void testRound() {
        double d = 49.0145908795;
        //49.01 49.015 49.0146
        Console.log(NumberUtil.round(d, 2), NumberUtil.round(d, 3), NumberUtil.round(d, 4));
    }

    /**
     * String decimalFormat(String pattern, Object value): 格式化 BigDecimal、BigInteger、Number 等类型
     * 1、pattern 格式 格式中主要以 # 和 0 两种占位符号来指定数字长度。0 表示如果位数不足则以 0 填充，# 表示只要有可能就把数字拉上这个位置。
     * <ul>
     * <li>0 =》 取一位整数</li>
     * <li>0.00 =》 取一位整数和两位小数</li>
     * <li>00.000 =》 取两位整数和三位小数</li>
     * <li># =》 取所有整数部分</li>
     * <li>#.00 =》 取所有整数部分，并保留2位小数，四舍五入，无小数时，使用两位 0补齐.</li>
     * <li>#.##% =》 以百分比方式计数，并取两位小数，四舍五入，不足两位小数时，不会补0</li>
     * <li>#.#####E0 =》 显示为科学计数法，并取五位小数</li>
     * <li>,### =》 每三位以逗号进行分隔，例如：299,792,458</li>
     * <li>,###.0000 =》 每三位以逗号进行分隔，并四舍五入保留4位小数，无小数时，使用两位 0补齐，例如：299,792,458.0000</li>
     * <li>光速大小为每秒,###米 =》 将格式嵌入文本</li>
     * </ul>
     * String formatPercent(double number, int scale): 格式化百分比，小数采用四舍五入方式
     */
    @Test
    public void testDecimalFormat() {
        long c = 299792458;//光速
        String format1 = NumberUtil.decimalFormat(",###", c);
        System.out.println(format1);//299,792,458

        Double d = 7798393.366237;
        System.out.println(NumberUtil.decimalFormat("#.00", d));//7798393.37
        System.out.println(NumberUtil.decimalFormat(",###.0000", d));//7,798,393.3662

        float f = 0.656F;
        System.out.println(NumberUtil.decimalFormat("#.##%", f));//65.6%

        String formatPercent = NumberUtil.formatPercent(0.3455, 2);
        Console.log(formatPercent);//34.55%
    }

    /**
     * 是否为数字
     * NumberUtil.isNumber: 是否为数字，支持包括：10进制、16进制数字（0x开头）、科学计数法形式（1234E3）、类型标识形式（123D）、正负数标识形式（+123、-234）
     */
    @Test
    public void testIsNumber() {
        // true
        System.out.println(NumberUtil.isNumber("0"));
        // true
        System.out.println(NumberUtil.isNumber("-12.0"));
        // false
        System.out.println(NumberUtil.isNumber("0x12.0"));
        // true
        System.out.println(NumberUtil.isNumber("0x12"));
        // true
        System.out.println(NumberUtil.isNumber("012.0"));
        // true
        System.out.println("-79D" + " -> " + NumberUtil.isNumber("-79D"));
        // true
        System.out.println(NumberUtil.isNumber("+79"));
        // true
        System.out.println(NumberUtil.isNumber("79"));
        // true
        System.out.println(NumberUtil.isNumber("79L"));
        // true
        System.out.println(NumberUtil.isNumber("79.0"));
        // true
        System.out.println(NumberUtil.isNumber("79.055F"));
        // true
        System.out.println(NumberUtil.isNumber("214748364700088"));
        // true
        System.out.println(NumberUtil.isNumber("2.14748364E13"));
        // false
        System.out.println(NumberUtil.isNumber("2.14748364A13"));
    }

    /**
     * NumberUtil.isPrimes：是否是质数（素数）。
     * 质数又称素数，指整数在一个大于1的自然数中,除了1和此整数自身外,没法被其他自然数整除的数。
     */
    @Test
    public void isPrimesTest() {
        // true true false
        Console.log(NumberUtil.isPrimes(11), NumberUtil.isPrimes(2), NumberUtil.isPrimes(9));
    }

    /**
     * int count(int total, int part): 计算等份个数，total：总数，part：每份的个数
     */
    @Test
    public void countTest() {
        int count = NumberUtil.count(230, 50);
        Console.log("230个元素，每份分50个，一共是{}份", count);//230个元素，每份分50个，一个是5份
    }

    /**
     * NumberUtil.isDouble：是否为浮点数
     * 1、支持类型限定符（3.1451212F）
     * 2、支持科学计数法（1234E3）
     */
    @Test
    public void isDoubleTest() {
        // false
        System.out.println(NumberUtil.isDouble("0"));
        // true
        System.out.println(NumberUtil.isDouble("0.0"));
        // true
        System.out.println(NumberUtil.isDouble("-12.0F"));
        // true
        System.out.println(NumberUtil.isDouble("79.9"));
        // false
        System.out.println(NumberUtil.isDouble("79"));
        // true
        System.out.println(NumberUtil.isDouble("79.555E2"));
        // true
        System.out.println(NumberUtil.isDouble("79.555e2"));
    }

    /**
     * NumberUtil.isInteger：是否为整数，支持 10 进制
     */
    @Test
    public void isIntegerTest() {
        // true
        System.out.println(NumberUtil.isInteger("0"));
        // false
        System.out.println(NumberUtil.isInteger("-12.0"));
        // true
        System.out.println(NumberUtil.isInteger("-79"));
        // true
        System.out.println(NumberUtil.isInteger("+79"));
        // true
        System.out.println(NumberUtil.isInteger("79"));
        // false
        System.out.println(NumberUtil.isInteger("79L"));
        // false
        System.out.println(NumberUtil.isInteger("79.0"));
        // false
        System.out.println(NumberUtil.isInteger("214748364700088"));
    }

    /**
     * boolean isLong(String s)：判断字符串是否是Long类型, 支持10进制
     */
    @Test
    public void isLongTest() {
        // true
        System.out.println(NumberUtil.isLong("0"));
        // false
        System.out.println(NumberUtil.isLong("-12.0"));
        // true
        System.out.println(NumberUtil.isLong("-79"));
        // true
        System.out.println(NumberUtil.isLong("+79"));
        // true
        System.out.println(NumberUtil.isLong("79"));
        // false
        System.out.println(NumberUtil.isLong("79L"));
        // false
        System.out.println(NumberUtil.isLong("79.0"));
        // true
        System.out.println(NumberUtil.isLong("214748364700088"));
    }

    /**
     * int[] generateRandomNumber(int begin, int end, int size, int[] seed)
     * int[] generateRandomNumber(int begin, int end, int size):生成不重复随机数 根据给定的最小数字和最大数字，以及随机数的个数，产生指定的不重复的数组
     * * begin 最小数字（包含该数）
     * * end   最大数字（不包含该数）
     * * size  指定产生随机数的个数，确保 begin < end ，并且 size 不能大于 end - begin
     * * seed  种子，用于取随机数的int池
     * Integer[] generateBySet(int begin, int end, int size): 生成不重复随机数 根据给定的最小数字和最大数字，以及随机数的个数，产生指定的不重复的数组
     */
    @Test
    public void testGenerateRandom() {
        int[] ints = NumberUtil.generateRandomNumber(0, 100, 11);
        Console.log(ints);//如 [16, 45, 31, 98, 75, 36, 13, 1, 93, 21, 26]

        Integer[] integers = NumberUtil.generateBySet(100, 1000, 20);
        Console.log(integers);//如 [385, 482, 643, 263, 553, 521, 618, 877, 238, 430, 817, 689, 657, 305, 146, 594, 498, 858, 541, 286]
    }

    /**
     * int[] range(int stop):生成一个有序整数列表，从[0,stop]，步进为1
     * int[] range(int start, int stop): 给定[start,stop]范围内的整数列表，步进为1
     * int[] range(int start, int stop, int step)：step 步进
     * <p>
     * T max(T[] numberArray)：取最大值
     * T min(T[] numberArray): 取最小值
     */
    @Test
    public void testRange() {
        int[] range1 = NumberUtil.range(20);
        int[] range2 = NumberUtil.range(20, 40);
        int[] range3 = NumberUtil.range(20, 40, 2);

        Console.log(range1);//[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        Console.log(range2);//[20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40]
        Console.log(range3);//[20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40]

        Console.log(NumberUtil.max(range3), NumberUtil.min(range3));//40 20
    }

    /**
     * BigDecimal toBigDecimal(String number) 数字转{@link BigDecimal}<br>
     * 1、null 或 "" 或 空白符转换为0
     * 2、无法解析时报错 java.lang.NumberFormatException
     * 3、支持类型限定符（3.4159F、34169L）
     * 4、支持科学计数法（8.8888888888888888E18）
     * 5、只支持10进制。
     */
    @Test
    public void toBigDecimalTest() {
        // 88
        System.out.println(NumberUtil.toBigDecimal("088"));
        // 3.4159
        System.out.println(NumberUtil.toBigDecimal("3.4159F"));
        // 34159
        System.out.println(NumberUtil.toBigDecimal("34159D"));
        // 34169
        System.out.println(NumberUtil.toBigDecimal("34169L"));
        // 34179
        System.out.println(NumberUtil.toBigDecimal("34179l"));

        BigDecimal bigDecimal = NumberUtil.toBigDecimal("8.8888888888888888E18");
        // 8888888888888888800
        System.out.println(bigDecimal);
        // 0
        System.out.println(NumberUtil.toBigDecimal(""));
        // 0
        System.out.println(NumberUtil.toBigDecimal("   "));
        String str = null;
        // 0
        System.out.println(NumberUtil.toBigDecimal(str));
        // java.lang.NumberFormatException
        // System.out.println(NumberUtil.toBigDecimal("str"));
    }

}
