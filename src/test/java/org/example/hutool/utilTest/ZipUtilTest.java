package org.example.hutool.utilTest;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ZipUtil;
import org.junit.Test;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * 压缩工具-ZipUtil
 * 1、ZipUtil 针对 java.util.zip 做工具化封装，使压缩解压操作可以一个方法搞定，并且自动处理文件和目录的问题，不再需要用户判断。
 * 2、压缩后的文件也会自动创建文件，自动创建父目录，大大简化的压缩解压的复杂度。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/2/3 14:45
 */
public class ZipUtilTest {

    /**
     * File zip(String srcPath): 打包到 srcPath 的当前目录，使用系统默认编码 ，返回打包好的压缩文件
     * File zip(File srcFile): 打包到 srcFile 的当前目录，使用系统默认编码，返回打包好的压缩文件
     * File zip(File srcFile, Charset charset)：指定 charset 编码
     */
    @Test
    public void zipTest1() {
        //将 E:\temp\logs 目录下的所有子孙目录/文件，打包到 E:\temp\logs.zip 文件中
        File file = ZipUtil.zip("E:\\temp\\logs");
        System.out.println(file);//E:\temp\logs.zip
    }

    /**
     * File zip(String srcPath, String zipPath): 对文件或文件目录进行压缩，不包含被打包目录
     * srcPath 要压缩的源文件路径。如果压缩一个文件，则为该文件的全路径；如果压缩一个目录，则为该目录的父目录路径
     * zipPath 压缩文件保存的路径，包括文件名，不存在时，自动创建。注意：zipPath不能是 srcPath 路径下的子文件夹
     * 返回压缩好的Zip文件
     */
    @Test
    public void zipTest2() {
        //将 E:\temp\logs 目录下的所有子孙目录/文件，打包到 E:\temp\logs.zip 文件中
        File file = ZipUtil.zip("E:\\temp\\logs", "E:\\temp\\zips\\a.zip");
        System.out.println(file);//E:\temp\zips\a.zip
    }

    /**
     * File zip(String srcPath, String zipPath, boolean withSrcDir)：对文件或文件目录进行压缩<br>
     * srcPath    要压缩的源文件路径。如果压缩一个文件，则为该文件的全路径；如果压缩一个目录，则为该目录的父层目录路径
     * zipPath    压缩文件保存的路径，包括文件名。注意：zipPath不能是srcPath路径下的子文件夹
     * withSrcDir 是否包含被打包目录
     */
    @Test
    public void zipTest3() {
        //将 E:\temp\logs 目录下的所有子孙目录/文件，打包到 E:\temp\logs.zip 文件中
        File file = ZipUtil.zip("E:\\temp\\logs", "E:\\temp\\zips\\b.zip", true);
        System.out.println(file);//E:\temp\zips\b.zip
    }

    /**
     * File zip(File zipFile, boolean withSrcDir, File... srcFiles)：对文件或文件目录进行压缩，使用系统默认编码
     * zipFile    生成的Zip文件，包括文件名。注意：zipPath不能是srcPath路径下的子文件夹
     * withSrcDir 是否包含被打包目录，只针对压缩目录有效。若为false，则只压缩目录下的文件或目录，为true则将本目录也压缩
     * srcFiles   要压缩的源文件或目录。
     * 返回压缩文件
     * File zip(File zipFile, Charset charset, boolean withSrcDir, File... srcFiles): 指定编码
     */
    @Test
    public void zipTest4() {
        File zip = ZipUtil.zip(FileUtil.file("d:/bbb/ccc.zip"), false,
                FileUtil.file("d:/test1/file1.txt"),
                FileUtil.file("d:/test1/file2.txt"),
                FileUtil.file("d:/test2/file1.txt"),
                FileUtil.file("d:/test2/file2.txt")
        );
        System.out.println(zip);//d:/bbb/ccc.zip
    }

    /**
     * File unzip(String zipFilePath)：解压到文件名相同的目录中，使用系统默认编码
     * File unzip(File zipFile, Charset charset)：指定编码
     * File unzip(File zipFile, File outFile)：解压到指定的目录 outFile，不存在时自动创建
     * File unzip(File zipFile, File outFile, Charset charset)：指定编码
     * File unzip(InputStream in, File outFile, Charset charset)：将 in 解压到 outFile
     */
    @Test
    public void unzipTest() {
        File unzip1 = ZipUtil.unzip("E:\\temp\\zips\\a.zip", Charset.forName("utf-8"));
        File unzip2 = ZipUtil.unzip("E:\\temp\\zips\\a.zip", "E:\\temp\\unzips\\a");

        System.out.println(unzip1);//E:\temp\zips\a
        System.out.println(unzip2);//E:/temp/unzips\a
    }

    /**
     * Gzip是网页传输中广泛使用的压缩方式
     * <p>
     * byte[] gzip(String content, String charset):将字符串内容压缩为字节流
     * byte[] gzip(byte[] buf)：将字节流 buf 压缩成字节流
     * byte[] gzip(File file)：将文件压缩为字节流
     * byte[] gzip(InputStream in)：将字节流 buf 压缩成字节流
     * <p>
     * byte[] unGzip(byte[] buf): 将字节数组 buf 使用系统默认编码解压为字节数组
     * String unGzip(byte[] buf, String charset)：将字节数组 buf 使用指定编码解压为字节数组
     * byte[] unGzip(InputStream in)：将字节输入流解压为字节数组
     */
    @Test
    public void gzipTest() {
        String content = "春风又绿江南岸";
        byte[] gzip = ZipUtil.gzip(content, "utf-8");

        System.out.println(content.getBytes().length + "," + gzip.length);//21,44

        String unGzip = ZipUtil.unGzip(gzip, "utf-8");
        System.out.println(unGzip);//春风又绿江南岸

        System.out.println(new String(gzip));//        ��春风又绿江南岸�++�
    }

    /**
     * 对文件输入流进行压缩，并指定输出流，可以用于 web 环境网络下载.
     *
     * @throws Exception
     */
    @Test
    public void testZipOutputStream() throws Exception {
        String classPath = "data/images/1.jpg";
        String destPath = "D:\\" + System.currentTimeMillis() + ".zip";
        FileOutputStream destOutputStream = new FileOutputStream(new File(destPath));

        //被压缩文件的输入流
        List<InputStream> inputStreams = new ArrayList<>();
        //压缩包中文件对应的名字
        String[] paths = {"125", "abc", "12的肌肤的", "酒店房s间的45", "的教会大家"};

        for (int i = 0; i < paths.length; i++) {
            InputStream inputStream = ZipUtilTest.class.getClassLoader().getResourceAsStream(classPath);
            inputStreams.add(inputStream);
            paths[i] = paths[i] + classPath.substring(classPath.lastIndexOf("."));
        }
        /**
         * zip(OutputStream out, String[] paths, InputStream[] ins)：将文件流压缩到目标流中
         *
         * @param out   目标流，压缩完成自动关闭，web 环境时，使用 response.getOutputStream() 返回 ServletOutputStream 输出即可进行网络下载.
         * @param paths 流数据在压缩文件中的路径或文件名，可以自己随意定义，会按顺序给后面的 ins 进行命名.
         * @param ins   要压缩的源，添加完成后自动关闭流
         */
        ZipUtil.zip(destOutputStream, paths, inputStreams.toArray(new InputStream[inputStreams.size()]));
        System.out.println("生成压缩文件：" + destPath);
    }
}
