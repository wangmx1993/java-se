package org.example.hutool.utilTest;

import cn.hutool.core.net.NetUtil;
import org.junit.Test;

import java.net.InetAddress;
import java.util.LinkedHashSet;

/**
 * 网络工具-NetUtil
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/2 10:39
 */
public class NetUtilTest {

    /**
     * long ipv4ToLong(String strIP) :根据 ip地址计算出long型的数据
     * String longToIpv4(long longIP): 根据 long 值获取ip v4地址
     * boolean isUsableLocalPort(int port):  检测本地端口可用性
     * boolean isValidPort(int port)：是否为有效的端口（有效端口是0～65535），此方法并不检查端口是否被占用
     * String hideIpPart(String ip): 隐藏掉IP地址的最后一部分为 * 代替
     * boolean isInnerIP(String ipAddress)： 判定是否为内网IP
     */
    @Test
    public void test1() {
        String ip1 = "10.104.65.195";
        long ip1Long = NetUtil.ipv4ToLong(ip1);
        System.out.println("ip1Long=" + ip1Long);//ip1Long=174604739

        //根据long值获取ip v4地址
        String ip2 = NetUtil.longToIpv4(ip1Long);
        System.out.println("ip2=" + ip2);//ip2=10.104.65.195

        //检测本地端口可用性
        boolean usableLocalPort = NetUtil.isUsableLocalPort(6379);
        System.out.println(usableLocalPort);//false

        //是否为有效的端口
        boolean validPort = NetUtil.isValidPort(6379);
        System.out.println(validPort);//true

        //隐藏掉IP地址
        String hideIpPart = NetUtil.hideIpPart(ip1);
        System.out.println(hideIpPart);//10.104.65.*

        System.out.println("是否内网IP：" + NetUtil.isInnerIP(ip1));
    }

    /**
     * String getIpByHost(String hostName) ：通过域名得到 IP
     * InetAddress getLocalhost() :获取本机网卡IP地址，不会抛出异常，获取失败将返回 null，规则如下：
     * 1. 查找所有网卡地址，必须非回路（loopback）地址、非局域网地址（siteLocal）、IPv4地址
     * 2. 如果无满足要求的地址，调用 {@link InetAddress#getLocalHost()} 获取地址
     * String getLocalHostName(): 获取主机名称，一次获取会缓存名称
     * String getLocalhostStr(): 获取本机网卡IP地址，这个地址为所有网卡中非回路地址的第一个。不会抛出异常，获取失败将返回 null，
     * String getLocalMacAddress(): 获得本机 MAC 地址
     * int getUsableLocalPort(): 查找 1024~65535 范围内的可用端口，随机返回一个
     * int getUsableLocalPort(int minPort)： 查找[minPort,65535]范围内的可用端口，随机返回一个
     * int getUsableLocalPort(int minPort, int maxPort): 查找[minPort,maxPort]范围内的可用端口，随机返回一个
     * LinkedHashSet<String> localIps() ： 获得本机的IP地址列表（包括Ipv4和Ipv6），返回的IP列表有序，按照系统设备顺序
     * LinkedHashSet<String> localIpv4s()： 获得本机的IPv4地址列表<br>
     * LinkedHashSet<String> localIpv6s()：获得本机的IPv6地址列表<br>
     */
    @Test
    public void test2() {
        String hostName = "www.baidu.com";
        String ipByHost = NetUtil.getIpByHost(hostName);
        System.out.println(ipByHost);//112.80.248.76

        String hostAddress = NetUtil.getLocalhost().getHostAddress();
        System.out.println(hostAddress);//10.104.65.195

        System.out.println(NetUtil.getLocalHostName());//wangMaoXiong
        System.out.println(NetUtil.getLocalhostStr());//10.104.65.195
        System.out.println(NetUtil.getLocalMacAddress());//e8-6a-64-72-c7-7e

        System.out.println(NetUtil.getUsableLocalPort());
        System.out.println(NetUtil.getUsableLocalPort(1000));
        System.out.println(NetUtil.getUsableLocalPort(10000, 20000));

        LinkedHashSet<String> localIps = NetUtil.localIps();
        for (String localIp : localIps) {
            System.out.println("localIp=" + localIp);
        }

        LinkedHashSet<String> localIpv4s = NetUtil.localIpv4s();
        for (String localIp : localIpv4s) {
            System.out.println("localIpv4=" + localIp);
        }

    }

    /**
     * boolean ping(String ip, int timeout): 检测IP地址是否能ping通, timeout: 检测超时（毫秒）
     */
    @Test
    public void test3() {
        String ip = "114.114.114.114";
        boolean ping = NetUtil.ping(ip, 10 * 1000);
        System.out.println(ping);
    }


}
