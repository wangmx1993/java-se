package org.example.hutool.utilTest;

import cn.hutool.system.*;
import org.junit.Test;

/**
 * Java 的 System 类封装工具类
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/3/16 20:45
 */
public class SystemUtilTest {

    /**
     * 取得 Java虚拟机规范的信息
     */
    @Test
    public void testJvmSpecInfo() {
        JvmSpecInfo jvmSpecInfo = SystemUtil.getJvmSpecInfo();
        System.out.println("name(名称)=" + jvmSpecInfo.getName());
        System.out.println("vendor（创作者）=" + jvmSpecInfo.getVendor());
        System.out.println("version（版本）=" + jvmSpecInfo.getVersion());

        System.out.println(jvmSpecInfo.toString());
    }

    /**
     * 取得  Java虚拟机实现 的信息
     */
    @Test
    public void testJvmInfo() {
        JvmInfo jvmInfo = SystemUtil.getJvmInfo();
        System.out.println("name=" + jvmInfo.getName());
        System.out.println("vendor=" + jvmInfo.getVendor());
        System.out.println("version=" + jvmInfo.getVersion());
        System.out.println("info=" + jvmInfo.getInfo());

        System.out.println(jvmInfo.toString());
    }

    /**
     * 获取 Java规范 信息
     */
    @Test
    public void testJavaSpecInfo() {
        JavaSpecInfo javaSpecInfo = SystemUtil.getJavaSpecInfo();
        System.out.println("name=" + javaSpecInfo.getName());
        System.out.println("vendor=" + javaSpecInfo.getVendor());
        System.out.println("version=" + javaSpecInfo.getVersion());

        System.out.println(javaSpecInfo.toString());

    }

    /**
     * 取得 Java实现 的信息
     */
    @Test
    public void testJavaInfo() {
        JavaInfo javaInfo = SystemUtil.getJavaInfo();
        System.out.println(javaInfo);
        /**
         * Java Version:    1.8.0_131
         * Java Vendor:     Oracle Corporation
         * Java Vendor URL: http://java.oracle.com/
         */
    }

    /**
     * 取得当前运行的JRE的信息。
     */
    @Test
    public void testJavaRuntimeInfo() {
        JavaRuntimeInfo javaRuntimeInfo = SystemUtil.getJavaRuntimeInfo();
        System.out.println(javaRuntimeInfo);
    }

    /**
     * 取得操作系统的信息
     */
    @Test
    public void testOsInfo() {
        OsInfo osInfo = SystemUtil.getOsInfo();
        System.out.println(osInfo.toString());
        /**
         * OS Arch:        amd64
         * OS Name:        Windows 10
         * OS Version:     10.0
         * File Separator: \
         * Line Separator:
         */
    }

    /**
     * 取得用户的信息
     */
    @Test
    public void testUserInfo() {
        UserInfo userInfo = SystemUtil.getUserInfo();
        System.out.println(userInfo.toString());
        /**
         * User Name:        Think
         * User Home Dir:    C:\Users\Think
         * User Current Dir: D:\project\IDEA_project\java-org.se
         * User Temp Dir:    C:\Users\Think\AppData\Local\Temp\
         * User Language:    zh
         * User Country:     CN
         */
    }

    /**
     * 获取主机信息
     */
    @Test
    public void testHostInfo() {
        HostInfo hostInfo = SystemUtil.getHostInfo();
        System.out.println(hostInfo.toString());
        /**
         * Host Name:    wangMaoXiong
         * Host Address: 10.104.65.195
         */
    }

    /**
     * 取得 运行时 信息。
     */
    @Test
    public void testRuntimeInfo() {
        RuntimeInfo runtimeInfo = SystemUtil.getRuntimeInfo();
        System.out.println(runtimeInfo);
        /**
         * Max Memory:    3.54 GB
         * Total Memory:     245.5 MB
         * Free Memory:     230.12 MB
         * Usable Memory:     3.52 GB
         */
    }

}
