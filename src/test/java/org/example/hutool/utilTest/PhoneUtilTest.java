package org.example.hutool.utilTest;

import cn.hutool.core.util.PhoneUtil;
import org.junit.Test;

/**
 * 手机号工具类 测试
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2023/8/2 10:08
 */
public class PhoneUtilTest {

    /**
     * boolean isMobile(CharSequence value): 验证是否为手机号码（中国）
     */
    @Test
    public void isMobileTest() {
        System.out.println("============手机============");
        // false
        System.out.println(PhoneUtil.isMobile("08745641254"));
        // true
        System.out.println(PhoneUtil.isMobile("18745641254"));
        // false
        System.out.println(PhoneUtil.isMobile("187456412541"));
        // true
        System.out.println(PhoneUtil.isMobile("+8618745641254"));
        // true
        System.out.println(PhoneUtil.isMobile("8618745641254"));
        // false
        System.out.println(PhoneUtil.isMobile("8718745641254"));
        // false
        System.out.println(PhoneUtil.isMobile("110"));
    }

    /**
     * boolean isTel(CharSequence value) :验证是否为座机号码（中国）
     */
    @Test
    public void isTelTest() {
        System.out.println("============座机号============");
        // false
        System.out.println(PhoneUtil.isTel("66100007"));
        // true
        System.out.println(PhoneUtil.isTel("0377-66100007"));
        // false
        System.out.println(PhoneUtil.isTel("037766100007"));
        // true
        System.out.println(PhoneUtil.isTel("010-88993353"));
        // false
        System.out.println(PhoneUtil.isTel("01-88993353"));
        // false
        System.out.println(PhoneUtil.isTel("110"));

    }

    /**
     * boolean isPhone(CharSequence value)：验证是否为座机号码+手机号码（中国）
     * 内部就是 return isMobile(value) || isTel(value);
     */
    @Test
    public void isPhoneTest() {
        System.out.println(PhoneUtil.isPhone("0377-66100007"));
        System.out.println(PhoneUtil.isPhone("+8618745641254"));
        System.out.println(PhoneUtil.isPhone("8618745641254"));
        System.out.println(PhoneUtil.isPhone("18745641254"));
        System.out.println(PhoneUtil.isPhone("110"));
    }

}
