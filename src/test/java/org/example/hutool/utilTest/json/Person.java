package org.example.hutool.utilTest.json;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/1 19:41
 */

public class Person implements Serializable {
    private Integer id;
    private String name;
    private Integer age;
    private Date birthday;
    private Float salary;

    public Person() {
    }

    public Person(Integer id, String name, Date birthday, Float salary) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
        this.salary = salary;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                ", salary=" + salary +
                '}';
    }
}
