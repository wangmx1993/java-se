package org.example.hutool.utilTest;

import cn.hutool.core.text.csv.*;
import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

/**
 * CSV文件处理工具-CsvUtil
 * 1、逗号分隔值-CSV，其文件以纯文本形式存储表格数据（数字和文本）。
 * 2、在数据处理领域，CSV（Comma-Separated Values）文件因其结构简单、兼容性强等特点，成为跨系统数据交换的首选格式。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/2 22:02
 */
public class CsvUtilTest {

    /**
     * CsvReader getReader() ： 获取 CSV 读取器
     * CsvData read(Reader reader): 从 Reader 中读取 CSV 数据，读取后关闭 Reader
     * CsvData read(File file, Charset charset): charset 文件编码，默认系统编码
     * List<T> read(Reader reader, Class<T> clazz): 从 Reader 中读取 CSV 数据并转换为 Java Bean 列表，读取后关闭 Reader。此方法默认识别首行为标题行。
     * CsvData: CSV 数据，包括头部信息和行数据
     * List<CsvRow> getRows(): 获取所有行
     * List<String> getRawList(): 获取本行所有字段值列表
     *
     * @throws IOException /
     */
    @Test
    public void testGetReader() throws IOException {
        // 获取类路径下的资源文件
        try (InputStream inputStream = CsvUtilTest.class.getClassLoader().getResourceAsStream("data/person.csv");
             CsvReader reader = CsvUtil.getReader()) {
            if (inputStream != null) {
                CsvData data = reader.read(new InputStreamReader(inputStream, "gbk"));
                List<CsvRow> rows = data.getRows();
                for (CsvRow csvRow : rows) {
                    List<String> rawList = csvRow.getRawList();
                    System.out.println(rawList);
                }
                // [序号, 姓名, 性别, 婚否, 年龄]
                // [1, 张三, 男, 否, 29]
                // [2, 李斯, 男, 是, 33]
                // [3, 白雪儿, 女, 否, 19]
            }
        }
    }

    /**
     * List<Map<String, String>> readMapList(Reader reader)
     * 1、从 Reader 中读取 CSV 数据，结果为 Map，读取后关闭 Reader。
     * 2、此方法默认识别首行为标题行，Map 的 key 是标题，value 是值
     * setContainsHeader(boolean containsHeader：设置是否首行做为标题行，默认false
     * setErrorOnDifferentFieldCount(boolean errorOnDifferentFieldCount) ：设置每行字段个数不同时是否抛出异常，默认false
     * setFieldSeparator(char fieldSeparator)：设置字段分隔符，默认逗号','
     * setSkipEmptyRows(boolean skipEmptyRows)：设置是否跳过空白行，默认true
     *
     * @throws IOException /
     */
    @Test
    public void testReadMapList() throws IOException {
        try (InputStream inputStream = CsvUtilTest.class.getClassLoader().getResourceAsStream("data/person.csv");
             InputStreamReader streamReader = new InputStreamReader(inputStream, "gbk");
             CsvReader reader = CsvUtil.getReader()) {
            List<Map<String, String>> readMapList = reader.readMapList(streamReader);
            for (Map<String, String> map : readMapList) {
                System.out.println(map);
            }
            // {序号=1, 姓名=张三, 性别=男, 婚否=否, 年龄=29}
            // {序号=2, 姓名=李斯, 性别=男, 婚否=是, 年龄=33}
            // {序号=3, 姓名=白雪儿, 性别=女, 婚否=否, 年龄=19}
        }
    }

    /**
     * CsvWriter getWriter(File file, Charset charset, boolean isAppend)：获取CSV生成器（写出器），使用默认配置
     * 1、file：File CSV文件；charset ：编码； isAppend 是否追加；
     * 2、Windows 系统下，用 gbk 编码写出，Microsoft Office 打开才不会乱码
     * CsvWriter writeLine(String... fields): 写出一行,fields 字段列表 ({@code null} 值会被做为空值追加)。返回对象自己
     * CsvWriter write(String[]... lines)：将多行写出到 Writer,每行数据可以是集合或者数组
     * CsvWriter write(Collection<?> lines): 将多行写出到 Writer,每行数据可以是集合或者数组
     */
    @Test
    public void testWrite() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = new File(homeDirectory, System.currentTimeMillis() + ".csv");
        try (CsvWriter csvWriter = CsvUtil.getWriter(file, Charset.forName("GBK"), false)) {
            csvWriter.writeLine("序号", "名称", "价格");
            csvWriter.write(
                    new String[]{"1", "华为P30", "3400.55"},
                    new String[]{"2", "红米10", "2400.55"},
                    new String[]{"3", "魅族30", "1400.55"},
                    new String[]{"4", "魅族30", null},
                    new String[]{"5", "", null}
            );
            csvWriter.flush();
            System.out.println(file.toPath());
            // 序号	名称	价格
            // 1	华为P30	3400.55
            // 2	红米10	2400.55
            // 3	魅族30	1400.55
            // 4	魅族30
            // 5
        }
    }

}
