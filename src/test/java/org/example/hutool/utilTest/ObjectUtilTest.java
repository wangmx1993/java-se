package org.example.hutool.utilTest;

import cn.hutool.core.lang.Console;
import cn.hutool.core.util.ObjectUtil;
import org.junit.Test;

import java.util.*;

/**
 * 对象工具-ObjectUtil，包括判空、克隆、序列化等操作
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/13 9:25
 */
public class ObjectUtilTest {

    /**
     * T clone(T obj): 克隆对象
     * 1、如果对象实现 Cloneable 接口，调用其 clone 方法，属于浅克隆
     * 2、如果对象实现 Serializable 接口，执行深度克隆
     * 3、否则返回 null
     * 4、浅克隆，即只对基本数据类型有效，对 list、Map、Set、java bean 等关联对象无效，即克隆出来的新对象仍然与源对象公用这些引用类型。
     */
    @Test
    public void testClone() {
        String[] colors = {"red", "yellow"};

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("id", "9989D");
        dataMap.put("name", "华安");
        dataMap.put("colors", colors);

        Map<String, Object> clone = ObjectUtil.clone(dataMap);
        clone.put("age", 33);
        String[] cc = (String[]) clone.get("colors");
        cc[1] = "blue";

        //{name=华安, id=9989D, colors=[Ljava.lang.String;@136432db, age=33} [red, blue]
        Console.log(clone, clone.get("colors"));

        //{name=华安, id=9989D, colors=[Ljava.lang.String;@136432db} [red, blue]
        Console.log(dataMap, dataMap.get("colors"));
    }

    /**
     * T cloneByStream(T obj)：序列化后拷贝流的方式克隆，支持 Java bean，Set、List、Map、Array 等等，
     * 1、如果是 Java bean，则自己必须实现 Serializable 接口，否则克隆报错
     * 2、此种克隆方式属于深度克隆，对象中关联的属性对象都能独立克隆.
     */
    @Test
    public void testCloneByStream1() {
        String[] colors = {"red", "yellow"};

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("id", "9989D");
        dataMap.put("name", "华安");
        dataMap.put("colors", colors);

        Map<String, Object> clone = ObjectUtil.cloneByStream(dataMap);
        clone.put("age", 33);
        String[] cc = (String[]) clone.get("colors");
        cc[1] = "blue";

        //{name=华安, id=9989D, colors=[Ljava.lang.String;@161cd475, age=33} [red, blue]
        Console.log(clone, clone.get("colors"));

        //{name=华安, id=9989D, colors=[Ljava.lang.String;@5eb5c224} [red, yellow]
        Console.log(dataMap, dataMap.get("colors"));
    }

    /**
     * 推荐使用 {@link ObjectUtil#cloneByStream(java.lang.Object)}
     */
    @Test
    public void testCloneByStream2() {
        String[] colors = {"red", "yellow"};

        Map<String, Object> dataMap1 = new HashMap<>();
        dataMap1.put("id", "9989D1");
        dataMap1.put("name", "华安1");
        dataMap1.put("colors", colors);

        Map<String, Object> dataMap2 = new HashMap<>();
        dataMap2.put("id", "9989D2");
        dataMap2.put("name", "华安2");
        dataMap2.put("colors", colors);

        List<Map<String, Object>> dataList = new ArrayList<>();
        dataList.add(dataMap1);
        dataList.add(dataMap2);

        List<Map<String, Object>> cloneByStream = ObjectUtil.cloneByStream(dataList);
        cloneByStream.get(0).put("age", 88);
        String[] cc = (String[]) cloneByStream.get(1).get("colors");
        cc[1] = "blue";

        //[{name=华安1, id=9989D1, colors=[Ljava.lang.String;@7006c658}, {name=华安2, id=9989D2, colors=[Ljava.lang.String;@7006c658}] [red, yellow]
        Console.log(dataList, dataList.get(1).get("colors"));

        //[{name=华安1, id=9989D1, colors=[Ljava.lang.String;@3cda1055, age=88}, {name=华安2, id=9989D2, colors=[Ljava.lang.String;@3cda1055}] [red, blue]
        Console.log(cloneByStream, cloneByStream.get(1).get("colors"));
    }

    /**
     * boolean equal(Object obj1, Object obj2) : 比较两个对象是否相等。相同的条件有两个，满足其一即可：
     * boolean equals(Object obj1, Object obj2)
     * * obj1 == null && obj2 == null
     * * obj1.equals(obj2)
     * * 如果是 BigDecimal 比较，0 == obj1.compareTo(obj2)
     * boolean notEqual(Object obj1, Object obj2)：比较两个对象是否不相等。
     */
    @Test
    public void testEqual() {
        boolean equal1 = ObjectUtil.equal(Integer.parseInt("100"), Long.parseLong("100"));
        boolean equal2 = ObjectUtil.equal(Integer.parseInt("100"), Integer.parseInt("100"));
        System.out.println(equal1);//false
        System.out.println(equal2);//true
    }

    /**
     * int length(Object obj):计算对象长度，如果是字符串调用其length函数，集合类调用其size函数，数组调用其length属性，其他可遍历对象遍历计算长度
     * 支持的类型包括：
     * * CharSequence
     * * Map
     * * Iterator
     * * Enumeration
     * * Array
     */
    @Test
    public void testLength() {
        String[] strings = {"1", "2d"};
        List<Integer> lists = null;
        String s = "中国！";
        System.out.println(ObjectUtil.length(strings));//2
        System.out.println(ObjectUtil.length(lists));//0
        System.out.println(ObjectUtil.length(s));//3
    }

    /**
     * int compare(T c1, T c2): {@code null} 安全的对象比较，{@code null} 对象排在末尾
     * 如果 c1 < c2，返回数小于0；如果 c1==c2 返回数等于 0 ；如果 c1 > c2，返回数 大于 0
     * int compare(T c1, T c2, boolean nullGreater): 被比较对象为 null 时是否排在前面
     */
    @Test
    public void testCompare() {
        System.out.println(ObjectUtil.compare(10, 3));//1
        System.out.println(ObjectUtil.compare(10, 33));//-1
        System.out.println(ObjectUtil.compare("李世民", "朱元璋"));//29
        System.out.println(ObjectUtil.compare("李世民", "李世民"));//0
        System.out.println(ObjectUtil.compare("李世民", null));//1
        System.out.println(ObjectUtil.compare(null, "李世民"));//-1
    }

    /**
     * boolean contains(Object obj, Object element): 对象中是否包含指定元素, 支持的对象类型包括：
     * * <ul>
     * * <li>String</li>
     * * <li>Collection</li>
     * * <li>Map</li>
     * * <li>Iterator</li>
     * * <li>Enumeration</li>
     * * <li>Array</li>
     * * </ul>
     */
    @Test
    public void testContains() {
        String[] colors = {"#231", "345", "#222", null};
        List<String> list = Arrays.asList(new String[]{"#231", "345", "#222"});

        System.out.println(ObjectUtil.contains(colors, "#333"));//false
        System.out.println(ObjectUtil.contains(colors, "#222"));//true
        System.out.println(ObjectUtil.contains(colors, null));//true

        System.out.println(ObjectUtil.contains(list, "#333"));//false
        System.out.println(ObjectUtil.contains(list, "#222"));//true
        System.out.println(ObjectUtil.contains(list, null));//false
    }

    /**
     * <T extends CharSequence> T defaultIfBlank(final T str, final T defaultValue)：如果给定对象为 null、或者 ""、或者空白符，则返回默认值.
     * <T extends CharSequence> T defaultIfEmpty(final T str, final T defaultValue)：如果给定对象为 null、或者 ""，则返回默认值
     * * ObjectUtil.defaultIfEmpty(null, null)      = null
     * * ObjectUtil.defaultIfEmpty(null, "")        = ""
     * * ObjectUtil.defaultIfEmpty("", "zz")      = "zz"
     * * ObjectUtil.defaultIfEmpty(" ", "zz")      = " "
     * * ObjectUtil.defaultIfEmpty("abc", *)        = "abc"
     * <T> T defaultIfNull(final T object, final T defaultValue)：如果给定对象为 null 返回默认值
     * * ObjectUtil.defaultIfNull(null, null)      = null
     * * ObjectUtil.defaultIfNull(null, "")        = ""
     * * ObjectUtil.defaultIfNull(null, "zz")      = "zz"
     * * ObjectUtil.defaultIfNull("abc", *)        = "abc"
     * * ObjectUtil.defaultIfNull(Boolean.TRUE, *) = Boolean.TRUE
     */
    @Test
    public void testDefaultIfBlank() {
        String defaultIfBlank = ObjectUtil.defaultIfBlank("大秦帝国", "T");
        System.out.println(defaultIfBlank);//大秦帝国
        System.out.println(ObjectUtil.defaultIfBlank("      ", "T"));//T
        System.out.println(ObjectUtil.defaultIfBlank("", "T"));//T
        System.out.println(ObjectUtil.defaultIfBlank(null, "T"));//T
        System.out.println(ObjectUtil.defaultIfBlank(new String(), "T"));//T
    }

    /**
     * boolean isEmpty(Object obj): 判断指定对象是否为空或者为 null，支持：
     * boolean isNotEmpty(Object obj): 判断指定对象是否为非空或者非 null，支持：
     * * 1. CharSequence
     * * 2. Map
     * * 3. Iterable
     * * 4. Iterator
     * * 5. Array
     * boolean isNull(Object obj): 检查对象是否为 null
     * boolean isNotNull(Object obj)：检查对象是否不为 null
     */
    @Test
    public void testIsEmpty() {
        System.out.println(ObjectUtil.isEmpty("      "));//false
        System.out.println(ObjectUtil.isEmpty(""));//true
        System.out.println(ObjectUtil.isEmpty(null));//true
        System.out.println(ObjectUtil.isEmpty(new String()));//true

        System.out.println("---------------------------------------------------");
        System.out.println(ObjectUtil.isNotEmpty("      "));//true
        System.out.println(ObjectUtil.isNotEmpty(""));//false
        System.out.println(ObjectUtil.isNotEmpty(null));//false
        System.out.println(ObjectUtil.isNotEmpty(new String()));//false
    }

    /**
     * int emptyCount(Object... objs): 存在多少个 null 或空对象，通过{@link ObjectUtil#isEmpty(Object)} 判断元素
     * boolean hasEmpty(Object... objs): 是否存在 null 或空对象，通过{@link ObjectUtil#isEmpty(Object)} 判断元素
     * boolean hasNull(Object... objs): 是否存在 null 对象，通过{@link ObjectUtil#isNull(Object)} 判断元素
     * boolean isAllEmpty(Object... objs)：是否全都为{@code null}或空对象，通过{@link ObjectUtil#isEmpty(Object)} 判断元素
     * boolean isAllNotEmpty(Object... objs): 是否全都不为{@code null}或空对象，通过{@link ObjectUtil#isEmpty(Object)} 判断元素
     */
    @Test
    public void testEmptyCount() {
        String str = " ";
        Map<String, Object> dataMap = new HashMap<>();
        List<String> dataList = new ArrayList<>();

        System.out.println(ObjectUtil.emptyCount(str, dataMap, dataList));//2
    }


    /**
     * <T> byte[] serialize(T obj): 对象序列化, 对象必须实现 Serializable 接口
     * <T> T deserialize(byte[] bytes)：对象反序列化，对象必须实现 Serializable 接口
     * 此方法不会检查反序列化安全，可能存在反序列化漏洞风险！！！
     */
    @Test
    public void testSerialize() {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("id", "892839Y");
        dataMap.put("name", "华安");
        dataMap.put("age", 34);

        byte[] serialize = ObjectUtil.serialize(dataMap);
        Map<String, Object> deserialize = ObjectUtil.deserialize(serialize);

        deserialize.put("salary", 13000.88);

        System.out.println(dataMap);//{name=华安, id=892839Y, age=34}
        System.out.println(deserialize);//{name=华安, id=892839Y, salary=13000.88, age=34}
    }

    /**
     * String toString(Object obj)：将 Object 转为 String
     * 1、null 转为 "null"
     * 2、调用 Convert.toStr(Object) 转换
     */
    @Test
    public void testToString() {
        Object[] args = {10, "好汉", "Hi"};
        // [Ljava.lang.Object;@52d455b8
        System.out.println(args);
        // [10, 好汉, Hi]
        System.out.println(ObjectUtil.toString(args));
    }


}
