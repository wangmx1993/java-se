package org.example.hutool.utilTest;

import cn.hutool.core.lang.Assert;
import org.example.SysException;
import org.junit.Test;

/**
 * 断言-Assert
 * 在方法或者任何地方对参数的有效性做校验。当不满足断言条件时，会抛出IllegalArgumentException或IllegalStateException异常。
 * 也可以抛出自定义异常。
 * <p>
 * 使用方式类似：https://gitee.com/wangmx1993/apache-study/blob/master/src/main/java/com/wmx/spring/AssertTest.java
 * <p>
 * isTrue 是否True
 * isNull 是否是null值，不为null抛出异常
 * notNull 是否非null值
 * notEmpty 是否非空
 * notBlank 是否非空白符
 * notContain 是否为子串
 * notEmpty 是否非空
 * noNullElements 数组中是否包含null元素
 * isInstanceOf 是否类实例
 * isAssignable 是子类和父类关系
 * state 会抛出IllegalStateException异常
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/15 14:25
 */
public class AssertTest {
    /**
     * T notNull(T object, String errorMsgTemplate, Object... params)
     * 1、断言对象是否不为{@code null} ，如果为{@code null} 抛出{@link IllegalArgumentException} 异常
     * * @param <T>              被检查对象泛型类型
     * * @param object           被检查对象
     * * @param errorMsgTemplate 错误消息模板，变量使用{}表示
     * * @param params           参数
     */
    @Test
    public void test1() {
        String a = null;
        // java.lang.IllegalArgumentException: 参数值不允许为 null.
        cn.hutool.core.lang.Assert.notNull(a, "参数值不允许为 null。");
    }

    /**
     * T notNull(T object, String errorMsgTemplate, Object... params)
     * 断言对象是否不为{@code null} ，如果为{@code null} 抛出{@link IllegalArgumentException} 异常 Assert that an object is not {@code null} .
     * * @param <T>              被检查对象泛型类型
     * * @param object           被检查对象
     * * @param errorMsgTemplate 错误消息模板，变量使用{}表示
     */
    @Test
    public void test2() {
        String agencyCode = "001001";
        String result = null;

        // java.lang.IllegalArgumentException: 根据单位编码 001001 查询到的要素结果为null，请检查！
        cn.hutool.core.lang.Assert.notNull(result, "根据单位编码 {} 查询到的要素结果为null，请检查！", agencyCode);
    }

    /**
     * <T, X extends Throwable> T notNull(T object, Supplier<X> errorSupplier)
     * 断言对象是否不为{@code null} ，如果为{@code null} 抛出指定类型异常,并使用指定的函数获取错误信息返回.
     * * @param <T>           被检查对象泛型类型
     * * @param <X>           异常类型
     * * @param object        被检查对象
     * * @param errorSupplier 错误抛出异常附带的消息生产接口
     */
    @Test
    public void test3() {
        String agencyCode = "001001";
        String result = null;

        // org.example.SysException: 根据单位编码 001001 查询到的要素结果为null，请检查！
        cn.hutool.core.lang.Assert.notNull(result, () -> new SysException("根据单位编码 " + agencyCode + " 查询到的要素结果为null，请检查！"));
    }

    /**
     * int checkBetween(int value, int min, int max)
     * long checkBetween(long value, long min, long max)
     * double checkBetween(double value, double min, double max)
     * Number checkBetween(Number value, Number min, Number max)
     * * @param value 值
     * * @param min   最小值（包含）
     * * @param max   最大值（包含）
     * * @return 检查后的长度值
     */
    @Test
    public void test4() {
        int page = 10000;
        // java.lang.IllegalArgumentException: Length must be between 0 and 1000.
        Assert.checkBetween(page, 0, 1000);
    }

    /**
     * int checkIndex(int index, int size)
     * int checkIndex(int index, int size, String errorMsgTemplate, Object... params)
     * 检查下标（数组、集合、字符串）是否符合要求，下标必须满足：
     * * @param index 下标
     * * @param size  长度
     * * @param errorMsgTemplate 异常时的消息模板
     * * @param params           参数列表
     * * @return 检查后的下标
     */
    @Test
    public void test5() {
        int index = 100;
        int size = 50;

        // java.lang.IndexOutOfBoundsException: [Assertion failed] (100) must be less than size (50)
        Assert.checkIndex(index, size);
    }

    /**
     * <T> T[] noNullElements(T[] array)
     * T[] noNullElements(T[] array, String errorMsgTemplate, Object... params)
     * T[] noNullElements(T[] array, Supplier<X> errorSupplier)
     * <p>
     * 断言给定数组是否不包含{@code null}元素，如果数组为空或 {@code null}将被认为不包含
     * * @param <T>   数组元素类型
     * * @param array 被检查的数组
     * * @return 被检查的数组
     */
    @Test
    public void test6() {
        String[] arr1 = null;
        Assert.noNullElements(arr1);

        String[] arr2 = {};
        Assert.noNullElements(arr2);

        String[] arr3 = {"1", "2", null};
        // java.lang.IllegalArgumentException: [Assertion failed] - this array must not contain any null elements
        Assert.noNullElements(arr3);
    }


}
