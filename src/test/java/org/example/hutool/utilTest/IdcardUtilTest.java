package org.example.hutool.utilTest;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdcardUtil;
import org.junit.Test;

/**
 * 身份证工具 - IdcardUtil
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/2/3 11:21
 */
public class IdcardUtilTest {

    /**
     * boolean isValidCard(String idCard)：是否有效身份证号，忽略X的大小写，支持18位、15位和港澳台的10位
     * String convert15To18(String idCard)：将15位身份证号码转换为18位，2000年之后不存在15位身份证号，所以只需要加上年份和最后的校验位即可
     * * 如果 idCard 不是有效得身份证号码，则报错：cn.hutool.core.date.DateException: Parse [xxxxxx] with format [yyMMdd] error!
     * String getBirthByIdCard(String idcard)：根据身份证号获取生日，只支持15或18位身份证号码，返回格式：yyyyMMdd
     * * 如果 idCard 不是有效得身份证号码，则报错：cn.hutool.core.date.DateException: Parse [xxxxxx] with format [yyMMdd] error!
     * int getAgeByIdCard(String idcard)：根据身份编号获取年龄，只支持15或18位身份证号码。计算的是出身日期到当前时间，适合活的人。
     * * 如果 idCard 不是有效得身份证号码，则报错：cn.hutool.core.date.DateException: Parse [xxxxxx] with format [yyMMdd] error!
     * int getAgeByIdCard(String idcard, Date dateToCompare)：根据身份编号获取指定日期当时的年龄年龄，只支持15或18位身份证号码，适合去世的人。
     * * 如果 idCard 不是有效得身份证号码，则报错：cn.hutool.core.date.DateException: Parse [xxxxxx] with format [yyMMdd] error!
     * Short getYearByIdCard(String idcard)：根据身份编号获取出生年份，只支持15或18位身份证号码
     * * 如果 idCard 不是有效得身份证号码，则报错：cn.hutool.core.date.DateException: Parse [xxxxxx] with format [yyMMdd] error!
     * Short getMonthByIdCard(String idcard)：根据身份编号获取生月份，只支持15或18位身份证号码
     * * 如果 idCard 不是有效得身份证号码，则报错：cn.hutool.core.date.DateException: Parse [xxxxxx] with format [yyMMdd] error!
     * Short getDayByIdCard(String idcard)：根据身份编号获取生日，只支持15或18位身份证号码
     * * 如果 idCard 不是有效得身份证号码，则报错：cn.hutool.core.date.DateException: Parse [xxxxxx] with format [yyMMdd] error!
     * int getGenderByIdCard(String idcard)：根据身份编号获取性别，只支持15或18位身份证号码。1-男 ，0-女
     * * 如果 idCard 不是有效得身份证号码，则报错：cn.hutool.core.date.DateException: Parse [xxxxxx] with format [yyMMdd] error!
     * String getProvinceByIdCard(String idcard)：根据身份编号获取户籍省份，只支持15或18位身份证号码
     * * 如果 idCard 不是有效得身份证号码，则返回 null
     * String getCityCodeByIdCard(String idcard) 根据身份编号获取市级编码，只支持15或18位身份证号码
     * String hide(String idcard, int startInclude, int endExclude) : 隐藏指定位置[startInclude,endExclude)几个身份证号数字为“*”
     */
    @Test
    public void test1() {
        String iden_no = "410105391028271";
//        String iden_no = "41010519821014103X";

        boolean valid15 = IdcardUtil.isValidCard(iden_no);
        System.out.println("valid15=" + valid15);//valid15=true

        String convert15To18 = IdcardUtil.convert15To18(iden_no);
        System.out.println(convert15To18);//410105193910282711

        String birth = IdcardUtil.getBirthByIdCard(iden_no);
        DateTime birthDate = IdcardUtil.getBirthDate(iden_no);
        System.out.println("birth=" + birth);//birth=19391028
        System.out.println("birthDate=" + birthDate);//birthDate=1939-10-28 00:00:00

        int age1 = IdcardUtil.getAgeByIdCard(iden_no);
        int age2 = IdcardUtil.getAgeByIdCard(iden_no, DateUtil.parse("1993-08-25"));
        System.out.println("age1=" + age1);//age1=81
        System.out.println("age2=" + age2);//age2=53

        System.out.println(IdcardUtil.getYearByIdCard(iden_no));//1939
        System.out.println(IdcardUtil.getMonthByIdCard(iden_no));//10
        System.out.println(IdcardUtil.getDayByIdCard(iden_no));//28

        int gender = IdcardUtil.getGenderByIdCard(iden_no);
        System.out.println(gender == 0 ? "女" : "男");//男

        String province = IdcardUtil.getProvinceByIdCard(iden_no);
        String cityCodeByIdCard = IdcardUtil.getCityCodeByIdCard(iden_no);
        System.out.println("province=" + province);//河南
        System.out.println("cityCodeByIdCard=" + cityCodeByIdCard);//河南

        String hide = IdcardUtil.hide(iden_no, 5, 12);
        System.out.println(hide);//41010*******271
    }

    /**
     * int getAgeByIdCard(String idcard, Date dateToCompare) :根据身份编号获取指定日期当时的年龄年龄，只支持15或18位身份证号码
     * * @param idcard        身份编号
     * * @param dateToCompare 以此日期为界，计算年龄。
     */
    @Test
    public void test2() {
        int age1 = IdcardUtil.getAgeByIdCard("432524199305198317", DateUtil.parse("20230518"));
        // 29
        System.out.println(age1);

        int age2 = IdcardUtil.getAgeByIdCard("xxxxxx19930518xxxx", DateUtil.parse("20230518"));
        // 30
        System.out.println(age2);
    }

}
