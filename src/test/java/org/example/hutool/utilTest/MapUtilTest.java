package org.example.hutool.utilTest;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.*;

/**
 * MapUtil 是针对Map的一系列工具方法的封装，包括getXXX的快捷值转换方法。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2023/2/17 14:51
 */
public class MapUtilTest {

    /**
     * HashMap<Object, Object> of(Object[] array)
     * 将数组转换为Map（HashMap），支持数组元素类型为：
     * * <pre>
     * 	 * Map.Entry
     * 	 * 长度大于1的数组（取前两个值），如果不满足跳过此元素
     * 	 * Iterable 长度也必须大于1（取前两个值），如果不满足跳过此元素
     * 	 * Iterator 长度也必须大于1（取前两个值），如果不满足跳过此元素
     * 	 * </pre>
     */
    @Test
    public void ofTest() {
        Map<Object, Object> colorMap = MapUtil.of(new String[][]{
                {"RED", "#FF0000"},
                {"GREEN", "#00FF00"},
                {"BLUE", "#0000FF"}
        });
        // {RED=#FF0000, GREEN=#00FF00, BLUE=#0000FF}
        System.out.println(colorMap);
    }

    /**
     * Map<K, List<V>> toListMap(Iterable<? extends Map<K, V>> mapList
     * 行转列，合并相同的键，值合并为列表<br>
     * 将Map列表中相同key的值组成列表做为Map的value<br>
     * 是 toMapList 的逆方法<br>
     * <p>
     * 比如传入数据：
     * <pre>
     * [
     *  {a: 1, b: 1, c: 1}
     *  {a: 2, b: 2}
     *  {a: 3, b: 3}
     *  {a: 4}
     * ]
     * </pre>
     * <p>
     * 结果是：
     * <pre>
     * {
     *   a: [1,2,3,4]
     *   b: [1,2,3,]
     *   c: [1]
     * }
     * </pre>
     * <p>
     * List<Map<K, V>> toMapList(Map<K, ? extends Iterable<V>> listMap)：列转行，是 toListMap 的逆方法<br>
     *
     * @return Map
     */
    @Test
    public void toListMapTest() {
        List<Map<String, Object>> mapList = new ArrayList<>();
        Map<String, Object> map1 = new LinkedHashMap<>();
        Map<String, Object> map2 = new LinkedHashMap<>();
        Map<String, Object> map3 = new LinkedHashMap<>();

        map1.put("city_name", "深圳");
        map1.put("year", "2023");
        map1.put("all_salary", 450006.45F);

        map2.put("city_name", "北京");
        map2.put("year", "2021");
        map2.put("all_salary", null);

        map3.put("city_name", "深圳");
        map3.put("all_salary", 551006.45F);

        mapList.add(map1);
        mapList.add(map2);
        mapList.add(map3);

        Map<String, List<Object>> listMap = MapUtil.toListMap(mapList);
        // {all_salary=[450006.44, null, 551006.44], city_name=[深圳, 北京, 深圳], year=[2023, 2021]}
        System.out.println(listMap);

        List<Map<String, Object>> mapList1 = MapUtil.toMapList(listMap);
        // [
        // {all_salary=450006.44, city_name=深圳, year=2023},
        // {all_salary=null, city_name=北京, year=2021},
        // {all_salary=551006.44, city_name=深圳}
        // ]
        System.out.println(mapList1);
    }

    /**
     * String join(Map<K, V> map, String separator, String keyValueSeparator, boolean isIgnoreNull, String... otherParams)
     * String sortJoin(Map<?, ?> params, String separator, String keyValueSeparator, boolean isIgnoreNull, String... otherParams)
     * 根据参数排序后拼接为字符串，常用于签名。
     * * @param params            参数
     * * @param separator         entry之间的连接符
     * * @param keyValueSeparator kv之间的连接符
     * * @param isIgnoreNull      是否忽略null的键和值
     * * @param otherParams       其它附加参数字符串（例如密钥）
     * * @return 签名字符串
     * String joinIgnoreNull(Map<K, V> map, String separator, String keyValueSeparator, String... otherParams)
     * 将map转成字符串，忽略null的键和值
     */
    @Test
    public void joinTest() {
        Map<String, Object> map2 = new LinkedHashMap<>();
        map2.put("city_name", "北京");
        map2.put("year", "2021");
        map2.put("all_salary", null);

        String join1 = MapUtil.sortJoin(map2, StrUtil.EMPTY, StrUtil.EMPTY, false);
        String join2 = MapUtil.sortJoin(map2, ",", "=", false, "_CYHY");

        // all_salarynullcity_name北京year2021
        System.out.println(join1);
        // all_salary=null,city_name=北京,year=2021_CYHY
        System.out.println(join2);

        String join3 = MapUtil.join(map2, ",", "=", false);
        // city_name=北京,year=2021,all_salary=null
        System.out.println(join3);

        String join4 = MapUtil.joinIgnoreNull(map2, ",", "=");
        // city_name=北京,year=2021
        System.out.println(join4);
    }

    /**
     * Map<T, T> reverse(Map<T, T> map)
     * * Map的键和值互换
     * * 互换键值对不检查值是否有重复，如果有则后加入的元素替换先加入的元素<br>
     * * 值的顺序在HashMap中不确定，所以谁覆盖谁也不确定，在有序的Map中按照先后顺序覆盖，保留最后的值
     */
    @Test
    public void reverseTest() {
        Map<Object, Object> map2 = new LinkedHashMap<>();
        map2.put("city_name", "北京");
        map2.put("year", "2021");
        map2.put("all_salary", null);
        map2.put("pre_year", "2021");

        Map<Object, Object> reverse = MapUtil.reverse(map2);
        // {北京=city_name, 2021=pre_year, null=all_salary}
        System.out.println(reverse);
    }

    /**
     * T get(Map<?, ?> map, Object key, Class<T> type)
     * 1、解析数字类型时，如果无法解析，则抛差异；解析日期类型时，如果无法解析，则返回null。
     * <p>
     * T get(Map<?, ?> map, Object key, Class<T> type, T defaultValue)
     * 1、解析数字类型时，如果无法解析，则抛差异；解析日期类型时，如果无法解析，则使用默认值。
     * <p>
     * T get(Map<?, ?> map, Object key, TypeReference<T> type, T defaultValue)
     * 1、获取Map指定key的值，并转换为指定类型
     * 2、如果无法解析，则异常；日期比较特殊：如果无法解析，则使用默认值，如果没有设置默认值，则返回 null。
     */
    @Test
    public void getTest1() {
        Map<Object, Object> map2 = new LinkedHashMap<>();
        map2.put("city_name", "北京");
        map2.put("year", 2021);
        map2.put("is_marry", true);

        List<Map<String, Object>> mapList = new ArrayList<>();
        Map<String, Object> map11 = new LinkedHashMap<>();
        Map<String, Object> map21 = new LinkedHashMap<>();
        Map<String, Object> map31 = new LinkedHashMap<>();

        map11.put("city_name", "深圳");
        map11.put("year", "2023");
        map11.put("all_salary", 450006.45F);

        map21.put("city_name", "北京");
        map21.put("year", "2021");
        map21.put("all_salary", null);

        map31.put("city_name", "深圳");
        map31.put("all_salary", 551006.45F);

        mapList.add(map11);
        mapList.add(map21);
        mapList.add(map31);

        map2.put("mapList", mapList);

        String cityName = MapUtil.get(map2, "city_name", String.class, "");
        Float year = MapUtil.getFloat(map2, "year", 0F);
        Boolean isMarry = MapUtil.getBool(map2, "is_marry");

        System.out.println(cityName);// 北京
        System.out.println(year);// 2021.0
        System.out.println(isMarry);// true

        List<Map<String, Object>> agencyCodeList = MapUtil.get(map2, "mapList", new TypeReference<List<Map<String, Object>>>() {
            @Override
            public Type getType() {
                return super.getType();
            }
        }, new ArrayList<>());

        // [{all_salary=450006.44, city_name=深圳, year=2023}, {all_salary=null, city_name=北京, year=2021}, {all_salary=551006.44, city_name=深圳}]
        System.out.println(agencyCodeList);
    }

    /**
     * 获取Map指定key的值，并转换为Integer，转换不了时报错：java.lang.NumberFormatException: Unparseable number: "北京"
     * Integer getInt(Map<?, ?> map, Object key)
     * Integer getInt(Map<?, ?> map, Object key, Integer defaultValue)
     */
    @Test
    public void getIntTest1() {
        Map<Object, Object> map2 = new LinkedHashMap<>();
        map2.put("year", 2021);
        map2.put("sal", "8858.68");
        map2.put("name", "中国");
        map2.put("sal3", "");
        map2.put("sal4", " ");
        map2.put("sal5", "2020阿BC");
        map2.put("sal6", "和环境2020阿BC");

        // null
        Integer sal = MapUtil.getInt(null, "sal", 10000);
        System.out.println(sal);

        // 8858
        System.out.println(MapUtil.getInt(map2, "sal", 10000));
        // 10000
        System.out.println(MapUtil.getInt(map2, "sal2", 10000));
        // 10000
        System.out.println(MapUtil.getInt(map2, "sal3", 10000));
        // 2220
        System.out.println(MapUtil.getInt(map2, "sal4", 2220));
        // 2020
        System.out.println(MapUtil.getInt(map2, "sal5", 222));

        // java.lang.NumberFormatException: Unparseable number: "和环境2020阿BC"
        // System.out.println(MapUtil.getInt(map2, "sal6", 333));

        // java.lang.NumberFormatException: Unparseable number: "中国"
        // System.out.println(MapUtil.getInt(map2, "name", 10000));
    }

    /**
     * 获取Map指定key的值，并转换为 Float，转换不了时报错：java.lang.NumberFormatException: Unparseable number: "北京"
     * Float getFloat(Map<?, ?> map, Object key)
     * Float getFloat(Map<?, ?> map, Object key, Integer defaultValue)
     */
    @Test
    public void getFloatTest1() {
        Map<Object, Object> map2 = new LinkedHashMap<>();
        map2.put("year", 2021);
        map2.put("sal", "8858.68");
        map2.put("name", "中国");
        map2.put("sal3", "");
        map2.put("sal4", " ");
        map2.put("sal5", "2020阿BC");
        map2.put("sal6", "和环境2020阿BC");

        // null
        Float sal = MapUtil.getFloat(null, "sal", 1000.0F);
        System.out.println(sal);

        // 8858.68
        System.out.println(MapUtil.getFloat(map2, "sal", 1000.0F));
        // 1000.0
        System.out.println(MapUtil.getFloat(map2, "sal2", 1000.0F));
        // 1000.0
        System.out.println(MapUtil.getFloat(map2, "sal3", 1000.0F));
        // 222.0
        System.out.println(MapUtil.getFloat(map2, "sal4", 222.0F));
        // 2020.0
        System.out.println(MapUtil.getFloat(map2, "sal5", 22.2F));

        // java.lang.NumberFormatException: Unparseable number: "和环境2020阿BC"
        // System.out.println(MapUtil.getFloat(map2, "sal6", 333F));

        // java.lang.NumberFormatException: Unparseable number: "中国"
        // System.out.println(MapUtil.getFloat(map2, "name", 1001.0F));
    }

    /**
     * 获取Map指定key的值，并转换为字符串
     * String getStr(Map<?, ?> map, Object key)
     * String getStr(Map<?, ?> map, Object key, String defaultValue)
     * * defaultValue 默认值
     */
    @Test
    public void getStrTest1() {
        Map<Object, Object> map2 = new LinkedHashMap<>();
        map2.put("city_name", "长沙");
        map2.put("year", 2021);
        map2.put("is_marry", true);

        // 长沙
        String cityName = MapUtil.getStr(map2, "city_name", "");
        System.out.println(cityName);

        System.out.println(MapUtil.getStr(map2, ""));// null
        System.out.println(MapUtil.getStr(map2, " "));// null
        System.out.println(MapUtil.getStr(map2, null));// null
        System.out.println(MapUtil.getStr(null, null));// null

        System.out.println("====================================");

        // null
        System.out.println(MapUtil.getStr(null, "code"));
        // null
        System.out.println(MapUtil.getStr(null, "code", "001"));
        // 001
        System.out.println(MapUtil.getStr(map2, "code", "001"));
        // 长沙
        System.out.println(MapUtil.getStr(map2, "city_name", "001"));
    }

    /**
     * Date getDate(Map<?, ?> map, Object key) 获取Map指定key的值，并转换为{@link Date}
     * 1、如果无法解析，则使用默认值，如果没有设置默认值，则返回 null。
     */
    @Test
    public void getDateTest() {
        Map<Object, Object> map2 = new LinkedHashMap<>();
        map2.put("date0", 2021);
        map2.put("date1", "1993/08/26");
        map2.put("date2", "1993-08-27");
        map2.put("date3", "1993-08-27 12:00:45");
        map2.put("date4", "1993-08-27 uiuiu");
        map2.put("date5", "jdjhjfh");
        map2.put("date6", DateUtil.parse("1993/08/25"));
        map2.put("date7", 0);

        // Thu Jan 01 08:00:02 CST 1970
        System.out.println(MapUtil.getDate(map2, "date0"));
        // Thu Aug 26 00:00:00 CST 1993
        System.out.println(MapUtil.getDate(map2, "date1"));
        // Fri Aug 27 00:00:00 CST 1993
        System.out.println(MapUtil.getDate(map2, "date2"));
        // Fri Aug 27 12:00:45 CST 1993
        System.out.println(MapUtil.getDate(map2, "date3"));
        // null
        System.out.println(MapUtil.getDate(map2, "date4"));
        // null
        System.out.println(MapUtil.getDate(map2, "date5"));
        // 1993-08-25 00:00:00
        System.out.println(MapUtil.getDate(map2, "date6"));
        // Thu Jan 01 08:00:00 CST 1970
        System.out.println(MapUtil.getDate(map2, "date7"));
    }


}
