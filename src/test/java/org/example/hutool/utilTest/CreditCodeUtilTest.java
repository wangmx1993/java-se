package org.example.hutool.utilTest;

import cn.hutool.core.util.CreditCodeUtil;
import org.junit.Test;

/**
 * 统一社会信用代码工具类 CreditCodeUtil
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/12 14:47
 */
public class CreditCodeUtilTest {

    /**
     * String randomCreditCode(): 获取一个随机的统一社会信用代码
     */
    @Test
    public void testRandomCreditCode() {
        for (int i = 0; i < 10; i++) {
            String randomCreditCode = CreditCodeUtil.randomCreditCode();
            // CU37779212JHADK3EL 长度=18
            // DA207251NN8UU1153Q 长度=18
            // W7552185DXFM8EC9JG 长度=18
            // 1X2189372E4TC0GX01 长度=18
            // 18691772E8TKFJ4XXK 长度=18
            // AQ229009M0F47U60QX 长度=18
            // H2730864Q3A5JBHWW4 长度=18
            // JW181199WHGNPCR24F 长度=18
            // EJ720366PBJCK16TG7 长度=18
            // 9G6212620QUQRWLEMR 长度=18
            System.out.println(randomCreditCode + " 长度=" + randomCreditCode.length());
        }
    }

    /**
     * boolean isCreditCode(CharSequence creditCode): 是否是有效的统一社会信用代码
     * boolean isCreditCodeSimple(CharSequence creditCode)：正则校验统一社会信用代码（18位），比上面的校验条件要宽松一些
     * * 第一部分：登记管理部门代码1位 (数字或大写英文字母)
     * * 第二部分：机构类别代码1位 (数字或大写英文字母)
     * * 第三部分：登记管理机关行政区划码6位 (数字)
     * * 第四部分：主体标识码（组织机构代码）9位 (数字或大写英文字母)
     * * 第五部分：校验码1位 (数字或大写英文字母)
     */
    @Test
    public void testIsCreditCode() {
        String creditCode = "123456789012345671";
        boolean isCreditCode = CreditCodeUtil.isCreditCode(creditCode);
        System.out.println(isCreditCode);//false

        boolean isCreditCodeSimple = CreditCodeUtil.isCreditCodeSimple(creditCode);
        System.out.println(isCreditCodeSimple);//true
    }


}
