package org.example.hutool.utilTest;

import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.util.ReflectUtil;
import org.example.uitls.BasicDataRuleUtil;
import org.junit.Test;
import org.se.exception.BasicException;
import org.springframework.util.Assert;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 异常工具-ExceptionUtil
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2022/12/16 17:03
 */
@SuppressWarnings("all")
public class ExceptionUtilTest {

    /**
     * Throwable getCausedBy(Throwable throwable, Class<? extends Exception>... causeClasses)
     * 1、获取是否由指定异常类(可以是整个异常链中的任意一个)引起的异常，如果是则返回目标异常(包括它的下层异常)，如果不是，则返回 null。
     * 2、比如一层一层调用下去之后，某一层校验未通过，抛出了自定义异常，当最终捕获处理的时候，我们只关心原始的自定义异常，后面嵌套包装的异常并不关心。
     *
     * @throws Throwable
     */
    @Test
    public void getCausedByTest() throws Throwable {
        String methodName = "agencyTypeErrorRule";
        Method agencyTypeErrorRule = ReflectUtil.getMethod(BasicDataRuleUtil.class, methodName, Map.class);
        Assert.notNull(agencyTypeErrorRule, methodName + " 方法未找到！");
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("agency_type_code", "100");
        try {
            /**抛出异常：
             * cn.hutool.core.exceptions.UtilException: InvocationTargetException: null
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:922)
             * 	at cn.hutool.core.util.ReflectUtil.invokeStatic(ReflectUtil.java:832)
             * 	at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)
             * Caused by: java.lang.reflect.InvocationTargetException
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
             * 	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
             * 	at java.lang.reflect.Method.invoke(Method.java:498)
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:920)
             * 	... 27 more
             * Caused by: org.se.exception.BasicException: 行政单位的经费供给方式必须填写！
             * 	at org.example.uitls.BasicDataRuleUtil.agencyTypeErrorRule(BasicDataRuleUtil.java:45)
             * 	... 32 more
             */
            Map<String, String> invoke = ReflectUtil.invokeStatic(agencyTypeErrorRule, dataMap);
            System.out.println(invoke);
        } catch (Exception e) {
            // 如上面所示，一个嵌套了三层异常，当这里取第3层时，结果中只包含第3层，取第2层时，结果中包含第2第3层。
            Throwable causedBy = ExceptionUtil.getCausedBy(e, BasicException.class);
            // Throwable causedBy = ExceptionUtil.getCausedBy(e, BasicException.class);
            if (causedBy != null) {
                throw causedBy;
            } else {
                throw e;
            }
        }
    }

    /**
     * String getMessage(Throwable e)
     * 1、获得完整消息，包括异常名，消息格式为：{SimpleClassName}: {ThrowableMessage}
     * 2、底层就是简单的 e.getMessage()，只适合获取单个异常信息，不适合读取嵌套异常信息，也不是适合取完整异常信息。
     * String getSimpleMessage(Throwable e)
     * 1、获得消息，底层就是简单的 e.getMessage()，不适合读取嵌套异常信息，也不是适合取完整异常信息。
     *
     * @throws Throwable
     */
    @Test
    public void getMessageTest() throws Throwable {
        String methodName = "agencyTypeErrorRule";
        Method agencyTypeErrorRule = ReflectUtil.getMethod(BasicDataRuleUtil.class, methodName, Map.class);
        Assert.notNull(agencyTypeErrorRule, methodName + " 方法未找到！");
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("agency_type_code", "100");
        try {
            /**抛出异常：
             * cn.hutool.core.exceptions.UtilException: InvocationTargetException: null
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:922)
             * 	at cn.hutool.core.util.ReflectUtil.invokeStatic(ReflectUtil.java:832)
             * 	at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)
             * Caused by: java.lang.reflect.InvocationTargetException
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
             * 	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
             * 	at java.lang.reflect.Method.invoke(Method.java:498)
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:920)
             * 	... 27 more
             * Caused by: org.se.exception.BasicException: 行政单位的经费供给方式必须填写！
             * 	at org.example.uitls.BasicDataRuleUtil.agencyTypeErrorRule(BasicDataRuleUtil.java:45)
             * 	... 32 more
             */
            Map<String, String> invoke = ReflectUtil.invokeStatic(agencyTypeErrorRule, dataMap);
            System.out.println(invoke);
        } catch (Exception e) {
            // UtilException: InvocationTargetException: null
            String message = ExceptionUtil.getMessage(e);
            System.out.println(message);
        }
    }

    /**
     * Throwable getRootCause(final Throwable throwable)
     * 1、获取异常链中最尾端的异常，即异常最早发生的异常对象。<br>
     * 2、此方法通过调用{@link Throwable#getCause()} 直到没有cause为止，如果异常本身没有cause，返回异常本身<br>
     * 3、传入null返回也为null
     *
     * @throws Throwable
     */
    @Test
    public void getRootCauseTest() throws Throwable {
        String methodName = "agencyTypeErrorRule";
        Method agencyTypeErrorRule = ReflectUtil.getMethod(BasicDataRuleUtil.class, methodName, Map.class);
        Assert.notNull(agencyTypeErrorRule, methodName + " 方法未找到！");
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("agency_type_code", "100");
        try {
            /**抛出异常：
             * cn.hutool.core.exceptions.UtilException: InvocationTargetException: null
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:922)
             * 	at cn.hutool.core.util.ReflectUtil.invokeStatic(ReflectUtil.java:832)
             * 	at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)
             * Caused by: java.lang.reflect.InvocationTargetException
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
             * 	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
             * 	at java.lang.reflect.Method.invoke(Method.java:498)
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:920)
             * 	... 27 more
             * Caused by: org.se.exception.BasicException: 行政单位的经费供给方式必须填写！
             * 	at org.example.uitls.BasicDataRuleUtil.agencyTypeErrorRule(BasicDataRuleUtil.java:45)
             * 	... 32 more
             */
            Map<String, String> invoke = ReflectUtil.invokeStatic(agencyTypeErrorRule, dataMap);
            System.out.println(invoke);
        } catch (Exception e) {
            // 如果最底层的是自定义异常，则只往外抛出它，否则原样抛出
            Throwable causedBy = ExceptionUtil.getRootCause(e);
            if (causedBy instanceof BasicException) {
                throw causedBy;
            } else {
                throw e;
            }
        }
    }

    /**
     * String getRootCauseMessage(final Throwable th)
     * 1、获取异常链中最尾端的异常的消息，消息格式为：{SimpleClassName}: {ThrowableMessage}
     * 2、就是取了最底层的异常之后，e.getMessage() 一下
     *
     * @throws Throwable
     */
    @Test
    public void getRootCauseMessageTest() throws Throwable {
        String methodName = "agencyTypeErrorRule";
        Method agencyTypeErrorRule = ReflectUtil.getMethod(BasicDataRuleUtil.class, methodName, Map.class);
        Assert.notNull(agencyTypeErrorRule, methodName + " 方法未找到！");
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("agency_type_code", "100");
        try {
            /**抛出异常：
             * cn.hutool.core.exceptions.UtilException: InvocationTargetException: null
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:922)
             * 	at cn.hutool.core.util.ReflectUtil.invokeStatic(ReflectUtil.java:832)
             * 	at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)
             * Caused by: java.lang.reflect.InvocationTargetException
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
             * 	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
             * 	at java.lang.reflect.Method.invoke(Method.java:498)
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:920)
             * 	... 27 more
             * Caused by: org.se.exception.BasicException: 行政单位的经费供给方式必须填写！
             * 	at org.example.uitls.BasicDataRuleUtil.agencyTypeErrorRule(BasicDataRuleUtil.java:45)
             * 	... 32 more
             */
            Map<String, String> invoke = ReflectUtil.invokeStatic(agencyTypeErrorRule, dataMap);
            System.out.println(invoke);
        } catch (Exception e) {
            // BasicException: 行政单位的经费供给方式必须填写！
            String message = ExceptionUtil.getRootCauseMessage(e);
            System.out.println(message);
        }
    }

    /**
     * List<Throwable> getThrowableList(Throwable throwable)
     * 1、获取异常链上所有异常的集合，如果{@link Throwable} 对象没有cause，返回只有一个节点的List<br>
     * 2、如果传入null，返回空集合
     *
     * @throws Throwable
     */
    @Test
    public void getThrowableListTest() throws Throwable {
        String methodName = "agencyTypeErrorRule";
        Method agencyTypeErrorRule = ReflectUtil.getMethod(BasicDataRuleUtil.class, methodName, Map.class);
        Assert.notNull(agencyTypeErrorRule, methodName + " 方法未找到！");
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("agency_type_code", "100");
        try {
            /**抛出异常：
             * cn.hutool.core.exceptions.UtilException: InvocationTargetException: null
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:922)
             * 	at cn.hutool.core.util.ReflectUtil.invokeStatic(ReflectUtil.java:832)
             * 	at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)
             * Caused by: java.lang.reflect.InvocationTargetException
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
             * 	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
             * 	at java.lang.reflect.Method.invoke(Method.java:498)
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:920)
             * 	... 27 more
             * Caused by: org.se.exception.BasicException: 行政单位的经费供给方式必须填写！
             * 	at org.example.uitls.BasicDataRuleUtil.agencyTypeErrorRule(BasicDataRuleUtil.java:45)
             * 	... 32 more
             */
            Map<String, String> invoke = ReflectUtil.invokeStatic(agencyTypeErrorRule, dataMap);
            System.out.println(invoke);
        } catch (Exception e) {
            // 获取异常链上所有异常的集合
            List<Throwable> throwables = ExceptionUtil.getThrowableList(e);
            // 往外抛出最底层的原始异常
            throw throwables.get(throwables.size() - 1);
        }
    }

    /**
     * boolean isCausedBy(Throwable throwable, Class<? extends Exception>... causeClasses)
     * 1、获取由指定异常类(可以是整个异常链中的任意一个)引起的异常
     */
    @Test
    public void isCausedByTest() throws Throwable {
        String methodName = "agencyTypeErrorRule";
        Method agencyTypeErrorRule = ReflectUtil.getMethod(BasicDataRuleUtil.class, methodName, Map.class);
        Assert.notNull(agencyTypeErrorRule, methodName + " 方法未找到！");
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("agency_type_code", "100");
        try {
            /**抛出异常：
             * cn.hutool.core.exceptions.UtilException: InvocationTargetException: null
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:922)
             * 	at cn.hutool.core.util.ReflectUtil.invokeStatic(ReflectUtil.java:832)
             * 	at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)
             * Caused by: java.lang.reflect.InvocationTargetException
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
             * 	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
             * 	at java.lang.reflect.Method.invoke(Method.java:498)
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:920)
             * 	... 27 more
             * Caused by: org.se.exception.BasicException: 行政单位的经费供给方式必须填写！
             * 	at org.example.uitls.BasicDataRuleUtil.agencyTypeErrorRule(BasicDataRuleUtil.java:45)
             * 	... 32 more
             */
            Map<String, String> invoke = ReflectUtil.invokeStatic(agencyTypeErrorRule, dataMap);
            System.out.println(invoke);
        } catch (Exception e) {
            // boolean causedBy = ExceptionUtil.isCausedBy(e, BasicException.class);
            boolean causedBy = ExceptionUtil.isCausedBy(e, InvocationTargetException.class);
            // true
            System.out.println(causedBy);
        }
    }

    /**
     * String stacktraceToOneLineString(Throwable throwable)
     * String stacktraceToOneLineString(Throwable throwable, int limit)
     * 1、将整个堆栈链转为单行完整字符串
     * * @param throwable 异常对象
     * * @param limit     限制最大长度
     * String stacktraceToString(Throwable throwable)
     * String stacktraceToString(Throwable throwable, int limit)
     * 1、将整个堆栈链转为完整字符串
     * * @param throwable 异常对象
     * * @param limit     限制最大长度，默认取 3000 个字符长度。
     *
     * @throws Throwable
     */
    @Test
    public void stacktraceToStringTest() throws Throwable {
        String methodName = "agencyTypeErrorRule";
        Method agencyTypeErrorRule = ReflectUtil.getMethod(BasicDataRuleUtil.class, methodName, Map.class);
        Assert.notNull(agencyTypeErrorRule, methodName + " 方法未找到！");
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("agency_type_code", "100");
        try {
            /**抛出异常：
             * cn.hutool.core.exceptions.UtilException: InvocationTargetException: null
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:922)
             * 	at cn.hutool.core.util.ReflectUtil.invokeStatic(ReflectUtil.java:832)
             * 	at com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)
             * Caused by: java.lang.reflect.InvocationTargetException
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
             * 	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
             * 	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
             * 	at java.lang.reflect.Method.invoke(Method.java:498)
             * 	at cn.hutool.core.util.ReflectUtil.invoke(ReflectUtil.java:920)
             * 	... 27 more
             * Caused by: org.se.exception.BasicException: 行政单位的经费供给方式必须填写！
             * 	at org.example.uitls.BasicDataRuleUtil.agencyTypeErrorRule(BasicDataRuleUtil.java:45)
             * 	... 32 more
             */
            Map<String, String> invoke = ReflectUtil.invokeStatic(agencyTypeErrorRule, dataMap);
            System.out.println(invoke);
        } catch (Exception e) {
            // String oneLineString = ExceptionUtil.stacktraceToOneLineString(e);
            String oneLineString = ExceptionUtil.stacktraceToString(e, Integer.MAX_VALUE);
            System.out.println(oneLineString);
        }
    }

}
