package org.example.hutool.utilTest;

import cn.hutool.core.lang.Console;
import cn.hutool.core.thread.ConcurrencyTester;
import cn.hutool.core.thread.ExecutorBuilder;
import cn.hutool.core.thread.RejectPolicy;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.RandomUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.concurrent.*;

/**
 * 线程工具-ThreadUtil
 * Hutool使用GlobalThreadPool持有一个全局的线程池，默认所有异步方法在这个线程池中执行。
 * <p>
 * 自定义线程池-ExecutorBuilder
 * 在JDK中，提供了Executors用于创建自定义的线程池对象ExecutorService，但是考虑到线程池中存在众多概念，
 * 这些概念通过不同的搭配实现灵活的线程管理策略，单独使用Executors无法满足需求，构建了ExecutorBuilder。
 * <pre>
 *     corePoolSize 初始池大小
 *     maxPoolSize 最大池大小（允许同时执行的最大线程数）
 *     workQueue 队列，用于存在未执行的线程
 *     handler 当线程阻塞（block）时的异常处理器，所谓线程阻塞即线程池和等待队列已满，无法处理线程时采取的策略
 * </pre>
 * <pre>
 *   线程池对待线程的策略:
 *     如果池中任务数 < corePoolSize -> 放入立即执行
 *     如果池中任务数 > corePoolSize -> 放入队列等待
 *     队列满 -> 新建线程立即执行
 *     执行中的线程 > maxPoolSize -> 触发handler（RejectedExecutionHandler）异常
 * </pre>
 * <pre>
 *     workQueue线程池策略
 *     SynchronousQueue 它将任务直接提交给线程而不保持它们。当运行线程小于maxPoolSize时会创建新线程，否则触发异常策略
 *     LinkedBlockingQueue 默认无界队列，当运行线程大于corePoolSize时始终放入此队列，此时maxPoolSize无效，
 *          当构造LinkedBlockingQueue对象时传入参数，变为有界队列，队列满时，运行线程小于maxPoolSize时会创建新线程，否则触发异常策略
 *     ArrayBlockingQueue 有界队列，相对无界队列有利于控制队列大小，队列满时，运行线程小于maxPoolSize时会创建新线程，否则触发异常策略
 * </pre>
 * <p>
 * 高并发测试-ConcurrencyTester
 * 很多时候，我们需要简单模拟N个线程调用某个业务测试其并发状况，于是Hutool提供了一个简单的并发测试类——ConcurrencyTester。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2023/5/27 20:37
 */
public class ThreadUtilTest {

    private static final Logger log = LoggerFactory.getLogger(ThreadUtilTest.class);

    /**
     * void execute(Runnable runnable)：直接在公共线程池中执行线程
     * boolean sleep(long millis)：挂起当前线程，是Thread.sleep的封装，通过返回boolean值表示是否被打断，而不是抛出异常。
     * boolean safeSleep(long millis): 保证挂起足够时间的方法，当给定一个挂起时间，使用此方法可以保证挂起的时间大于或等于给定时间，
     * 解决Thread.sleep挂起时间不足问题，此方法在Hutool-cron的定时器中使用保证定时任务执行的准确性。
     */
    @Test
    public void executeTest() {
        ThreadUtil.execute(new Runnable() {
            @Override
            public void run() {
                int timer = 500 + new Random().nextInt(3000);
                log.info("支线任务开始，预计" + timer + "毫秒。");
                ThreadUtil.sleep(timer);
                log.info("支线任务完成。");
            }
        });
        log.info("主线任务5秒后完成。");
        ThreadUtil.safeSleep(5000);
        log.info("主线任务完成。");
        // [21:03:29:954] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.executeTest(ThreadUtilTest.java:42) - 主线任务5秒后完成。
        // [21:03:30:057] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$1.run(ThreadUtilTest.java:34) - 支线任务开始，预计1128毫秒。
        // [21:03:31:186] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$1.run(ThreadUtilTest.java:36) - 支线任务完成。
        // [21:03:34:979] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.executeTest(ThreadUtilTest.java:44) - 主线任务完成。
    }

    /**
     * Future<?> execAsync(Runnable runnable)：执行有返回值的异步方法
     * Future代表一个异步执行的操作，通过get()方法可以获得操作的结果，如果异步操作还没有完成，则，get()会使当前线程阻塞
     *
     * @throws InterruptedException
     */
    @Test
    public void execAsyncTest1() {
        ThreadUtil.execAsync(new Runnable() {
            @Override
            public void run() {
                int timer = 500 + new Random().nextInt(3000);
                log.info("支线任务开始，预计" + timer + "毫秒。");
                ThreadUtil.sleep(timer);
                log.info("支线任务完成。");
            }
        });
        log.info("主线任务5秒后完成。");
        ThreadUtil.safeSleep(5000);
        log.info("主线任务完成。");
        // [21:04:47:384] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.execAsyncTest(ThreadUtilTest.java:66) - 主线任务5秒后完成。
        // [21:04:47:424] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$2.run(ThreadUtilTest.java:58) - 支线任务开始，预计2022毫秒。
        // [21:04:49:449] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$2.run(ThreadUtilTest.java:60) - 支线任务完成。
        // [21:04:52:407] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.execAsyncTest(ThreadUtilTest.java:68) - 主线任务完成。
    }

    /**
     * Future<T> execAsync(Callable<T> task)：执行有返回值的异步方法
     * Future代表一个异步执行的操作，通过get()方法可以获得操作的结果，如果异步操作还没有完成，则，get()会使当前线程阻塞
     */
    @Test
    public void execAsyncTest2() throws ExecutionException, InterruptedException {
        Future<String> future = ThreadUtil.execAsync(new Callable<String>() {
            @Override
            public String call() throws Exception {
                int timer = 500 + new Random().nextInt(3000);
                log.info("支线任务开始，预计" + timer + "毫秒。");
                ThreadUtil.sleep(timer);
                log.info("支线任务完成。");
                return "200";
            }
        });
        log.info("主线开始等待子线程执行结果。");
        String s = future.get();
        log.info("主线任务完成，子线程返回结果：" + s);
        // [21:49:09:746] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.execAsyncTest2(ThreadUtilTest.java:91) - 主线开始等待子线程执行结果。
        // [21:49:09:863] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$3.call(ThreadUtilTest.java:85) - 支线任务开始，预计1475毫秒。
        // [21:49:11:338] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$3.call(ThreadUtilTest.java:87) - 支线任务完成。
        // [21:49:11:338] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.execAsyncTest2(ThreadUtilTest.java:93) - 主线任务完成，子线程返回结果：200
    }

    /**
     * ExecutorService newExecutor()
     * 获得一个新的线程池，默认的策略如下：
     * <pre>
     *    1. 初始线程数为 0
     *    2. 最大线程数为Integer.MAX_VALUE
     *    3. 使用SynchronousQueue
     *    4. 任务直接提交给线程而不保持它们
     * </pre>
     * ExecutorService newExecutor(int corePoolSize)：初始线程数为corePoolSize指定的大小
     * ThreadPoolExecutor newExecutor(int corePoolSize, int maximumPoolSize)
     * * @param corePoolSize    初始线程池大小
     * * @param maximumPoolSize 最大线程池大小
     * ExecutorService newExecutor(int corePoolSize, int maximumPoolSize, int maximumQueueSize)
     * maximumQueueSize 最大任务队列大小
     *
     * @return ExecutorService
     */
    @Test
    public void newExecutorTest() {
        int size = 3;
        ExecutorService executorService = ThreadUtil.newExecutor();
        for (int i = 1; i <= size; i++) {
            executorService.submit(new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    int timer = 500 + new Random().nextInt(3000);
                    log.info("支线任务开始，预计" + timer + "毫秒。");
                    ThreadUtil.sleep(timer);
                    log.info("支线任务完成。");
                    return 1;
                }
            });
        }
        log.info("主线任务5秒后完成。");
        ThreadUtil.safeSleep(5000);
        log.info("主线任务完成。");
        // [07:10:38:688] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.newExecutorTest(ThreadUtilTest.java:134) - 主线任务5秒后完成。
        // [07:10:38:751] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$4.call(ThreadUtilTest.java:127) - 支线任务开始，预计938毫秒。
        // [07:10:38:756] [INFO] - [pool-2-thread-2] - org.example.hutool.utilTest.ThreadUtilTest$4.call(ThreadUtilTest.java:127) - 支线任务开始，预计1710毫秒。
        // [07:10:38:758] [INFO] - [pool-2-thread-3] - org.example.hutool.utilTest.ThreadUtilTest$4.call(ThreadUtilTest.java:127) - 支线任务开始，预计1467毫秒。
        // [07:10:39:694] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$4.call(ThreadUtilTest.java:129) - 支线任务完成。
        // [07:10:40:225] [INFO] - [pool-2-thread-3] - org.example.hutool.utilTest.ThreadUtilTest$4.call(ThreadUtilTest.java:129) - 支线任务完成。
        // [07:10:40:472] [INFO] - [pool-2-thread-2] - org.example.hutool.utilTest.ThreadUtilTest$4.call(ThreadUtilTest.java:129) - 支线任务完成。
        // [07:10:43:764] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.newExecutorTest(ThreadUtilTest.java:136) - 主线任务完成。
    }

    /**
     * ExecutorService newSingleExecutor()
     * 获得一个新的线程池，只有单个线程，策略如下：
     * <pre>
     *    1. 初始线程数为 1
     *    2. 最大线程数为 1
     *    3. 默认使用LinkedBlockingQueue，默认队列大小为1024
     *    4. 同时只允许一个线程工作，剩余放入队列等待，等待数超过1024报错
     * </pre>
     */
    @Test
    public void newSingleExecutorTest() {
        ExecutorService singleExecutor = ThreadUtil.newSingleExecutor();
        ThreadFactory threadFactory = ThreadUtil.newNamedThreadFactory("W_", true);
        int size = 3;
        for (int i = 1; i <= size; i++) {
            singleExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    int timer = 500 + new Random().nextInt(3000);
                    log.info("支线任务开始，预计" + timer + "毫秒。");
                    ThreadUtil.sleep(timer);
                    log.info("支线任务完成。");
                }
            });
        }
        log.info("主线任务15秒后完成。");
        ThreadUtil.safeSleep(15000);
        log.info("主线任务完成。");
        // [07:08:38:405] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.newSingleExecutorTest(ThreadUtilTest.java:166) - 主线任务15秒后完成。
        // [07:08:38:486] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$5.run(ThreadUtilTest.java:160) - 支线任务开始，预计2856毫秒。
        // [07:08:41:347] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$5.run(ThreadUtilTest.java:162) - 支线任务完成。
        // [07:08:41:347] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$5.run(ThreadUtilTest.java:160) - 支线任务开始，预计1517毫秒。
        // [07:08:42:897] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$5.run(ThreadUtilTest.java:162) - 支线任务完成。
        // [07:08:42:897] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$5.run(ThreadUtilTest.java:160) - 支线任务开始，预计2474毫秒。
        // [07:08:45:373] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$5.run(ThreadUtilTest.java:162) - 支线任务完成。
        // [07:08:53:428] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.newSingleExecutorTest(ThreadUtilTest.java:168) - 主线任务完成。
    }

    /**
     * CompletionService<T> newCompletionService()
     * CompletionService<T> newCompletionService(ExecutorService executor)
     * 新建一个CompletionService，调用其submit方法可以异步执行多个任务，最后调用take方法按照完成的顺序获得其结果。<br>
     * 若未完成，则会阻塞。
     */
    @Test
    public void newCompletionServiceTest() {
        CompletionService<Object> completionService = ThreadUtil.newCompletionService();
    }

    /**
     * CountDownLatch newCountDownLatch(int threadCount)
     * 新建一个CountDownLatch，一个同步辅助类，在完成一组正在其他线程中执行的操作之前，它允许一个或多个线程一直等待。
     *
     * @throws InterruptedException
     */
    @Test
    public void newCountDownLatchTest() throws InterruptedException {
        int count = 3;
        CountDownLatch countDownLatch = ThreadUtil.newCountDownLatch(3);
        for (int i = 0; i < count; i++) {
            ThreadUtil.execAsync(new Callable<Object>() {
                @Override
                public Object call() {
                    int timer = 500 + new Random().nextInt(3000);
                    log.info("支线任务开始，预计" + timer + "毫秒。");
                    ThreadUtil.sleep(timer);
                    countDownLatch.countDown();
                    log.info("支线任务完成。总共：" + count + "，完成：" + (count - countDownLatch.getCount()) + "，剩余：" + countDownLatch.getCount());
                    return 1;
                }
            });
        }
        log.info("主线任务开始等待支线任务执行完成。");
        countDownLatch.await();
        log.info("主线任务完成。");
        // [21:35:55:806] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.countDownLatchTest(ThreadUtilTest.java:154) - 主线任务开始等待支线任务执行完成。
        // [21:35:55:891] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$4.call(ThreadUtilTest.java:146) - 支线任务开始，预计938毫秒。
        // [21:35:55:894] [INFO] - [pool-2-thread-2] - org.example.hutool.utilTest.ThreadUtilTest$4.call(ThreadUtilTest.java:146) - 支线任务开始，预计2701毫秒。
        // [21:35:55:894] [INFO] - [pool-2-thread-3] - org.example.hutool.utilTest.ThreadUtilTest$4.call(ThreadUtilTest.java:146) - 支线任务开始，预计1517毫秒。
        // [21:35:56:861] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$4.call(ThreadUtilTest.java:149) - 支线任务完成。总共：3，完成：1，剩余：2
        // [21:35:57:436] [INFO] - [pool-2-thread-3] - org.example.hutool.utilTest.ThreadUtilTest$4.call(ThreadUtilTest.java:149) - 支线任务完成。总共：3，完成：2，剩余：1
        // [21:35:58:595] [INFO] - [pool-2-thread-2] - org.example.hutool.utilTest.ThreadUtilTest$4.call(ThreadUtilTest.java:149) - 支线任务完成。总共：3，完成：3，剩余：0
        // [21:35:58:595] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.countDownLatchTest(ThreadUtilTest.java:156) - 主线任务完成。
    }

    /**
     * ThreadFactory newNamedThreadFactory(String prefix, boolean isDaemon)
     */
    @Test
    public void newNamedThreadFactoryTest() {
        ThreadFactory threadFactory = ThreadUtil.newNamedThreadFactory("_pay_", true);
        Thread thread = threadFactory.newThread(new Runnable() {
            @Override
            public void run() {
                int timer = 500 + new Random().nextInt(3000);
                log.info("支线任务开始，预计" + timer + "毫秒。");
                ThreadUtil.sleep(timer);
                log.info("支线任务完成。");
            }
        });
        thread.start();
        log.info("主线任务5秒后完成。");
        ThreadUtil.safeSleep(5000);
        log.info("主线任务完成。");
        // [07:16:43:960] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.newNamedThreadFactoryTest(ThreadUtilTest.java:249) - 主线任务5秒后完成。
        // [07:16:43:980] [INFO] - [_pay_1] - org.example.hutool.utilTest.ThreadUtilTest$7.run(ThreadUtilTest.java:243) - 支线任务开始，预计1840毫秒。
        // [07:16:45:834] [INFO] - [_pay_1] - org.example.hutool.utilTest.ThreadUtilTest$7.run(ThreadUtilTest.java:245) - 支线任务完成。
        // [07:16:48:992] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.newNamedThreadFactoryTest(ThreadUtilTest.java:251) - 主线任务完成。
    }

    /**
     * Thread newThread(Runnable runnable, String name)
     * 创建新线程，非守护线程，正常优先级，线程组与当前线程的线程组一致
     * Thread newThread(Runnable runnable, String name, boolean isDaemon)
     * isDaemon 是否守护线程
     * <p>
     * void waitForDie(Thread thread)
     * 等待线程结束. 调用 {@link Thread#join()} 并忽略 {@link InterruptedException}
     */
    @Test
    public void newThreadTest() {
        Thread thread = ThreadUtil.newThread(new Runnable() {
            @Override
            public void run() {
                int timer = 500 + new Random().nextInt(3000);
                log.info("支线任务开始，预计" + timer + "毫秒。");
                ThreadUtil.sleep(timer);
                log.info("支线任务完成。");
            }
        }, "_pay_");
        thread.start();
        log.info("主线任务开始等待子线程完成。");
        ThreadUtil.waitForDie(thread);
        log.info("主线任务完成。");
        // [07:20:10:284] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.newThreadTest(ThreadUtilTest.java:276) - 主线任务开始等待子线程完成。
        // [07:20:10:365] [INFO] - [_pay_] - org.example.hutool.utilTest.ThreadUtilTest$8.run(ThreadUtilTest.java:270) - 支线任务开始，预计2846毫秒。
        // [07:20:13:233] [INFO] - [_pay_] - org.example.hutool.utilTest.ThreadUtilTest$8.run(ThreadUtilTest.java:272) - 支线任务完成。
        // [07:20:13:233] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.newThreadTest(ThreadUtilTest.java:278) - 主线任务完成。
    }

    /**
     * void interrupt(Thread thread, boolean isJoin)
     * 结束线程，调用此方法后，线程将抛出 {@link InterruptedException}异常
     * isJoin 是否等待结束
     */
    @Test
    public void interruptTest() {
        Thread thread = ThreadUtil.newThread(new Runnable() {
            @Override
            public void run() {
                int timer = 3000 + new Random().nextInt(3000);
                log.info("支线任务开始，预计" + timer + "毫秒。");
                ThreadUtil.sleep(timer);
                log.info("支线任务完成。");
            }
        }, "_pay_");
        thread.start();
        for (int i = 2; i > 0; i--) {
            log.info("主线任务{}后自动结束子线程：" + thread.getName(), i);
            ThreadUtil.safeSleep(1000);
        }
        ThreadUtil.interrupt(thread, false);
        ThreadUtil.safeSleep(10000);
        log.info("主线任务完成。");
        // [07:30:12:383] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.interruptTest(ThreadUtilTest.java:306) - 主线任务2后自动结束子线程：_pay_
        // [07:30:12:474] [INFO] - [_pay_] - org.example.hutool.utilTest.ThreadUtilTest$9.run(ThreadUtilTest.java:299) - 支线任务开始，预计5269毫秒。
        // [07:30:13:444] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.interruptTest(ThreadUtilTest.java:306) - 主线任务1后自动结束子线程：_pay_
        // [07:30:14:451] [INFO] - [_pay_] - org.example.hutool.utilTest.ThreadUtilTest$9.run(ThreadUtilTest.java:301) - 支线任务完成。
        // [07:30:24:459] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.interruptTest(ThreadUtilTest.java:311) - 主线任务完成。
    }


    // ========================自定义线程池-ExecutorBuilder========================./start

    /**
     * 单线程线程池
     * 初始线程数为 1
     * 最大线程数为 1
     * 默认使用LinkedBlockingQueue，默认队列大小为1024
     * 同时只允许一个线程工作，剩余放入队列等待，等待数超过1024报错
     */
    @Test
    public void executorBuilderTest1() {
        ExecutorService executorService = ExecutorBuilder.create()
                .setCorePoolSize(1) // 设置线程执行超时后是否回收线程
                .setMaxPoolSize(1) // 设置最大池大小（允许同时执行的最大线程数）
                .setKeepAliveTime(0) // 设置线程存活时间，即当池中线程多于初始大小时，多出的线程保留的时长，单位纳秒。默认 1分钟
                .build();

        int size = 5;
        for (int i = 0; i < size; i++) {
            int finalI = i;
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    int timer = 500 + new Random().nextInt(3000);
                    log.info(finalI + "支线任务开始，预计" + timer + "毫秒。");
                    ThreadUtil.sleep(timer);
                    log.info(finalI + "支线任务完成。");
                }
            });
        }
        log.info("主线任务15秒后完成。");
        ThreadUtil.safeSleep(15000);
        log.info("主线任务完成。");
        // [08:12:04:953] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.executorBuilderTest(ThreadUtilTest.java:375) - 主线任务15秒后完成。
        // [08:12:05:044] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$10.run(ThreadUtilTest.java:369) - 0支线任务开始，预计3155毫秒。
        // [08:12:08:203] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$10.run(ThreadUtilTest.java:371) - 0支线任务完成。
        // [08:12:08:203] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$10.run(ThreadUtilTest.java:369) - 1支线任务开始，预计1761毫秒。
        // [08:12:09:966] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$10.run(ThreadUtilTest.java:371) - 1支线任务完成。
        // [08:12:09:966] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$10.run(ThreadUtilTest.java:369) - 2支线任务开始，预计3381毫秒。
        // [08:12:13:349] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$10.run(ThreadUtilTest.java:371) - 2支线任务完成。
        // [08:12:13:349] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$10.run(ThreadUtilTest.java:369) - 3支线任务开始，预计902毫秒。
        // [08:12:14:252] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$10.run(ThreadUtilTest.java:371) - 3支线任务完成。
        // [08:12:14:252] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$10.run(ThreadUtilTest.java:369) - 4支线任务开始，预计2808毫秒。
        // [08:12:17:061] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$10.run(ThreadUtilTest.java:371) - 4支线任务完成。
        // [08:12:19:979] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.executorBuilderTest(ThreadUtilTest.java:377) - 主线任务完成。
    }

    /**
     * 更多选项的线程池
     * 初始3个线程
     * 最大10个线程
     * 有界等待队列，最大等待数是100
     */
    @Test
    public void executorBuilderTest2() {
        ExecutorService executorService = ExecutorBuilder.create()
                .setCorePoolSize(3)
                .setMaxPoolSize(10)
                .setWorkQueue(new LinkedBlockingQueue<>(100)) // 设置队列，用于存在未执行的线程
                .setKeepAliveTime(3, TimeUnit.MINUTES) // 设置线程存活时间，即当池中线程多于初始大小时，多出的线程保留的时长
                .build();
        int size = 5;
        for (int i = 0; i < size; i++) {
            int finalI = i;
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    int timer = 500 + new Random().nextInt(3000);
                    log.info(finalI + "支线任务开始，预计" + timer + "毫秒。");
                    ThreadUtil.sleep(timer);
                    log.info(finalI + "支线任务完成。");
                }
            });
        }
        log.info("主线任务15秒后完成。");
        ThreadUtil.safeSleep(15000);
        log.info("主线任务完成。");
        // [08:16:41:011] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.test(ThreadUtilTest.java:419) - 主线任务15秒后完成。
        // [08:16:41:102] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$11.run(ThreadUtilTest.java:413) - 0支线任务开始，预计2613毫秒。
        // [08:16:41:106] [INFO] - [pool-2-thread-2] - org.example.hutool.utilTest.ThreadUtilTest$11.run(ThreadUtilTest.java:413) - 1支线任务开始，预计3327毫秒。
        // [08:16:41:109] [INFO] - [pool-2-thread-3] - org.example.hutool.utilTest.ThreadUtilTest$11.run(ThreadUtilTest.java:413) - 2支线任务开始，预计1230毫秒。
        // [08:16:42:352] [INFO] - [pool-2-thread-3] - org.example.hutool.utilTest.ThreadUtilTest$11.run(ThreadUtilTest.java:415) - 2支线任务完成。
        // [08:16:42:353] [INFO] - [pool-2-thread-3] - org.example.hutool.utilTest.ThreadUtilTest$11.run(ThreadUtilTest.java:413) - 3支线任务开始，预计3339毫秒。
        // [08:16:43:735] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$11.run(ThreadUtilTest.java:415) - 0支线任务完成。
        // [08:16:43:735] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$11.run(ThreadUtilTest.java:413) - 4支线任务开始，预计1367毫秒。
        // [08:16:44:468] [INFO] - [pool-2-thread-2] - org.example.hutool.utilTest.ThreadUtilTest$11.run(ThreadUtilTest.java:415) - 1支线任务完成。
        // [08:16:45:103] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$11.run(ThreadUtilTest.java:415) - 4支线任务完成。
        // [08:16:45:696] [INFO] - [pool-2-thread-3] - org.example.hutool.utilTest.ThreadUtilTest$11.run(ThreadUtilTest.java:415) - 3支线任务完成。
        // [08:16:56:113] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.test(ThreadUtilTest.java:421) - 主线任务完成。
    }

    /**
     * 特殊策略的线程池
     * 初始3个线程
     * 最大10个线程
     * <p>
     * useArrayBlockingQueue(int capacity)：使用{@link ArrayBlockingQueue} 做为等待队列
     * 有界队列，相对无界队列有利于控制队列大小，队列满时，运行线程小于maxPoolSize时会创建新线程，否则触发异常策略
     */
    @Test
    public void executorBuilderTest3() {
        ExecutorService executorService = ExecutorBuilder.create()
                .setCorePoolSize(3)
                .setMaxPoolSize(10)
                // 设置当线程阻塞（block）时的异常处理器，所谓线程阻塞即线程池和等待队列已满，无法处理线程时采取的策略
                .setHandler(RejectPolicy.CALLER_RUNS.getValue())
                // 使用{@link SynchronousQueue} 做为等待队列（非公平策略）
                // 它将任务直接提交给线程而不保持它们。当运行线程小于maxPoolSize时会创建新线程，否则触发异常策略
                .useSynchronousQueue()
                .buildFinalizable();// 创建有回收关闭功能的ExecutorService，保证ExecutorService在对象回收时正常结束

        int size = 15;
        for (int i = 0; i < size; i++) {
            int finalI = i + 1;
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    int timer = 500 + new Random().nextInt(3000);
                    log.info(finalI + "支线任务开始，预计" + timer + "毫秒。");
                    ThreadUtil.sleep(timer);
                    log.info(finalI + "支线任务完成。");
                }
            });
        }
        log.info("主线任务15秒后完成。");
        ThreadUtil.safeSleep(15000);
        log.info("主线任务完成。");
        // [08:22:27:853] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.executorBuilderTest3(ThreadUtilTest.java:462) - 主线任务15秒后完成。
        // [08:22:27:874] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$12.run(ThreadUtilTest.java:456) - 1支线任务开始，预计1631毫秒。
        // [08:22:27:941] [INFO] - [pool-2-thread-2] - org.example.hutool.utilTest.ThreadUtilTest$12.run(ThreadUtilTest.java:456) - 2支线任务开始，预计1739毫秒。
        // [08:22:27:950] [INFO] - [pool-2-thread-3] - org.example.hutool.utilTest.ThreadUtilTest$12.run(ThreadUtilTest.java:456) - 3支线任务开始，预计925毫秒。
        // [08:22:27:951] [INFO] - [pool-2-thread-5] - org.example.hutool.utilTest.ThreadUtilTest$12.run(ThreadUtilTest.java:456) - 5支线任务开始，预计1065毫秒。
        // [08:22:27:951] [INFO] - [pool-2-thread-4] - org.example.hutool.utilTest.ThreadUtilTest$12.run(ThreadUtilTest.java:456) - 4支线任务开始，预计2642毫秒。
        // [08:22:28:875] [INFO] - [pool-2-thread-3] - org.example.hutool.utilTest.ThreadUtilTest$12.run(ThreadUtilTest.java:458) - 3支线任务完成。
        // [08:22:29:023] [INFO] - [pool-2-thread-5] - org.example.hutool.utilTest.ThreadUtilTest$12.run(ThreadUtilTest.java:458) - 5支线任务完成。
        // [08:22:29:511] [INFO] - [pool-2-thread-1] - org.example.hutool.utilTest.ThreadUtilTest$12.run(ThreadUtilTest.java:458) - 1支线任务完成。
        // [08:22:29:680] [INFO] - [pool-2-thread-2] - org.example.hutool.utilTest.ThreadUtilTest$12.run(ThreadUtilTest.java:458) - 2支线任务完成。
        // [08:22:30:618] [INFO] - [pool-2-thread-4] - org.example.hutool.utilTest.ThreadUtilTest$12.run(ThreadUtilTest.java:458) - 4支线任务完成。
        // [08:22:42:889] [INFO] - [main] - org.example.hutool.utilTest.ThreadUtilTest.executorBuilderTest3(ThreadUtilTest.java:464) - 主线任务完成。
    }

    // ========================自定义线程池-ExecutorBuilder========================./end


    // ========================高并发测试-ConcurrencyTester========================./start
    @Test
    public void concurrencyTest() {
        ConcurrencyTester tester = ThreadUtil.concurrencyTest(100, new Runnable() {
            @Override
            public void run() {
                // 测试的逻辑内容
                long delay = RandomUtil.randomLong(500, 1000);
                ThreadUtil.sleep(delay);
                Console.log("{} test finished, delay: {}", Thread.currentThread().getName(), delay);
            }
        });
        // 获取总的执行时间，单位毫秒
        Console.log("总的执行时间：{}", tester.getInterval());
    }
    // ========================高并发测试-ConcurrencyTester========================./end

}
