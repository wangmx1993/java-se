package org.example.hutool.utilTest;

import cn.hutool.core.util.RuntimeUtil;
import org.junit.Test;

import java.util.List;

/**
 * 系统运行时工具类，用于执行系统命令的工具
 * <p>
 * Hutool 封装了 JDK 的 Process 类，用于执行命令行命令（在Windows下是cmd，在Linux下是shell命令）。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/2/3 15:35
 */
public class RuntimeUtilTest {

    /**
     * String execForStr(String... cmds): 执行系统命令，使用系统默认编码
     * cmds: 命令列表，每个元素代表一条命令
     * 返回执行结果
     */
    @Test
    public void execForStrTest() {
        //Windows下获取网卡信息。
        String str = RuntimeUtil.execForStr("ipconfig");
        System.out.println(str);
    }

    /**
     * List<String> execForLines(String... cmds)：执行系统命令，使用系统默认编码
     * cmds： 命令列表，每个元素代表一条命令
     * 返回执行结果，按行区分
     */
    @Test
    public void execForLines() {
        //Windows下获取网卡信息。
        List<String> lines = RuntimeUtil.execForLines("ipconfig");
        int i = 1;
        for (String line : lines) {
            System.out.println((i++) + "：" + line);
        }
    }

    /**
     * Process exec(String... cmds) : 执行命令, 命令带参数时参数可作为其中一个参数，也可以将命令和参数组合为一个字符串传入
     * String getResult(Process process) : 获取命令执行结果，使用系统默认编码，获取后销毁进程
     * String getResult(Process process, Charset charset): 使用指定编码
     * List<String> getResultLines(Process process)：获取命令执行结果，使用系统默认编码，获取后销毁进程
     * List<String> getResultLines(Process process, Charset charset)：获取命令执行结果，使用系统默认编码，获取后销毁进程
     */
    @Test
    public void test() {
        Process process = RuntimeUtil.exec("ping 10.104.65.190");
        String result = RuntimeUtil.getResult(process);
        System.out.println(result);
    }

    /**
     * ddShutdownHook(Runnable hook): 增加一个JVM关闭后的钩子，用于在JVM关闭时执行某些操作
     */
    @Test
    public void addShutdownHookTest() {
        RuntimeUtil.addShutdownHook(new Runnable() {
            @Override
            public void run() {
                System.out.println("JVM 关闭，执行特定操作。");
            }
        });
        System.out.println(1);
    }

    /**
     * long getMaxMemory(): 获得JVM中可以从系统中获取的最大的内存数，单位byte，以-Xmx参数为准
     * long getFreeMemory(): 获得JVM中剩余的内存数，单位byte
     * long getTotalMemory(): 获得JVM已经从系统中获取到的总共的内存数，单位byte
     * long getUsableMemory(): 获得JVM最大可用内存，计算方法为：<br>. 最大内存-总内存+剩余内存
     * int getProcessorCount(): 获得JVM可用的处理器数量（一般为CPU核心数）
     */
    @Test
    public void memoryTest() {
        long maxMemory = RuntimeUtil.getMaxMemory();
        long freeMemory = RuntimeUtil.getFreeMemory();
        long totalMemory = RuntimeUtil.getTotalMemory();
        long usableMemory = RuntimeUtil.getUsableMemory();
        int processorCount = RuntimeUtil.getProcessorCount();

        System.out.println("大的内存数=" + maxMemory);
        System.out.println("余的内存数=" + freeMemory);
        System.out.println("总共的内存数=" + totalMemory);
        System.out.println("最大可用内存=" + usableMemory);
        System.out.println("处理器数量=" + processorCount);
    }
}
