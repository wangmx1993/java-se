package org.example.hutool.utilTest;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.lang.Editor;
import org.example.hutool.pojo.Grade;
import org.example.hutool.pojo.Student;
import org.junit.Test;

import java.util.*;

/**
 * Bean工具-BeanUtil - 测试
 * <p>
 * 什么是Bean？
 * 把一个拥有对属性进行set和get方法的类，我们就可以称之为JavaBean。
 * 实际上JavaBean就是一个Java类，在这个Java类中就默认形成了一种规则——对属性进行设置和获得。
 * 而反之将说Java类就是一个JavaBean，这种说法是错误的，因为一个java类中不一定有对属性的设置和获得的方法（也就是不一定有set和get方法）。
 * 通常Java中对Bean的定义是包含setXXX和getXXX方法的对象，在Hutool中，采取一种简单的判定Bean的方法：是否存在只有一个参数的setXXX方法。
 * Bean工具类主要是针对这些setXXX和getXXX方法进行操作，比如将Bean对象转为Map等等。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/1/7 16:20
 */
public class BeanUtilTest {

    /**
     * boolean isBean(Class<?> clazz) 判断是否为 Java bean 对象，以下两个条件任意满足一个，则判定为是：
     * <pre>
     *     1、是否存在只有一个参数的setXXX方法，即只要但凡有一个方法的名叫 setXxx，且参数还只有一个，则判定为是。
     *     2、是否存在public类型的字段
     * </pre>
     */
    @Test
    public void testIsBean() {
        // false
        System.out.println(BeanUtil.isBean(HashMap.class));
        // false
        System.out.println(BeanUtil.isBean(ArrayList.class));
        // true
        System.out.println(BeanUtil.isBean(Grade.class));
        // true
        System.out.println(BeanUtil.isBean(Student.class));
    }

    /**
     * boolean isEmpty(Object bean, String... ignoreFiledNames)
     * * @param bean             Bean对象
     * * @param ignoreFiledNames 忽略检查的字段名
     * 判断 java Bean是否为空对象，空对象表示本身为{@code null}或者所有属性都为{@code null}<br>
     * 此方法不判断static属性
     * <p>
     * boolean isNotEmpty(Object bean, String... ignoreFiledNames)
     * 判断Bean是否为非空对象，非空对象表示本身不为{@code null}或者含有非{@code null}属性的对象
     * * return false == isEmpty(bean, ignoreFiledNames);
     */
    @Test
    public void testIsEmpty() {
        // true
        System.out.println(BeanUtil.isEmpty(null));

        Grade grade = new Grade();
        // true
        System.out.println(BeanUtil.isEmpty(grade));
        grade.setName("长郡班");
        // false
        System.out.println(BeanUtil.isEmpty(grade));

        Map<String, Object> data = new HashMap<>();
        // false
        System.out.println(BeanUtil.isEmpty(data));

        data.put("code", 200);
        // false
        System.out.println(BeanUtil.isEmpty(data));
    }

    /**
     * boolean isMatchName(Object bean, String beanClassName, boolean isSimple)
     * * @param bean          Bean
     * * @param beanClassName Bean的类名
     * * @param isSimple      是否只匹配类名而忽略包名，true表示忽略包名
     * 给定的Bean的类名是否匹配指定类名字符串<br>
     * 如果isSimple为{@code false}，则只匹配类名而忽略包名，例如：cn.hutool.TestEntity只匹配TestEntity<br>
     * 如果isSimple为{@code true}，则匹配包括包名的全类名，例如：cn.hutool.TestEntity匹配cn.hutool.TestEntity
     */
    @Test
    public void testIsMatchName() {
        Grade grade = new Grade();
        // true
        System.out.println(BeanUtil.isMatchName(grade, Grade.class.getName(), false));
        // true
        System.out.println(BeanUtil.isMatchName(grade, "Grade", true));
        // true
        System.out.println(BeanUtil.isMatchName(grade, "org.example.hutool.pojo.Grade", false));
    }

    /**
     * copyProperties(Object source, Object target, String... ignoreProperties)
     * * 复制Bean对象属性<br>
     * * @param source           源Bean对象，也可以是 Map 对象。
     * * @param target           目标Bean对象，也可以是 Map 对象。
     * * @param ignoreProperties 不拷贝的的属性列表
     * <p>
     * copyProperties(Object source, Object target, CopyOptions copyOptions)
     * * 复制Bean对象属性<br>
     * * @param source      源Bean对象
     * * @param target      目标Bean对象
     * * @param copyOptions 拷贝选项，见 {@link CopyOptions}
     * <p>
     * copyProperties(Object source, Object target, boolean ignoreCase)
     * * 复制Bean对象属性<br>
     * * @param source     源Bean对象
     * * @param target     目标Bean对象
     * * @param ignoreCase 是否忽略大小写
     * <p>
     */
    @Test
    public void testCopyProperties1() {
        Grade grade1 = new Grade();
        grade1.setNum("2024001");
        grade1.setName("长郡班");
        grade1.setCreateTime(new Date());
        grade1.setUpdateTime(null);

        List<Student> students = new ArrayList<>();
        Map<String, Object> extMap = new HashMap<>();
        extMap.put("remark", "调皮");

        Student student1 = new Student("1", "张三", grade1.getNum(), new Date(), 2, null);
        Student student2 = new Student("2", "李四", grade1.getNum(), new Date(), 2, extMap);
        Student student3 = new Student("2", "王五", grade1.getNum(), new Date(), 2, null);
        students.add(student1);
        students.add(student2);
        students.add(student3);

        grade1.setStudents(students);

        System.out.println("================targetMap1(bean to map)==================");
        Map<String, Object> targetMap1 = new LinkedHashMap<>();
        BeanUtil.copyProperties(grade1, targetMap1);
        // {num=2024001, name=长郡班, createTime=Sun Jan 07 17:47:06 CST 2024, updateTime=null, students=[Student{sid='1', name='张三', gradeNum=2024001, birthday=Sun Jan 07 17:47:06 CST 2024, isDelete=2, extMap=null}, Student{sid='2', name='李四', gradeNum=2024001, birthday=Sun Jan 07 17:47:06 CST 2024, isDelete=2, extMap={remark=调皮}}, Student{sid='2', name='王五', gradeNum=2024001, birthday=Sun Jan 07 17:47:06 CST 2024, isDelete=2, extMap=null}]}
        System.out.println(targetMap1);

        System.out.println("================targetMap2(map to map)==================");
        Map<String, Object> targetMap2 = new LinkedHashMap<>();
        BeanUtil.copyProperties(targetMap1, targetMap2);
        System.out.println(targetMap2);

        System.out.println("================grade2(bean to bean)==================");
        Grade grade2 = new Grade();
        BeanUtil.copyProperties(grade1, grade2);
        grade1.setName("重点班");
        // Grade{num='2024001', name='长郡班', createTime=Sun Jan 07 17:49:58 CST 2024, updateTime=null, students=[Student{sid='1', name='张三', gradeNum=2024001, birthday=Sun Jan 07 17:49:58 CST 2024, isDelete=2, extMap=null}, Student{sid='2', name='李四', gradeNum=2024001, birthday=Sun Jan 07 17:49:58 CST 2024, isDelete=2, extMap={remark=调皮}}, Student{sid='2', name='王五', gradeNum=2024001, birthday=Sun Jan 07 17:49:58 CST 2024, isDelete=2, extMap=null}]}
        System.out.println(grade2);

        System.out.println("================grade3(amp to bean)==================");
        Grade grade3 = new Grade();
        BeanUtil.copyProperties(targetMap2, grade3);
        // Grade{num='2024001', name='长郡班', createTime=Sun Jan 07 17:54:20 CST 2024, updateTime=null, students=[Student{sid='1', name='张三', gradeNum=2024001, birthday=Sun Jan 07 17:54:20 CST 2024, isDelete=2, extMap=null}, Student{sid='2', name='李四', gradeNum=2024001, birthday=Sun Jan 07 17:54:20 CST 2024, isDelete=2, extMap={remark=调皮}}, Student{sid='2', name='王五', gradeNum=2024001, birthday=Sun Jan 07 17:54:20 CST 2024, isDelete=2, extMap=null}]}
        System.out.println(grade3);
    }

    /**
     * T copyProperties(Object source, Class<T> tClass, String... ignoreProperties)
     * * 按照Bean对象属性创建对应的Class对象，并忽略某些属性。
     * * 注意：无法对子对象进行级联复制。
     * * @param source           源Bean对象，也可以是 Map 对象。
     * * @param tClass           目标Class，也可以是 Map.class。
     * * @param ignoreProperties 不拷贝的的属性列表
     */
    @Test
    public void testCopyProperties2() {
        Grade grade1 = new Grade();
        grade1.setNum("2024001");
        grade1.setName("长郡班");
        grade1.setCreateTime(new Date());
        grade1.setUpdateTime(null);

        List<Student> students = new ArrayList<>();
        Map<String, Object> extMap = new HashMap<>();
        extMap.put("remark", "调皮");

        Student student1 = new Student("1", "张三", grade1.getNum(), new Date(), 2, null);
        Student student2 = new Student("2", "李四", grade1.getNum(), new Date(), 2, extMap);
        Student student3 = new Student("2", "王五", grade1.getNum(), new Date(), 2, null);
        students.add(student1);
        students.add(student2);
        students.add(student3);

        System.out.println("==============grade2(bean to bean)===============");
        Grade grade2 = BeanUtil.copyProperties(grade1, Grade.class);
        // Grade{num='2024001', name='长郡班', createTime=Sun Jan 07 18:02:55 CST 2024, updateTime=null, students=null}
        System.out.println(grade2);

        System.out.println("==============map1(bean to map)===============");
        Map<String, Object> map1 = BeanUtil.copyProperties(grade1, Map.class);
        // {createTime=Sun Jan 07 18:03:59 CST 2024, num=2024001, name=长郡班, students=null, updateTime=null}
        System.out.println(map1);

        System.out.println("==============grade3(map to bean)===============");
        Grade grade3 = BeanUtil.copyProperties(map1, Grade.class);
        // Grade{num='2024001', name='长郡班', createTime=Sun Jan 07 18:05:25 CST 2024, updateTime=null, students=null}
        System.out.println(grade3);

        System.out.println("==============map2(map to map)===============");
        Map<String, Object> map2 = BeanUtil.copyProperties(map1, Map.class);
        // {name=长郡班, students=null, updateTime=null, createTime=Sun Jan 07 18:06:31 CST 2024, num=2024001}
        System.out.println(map2);
    }

    /**
     * Map<String, Object> beanToMap(Object bean)：Java Bean 对象转Map，不进行驼峰转下划线，不忽略值为空的字段
     * <p>
     * Map<String, Object> beanToMap(Object bean, boolean isToUnderlineCase, boolean ignoreNullValue) Java Bean 对象转Map
     * * @param bean              bean对象
     * * @param isToUnderlineCase 是否转换为下划线模式，关联的子对象不会处理
     * * @param ignoreNullValue   是否忽略值为空的字段，关联的子对象不会处理
     * <p>
     * Map<String, Object> beanToMap(Object bean, Map<String, Object> targetMap, final boolean isToUnderlineCase, boolean ignoreNullValue)
     * * Java Bean 对象转Map
     * * @param bean              bean对象
     * * @param targetMap         目标的Map，用于接收的Map，如果转换前已经有值，转换时如果属性同名，则自动覆盖。
     * * @param isToUnderlineCase 是否转换为下划线模式
     * * @param ignoreNullValue   是否忽略值为空的字段
     * <p>
     * Map<String, Object> beanToMap(Object bean, Map<String, Object> targetMap, boolean ignoreNullValue, Editor<String> keyEditor)
     * * 对象转Map<br>
     * * 通过实现{@link Editor} 可以自定义字段值，如果这个Editor返回null则忽略这个字段，以便实现：
     * *
     * * <pre>
     * 	 * 1. 字段筛选，可以去除不需要的字段
     * 	 * 2. 字段变换，例如实现驼峰转下划线
     * 	 * 3. 自定义字段前缀或后缀等等
     * 	 * </pre>
     * * @param bean            bean对象
     * * @param targetMap       目标的Map，用于接收的Map，如果转换前已经有值，转换时如果属性同名，则自动覆盖。
     * * @param ignoreNullValue 是否忽略值为空的字段
     * * @param keyEditor       属性字段（Map的key）编辑器，用于筛选、编辑key，如果这个Editor返回null则忽略这个字段
     */
    @Test
    public void testBeanToMap1() {
        Grade grade1 = new Grade();
        grade1.setNum("2024001");
        grade1.setName("长郡班");
        grade1.setCreateTime(new Date());
        grade1.setUpdateTime(null);

        List<Student> students = new ArrayList<>();
        Map<String, Object> extMap = new HashMap<>();
        extMap.put("remark", "调皮");

        Student student1 = new Student("1", "张三", grade1.getNum(), new Date(), 2, null);
        Student student2 = new Student("2", "李四", grade1.getNum(), new Date(), 2, extMap);
        Student student3 = new Student("2", "王五", grade1.getNum(), new Date(), 2, null);
        students.add(student1);
        students.add(student2);
        students.add(student3);

        grade1.setStudents(students);

        Map<String, Object> beanToMap1 = BeanUtil.beanToMap(grade1);
        System.out.println("=========beanToMap1==========");
        // {num=2024001, name=长郡班, createTime=Sun Jan 07 17:13:33 CST 2024, updateTime=null, students=[Student{sid='1', name='张三', gradeNum=2024001, birthday=Sun Jan 07 17:13:33 CST 2024, isDelete=2, extMap=null}, Student{sid='2', name='李四', gradeNum=2024001, birthday=Sun Jan 07 17:13:33 CST 2024, isDelete=2, extMap={remark=调皮}}, Student{sid='2', name='王五', gradeNum=2024001, birthday=Sun Jan 07 17:13:33 CST 2024, isDelete=2, extMap=null}]}
        System.out.println(beanToMap1);

        Map<String, Object> beanToMap2 = BeanUtil.beanToMap(grade1, true, true);
        System.out.println("=========beanToMap2==========");
        // {num=2024001, name=长郡班, create_time=Sun Jan 07 17:13:33 CST 2024, students=[Student{sid='1', name='张三', gradeNum=2024001, birthday=Sun Jan 07 17:13:33 CST 2024, isDelete=2, extMap=null}, Student{sid='2', name='李四', gradeNum=2024001, birthday=Sun Jan 07 17:13:33 CST 2024, isDelete=2, extMap={remark=调皮}}, Student{sid='2', name='王五', gradeNum=2024001, birthday=Sun Jan 07 17:13:33 CST 2024, isDelete=2, extMap=null}]}
        System.out.println(beanToMap2);

        Map<String, Object> targetMap1 = new HashMap<>();
        targetMap1.put("id", "9527");
        targetMap1.put("name", "原始Name");
        BeanUtil.beanToMap(grade1, targetMap1, true, true);
        System.out.println("=========targetMap1==========");
        // {create_time=Sun Jan 07 17:13:33 CST 2024, num=2024001, name=长郡班, students=[Student{sid='1', name='张三', gradeNum=2024001, birthday=Sun Jan 07 17:13:33 CST 2024, isDelete=2, extMap=null}, Student{sid='2', name='李四', gradeNum=2024001, birthday=Sun Jan 07 17:13:33 CST 2024, isDelete=2, extMap={remark=调皮}}, Student{sid='2', name='王五', gradeNum=2024001, birthday=Sun Jan 07 17:13:33 CST 2024, isDelete=2, extMap=null}], id=9527}
        System.out.println(targetMap1);

        Map<String, Object> targetMap2 = new HashMap<>();
        BeanUtil.beanToMap(grade1, targetMap2, true, new Editor() {
            /**参数 o 是对象的属性名称*/
            @Override
            public Object edit(Object o) {
                // 为 createTime 字段名称加上一个前缀
                if (Objects.equals(o, "createTime")) {
                    return "_" + o;
                }
                return o;
            }
        });
        System.out.println("=========targetMap2==========");
        // {_createTime=Sun Jan 07 17:13:33 CST 2024, num=2024001, name=长郡班, students=[Student{sid='1', name='张三', gradeNum=2024001, birthday=Sun Jan 07 17:13:33 CST 2024, isDelete=2, extMap=null}, Student{sid='2', name='李四', gradeNum=2024001, birthday=Sun Jan 07 17:13:33 CST 2024, isDelete=2, extMap={remark=调皮}}, Student{sid='2', name='王五', gradeNum=2024001, birthday=Sun Jan 07 17:13:33 CST 2024, isDelete=2, extMap=null}]}
        System.out.println(targetMap2);
    }

    /**
     * T mapToBean(Map<?, ?> map, Class<T> beanClass, boolean isToCamelCase, CopyOptions copyOptions)
     * * Map转换为Bean对象
     * *
     * * @param <T>           Bean类型
     * * @param map           {@link Map}
     * * @param beanClass     Bean Class
     * * @param isToCamelCase 是否将Map中的下划线风格key转换为驼峰风格
     * * @param copyOptions   转Bean选项
     */
    @Test
    public void testMapToBean() {
        Grade grade1 = new Grade();
        grade1.setNum("2024001");
        grade1.setName("长郡班");
        grade1.setCreateTime(new Date());
        grade1.setUpdateTime(null);

        List<Student> students = new ArrayList<>();
        Map<String, Object> extMap = new HashMap<>();
        extMap.put("remark", "调皮");

        Student student1 = new Student("1", "张三", grade1.getNum(), new Date(), 2, null);
        Student student2 = new Student("2", "李四", grade1.getNum(), new Date(), 2, extMap);
        Student student3 = new Student("2", "王五", grade1.getNum(), new Date(), 2, null);
        students.add(student1);
        students.add(student2);
        students.add(student3);

        grade1.setStudents(students);

        Map<String, Object> beanToMap1 = BeanUtil.beanToMap(grade1);

        Grade mapToBean1 = BeanUtil.mapToBean(beanToMap1, Grade.class, true, CopyOptions.create().ignoreError());
        System.out.println("=========mapToBean1==================");
        // Grade{num='2024001', name='长郡班', createTime=Sun Jan 07 17:20:03 CST 2024, updateTime=null, students=[Student{sid='1', name='张三', gradeNum=2024001, birthday=Sun Jan 07 17:20:03 CST 2024, isDelete=2, extMap=null}, Student{sid='2', name='李四', gradeNum=2024001, birthday=Sun Jan 07 17:20:03 CST 2024, isDelete=2, extMap={remark=调皮}}, Student{sid='2', name='王五', gradeNum=2024001, birthday=Sun Jan 07 17:20:03 CST 2024, isDelete=2, extMap=null}]}
        System.out.println(mapToBean1);
    }

    /**
     * T toBean(Object source, Class<T> clazz)
     * * Java bean 对象或 Map 转 Java Bean，底层就是使用 copyProperties 进行的属性复制。
     * * @param source Bean对象或Map
     * * @param clazz  目标的Bean类型
     * <p>
     * T toBean(Object source, Class<T> clazz, CopyOptions options)
     * * 对象或Map转Bean，底层就是使用 copyProperties 进行的属性复制。
     * * @param source  Bean对象或Map
     * * @param clazz   目标的Bean类型
     * * @param options 属性拷贝选项
     * <p>
     * T toBean(Class<T> beanClass, ValueProvider<String> valueProvider, CopyOptions copyOptions)
     * * ServletRequest 参数转Bean
     * * @param beanClass     Bean Class
     * * @param valueProvider 值提供者
     * * @param copyOptions   拷贝选项，见 {@link CopyOptions}
     */
    @Test
    public void testToBean1() {
        Grade grade1 = new Grade();
        grade1.setNum("2024001");
        grade1.setName("长郡班");
        grade1.setCreateTime(new Date());
        grade1.setUpdateTime(null);

        List<Student> students = new ArrayList<>();
        Map<String, Object> extMap = new HashMap<>();
        extMap.put("remark", "调皮");

        Student student1 = new Student("1", "张三", grade1.getNum(), new Date(), 2, null);
        Student student2 = new Student("2", "李四", grade1.getNum(), new Date(), 2, extMap);
        Student student3 = new Student("2", "王五", grade1.getNum(), new Date(), 2, null);
        students.add(student1);
        students.add(student2);
        students.add(student3);

        grade1.setStudents(students);

        Grade toBean1 = BeanUtil.toBean(grade1, Grade.class);
        System.out.println("===============toBean1============");
        // Grade{num='2024001', name='长郡班', createTime=Sun Jan 07 17:26:51 CST 2024, updateTime=null, students=[Student{sid='1', name='张三', gradeNum=2024001, birthday=Sun Jan 07 17:26:51 CST 2024, isDelete=2, extMap=null}, Student{sid='2', name='李四', gradeNum=2024001, birthday=Sun Jan 07 17:26:51 CST 2024, isDelete=2, extMap={remark=调皮}}, Student{sid='2', name='王五', gradeNum=2024001, birthday=Sun Jan 07 17:26:51 CST 2024, isDelete=2, extMap=null}]}
        System.out.println(toBean1);

        Grade toBean2 = BeanUtil.toBean(grade1, Grade.class, CopyOptions.create().ignoreError());
        grade1.setName("重点班");
        System.out.println("===============toBean2============");
        // Grade{num='2024001', name='长郡班', createTime=Sun Jan 07 17:30:19 CST 2024, updateTime=null, students=[Student{sid='1', name='张三', gradeNum=2024001, birthday=Sun Jan 07 17:30:19 CST 2024, isDelete=2, extMap=null}, Student{sid='2', name='李四', gradeNum=2024001, birthday=Sun Jan 07 17:30:19 CST 2024, isDelete=2, extMap={remark=调皮}}, Student{sid='2', name='王五', gradeNum=2024001, birthday=Sun Jan 07 17:30:19 CST 2024, isDelete=2, extMap=null}]}
        System.out.println(toBean2);
    }

    /**
     * T toBeanIgnoreCase(Object source, Class<T> clazz, boolean ignoreError)
     * * 对象或Map转Bean，忽略字段转换时大小写，底层就是使用 copyProperties 进行的属性复制。
     * * @param source      Bean对象或Map
     * * @param clazz       目标的Bean类型
     * * @param ignoreError 是否忽略注入错误
     * <p>
     * T toBeanIgnoreError(Object source, Class<T> clazz)
     * * 对象或Map转Bean，忽略字段转换时发生的异常，底层就是使用 copyProperties 进行的属性复制。
     * * @param source      Bean对象或Map
     * * @param clazz       目标的Bean类型
     */
    @Test
    public void testToBeanIgnoreXxx1() {
        Grade grade1 = new Grade();
        grade1.setNum("2024001");
        grade1.setName("长郡班");
        grade1.setCreateTime(new Date());
        grade1.setUpdateTime(null);

        List<Student> students = new ArrayList<>();
        Map<String, Object> extMap = new HashMap<>();
        extMap.put("remark", "调皮");

        Student student1 = new Student("1", "张三", grade1.getNum(), new Date(), 2, null);
        Student student2 = new Student("2", "李四", grade1.getNum(), new Date(), 2, extMap);
        Student student3 = new Student("2", "王五", grade1.getNum(), new Date(), 2, null);
        students.add(student1);
        students.add(student2);
        students.add(student3);

        grade1.setStudents(students);

        Grade toBeanIgnoreError = BeanUtil.toBeanIgnoreError(grade1, Grade.class);
        System.out.println("===============toBeanIgnoreError============");
        // Grade{num='2024001', name='长郡班', createTime=Sun Jan 07 17:33:48 CST 2024, updateTime=null, students=[Student{sid='1', name='张三', gradeNum=2024001, birthday=Sun Jan 07 17:33:48 CST 2024, isDelete=2, extMap=null}, Student{sid='2', name='李四', gradeNum=2024001, birthday=Sun Jan 07 17:33:48 CST 2024, isDelete=2, extMap={remark=调皮}}, Student{sid='2', name='王五', gradeNum=2024001, birthday=Sun Jan 07 17:33:48 CST 2024, isDelete=2, extMap=null}]}
        System.out.println(toBeanIgnoreError);
    }


}
