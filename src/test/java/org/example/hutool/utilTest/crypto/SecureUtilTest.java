package org.example.hutool.utilTest.crypto;

import cn.hutool.crypto.digest.DigestUtil;
import org.junit.Test;

/**
 * 加密解密工具-SecureUtil，主要针对常用加密算法构建快捷方式，还有提供一些密钥生成的快捷工具方法。
 * https://www.hutool.cn/docs/#/crypto/%E6%A6%82%E8%BF%B0
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2023/1/13 9:39
 */
public class SecureUtilTest {

    //  * 摘要算法介绍
    //  * 摘要算法是一种能产生特殊输出格式的算法，这种算法的特点是：无论用户输入什么长度的原始数据，经过计算后输出的密文都是固定长度的，
    //  * 这种算法的原理是根据一定的运算规则对原数据进行某种形式的提取，这种提取就是摘要，被摘要的数据内容与原数据有密切联系，只要原数据稍有改变，输出的“摘要”便完全不同，
    //  * 因此，基于这种原理的算法便能对数据完整性提供较为健全的保障。
    //  * 但是，由于输出的密文是提取原数据经过处理的定长值，所以它已经不能还原为原数据，即消息摘要算法是不可逆的，理论上无法通过反向运算取得原数据内容，
    //  * 因此它通常只能被用来做数据完整性验证。
    //  * 摘要算法：MD2，MD5，SHA-1，SHA-256，SHA-384，SHA-512

    /**
     * byte[] md5(byte[] data) 计算32位MD5摘要值
     * byte[] md5(File file) 计算32位MD5摘要值
     * byte[] md5(InputStream data) 计算32位MD5摘要值
     * byte[] md5(String data) 计算32位MD5摘要值,使用UTF-8编码
     * byte[] md5(String data, String charset) 计算32位MD5摘要值
     */

    /**
     * String md5Hex(byte[] data) 计算32位MD5摘要值，并转为16进制字符串
     * String md5Hex(File file) 计算32位MD5摘要值，并转为16进制字符串
     * String md5Hex(InputStream data) 计算32位MD5摘要值，并转为16进制字符串
     * String md5Hex(String data) 计算32位MD5摘要值，并转为16进制字符串
     * String md5Hex(String data, Charset charset) 计算32位MD5摘要值，并转为16进制字符串
     * String md5Hex(String data, String charset) 计算32位MD5摘要值，并转为16进制字符串
     */
    @Test
    public void md5Test1() {
        String testStr = "Come de Way, ABC，蚩尤后裔！";
        String md5Hex1 = DigestUtil.md5Hex(testStr);
        // 880f8acc7e92df17ebafbf9b888f49ef
        System.out.println(md5Hex1);
    }

    /**
     * String md5Hex16(byte[] data) 计算16位MD5摘要值，并转为16进制字符串
     * String md5Hex16(File file) 计算16位MD5摘要值，并转为16进制字符串
     * String md5Hex16(InputStream data) 计算16位MD5摘要值，并转为16进制字符串
     * String md5Hex16(String data) 计算16位MD5摘要值，并转为16进制字符串
     * String md5Hex16(String data, Charset charset)  计算16位MD5摘要值，并转为16进制字符串
     */
    @Test
    public void md5Test2() {
        String testStr = "Come de Way, ABC，蚩尤后裔！";
        String md5Hex1 = DigestUtil.md5Hex16(testStr);
        // 7e92df17ebafbf9b
        System.out.println(md5Hex1);
    }

    /**
     * String md5HexTo16(String md5Hex) 32位MD5转16位MD5
     * * @param md5Hex 32位MD5，长度不能小于 24 ，否则报错：StringIndexOutOfBoundsException: String index out of range: 24
     * * @return 16位MD5
     */
    @Test
    public void md5Test3() {
        String md5For32 = "880f8acc880f8acc880f8ac";
        // 7e92df17ebafbf9b
        System.out.println(DigestUtil.md5HexTo16(md5For32));
    }

}
