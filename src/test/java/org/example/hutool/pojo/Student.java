package org.example.hutool.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * 学生
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/1/31 17:34
 */
public class Student implements Serializable {
    private String sid;
    private String name;
    private String gradeNum;
    private Date birthday;
    private Integer isDelete;
    private Map<String, Object> extMap;

    public Student() {
    }

    public Student(String sid, String name, String gradeNum, Date birthday, Integer isDelete, Map<String, Object> extMap) {
        this.sid = sid;
        this.name = name;
        this.gradeNum = gradeNum;
        this.birthday = birthday;
        this.isDelete = isDelete;
        this.extMap = extMap;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGradeNum() {
        return gradeNum;
    }

    public void setGradeNum(String gradeNum) {
        this.gradeNum = gradeNum;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Map<String, Object> getExtMap() {
        return extMap;
    }

    public void setExtMap(Map<String, Object> extMap) {
        this.extMap = extMap;
    }

    @Override
    public String toString() {
        return "Student{" +
                "sid='" + sid + '\'' +
                ", name='" + name + '\'' +
                ", gradeNum=" + gradeNum +
                ", birthday=" + birthday +
                ", isDelete=" + isDelete +
                ", extMap=" + extMap +
                '}';
    }
}
