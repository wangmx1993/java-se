package org.example.hutool.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 班级
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/1/7 16:09
 */
public class Grade implements Serializable {
    private String num;
    private String name;
    private Date createTime;
    private Date updateTime;
    private List<Student> students;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "num='" + num + '\'' +
                ", name='" + name + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", students=" + students +
                '}';
    }
}

