package org.example.hutool.http;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.StreamProgress;
import cn.hutool.core.lang.Console;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.http.HttpUtil;
import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * HttpUtil 下载文件
 * https://www.hutool.cn/docs/#/http/Http%E5%AE%A2%E6%88%B7%E7%AB%AF%E5%B7%A5%E5%85%B7%E7%B1%BB-HttpUtil?id=%e4%b8%8b%e8%bd%bd%e6%96%87%e4%bb%b6
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/3/22 17:06
 */
public class downloadFileTest {

    /**
     * long downloadFile(String url, File destFile): 下载远程文件
     * long downloadFile(String url, File destFile, int timeout)
     * long downloadFile(String url, String dest)
     * url：请求的 url
     * destFile：目标文件或目录，当为目录时，取URL中的文件名，取不到使用编码后的URL做为文件名
     * timeout：超时，单位毫秒。包括连接和读取总共的时间
     * 返回文件大小
     */
    @Test
    public void testDownloadFile1() {
        String url = "http://www.gov.cn/xinwen/2021-03/21/5594239/images/c83e5d51b2314f75bdeb1e0b3e021daa.jpg";
        String name = FileUtil.getName(url);

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = new File(homeDirectory, name);

        long size = HttpUtil.downloadFile(url, file, 5 * 60 * 1000);
        System.out.println(size + ":" + file.getAbsolutePath());
    }

    /**
     * long downloadFile(String url, File destFile, StreamProgress streamProgress)：下载远程文件
     * long downloadFile(String url, File destFile, int timeout, StreamProgress streamProgress)
     * url ：请求的url
     * destFile：目标文件或目录，当为目录时，取URL中的文件名，取不到使用编码后的URL做为文件名
     * streamProgress：进度条回调
     * timeout：超时，单位毫秒。包括连接和读取总共的时间
     * 返回文件大小
     *
     * @throws MalformedURLException
     */
    @Test
    public void testDownloadFile2() throws MalformedURLException {
        String url = "http://www.gov.cn/xinwen/2021-03/21/5594239/images/c83e5d51b2314f75bdeb1e0b3e021daa.jpg";
        String name = FileUtil.getName(url);

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = new File(homeDirectory, name);

        //获取指定URL对应资源的内容长度
        long length = URLUtil.getContentLength(new URL(url));

        //带进度显示的文件下载
        long size = HttpUtil.downloadFile(url, file, new StreamProgress() {

            @Override
            public void start() {
                Console.log("开始下载。。。。");
            }

            @Override
            public void progress(long total, long progressSize) {
                String fileSize = FileUtil.readableFileSize(progressSize);
                String percentage0 = String.format("%.2f", (progressSize * 1.0 / length * 100)) + "%";
                String percentage = String.format("%.2f", (progressSize * 1.0 / total * 100)) + "%";
                Console.log("已下载：{}，百分比：{}", fileSize, percentage);
                /**
                 * 已下载：8 kB，百分比：3.58%
                 * ...
                 * 已下载：219.62 kB，百分比：98.20%
                 * 已下载：223.65 kB，百分比：100.00%
                 */
            }

            @Override
            public void finish() {
                Console.log("下载完成！");
            }
        });
        System.out.println(size + ":" + file.getAbsolutePath());
    }

    /**
     * String downloadString(String url, Charset customCharset)：下载远程文本
     * String downloadString(String url, String customCharset)
     * * @param url           请求的url
     * * @param customCharset 自定义的字符集，可以使用{@link CharsetUtil#charset} 方法转换
     * 实质就是直接读取了远程文件的内容，比如如果是图片，则读取的是乱码的内容，如果是文本文件，则能正常读取
     */
    @Test
    public void testDownloadString1() {
        //如果是网页，则读取的网页源码
//        String url = "https://news.ifeng.com/c/84onpoy4Nh0";

        String url = "http://www.gov.cn/govweb/xhtml/2016gov/js/jquery.date_input.pack.js";
        String string = HttpUtil.downloadString(url, CharsetUtil.CHARSET_UTF_8);
        System.out.println(string);
    }

    /**
     * String downloadString(String url, Charset customCharset, StreamProgress streamPress)：下载远程文本，带进度条
     *
     * @throws MalformedURLException
     */
    @Test
    public void testDownloadString2() throws MalformedURLException {
        String url = "https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js";

        //获取指定URL对应资源的内容长度
        long length = URLUtil.getContentLength(new URL(url));

        //带进度显示的文件下载
        String content = HttpUtil.downloadString(url, CharsetUtil.CHARSET_UTF_8, new StreamProgress() {

            @Override
            public void start() {
                Console.log("开始下载。。。。");
            }

            @Override
            public void progress(long total, long progressSize) {
                String fileSize = FileUtil.readableFileSize(progressSize);
                String percentage0 = String.format("%.2f", (progressSize * 1.0 / length * 100)) + "%";
                String percentage = String.format("%.2f", (progressSize * 1.0 / total * 100)) + "%";
                Console.log("已下载：{}，百分比：{}", fileSize, percentage);
                /**
                 * 已下载：8 kB，百分比：3.58%
                 * ...
                 * 已下载：219.62 kB，百分比：98.20%
                 * 已下载：223.65 kB，百分比：100.00%
                 */
            }

            @Override
            public void finish() {
                Console.log("下载完成！");
            }
        });
        System.out.println(content);
    }


}
