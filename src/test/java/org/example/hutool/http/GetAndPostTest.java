package org.example.hutool.http;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.http.HttpUtil;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Http 客户端工具类 - HttpUtil 发生 Get 与 Post 请求(包括文件上传)
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/3/21 16:17
 */
public class GetAndPostTest {

    /**
     * String get(String urlString) :发送 get 请求,urlString 网址
     * 最简单的 HTTP get 请求，可以自动通过 header 等信息判断编码，不区分 HTTP 和 HTTPS
     * 返回内容: 如果只检查状态码，正常只返回 ""，不正常返回 null；如果是网页，则返回网页右键"查看页面源码"所显示的内容
     */
    @Test
    public void testGet1() {
        // 一个凤凰新闻的浏览器 url 地址，返回的是凤凰网新闻的 html 内容
        String url = "https://news.ifeng.com/c/84lDfS96Ri9";
        String content = HttpUtil.get(url);
        System.out.println(content);
    }

    /**
     * String get(String urlString, int timeout)：发送 get 请求
     * urlString：网址
     * timeout： 超时时长，-1表示默认超时，单位毫秒，包括连接时间与读取时间
     * 返回内容，如果只检查状态码，正常只返回 ""，不正常返回 null
     */
    @Test
    public void testGet2() {
        //一个爱奇艺的 get 数据接口，返回的是 json 数据
        String url = " https://data.video.iqiyi.com/v.f4v";
        String content = HttpUtil.get(url, 60 * 1000);

        //{"t":"CT|HuNan_ChangSha-175.0.251.212","uproxy":"1","v":"2021-03-01 v21.09.1","direct_h2c":"1","mrc":"0","httpsock":"1","time":"1616315534","z":"hengyang11_ct","l":"https://v-af06e0cc.71edge.com/v.f4v?uuid=af00fbd4-6057048e-fb&src=iqiyi.com"}
        System.out.println(content);
    }

    /**
     * String get(String urlString, Charset customCharset): 当无法识别页面编码的时候，可以自定义请求页面的编码
     */
    @Test
    public void testGet3() {
        //一个腾讯新闻的 get 数据接口
        String url = "https://i.news.qq.com/trpc.qqnews_web.pc_base_srv.base_http_proxy/NinjaPageContentSync?pull_urls=today_topic_2018";
        String result2 = HttpUtil.get(url, CharsetUtil.CHARSET_UTF_8);

        //{"ret":0,"msg":"success","data":[{"url":"https://new.qq.com/omn/20210321/20210321A01T2M00.html","img_url":"http://inews.gtimg.com/newsapp_ls/0/13313802728_640330/0","article_id":"20210321a01t2m00","comment_id":"0","title":"拜登最尴尬的三秒钟，事情没那么简单","group":0},{"url":"https://new.qq.com/omn/author/1124","img_url":"","article_id":"1124","comment_id":"0","title":"中国新闻网","group":1},{"url":"https://new.qq.com/omn/20210321/20210321A004C600.html","img_url":"http://inews.gtimg.com/newsapp_ls/0/13313423073_640330/0","article_id":"20210321a004c600","comment_id":"0","title":"三星堆是外星人遗迹？最新考古成果里藏着答案","group":1},{"url":"https://new.qq.com/omn/author/1051","img_url":"","article_id":"1051","comment_id":"0","title":"人民日报","group":2},{"url":"https://view.inews.qq.com/a/20210321A01LRK00","img_url":"http://inews.gtimg.com/newsapp_ls/0/13312195396_640330/0","article_id":"20210321A01LRK00","comment_id":"0","title":"中国首条时速350公里跨海高铁来了！","group":2},{"url":"https://new.qq.com/omn/author/5069188","img_url":"","article_id":"5069188","comment_id":"0","title":"中国新闻周刊","group":3},{"url":"https://new.qq.com/omn/20210321/20210321A025OQ00.html","img_url":"http://inews.gtimg.com/newsapp_ls/0/13313879765_640330/0","article_id":"20210321a025oq00","comment_id":"0","title":"“伪央企”屡禁不绝，谁给了它们唬人的身份？","group":3},{"url":"https://new.qq.com/omn/author/10123904","img_url":"","article_id":"10123904","comment_id":"0","title":"谷雨数据","group":4},{"url":"https://new.qq.com/omn/20210320/20210320A0ABI100.html","img_url":"http://inews.gtimg.com/newsapp_ls/0/13313718727_640330/0","article_id":"20210320a0abi100","comment_id":"0","title":"超3亿中国人睡眠障碍 数据告诉你他们在焦虑什么","group":4},{"url":"https://new.qq.com/omn/author/5107513","img_url":"","article_id":"5107513","comment_id":"0","title":"较真","group":5},{"url":"https://view.inews.qq.com/a/20210320A02PC500","img_url":"http://inews.gtimg.com/newsapp_ls/0/13306325365_640330/0","article_id":"20210320A02PC500","comment_id":"0","title":"郭美美因销售违禁减肥产品被刑拘，滥用这些是在玩命","group":5},{"url":"https://new.qq.com/omn/author/5157372","img_url":"","article_id":"5157372","comment_id":"0","title":"深网","group":6},{"url":"https://view.inews.qq.com/a/20210318A013VT00","img_url":"http://inews.gtimg.com/newsapp_ls/0/13305521441_640330/0","article_id":"20210318A013VT00","comment_id":"0","title":"揭秘拼多多网状管理架构：黄峥为何敢放手拼多多？","group":6}]}
        System.out.println(result2);
    }

    /**
     * String get(String urlString, Map<String, Object> paramMap, int timeout): 发送 get 请求
     * timeout：超时时间，单位为毫秒，包括连接时间与读取时间
     * get 请求的查询参数可以直接带在 url 后面，如 ?a=1&b=40 等等，也可以像 post 请求一样，通过 map 封装参数。
     * 这样参数会自动做 URL 编码，拼接在URL中，对于特殊字符就无需自己做 url 编码了。
     */
    @Test
    public void testGet4() {
        //一个腾讯新闻的 get 数据接口
        String url = "https://i.news.qq.com/trpc.qqnews_web.kv_srv.kv_srv_http_proxy/list";

        //将查询参数通过 map 封装，自动进行 url 编码，拼接在 url 后面
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("sub_srv_id", "24hours");
        paramMap.put("srv_id", "pc");
        paramMap.put("offset", 0);
        paramMap.put("limit", 20);
        paramMap.put("strategy", 1);
        paramMap.put("ext", "{\"pool\":[\"top\"],\"is_filter\":7,\"check_type\":true}");

        String result1 = HttpUtil.get(url, paramMap, 60 * 1000);
        //{"ret":0,"msg":"success","data":{"list":[{"cms_id":"20210321A03V1700","title":"触目惊心！黄河大堤内哪来这么多死猪？",...
        System.out.println(result1);
    }

    /**
     * String post(String urlString, String body): 发送 post 请求
     * String post(String urlString, String body, int timeout)
     * 1、urlString 网址、body 请求体数据，可以是map，也可以直接是 json 格式的字符串
     * 2、请求体 body 参数支持两种类型：
     * * 1. 标准查询参数：例如 a=1&amp;b=2 这种格式
     * * 2. Rest模式：此时body需要传入一个JSON或者XML字符串，Hutool会自动绑定其对应的Content-Type
     * 3、timeout：超时时间，单位为毫秒，包括连接时间与读取时间
     */
    @Test
    public void testPost1() {
        //一个 腾讯新闻的 post 数据请求接口
        String url = "https://news.ssp.qq.com/app";
        String body = "{\"adReqData\":{\"chid\":9,\"adtype\":0,\"uin\":null,\"qq_openid\":\"\",\"slot\":[{\"loid\":\"1\",\"cur\":0,\"channel\":\"world\",\"orders_info\":[],\"article_id\":\"\",\"refresh_type\":1,\"current_rot\":\"\",\"seq\":\"\",\"seq_loid\":\"\"}],\"netstatus\":\"unknown\",\"pf\":\"pc\",\"appversion\":\"200717\",\"plugin_news_cnt\":20,\"plugin_page_type\":\"\",\"plugin_text_ad\":false,\"plugin_bucket_id\":\"\"}}";
        String post = HttpUtil.post(url, body);

        //{"adList":"{ \"ret\": 0,\"index\": [{ \"channel\":\"world\",\"channel_id\":7365,\"stream\": { \"loc\": \"PC_News_Stream\",\"rot\":\"3164654888,3099732480,356634000,3123822998\",\"seq\":\"2,8,14,20\",\"subtype\":\",,,\",\"replace_type\":\"0,0,0,0\",\"server_data\":\"c29pZD1BRjAwRkJENDkxRDI2MDU3MTBBRjAxNkQ5NzAxJmluZGV4PTEmY21wX3ZlcnNpb249JmlzX2xvY2FsPTAmY2hhbm5lbF9pZD03MzY1JmF2Z19lY3BtPTExNzc1MDAuMDAwMDAwJnVwX3ZlcnNpb249IDEuMC4yNjgmc3ViX3R5cGU9MTEmbGF1bmNoPTAmbGF1bmNoX3RpbWVzdGFtcD0mYWRsb2FkX2NvbmRpZD0mYWRsb2FkX2FsZ29pZD0=,c29pZD1BRjAwRkJENDkxRDI2MDU3MTBBRjAxNkQ5NzAyJmluZGV4PTImY21wX3ZlcnNpb249JmlzX2xvY2FsPTAmY2hhbm5lbF9pZD03MzY1JmF2Z19lY3BtPTExNzc1MDAuMDAwMDAwJnVwX3ZlcnNpb249IDEuMC4yNjgmc3ViX3R5cGU9MTEmbGF1bmNoPTAmbGF1bmNoX3RpbWVzdGFtcD0mYWRsb2FkX2NvbmRpZD0mYWRsb2FkX2FsZ29pZD0=,c29pZD1BRjAwRkJENDkxRDI2MDU3MTBBRjAxNkQ5NzAzJmluZGV4PTMmY21wX3ZlcnNpb249JmlzX2xvY2FsPTAmY2hhbm5lbF9pZD03MzY1JmF2Z19lY3BtPTExNzc1MDAuMDAwMDAwJnVwX3ZlcnNpb249IDEuMC4yNjgmc3ViX3R5cGU9MTEmbGF1bmNoPTAmbGF1bmNoX3RpbWVzdGFtcD0mYWRsb2FkX2NvbmRpZD0mYWRsb2FkX2FsZ29pZD0=,c29pZD1BRjAwRkJENDkxRDI2MDU3MTBBRjAxNkQ5NzA0JmluZGV4PTQmY21wX3ZlcnNpb249JmlzX2xvY2FsPTAmY2hhbm5lbF9pZD03MzY1JmF2Z19lY3BtPTExNzc1MDAuMDAwMDAwJnVwX3ZlcnNpb249IDEuMC4yNjgmc3ViX3R5cGU9MTEmbGF1bmNoPTAmbGF1bmNoX3RpbWVzdGFtcD0mYWRsb2FkX2NvbmRpZD0mYWRsb2FkX2FsZ29pZD0=\",\"order_source\":\"110,110,110,110\",\"empty_ad_report_url\":[\"\",\"\",\"\",\"\"]}}],\"order\": [{\"sub_type\":11,\"act_type\":2,\"oid\":\"3164654888\",\"cid\":\"3164654896\",\"soid\":\"AF00FBD491D2605710AF016D9701\",\"loc\":\"PC_News_Stream\",\"stream_type\":1,\"navTitle\":\"\",\"shareTitle\":\"在长沙满足2个条件，就能考消防证，挑战年薪40万\",\"resource_url0\":\"http://pgdt.gtimg.cn/gdt/0/EAA5g0WAKAAFoAAAJUMBgPfxIAEt1vlId.jpg/0?ck=0baf73548b1b9cdad47b6a63267030cc\",\"title\":\"在长沙满足2个条件，就能考消防证，挑战年薪40万\",\"abstract\":\"\",\"thumbnails\":\"\",\"url\":\"https://c.gdt.qq.com/gdt_click.fcg?viewid=K995_eExOtY9UhIl!TSpA6Vs42iuKJrg7GbLmuBiPeLC3s!pj!CNlx3XTyrB0QS3w2olAnEdYkv5B49CGKvuWuNQEKlaHt8BIJIBR7wD72!Fmg16uUrj3OSew2R!jy5d98clJaFYv5Zi2FD31bO2YF8TZTIkJINH&jtype=0&i=1&os=3&s_lp=101&acttype=__ACT_TYPE__&ch=__CHANNEL_ID__&seq=__SEQ__&aseq=__ABS_SEQ__&rt=__RETURN_TYPE__&s=%7B%22da%22%3A%22__WIDTH__%22%2C%22db%22%3A%22__HEIGHT__%22%2C%22down_x%22%3A%22__DOWN_X__%22%2C%22down_y%22%3A%22__DOWN_Y__%22%2C%22up_x%22%3A%22__UP_X__%22%2C%22up_y%22%3A%22__UP_Y__%22%7D&clklpp=__CLICK_LPP__&xp=3&vto=__VIDEO_PLAY_TIME__&tl=1\",\"url_type\":0,\"icon\":\"\",\"pvLimit\":0,\"pvFcs\":0,\"reportUrlSdk\":[],\"reportUrlOther\":[],\"clickReportUrlOther\":[],\"webp\":0,\"imagew\":640,\"imageh\":360,\"order_prio\":0,\"comment\":0,\"newsId\":\"\",\"creative_plugin\":0,\"shareable\":1,\"viewReportUrl\":\"https://v.gdt.qq.com/gdt_stats.fcg?viewid=K995_eExOtY9UhIl!TSpA6Vs42iuKJrg7GbLmuBiPeLC3s!pj!CNlx3XTyrB0QS3w2olAnEdYkv5B49CGKvuWuNQEKlaHt8BIJIBR7wD72!Fmg16uUrj3OSew2R!jy5d98clJaFYv5Zi2FD31bO2YF8TZTIkJINH&i=1&os=3&datatype=jsonp&etype=__EXPOSURE_TYPE__&ch=__CHANNEL_ID__&seq=__SEQ__&aseq=__ABS_SEQ__&xp=3&tl=1\",\"feedbackReportUrl\":\"https://nc.gdt.qq.com/gdt_report.fcg?viewid=K995_eExOtY9UhIl!TSpA6Vs42iuKJrg7GbLmuBiPeLC3s!pj!CNlx3XTyrB0QS3w2olAnEdYkv5B49CGKvuWuNQEKlaHt8BIJIBR7wD72!Fmg16uUrj3OSew2R!jy5d98clJaFYv5Zi2FD31bO2YF8TZTIkJINH&acttype=__ACT_TYPE__&s=27\",\"downloadReportUrl\":\"https://t.gdt.qq.com/conv/src/61/conv?client=61&product_id=&cuser=src&click_id=__CLICK_ID__&actionid=__ACTION_ID__\",\"download\":0,\"advertiser_id\":15076630,\"product_id\":923365719,\"product_type\":1000,\"industry_id\":506,\"icon_url\":\"http://pgdt.gtimg.cn/gdt/0/DAAAGQKAA2AA2AAABbDlPFCGCrMo_3.png/0?ck=be984000a753846ccd3b7ffc0b821e4c\",\"local_ad\":{\"store_name\":\"\",\"pop_sheet\":\"\"},\"form_component_info\":{},\"dest_type\":0,\"rich_media_url\":\"\",\"rich_media_id\":\"\",\"unlike_btn\":\"1\",\"open_mini_program\":{\"user_name\":\"\",\"path\":\"\",\"token\":\"\",\"wx_appid\":\"\",\"ad_trace_data\":\"\"},\"avoidDialog\":0,\"brandIcon\":\"\",\"trace_id\":\"7ma5nusf7k3rs01\",\"jdt_frame\":0,\"video_time\":0,\"open_scheme\":\"\",\"labels\":[],\"short_title\":\"\", \"ping_data\":\"b21naWQ9JnFxPTAmd3hpZD0mcXFvcGVuaWQ9JmlwPTE3NS4wLjI1MS4yMTImaXB2Nj0mc29pZD1BRjAwRkJENDkxRDI2MDU3MTBBRjAxNkQ5NzAxJmRpc3BsYXlfaWQ9OTAwNDYmZmVhdHVyZV90eXBlPTEmdXNlcl9pbmZvPW9CM2puQ000Q1JhZ0hlT2NJemdKRnFBZDQ1czZPaEFYb0E9PSZnYW1lX2lkPTAmd3hfd2Vpc2hpPTAmZWNwbT1PS1JtcmoydWFxV2IrYmlTelFkQTNIT1NvS3dzcjgwRSZjcGM9Z1h3SVVpeUswNWJ3L0dyMVJqcWNVakZUS0tuZGE1SUImaW5kdXN0cnlfaWQ9NTA2JmFkdmVydGlzZXJfaWQ9MTUwNzY2MzAmcHJvZHVjdF9pZD05MjMzNjU3MTkmcHJvZHVjdF90eXBlPTEwMDAmc3RhdHVzX2NvZGU9MTAxJmRzcF9pcD05LjE0Ni42OS4xNjkmb3JkZXJfdHlwZT0yJnNwbGFzaF9vcHRpbWFsPTAmZ2VuZGVyPTAmYWdlPTAmbW9iaWxlX3RhZz0wJmJvb2tfaWQ9JnRhYl90ZXN0X2lkPQ==\",\"click_data\": \"b21naWQ9JnFxPTAmd3hpZD0mcXFvcGVuaWQ9JmlwPTE3NS4wLjI1MS4yMTImaXB2Nj0mc29pZD1BRjAwRkJENDkxRDI2MDU3MTBBRjAxNkQ5NzAxJmRpc3BsYXlfaWQ9OTAwNDYmZmVhdHVyZV90eXBlPTEmdXNlcl9pbmZvPW9CM2puQ000Q1JhZ0hlT2NJemdKRnFBZDQ1czZPaEFYb0E9PSZnYW1lX2lkPTAmd3hfd2Vpc2hpPTAmZWNwbT1Da3h3d1lRZi9BanlMN0JSM2Z1SFN1UnhFUnIzWDBIcCZjcGM9TXlRWm5EY0VqdUtTRTEyUmNMTVczeklSbUJaYkROOHEmaW5kdXN0cnlfaWQ9NTA2JmFkdmVydGlzZXJfaWQ9MTUwNzY2MzAmcHJvZHVjdF9pZD05MjMzNjU3MTkmcHJvZHVjdF90eXBlPTEwMDAmc3RhdHVzX2NvZGU9MTAxJmRzcF9pcD05LjE0Ni42OS4xNjkmb3JkZXJfdHlwZT0yJnNwbGFzaF9vcHRpbWFsPTAmZ2VuZGVyPTAmYWdlPTAmbW9iaWxlX3RhZz0wJmJvb2tfaWQ9JnRhYl90ZXN0X2lkPQ==\",\"extraReportUrl\":\"http://rpt.gdt.qq.com/landing_page?click_id=__CLICK_ID__&trace_id=7ma5nusf7k3rs01&pa=__PAGE_ACTION_ID__&pat=__PAGE_TIME__&lec=__LANDING_ERROR_CODE__&ot=__OS_TYPE__&ver=__VERSION__\",\"ams_traceid\":\"7ma5nusf7k3rs\",\"ad_context\":\"CLcHMOOksrkBOK6Kp7fE+4eSrAFQAGAAcg0IARCf6fun0+7ztvoBcgQIAhAA\",\"complaintUrl\":\"https://i.gtimg.cn/qzone/biz/gdt/ams_ad_audit/h5/feedback?rsApiUrl=https%3A%2F%2Fnc.gdt.qq.com%2Fgdt_report.fcg%3Fviewid%3D7pKSSzl7kUb0W6!1GZnJ2qneQ7p7lZ_GiHrgCL9zw4btp3ieEeWyO4pYNK4WYFi2qxQMDd0MJkYb8NFeMrcrOQ%26acttype%3D5009%26s%3D27\",\"splash_optimal\":0,\"marketing_scene\":0},{\"sub_type\":11,\"act_type\":2,\"oid\":\"3099732480\",\"cid\":\"3099732496\",\"soid\":\"AF00FBD491D2605710AF016D9702\",\"loc\":\"PC_News_Stream\",\"stream_type\":1,\"navTitle\":\"\",\"shareTitle\":\"老婆考了消防证，收入竟够养全家，在长沙满足2点你也能考！\",\"resource_url0\":\"http://pgdt.gtimg.cn/gdt/0/EABGAzlAKAAFoAAAIFiBf4xBYAyqz3Yo_.jpg/0?ck=c45f9548d53e4f12236ad97731d9b223\",\"title\":\"老婆考了消防证，收入竟够养全家，在长沙满足2点你也能考！\",\"abstract\":\"\",\"thumbnails\":\"\",\"url\":\"https://c.gdt.qq.com/gdt_click.fcg?viewid=g1RoWcsaMD09UhIl!TSpA2tmoq2sEcbxDmaiCJHD5yfP_fZWTmm8O!0nnSrb3qzvY20YBOBGDpPjHMxbYv79EsybpCTV0mH4L30h!KTVbaoCvAuRy9nS7IMFiA5rVIxDdN!wQRvQGzJLQbSIhle5Yl_CGkbzIHmx&jtype=0&i=1&os=3&s_lp=101&acttype=__ACT_TYPE__&ch=__CHANNEL_ID__&seq=__SEQ__&aseq=__ABS_SEQ__&rt=__RETURN_TYPE__&s=%7B%22da%22%3A%22__WIDTH__%22%2C%22db%22%3A%22__HEIGHT__%22%2C%22down_x%22%3A%22__DOWN_X__%22%2C%22down_y%22%3A%22__DOWN_Y__%22%2C%22up_x%22%3A%22__UP_X__%22%2C%22up_y%22%3A%22__UP_Y__%22%7D&clklpp=__CLICK_LPP__&xp=3&vto=__VIDEO_PLAY_TIME__&tl=1\",\"url_type\":0,\"icon\":\"\",\"pvLimit\":0,\"pvFcs\":0,\"reportUrlSdk\":[],\"reportUrlOther\":[],\"clickReportUrlOther\":[],\"webp\":0,\"imagew\":640,\"imageh\":360,\"order_prio\":0,\"comment\":0,\"newsId\":\"\",\"creative_plugin\":0,\"shareable\":1,\"viewReportUrl\":\"https://v.gdt.qq.com/gdt_stats.fcg?viewid=g1RoWcsaMD09UhIl!TSpA2tmoq2sEcbxDmaiCJHD5yfP_fZWTmm8O!0nnSrb3qzvY20YBOBGDpPjHMxbYv79EsybpCTV0mH4L30h!KTVbaoCvAuRy9nS7IMFiA5rVIxDdN!wQRvQGzJLQbSIhle5Yl_CGkbzIHmx&i=1&os=3&datatype=jsonp&etype=__EXPOSURE_TYPE__&ch=__CHANNEL_ID__&seq=__SEQ__&aseq=__ABS_SEQ__&xp=3&tl=1\",\"feedbackReportUrl\":\"https://nc.gdt.qq.com/gdt_report.fcg?viewid=g1RoWcsaMD09UhIl!TSpA2tmoq2sEcbxDmaiCJHD5yfP_fZWTmm8O!0nnSrb3qzvY20YBOBGDpPjHMxbYv79EsybpCTV0mH4L30h!KTVbaoCvAuRy9nS7IMFiA5rVIxDdN!wQRvQGzJLQbSIhle5Yl_CGkbzIHmx&acttype=__ACT_TYPE__&s=27\",\"downloadReportUrl\":\"https://t.gdt.qq.com/conv/src/61/conv?client=61&product_id=&cuser=src&click_id=__CLICK_ID__&actionid=__ACTION_ID__\",\"download\":0,\"advertiser_id\":18353381,\"product_id\":2692031232,\"product_type\":1000,\"industry_id\":506,\"icon_url\":\"http://pgdt.gtimg.cn/gdt/0/DAAAGQKAA2AA2AAABbDlPFCGCrMo_3.png/0?ck=be984000a753846ccd3b7ffc0b821e4c\",\"local_ad\":{\"store_name\":\"\",\"pop_sheet\":\"\"},\"form_component_info\":{},\"dest_type\":0,\"rich_media_url\":\"\",\"rich_media_id\":\"\",\"unlike_btn\":\"1\",\"open_mini_program\":{\"user_name\":\"\",\"path\":\"\",\"token\":\"\",\"wx_appid\":\"\",\"ad_trace_data\":\"\"},\"avoidDialog\":0,\"brandIcon\":\"\",\"trace_id\":\"7ma5nusf7k3rs02\",\"jdt_frame\":0,\"video_time\":0,\"open_scheme\":\"\",\"labels\":[],\"short_title\":\"\", \"ping_data\":\"b21naWQ9JnFxPTAmd3hpZD0mcXFvcGVuaWQ9JmlwPTE3NS4wLjI1MS4yMTImaXB2Nj0mc29pZD1BRjAwRkJENDkxRDI2MDU3MTBBRjAxNkQ5NzAyJmRpc3BsYXlfaWQ9OTAwNDYmZmVhdHVyZV90eXBlPTEmdXNlcl9pbmZvPW9CM2puQ000Q1JhZ0hlT2NJemdKRnFBZDQ1czZPaEFYb0E9PSZnYW1lX2lkPTAmd3hfd2Vpc2hpPTAmZWNwbT01SUxNRmErS29ENTFqS29sU0FmMU93VHgyc1Uyc3FMMiZjcGM9MXM4TktPZjEzcitxOE5lNG5wWFBGRkErOVdUNmNLMzMmaW5kdXN0cnlfaWQ9NTA2JmFkdmVydGlzZXJfaWQ9MTgzNTMzODEmcHJvZHVjdF9pZD0yNjkyMDMxMjMyJnByb2R1Y3RfdHlwZT0xMDAwJnN0YXR1c19jb2RlPTEwMSZkc3BfaXA9OS4xNDYuNjkuMTY5Jm9yZGVyX3R5cGU9MiZzcGxhc2hfb3B0aW1hbD0wJmdlbmRlcj0wJmFnZT0wJm1vYmlsZV90YWc9MCZib29rX2lkPSZ0YWJfdGVzdF9pZD0=\",\"click_data\": \"b21naWQ9JnFxPTAmd3hpZD0mcXFvcGVuaWQ9JmlwPTE3NS4wLjI1MS4yMTImaXB2Nj0mc29pZD1BRjAwRkJENDkxRDI2MDU3MTBBRjAxNkQ5NzAyJmRpc3BsYXlfaWQ9OTAwNDYmZmVhdHVyZV90eXBlPTEmdXNlcl9pbmZvPW9CM2puQ000Q1JhZ0hlT2NJemdKRnFBZDQ1czZPaEFYb0E9PSZnYW1lX2lkPTAmd3hfd2Vpc2hpPTAmZWNwbT1DUkVHMGlET0ZFS2liaU4xWEtVQURaWVZEb3I4ZU1mVSZjcGM9aEgvRnByTk1xcllNWXhEcWJsUVBxa0x6dm5sKzZUdmgmaW5kdXN0cnlfaWQ9NTA2JmFkdmVydGlzZXJfaWQ9MTgzNTMzODEmcHJvZHVjdF9pZD0yNjkyMDMxMjMyJnByb2R1Y3RfdHlwZT0xMDAwJnN0YXR1c19jb2RlPTEwMSZkc3BfaXA9OS4xNDYuNjkuMTY5Jm9yZGVyX3R5cGU9MiZzcGxhc2hfb3B0aW1hbD0wJmdlbmRlcj0wJmFnZT0wJm1vYmlsZV90YWc9MCZib29rX2lkPSZ0YWJfdGVzdF9pZD0=\",\"extraReportUrl\":\"http://rpt.gdt.qq.com/landing_page?click_id=__CLICK_ID__&trace_id=7ma5nusf7k3rs02&pa=__PAGE_ACTION_ID__&pat=__PAGE_TIME__&lec=__LANDING_ERROR_CODE__&ot=__OS_TYPE__&ver=__VERSION__\",\"ams_traceid\":\"7ma5nusf7k3rs\",\"ad_context\":\"CLcHMOOksrkBOIub24LRgqWfElAAYAByDAgBEMq6+Kfe5suGbnIECAIQAA==\",\"complaintUrl\":\"https://i.gtimg.cn/qzone/biz/gdt/ams_ad_audit/h5/feedback?rsApiUrl=https%3A%2F%2Fnc.gdt.qq.com%2Fgdt_report.fcg%3Fviewid%3D7pKSSzl7kUb0W6!1GZnJ2hz3lNxVTrTXiHrgCL9zw4btp3ieEeWyO5k6ZzEgocpDuRBo7G66PAxMEIVUb5DqtA%26acttype%3D5009%26s%3D27\",\"splash_optimal\":0,\"marketing_scene\":0},{\"sub_type\":11,\"act_type\":2,\"oid\":\"356634000\",\"cid\":\"446923741\",\"soid\":\"AF00FBD491D2605710AF016D9703\",\"loc\":\"PC_News_Stream\",\"stream_type\":1,\"navTitle\":\"\",\"shareTitle\":\"老人终身未嫁，只为等一个人的到来\",\"resource_url0\":\"http://pgdt.gtimg.cn/gdt/0/EABH8oqAKAAFoAAAO15Bf5IdECpYLz_68.jpg/0?ck=1efa8664a5f67fb15cf29624ed1d2e2b\",\"title\":\"老人终身未嫁，只为等一个人的到来\",\"abstract\":\"\",\"thumbnails\":\"\",\"url\":\"https://c.gdt.qq.com/gdt_click.fcg?viewid=Jr!y0Q8kQKc9UhIl!TSpAwixMARzwElPPW4t4qBQ2BFcC49FcYNxj9HYvjXljVT4jPK63pcLLaYIr0RYA9vX7fSmVoVtO3ibyG7x3A4IW9GxKAMkeYKrwNa6KCw1U59dIxB9VAYLCMMj8u6KzsGmIA&jtype=0&i=1&os=3&s_lp=101&acttype=__ACT_TYPE__&ch=__CHANNEL_ID__&seq=__SEQ__&aseq=__ABS_SEQ__&rt=__RETURN_TYPE__&s=%7B%22da%22%3A%22__WIDTH__%22%2C%22db%22%3A%22__HEIGHT__%22%2C%22down_x%22%3A%22__DOWN_X__%22%2C%22down_y%22%3A%22__DOWN_Y__%22%2C%22up_x%22%3A%22__UP_X__%22%2C%22up_y%22%3A%22__UP_Y__%22%7D&clklpp=__CLICK_LPP__&xp=3&vto=__VIDEO_PLAY_TIME__&tl=1\",\"url_type\":0,\"icon\":\"\",\"pvLimit\":0,\"pvFcs\":0,\"reportUrlSdk\":[],\"reportUrlOther\":[],\"clickReportUrlOther\":[],\"webp\":0,\"imagew\":640,\"imageh\":360,\"order_prio\":0,\"comment\":0,\"newsId\":\"\",\"creative_plugin\":0,\"shareable\":1,\"viewReportUrl\":\"https://v.gdt.qq.com/gdt_stats.fcg?viewid=Jr!y0Q8kQKc9UhIl!TSpAwixMARzwElPPW4t4qBQ2BFcC49FcYNxj9HYvjXljVT4jPK63pcLLaYIr0RYA9vX7fSmVoVtO3ibyG7x3A4IW9GxKAMkeYKrwNa6KCw1U59dIxB9VAYLCMMj8u6KzsGmIA&i=1&os=3&datatype=jsonp&etype=__EXPOSURE_TYPE__&ch=__CHANNEL_ID__&seq=__SEQ__&aseq=__ABS_SEQ__&xp=3&tl=1\",\"feedbackReportUrl\":\"https://nc.gdt.qq.com/gdt_report.fcg?viewid=Jr!y0Q8kQKc9UhIl!TSpAwixMARzwElPPW4t4qBQ2BFcC49FcYNxj9HYvjXljVT4jPK63pcLLaYIr0RYA9vX7fSmVoVtO3ibyG7x3A4IW9GxKAMkeYKrwNa6KCw1U59dIxB9VAYLCMMj8u6KzsGmIA&acttype=__ACT_TYPE__&s=27\",\"downloadReportUrl\":\"https://t.gdt.qq.com/conv/src/61/conv?client=61&product_id=&cuser=src&click_id=__CLICK_ID__&actionid=__ACTION_ID__\",\"download\":0,\"advertiser_id\":18860586,\"product_id\":1959475910,\"product_type\":1000,\"industry_id\":5301,\"icon_url\":\"http://pgdt.gtimg.cn/gdt/0/DAAAGQKAA2AA2AAABbDlPFCGCrMo_3.png/0?ck=be984000a753846ccd3b7ffc0b821e4c\",\"local_ad\":{\"store_name\":\"\",\"pop_sheet\":\"\"},\"form_component_info\":{},\"dest_type\":0,\"rich_media_url\":\"\",\"rich_media_id\":\"\",\"unlike_btn\":\"1\",\"open_mini_program\":{\"user_name\":\"\",\"path\":\"\",\"token\":\"\",\"wx_appid\":\"\",\"ad_trace_data\":\"\"},\"avoidDialog\":0,\"brandIcon\":\"\",\"trace_id\":\"7ma5nusf7k3rs03\",\"jdt_frame\":0,\"video_time\":0,\"open_scheme\":\"\",\"labels\":[],\"short_title\":\"\", \"ping_data\":\"b21naWQ9JnFxPTAmd3hpZD0mcXFvcGVuaWQ9JmlwPTE3NS4wLjI1MS4yMTImaXB2Nj0mc29pZD1BRjAwRkJENDkxRDI2MDU3MTBBRjAxNkQ5NzAzJmRpc3BsYXlfaWQ9OTAwNDYmZmVhdHVyZV90eXBlPTEmdXNlcl9pbmZvPW9CM2puQ000Q1JhZ0hlT2NJemdKRnFBZDQ1czZPaEFYb0E9PSZnYW1lX2lkPTAmd3hfd2Vpc2hpPTAmZWNwbT1YMzh5cktGaWlHZFc2TVlBSGpCZWNOS2lPTmMycklMUCZjcGM9ZFVuOEVDVmFPYUJZNXB0VDk3Qnd4Y3NJVTVYVlJXVkcmaW5kdXN0cnlfaWQ9NTMwMSZhZHZlcnRpc2VyX2lkPTE4ODYwNTg2JnByb2R1Y3RfaWQ9MTk1OTQ3NTkxMCZwcm9kdWN0X3R5cGU9MTAwMCZzdGF0dXNfY29kZT0xMDEmZHNwX2lwPTkuMTQ2LjY5LjE2OSZvcmRlcl90eXBlPTImc3BsYXNoX29wdGltYWw9MCZnZW5kZXI9MCZhZ2U9MCZtb2JpbGVfdGFnPTAmYm9va19pZD0mdGFiX3Rlc3RfaWQ9\",\"click_data\": \"b21naWQ9JnFxPTAmd3hpZD0mcXFvcGVuaWQ9JmlwPTE3NS4wLjI1MS4yMTImaXB2Nj0mc29pZD1BRjAwRkJENDkxRDI2MDU3MTBBRjAxNkQ5NzAzJmRpc3BsYXlfaWQ9OTAwNDYmZmVhdHVyZV90eXBlPTEmdXNlcl9pbmZvPW9CM2puQ000Q1JhZ0hlT2NJemdKRnFBZDQ1czZPaEFYb0E9PSZnYW1lX2lkPTAmd3hfd2Vpc2hpPTAmZWNwbT1WL3p1TjJrU1YzVGw1R1NOaVFCWnhUS1Z6Ly9GM0pEYSZjcGM9WEdVeHpuVGJaREFkRk5uUHhXTnB3VVd2OHlPZGxmQnQmaW5kdXN0cnlfaWQ9NTMwMSZhZHZlcnRpc2VyX2lkPTE4ODYwNTg2JnByb2R1Y3RfaWQ9MTk1OTQ3NTkxMCZwcm9kdWN0X3R5cGU9MTAwMCZzdGF0dXNfY29kZT0xMDEmZHNwX2lwPTkuMTQ2LjY5LjE2OSZvcmRlcl90eXBlPTImc3BsYXNoX29wdGltYWw9MCZnZW5kZXI9MCZhZ2U9MCZtb2JpbGVfdGFnPTAmYm9va19pZD0mdGFiX3Rlc3RfaWQ9\",\"extraReportUrl\":\"http://rpt.gdt.qq.com/landing_page?click_id=__CLICK_ID__&trace_id=7ma5nusf7k3rs03&pa=__PAGE_ACTION_ID__&pat=__PAGE_TIME__&lec=__LANDING_ERROR_CODE__&ot=__OS_TYPE__&ver=__VERSION__\",\"ams_traceid\":\"7ma5nusf7k3rs\",\"ad_context\":\"CLcHMI7SvoEMOPCeiMCRiPK0vAFQAGAAcg0IARDeu8TP792938cBcgQIAhAA\",\"complaintUrl\":\"https://i.gtimg.cn/qzone/biz/gdt/ams_ad_audit/h5/feedback?rsApiUrl=https%3A%2F%2Fnc.gdt.qq.com%2Fgdt_report.fcg%3Fviewid%3D7pKSSzl7kUb0W6!1GZnJ2kve15ljgwePDxmpxXJlD6btp3ieEeWyO2wL7x6o1bz79wmMs9ihZsazjbxVXq4ozw%26acttype%3D5009%26s%3D27\",\"splash_optimal\":0,\"marketing_scene\":0},{\"sub_type\":11,\"act_type\":2,\"oid\":\"3123822998\",\"cid\":\"3123823000\",\"soid\":\"AF00FBD491D2605710AF016D9704\",\"loc\":\"PC_News_Stream\",\"stream_type\":1,\"navTitle\":\"\",\"shareTitle\":\"中国联通3月21日免费发放1万张绝版卡，月租永久19，年360G\",\"resource_url0\":\"http://pgdt.gtimg.cn/gdt/0/EABHoapAKAAFoAAAKFkBgSBITCqcPl0Pl.jpg/0?ck=39d0f8a2b4b4c08434bb6cb6794a76e6\",\"title\":\"中国联通3月21日免费发放1万张绝版卡，月租永久19，年360G\",\"abstract\":\"\",\"thumbnails\":\"\",\"url\":\"https://c.gdt.qq.com/gdt_click.fcg?viewid=FbU0AV7!xnE9UhIl!TSpA0u7EOqw4KbPXcQVoxHtq4ztJ50q296s72NtGATgRg6T4xzMW2L!_RLMm6Qk1dJh!Ob7sk7boGW9fcbRd9z92W5k_VntMQI3l4t0vvwhN7tOS0G0iIZXuWJfwhpG8yB5sQ&jtype=0&i=1&os=3&s_lp=101&acttype=__ACT_TYPE__&ch=__CHANNEL_ID__&seq=__SEQ__&aseq=__ABS_SEQ__&rt=__RETURN_TYPE__&s=%7B%22da%22%3A%22__WIDTH__%22%2C%22db%22%3A%22__HEIGHT__%22%2C%22down_x%22%3A%22__DOWN_X__%22%2C%22down_y%22%3A%22__DOWN_Y__%22%2C%22up_x%22%3A%22__UP_X__%22%2C%22up_y%22%3A%22__UP_Y__%22%7D&clklpp=__CLICK_LPP__&xp=3&vto=__VIDEO_PLAY_TIME__&tl=1\",\"url_type\":0,\"icon\":\"\",\"pvLimit\":0,\"pvFcs\":0,\"reportUrlSdk\":[],\"reportUrlOther\":[],\"clickReportUrlOther\":[],\"webp\":0,\"imagew\":640,\"imageh\":360,\"order_prio\":0,\"comment\":0,\"newsId\":\"\",\"creative_plugin\":0,\"shareable\":1,\"viewReportUrl\":\"https://v.gdt.qq.com/gdt_stats.fcg?viewid=FbU0AV7!xnE9UhIl!TSpA0u7EOqw4KbPXcQVoxHtq4ztJ50q296s72NtGATgRg6T4xzMW2L!_RLMm6Qk1dJh!Ob7sk7boGW9fcbRd9z92W5k_VntMQI3l4t0vvwhN7tOS0G0iIZXuWJfwhpG8yB5sQ&i=1&os=3&datatype=jsonp&etype=__EXPOSURE_TYPE__&ch=__CHANNEL_ID__&seq=__SEQ__&aseq=__ABS_SEQ__&xp=3&tl=1\",\"feedbackReportUrl\":\"https://nc.gdt.qq.com/gdt_report.fcg?viewid=FbU0AV7!xnE9UhIl!TSpA0u7EOqw4KbPXcQVoxHtq4ztJ50q296s72NtGATgRg6T4xzMW2L!_RLMm6Qk1dJh!Ob7sk7boGW9fcbRd9z92W5k_VntMQI3l4t0vvwhN7tOS0G0iIZXuWJfwhpG8yB5sQ&acttype=__ACT_TYPE__&s=27\",\"downloadReportUrl\":\"https://t.gdt.qq.com/conv/src/61/conv?client=61&product_id=&cuser=src&click_id=__CLICK_ID__&actionid=__ACTION_ID__\",\"download\":0,\"advertiser_id\":18777769,\"product_id\":4188401894,\"product_type\":1000,\"industry_id\":3601,\"icon_url\":\"http://pgdt.gtimg.cn/gdt/0/DAAAGQKAA2AA2AAABbDlPFCGCrMo_3.png/0?ck=be984000a753846ccd3b7ffc0b821e4c\",\"local_ad\":{\"store_name\":\"\",\"pop_sheet\":\"\"},\"form_component_info\":{},\"dest_type\":0,\"rich_media_url\":\"\",\"rich_media_id\":\"\",\"unlike_btn\":\"1\",\"open_mini_program\":{\"user_name\":\"\",\"path\":\"\",\"token\":\"\",\"wx_appid\":\"\",\"ad_trace_data\":\"\"},\"avoidDialog\":0,\"brandIcon\":\"\",\"trace_id\":\"7ma5nusf7k3rs04\",\"jdt_frame\":0,\"video_time\":0,\"open_scheme\":\"\",\"labels\":[],\"short_title\":\"\", \"ping_data\":\"b21naWQ9JnFxPTAmd3hpZD0mcXFvcGVuaWQ9JmlwPTE3NS4wLjI1MS4yMTImaXB2Nj0mc29pZD1BRjAwRkJENDkxRDI2MDU3MTBBRjAxNkQ5NzA0JmRpc3BsYXlfaWQ9OTAwNDYmZmVhdHVyZV90eXBlPTEmdXNlcl9pbmZvPW9CM2puQ000Q1JhZ0hlT2NJemdKRnFBZDQ1czZPaEFYb0E9PSZnYW1lX2lkPTAmd3hfd2Vpc2hpPTAmZWNwbT0wVmwyMEFXYmsyLzFjMTV2dEx6SE9uTjcwN2d1YkswWCZjcGM9bXBCY1FSRHpKbi9WZmZnVE1CZER0YkluMkZycTg2VXUmaW5kdXN0cnlfaWQ9MzYwMSZhZHZlcnRpc2VyX2lkPTE4Nzc3NzY5JnByb2R1Y3RfaWQ9NDE4ODQwMTg5NCZwcm9kdWN0X3R5cGU9MTAwMCZzdGF0dXNfY29kZT0xMDEmZHNwX2lwPTkuMTQ2LjY5LjE2OSZvcmRlcl90eXBlPTImc3BsYXNoX29wdGltYWw9MCZnZW5kZXI9MCZhZ2U9MCZtb2JpbGVfdGFnPTAmYm9va19pZD0mdGFiX3Rlc3RfaWQ9\",\"click_data\": \"b21naWQ9JnFxPTAmd3hpZD0mcXFvcGVuaWQ9JmlwPTE3NS4wLjI1MS4yMTImaXB2Nj0mc29pZD1BRjAwRkJENDkxRDI2MDU3MTBBRjAxNkQ5NzA0JmRpc3BsYXlfaWQ9OTAwNDYmZmVhdHVyZV90eXBlPTEmdXNlcl9pbmZvPW9CM2puQ000Q1JhZ0hlT2NJemdKRnFBZDQ1czZPaEFYb0E9PSZnYW1lX2lkPTAmd3hfd2Vpc2hpPTAmZWNwbT1uRnlYYjNUaFB1UGozWnEwNEI4OUt2N2c5OXMwbmpJbiZjcGM9MmYxYlpWb1BOR1hzaUQ0TE9zNnVmL1kxSkd0a0ZtREImaW5kdXN0cnlfaWQ9MzYwMSZhZHZlcnRpc2VyX2lkPTE4Nzc3NzY5JnByb2R1Y3RfaWQ9NDE4ODQwMTg5NCZwcm9kdWN0X3R5cGU9MTAwMCZzdGF0dXNfY29kZT0xMDEmZHNwX2lwPTkuMTQ2LjY5LjE2OSZvcmRlcl90eXBlPTImc3BsYXNoX29wdGltYWw9MCZnZW5kZXI9MCZhZ2U9MCZtb2JpbGVfdGFnPTAmYm9va19pZD0mdGFiX3Rlc3RfaWQ9\",\"extraReportUrl\":\"http://rpt.gdt.qq.com/landing_page?click_id=__CLICK_ID__&trace_id=7ma5nusf7k3rs04&pa=__PAGE_ACTION_ID__&pat=__PAGE_TIME__&lec=__LANDING_ERROR_CODE__&ot=__OS_TYPE__&ver=__VERSION__\",\"ams_traceid\":\"7ma5nusf7k3rs\",\"ad_context\":\"CLcHMIzNyr8FOLC+p8WCyM/eMlAAYAA=\",\"complaintUrl\":\"https://i.gtimg.cn/qzone/biz/gdt/ams_ad_audit/h5/feedback?rsApiUrl=https%3A%2F%2Fnc.gdt.qq.com%2Fgdt_report.fcg%3Fviewid%3D7pKSSzl7kUb0W6!1GZnJ2rJ8zJEck6oBiHrgCL9zw4btp3ieEeWyOz59ecx2u8sO9suqGsj400vHLSlzD2SCbw%26acttype%3D5009%26s%3D27\",\"splash_optimal\":0,\"marketing_scene\":0}]}"}
        System.out.println(post);
    }

    /**
     * String post(String urlString, String body)
     * String post(String urlString, Map<String, Object> paramMap, int timeout)
     * timeout：超时时间，单位为毫秒，包括连接时间与读取时间
     * 文件上传与常规的 post 请求完全一样，file 项的 key 的名称与后台一致即可，没有特殊要求，只需要将值指定为文件，然后 FileUtil 会自动转换为文件类型
     * 请求体中除了文件，仍然可以设置其它参数。
     */
    @Test
    public void testPost2() {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("file", FileUtil.file("C:\\Users\\A\\Desktop\\1616310218748.gif"));
        paramMap.put("id", "o_1f1a3pn9o14circr163cn9u8u6b");

        String url = "https://gifcompressor.com/upload/ozpcu62hvtlm7wer";
        String result = HttpUtil.post(url, paramMap, 5 * 60 * 1000);
        //{"data":{"file":"1616310218748.gif","file_size_human":"3.2K"},"id":"o_1f1a3pn9o14circr163cn9u8u6b","jsonrpc":"2.0","result":null}
        System.out.println(result);
    }

    /**
     * String toParams(Map<String, ?> paramMap): 将 Map 形式的 Form 表单数据转换为 Url 查询参数形式，会自动 url 编码键和值
     * Map<String, String> decodeParamMap(String paramsStr, Charset charset)
     * 1、将 URL 查询参数解析为 Map（也可以解析Post中的键值对参数）
     * 2、paramsStr：可以是 url 查询参数字符串，也可以带参数的 Path。
     */
    @Test
    public void testToParams() {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("sub_srv_id", "24hours");
        paramMap.put("srv_id", "pc");
        paramMap.put("offset", 0);
        paramMap.put("limit", 20);
        paramMap.put("strategy", 1);
        paramMap.put("ext", "{\"pool\":[\"top\"],\"is_filter\":7,\"check_type\":true}");

        String params = HttpUtil.toParams(paramMap);
        //params=sub_srv_id=24hours&ext=%7B%22pool%22%3A%5B%22top%22%5D%2C%22is_filter%22%3A7%2C%22check_type%22%3Atrue%7D&offset=0&limit=20&srv_id=pc&strategy=1
        System.out.println("params=" + params);

        Map<String, String> decodeParamMap = HttpUtil.decodeParamMap(params, CharsetUtil.CHARSET_UTF_8);
        //decodeParamMap={sub_srv_id=24hours, ext={"pool":["top"],"is_filter":7,"check_type":true}, offset=0, limit=20, srv_id=pc, strategy=1}
        System.out.println("decodeParamMap=" + decodeParamMap);

        //ext={"pool":["top"],"is_filter":7,"check_type":true}
        System.out.println("ext=" + decodeParamMap.get("ext"));
    }

    /**
     * encodeParams(String urlWithParams, Charset charset)：对 URL 参数做编码，只编码键和值
     * urlWithParams：可以是单独的查询参数，也可以直接整个 url 地址
     * 返回编码后的 url 或者参数
     */
    @Test
    public void testEncodeParams() {
        String url = "https://www.baidu.com/s?wd=新化县&wmx=lo ve";
        String encodeUrl = HttpUtil.encodeParams(url, CharsetUtil.CHARSET_UTF_8);
        //https://www.baidu.com/s?wd=%E6%96%B0%E5%8C%96%E5%8E%BF&wmx=lo+ve
        System.out.println(encodeUrl);

        String result = HttpUtil.get(encodeUrl);
        System.out.println(result);
    }

}
