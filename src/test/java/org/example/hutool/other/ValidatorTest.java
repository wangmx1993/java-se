package org.example.hutool.other;

import cn.hutool.core.lang.*;
import cn.hutool.core.util.CreditCodeUtil;
import cn.hutool.core.util.PhoneUtil;
import cn.hutool.core.util.ReUtil;
import org.example.hutool.utilTest.PhoneUtilTest;
import org.example.uitls.PatternTest;
import org.junit.Test;

import java.util.HashMap;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * 字段验证器-Validator. 验证给定字符串是否满足指定条件，一般用在表单字段验证里。
 * 1、常用正则： {@link Validator}
 * 2、正则相关工具类 {@link ReUtil}，src/main/java/org/example/hutool/ReUtilController.java
 * 3、常用正则表达式集合：{@link PatternPool}、常用正则表达式字符串池：{@link RegexPool}
 * 更多正则见: https://any86.github.io/any-rule/
 * 4、原生PAI {@link PatternTest}
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/3 13:33
 */
public class ValidatorTest {

    /**
     * boolean isBirthday(CharSequence value):验证是否为生日，支持：yyyyMMdd、yyyy-MM-dd、yyyy/MM/dd、yyyy.MM.dd、yyyy年MM月dd日
     * boolean isBirthday(int year, int month, int day): 验证是否为生日, year：年，从1900年开始计算； month：月，从1开始计数；day：日，从1开始计数
     * boolean isCitizenId(CharSequence value): 验证是否为身份证号码（支持18位、15位和港澳台的10位）
     * boolean isCreditCode(CharSequence creditCode): 是否是有效的统一社会信用代码，规则如下：
     * * <pre>
     * * 第一部分：登记管理部门代码1位 (数字或大写英文字母)
     * * 第二部分：机构类别代码1位 (数字或大写英文字母)
     * * 第三部分：登记管理机关行政区划码6位 (数字)
     * * 第四部分：主体标识码（组织机构代码）9位 (数字或大写英文字母)
     * * 第五部分：校验码1位 (数字或大写英文字母)
     * </pre>
     * boolean isHex(CharSequence value): 验证是否为Hex（16进制）字符串
     * boolean isIpv4(CharSequence value):验证是否为IPV4地址
     * boolean isIpv6(CharSequence value):验证是否为IPV6地址
     * boolean isMac(CharSequence value)：验证是否为MAC地址
     * boolean isZipCode(CharSequence value):验证是否为邮政编码（中国）
     * boolean isUUID(CharSequence value):验证是否为 UUID,包括带横线标准格式和不带横线的简单模式
     * boolean isUrl(CharSequence value): 验证是否为 URL
     * boolean isPlateNumber(CharSequence value):验证是否为中国车牌号，中间不支持"-" 或者 "·"
     * boolean isMoney(CharSequence value)： 验证是否为货币，支持小数点，但是不支持负数
     */
    @Test
    public void testValidator1() {
        boolean birthday = Validator.isBirthday("1993-08-25");
        boolean birthday1 = Validator.isBirthday(1993, 2, 29);
        boolean citizenId = Validator.isCitizenId("432524199308258319");
        boolean hex = Validator.isHex("76676767abc");
        boolean ipv4 = Validator.isIpv4("10.104.65.195");
        boolean ipv6 = Validator.isIpv6("fe80::f03b:15:e258:c879%3");
        boolean creditCode = Validator.isCreditCode(CreditCodeUtil.randomCreditCode());
        boolean mac = Validator.isMac("48-8A-D2-8A-03-D2");
        boolean zipCode = Validator.isZipCode("417560");
        boolean uuid = Validator.isUUID(UUID.randomUUID().toString());
        boolean url = Validator.isUrl("www.baidu.com");
        boolean money = Validator.isMoney("7483748.78");
        boolean mobile = Validator.isMobile("18124578125");

        Console.log(hex, Validator.isHex("8877878FP"));//true false
        Console.log(ipv4, Validator.isIpv4("10.104.65"));//true false
        Console.log(ipv6);//true
        Console.log("creditCode={}", creditCode);//creditCode=true
        Console.log("{},{}", birthday, birthday1);//true,false
        Console.log(citizenId);//false
        Console.log(mac, Validator.isMac("55-8a-d2-8a-03-d2"));//true true
        Console.log(zipCode, Validator.isZipCode("41756"));//true false
        Console.log(uuid);//true
        Console.log(url, Validator.isUrl("https://www.baidu.com"));//false tru
        Console.log("money={},{}", money, Validator.isMoney("9090 RMB"));//money=true,false
        Console.log("isMobile={},{}", mobile, Validator.isMobile("110"));//isMobile=true,false

        System.out.println("-----车牌号-----");
        // 验证是否为中国车牌号，中间不支持"-" 或者 "·"
        System.out.println(Validator.isPlateNumber("湘A8997T"));//true
        System.out.println(Validator.isPlateNumber("湘A·8997T"));//false
        System.out.println(Validator.isPlateNumber("湘A-8997T"));//false
        System.out.println(Validator.isPlateNumber("湘A0006警"));//true

        /**
         * 推荐使用 手机号工具类-{@link PhoneUtil}，支持 座机号码+手机号码
         * {@link PhoneUtilTest}
         */
        System.out.println("-------手机号号码-----");
        System.out.println(Validator.isMobile("18673886425"));
        System.out.println(Validator.isMobile("868673886425"));
    }

    /**
     * boolean isBetween(Number value, Number min, Number max): 检查给定的数字(value)是否在指定范围[min,max]内
     * boolean isGeneralWithChinese(CharSequence value): 验证是否为中文字、英文字母、数字和下划线
     * boolean isLetter(CharSequence value): 判断字符串是否全部为字母组成，包括大写和小写字母和汉字
     * boolean isLowerCase(CharSequence value)：判断字符串是否全部为小写字母.空格都不行。
     * boolean isUpperCase(CharSequence value): 判断字符串是否全部为大写字母
     * boolean isWord(CharSequence value): 验证该字符串是否是字母（包括大写和小写字母）
     * boolean isGeneral(CharSequence value)：验证是否为英文字母 、数字和下划线
     * boolean isGeneral(CharSequence value, int min)：验证是否为给定最小长度(min)的英文字母 、数字和下划线
     * * min: 最小长度，负数自动识别为0
     * isGeneral(CharSequence value, int min, int max)：验证是否为给定长度范围[min,max]的英文字母 、数字和下划线
     * boolean isNumber(CharSequence value): 验证该字符串是否是数字,包括小数点和负数
     */
    @Test
    public void testValidator2() {
        boolean between = Validator.isBetween(20, 0, 100);
        boolean generalWithChinese = Validator.isGeneralWithChinese("中国China_8888");
        boolean letter = Validator.isLetter("Hadoop中国");
        boolean hello = Validator.isLowerCase("how are you");
        boolean upperCase = Validator.isUpperCase("WORLD");
        boolean word = Validator.isWord("Hello,are you ok");
        boolean general = Validator.isGeneral("323SDfd_98");
        boolean fd2 = Validator.isGeneral("fd2", 2);
        boolean general1 = Validator.isGeneral("45hhh", 5, 8);
        boolean number = Validator.isNumber("-378.34");

        Console.log(between, Validator.isBetween(111, 0, 100));//true false
        Console.log(generalWithChinese, Validator.isGeneralWithChinese("H&M"));//true false
        Console.log(letter, Validator.isLetter("Spring4.1"));//true false
        Console.log(hello, Validator.isLowerCase("hi"));//false true
        Console.log(upperCase, Validator.isUpperCase("OK!"));//true false
        Console.log(word, Validator.isWord("Love"));//false true
        Console.log(general, Validator.isGeneral("3.14"));//true false
        Console.log(fd2, Validator.isGeneral("tt", 5));//true false
        Console.log(general1);//true
        Console.log("isNumber={},{}", number, Validator.isNumber("100 RMB"));//true false
    }

    /**
     * boolean isEmpty(Object value): 验证是否为空/null，对于String类型判定是否为empty(null 或 "")
     * boolean isNotEmpty(Object value): 验证是否为非空,对于String类型判定是否为empty(null 或 "")
     * boolean isFalse(boolean value):给定值是否为{@code false}
     * boolean isTrue(boolean value):给定值是否为{@code true}
     * boolean isNull(Object value):给定值是否为{@code null}
     * boolean isNotNull(Object value): 给定值是否不为{@code null}
     */
    @Test
    public void testValidator3() {
        boolean empty = Validator.isEmpty(" ");
        boolean notEmpty = Validator.isNotEmpty(" ");
        boolean aFalse = Validator.isFalse(1 == 2);
        boolean aTrue = Validator.isTrue(1 == 2);
        boolean aNull = Validator.isNull(null);
        boolean notNull = Validator.isNotNull(null);

        Console.log(empty, Validator.isEmpty(new String[]{}));//false false
        Console.log(notEmpty, Validator.isNotEmpty(new String[]{}));//true true
        Console.log(aFalse, Validator.isFalse(3 > 1));//true false
        Console.log(aTrue, Validator.isTrue(3 > 1));//false true
        Console.log(aNull, Validator.isNull(new HashMap<>()));//true false
        Console.log(notNull, Validator.isNotNull(new HashMap<>()));//false true
    }

    /**
     * boolean isEmail(CharSequence value): 验证是否为可用邮箱地址
     */
    @Test
    public void isEmailTest() {
        // true
        System.out.println(Validator.isEmail("2268461756@qq.com"));
        // true
        System.out.println(Validator.isEmail("168756421@163.com"));
        // true
        System.out.println(Validator.isEmail("168756421@163.cn"));
        // true
        System.out.println(Validator.isEmail("wanaoxiong2@ctjsoft.com"));
    }

    /**
     * 1、上面所有的 isXxx 方法都有对应的 validateXxx 方法，而且 validateXxx 还更多一些
     * 2、validateXxx 当条件不满足时，直接会抛异常，类似断言
     * validateNotEmptyAndEqual(Object t1, Object t2, String errorMsg)：验证是否非空且与指定值相等，否则抛异常
     */
    @Test
    public void testValidate() {
        Validator.validateNotEmptyAndEqual(100, 200, "参数值不相等");
        Assert.isFalse(false);
    }

    /**
     * boolean isChinese(CharSequence value): 验证是否都为汉字,标点符号不是汉字。
     */
    @Test
    public void testIsChinese() {
        // true
        Console.log(Validator.isChinese("伟大复兴"));
        // false
        Console.log(Validator.isChinese("哇哦！"));

        // 只取其中的汉字内容
        String str = "职务-职称(省本级).";
        StringBuilder sBuilder = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            if (Validator.isChinese(String.valueOf(str.charAt(i)))) {
                sBuilder.append(str.charAt(i));
            }
        }
        // 职务职称省本级
        System.out.println(sBuilder);
    }

    /**
     * boolean hasChinese(CharSequence value): 验证是否包含汉字
     */
    @Test
    public void testHasChinese() {
        //false
        System.out.println(Validator.hasChinese("java.lang.IllegalArgumentException: "));
        //true
        System.out.println(Validator.hasChinese("java.lang.IllegalArgumentException: 大类编码不能为空。"));
    }

    /**
     * 如果自带的方法无法满足需要，则可以自己定义正则
     * boolean isMatchRegex(String regex, CharSequence value):通过正则表达式验证
     * boolean isMatchRegex(Pattern pattern, CharSequence value):通过正则表达式验证
     */
    @Test
    public void testIsMatchRegex() {
        // 以字母或下划线开头，只能包含字母、数字、下划线，长度为6~18
        String regex = "^[a-zA-Z_]\\w{5,17}$";
        // false
        System.out.println(Validator.isMatchRegex(regex, "102wmx999p"));
        // true
        System.out.println(Validator.isMatchRegex(regex, "_999wm8888"));

        System.out.println("-----内容只能是大小写字母，数字、下划线以及中划线---------");
        // String regex2 = "^[A-Za-z0-9_]+$|^[A-Za-z0-9-]+$";
        String regex2 = "^[A-Za-z0-9-_]+$";
        // true
        System.out.println(Validator.isMatchRegex(regex2, "102wmx999_p"));
        // true
        System.out.println(Validator.isMatchRegex(regex2, "102-wmx999p"));
        // false
        System.out.println(Validator.isMatchRegex(regex2, "’102wmx99_9p"));
        // false
        System.out.println(Validator.isMatchRegex(regex2, "102wmx99//9p"));
        // false
        System.out.println(Validator.isMatchRegex(regex2, "102wmx99和9p"));
    }

    /**
     * 如果自带的方法无法满足需要，则可以自己定义正则
     * boolean isMatchRegex(String regex, CharSequence value):通过正则表达式验证
     * boolean isMatchRegex(Pattern pattern, CharSequence value):通过正则表达式验证
     */
    @Test
    public void testIsMatchRegex2() {
        // 以字母或下划线开头，只能包含字母、数字、下划线，长度为6~18
        String regex = "^[a-zA-Z_]\\w{5,17}$";
        Pattern pattern = Pattern.compile(regex);

        // false
        System.out.println(Validator.isMatchRegex(pattern, "huTo"));
        // true
        System.out.println(Validator.isMatchRegex(pattern, "huTo99999"));
    }

    //========================cn.hutool.core.util.ReUtil==========./start======================================
    // 参考 src/main/java/org/example/hutool/ReUtilController.java

    /**
     * int count(Pattern pattern, CharSequence content): 计算指定字符串中，匹配 pattern 的个数
     */
    @Test
    public void reUtilCountTest() {
        String exception = "java.lang.IllegalArgumentException: 大类编码不能为空，且长度不能超过50个字符。";
        // 计算汉字的个数
        System.out.println(ReUtil.count(PatternPool.CHINESE, exception));//18
        // 计算汉字连续出现的次数：'大类编码不能为空' 算一次，'且长度不能超过' 算一次，'个字符'算一次
        System.out.println(ReUtil.count(PatternPool.CHINESES, exception));//3
    }

    /**
     * Integer getFirstNumber(CharSequence StringWithNumber)
     * 从字符串中获得第一个整数
     */
    @Test
    public void reUtilGetFirstNumberTest() {
        // 4783
        System.out.println(ReUtil.getFirstNumber("防火等级回房间4783翻江倒海fjhd"));
        // 2
        System.out.println(ReUtil.getFirstNumber("防火fd等2级回房间4783翻江倒海fjhd"));
        // 88
        System.out.println(ReUtil.getFirstNumber("防火fd等级回房间 翻江倒海fjhd dhj88i8"));
        // null
        System.out.println(ReUtil.getFirstNumber("防火fd等级回房间 翻江倒海fjhd"));
    }

    // 参考 src/main/java/org/example/hutool/ReUtilController.java
    //========================cn.hutool.core.util.ReUtil==========./end======================================

}
