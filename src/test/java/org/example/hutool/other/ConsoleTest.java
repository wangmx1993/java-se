package org.example.hutool.other;

import cn.hutool.core.lang.Console;
import cn.hutool.core.lang.ConsoleTable;
import org.junit.Test;

import java.util.Date;

/**
 * 控制台打印封装-Console
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/3 10:12
 */
public class ConsoleTest {

    /**
     * log() : 同 System.out.println()方法，打印控制台日志
     * log(Object obj) ：如果传入打印对象为{@link Throwable}对象，那么同时打印堆栈
     * log(Object obj1, Object... otherObjs) ：obj1-第一个要打印的对象，otherObjs-其它要打印的对象
     * log(String template, Object... values)：当传入template无"{}"时，被认为非模板，直接打印多个参数以空格分隔
     * * template-文本模板，被替换的部分用 {} 表示；
     * * values-值
     */
    @Test
    public void testLog() {
        String[] strings = {"中国", "美国", "法国"};
        String name = "张三";
        Integer age = 33;
        Date time = new Date();
        RuntimeException runtimeException = new RuntimeException("错误错误！");

        Console.log(strings, name, age);//[中国, 美国, 法国] 张三 33
        //姓名：张三，年龄：33，入职日期：Sat Apr 03 10:18:19 CST 2021
        Console.log("姓名：{}，年龄：{}，入职日期：{}", name, age, time);
        Console.log(runtimeException);
    }

    /**
     * 同 System.out.println()方法，打印控制台日志
     */
    @Test
    public void testError() {
        String[] strings = {"中国", "美国", "法国"};
        String name = "张三";
        Integer age = 33;
        Date time = new Date();
        RuntimeException runtimeException = new RuntimeException("错误错误！");

        Console.error(strings, name, age);//[中国, 美国, 法国] 张三 33
        //姓名：张三，年龄：33，入职日期：Sat Apr 03 10:18:19 CST 2021
        Console.error("姓名：{}，年龄：{}，入职日期：{}", name, age, time);
        Console.error(runtimeException);
    }

    /**
     * ConsoleTable ： 控制台打印表格工具
     * ConsoleTable create()：创建ConsoleTable对象
     * ConsoleTable addHeader(String... titles): 添加头信息，元素不能为 null
     * ConsoleTable addBody(String... values): 添加体信息，元素不能为 null
     * print()：打印表格到控制台
     * table(ConsoleTable consoleTable)：打印表格到控制台
     */
    @Test
    public void testConsoleTable() {
        ConsoleTable consoleTable = ConsoleTable.create();
        consoleTable.addHeader("序号", "姓名", "年龄", "居住地");
        consoleTable.addBody("1", "张三", "24", "长沙");
        consoleTable.addBody("2", "李斯", "", "咸阳");
        consoleTable.addBody("3", "赵光义", "34", "开封");

        consoleTable.print();

        System.out.println();
        System.out.println();
        System.out.println();

        Console.table(consoleTable);
    }


}
