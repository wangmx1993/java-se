package org.example.hutool.other;

import cn.hutool.core.codec.Base32;
import cn.hutool.core.codec.Base62;
import cn.hutool.core.codec.Base64;
import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.InputStream;

/**
 * Base32就是用32（2的5次方）个特定ASCII码来表示256个ASCII码。所以，5个ASCII字符经过base32编码后会变为8个字符（公约数为40），长度增加3/5.不足8n用“=”补足。
 * Base62编码是由10个数字、26个大写英文字母和26个小写英文字母组成，多用于安全领域和短URL生成。
 * Base64编码是用64（2的6次方）个ASCII字符来表示256（2的8次方）个ASCII字符，也就是三位二进制数组经过编码后变为四位的ASCII字符显示，长度比原来增加1/3。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/3 9:39
 */
public class BaseCode {

    /**
     * base32编码
     * String encode(final byte[] bytes)
     * String encode(String source)，默认 UTF-8 编码
     * String encode(String source, Charset charset)
     * String encode(String source, String charset)
     * <p>
     * base32解码：
     * byte[] decode(final String base32)
     * String decodeStr(String source)，默认 UTF-8 解码
     * String decodeStr(String source, Charset charset)
     * String decodeStr(String source, String charset)
     */
    @Test
    public void base32Test1() {
        String text = "万里长城今犹在，不见当年秦始皇。";

        String base32Encode = Base32.encode(text);
        String base32decode = Base32.decodeStr(base32Encode);
        //base32Encode=4S4IP2MHRTUZLP7FT6HOJO4K46FLTZM4VDX3ZDHEXCG6RJ4B4W6ZHZNZWTT2PJXFU6F6PGUH4OAIE
        //base32decode=万里长城今犹在，不见当年秦始皇。
        System.out.println("base32Encode=" + base32Encode);
        System.out.println("base32decode=" + base32decode);
    }

    /**
     * Base62编码
     * String encode(byte[] source)
     * String encode(CharSequence source)，默认 UTF-8 编码
     * String encode(CharSequence source, Charset charset)
     * String encode(File file)，对文件进行Base62编码，默认 UTF-8 解码
     * String encode(InputStream in)，对文件流进行Base62编码（一般为图片流或者文件流），默认 UTF-8 解码
     * <p>
     * 解码Base62
     * byte[] decode(byte[] base62bytes)
     * byte[] decode(CharSequence base62Str)，默认 UTF-8 解码
     * String decodeStr(CharSequence source)，默认 UTF-8 解码
     * String decodeStr(CharSequence source, Charset charset)
     * String decodeStrGbk(CharSequence source)
     * File decodeToFile(CharSequence Base62, File destFile)，解码并输出到目标文件中
     * void decodeToStream(CharSequence base62Str, OutputStream out, boolean isCloseOut)，解码并输出到目标流中。
     */
    @Test
    public void base62Test() {
        String text = "万里长城今犹在，不见当年秦始皇。";
        String base62Encode = Base62.encode(text);
        String base62decode = Base62.decodeStr(base62Encode);
        //base62Encode=6oZsea0jmzWPWXugCl6jI6ITFh2lbUP6vEuBUI9IFM8oIXPwyPstBhq1Ds8v9M4FG
        //base62decode=万里长城今犹在，不见当年秦始皇。
        System.out.println("base62Encode=" + base62Encode);
        System.out.println("base62decode=" + base62decode);
    }

    /**
     * base64编码
     * String encode(byte[] source)
     * byte[] encode(byte[] arr, boolean lineSep)，编码为Base64，非URL安全的
     * byte[] encode(byte[] arr, boolean isMultiLine, boolean isUrlSafe)编码为Base64，如果isMultiLine为true，则每76个字符一个换行符，否则在一行显示
     * String encode(CharSequence source) ，默认 UTF-8 编码
     * String encode(CharSequence source, Charset charset)
     * String encode(CharSequence source, String charset)
     * String encodeWithoutPadding(byte[] source) base64编码，不进行padding(末尾不会填充'=')
     * String encodeWithoutPadding(CharSequence source, String charset) base64编码，不进行padding(末尾不会填充'=')
     * <p>
     * 解码Base64
     * byte[] decode(byte[] in)
     * byte[] decode(CharSequence base64)，默认 UTF-8 解码
     * String decodeStr(CharSequence source)，默认 UTF-8 解码
     * String decodeStr(CharSequence source, Charset charset)
     * String decodeStr(CharSequence source, String charset)
     * String decodeStrGbk(CharSequence source)
     */
    @Test
    public void base64Test1() {
        String text = "万里长城今犹在，不见当年秦始皇。";

        String base64Encode = Base64.encode(text);
        String base64decode = Base64.decodeStr(base64Encode);
        //base64Encode=5LiH6YeM6ZW/5Z+O5LuK54q55Zyo77yM5LiN6KeB5b2T5bm056em5aeL55qH44CC
        //base64decode=万里长城今犹在，不见当年秦始皇。
        System.out.println("base64Encode=" + base64Encode);
        System.out.println("base64decode=" + base64decode);

        // 空白：
        // 空格：
        // null：null
        System.out.println("空白："+Base64.decodeStr(""));
        System.out.println("空格："+Base64.decodeStr("     "));
        System.out.println("null："+Base64.decodeStr(null));
    }

    /**
     * base64编码
     * String encode(File file) ，对文件进行Base64编码，默认 UTF-8 编码
     * String encode(InputStream in)，对文件流进行Base62编码（一般为图片流或者文件流），默认 UTF-8 编码
     * <p>
     * 解码Base64
     * File decodeToFile(CharSequence base64, File destFile)，解码并输出到目标文件中，默认 UTF-8 解码
     * void decodeToStream(CharSequence base64, OutputStream out, boolean isCloseOut)，默认 UTF-8 解码
     */
    @Test
    public void base64Test2() {
        String fileName = "data/config.properties";
        InputStream inputStream = BaseCode.class.getClassLoader().getResourceAsStream(fileName);
        String encode = Base64.encode(inputStream);
        System.out.println("base64编码\n" + encode);

        File dirFile = FileSystemView.getFileSystemView().getHomeDirectory();
        File targetFile = new File(dirFile, fileName);
        Base64.decodeToFile(encode, targetFile);
        System.out.println();
        System.out.println("解码内容输出到文件：" + targetFile.getAbsolutePath());
    }

    /**
     * base64编码
     * String encodeUrlSafe(byte[] source) base64编码,URL安全的
     * byte[] encodeUrlSafe(byte[] arr, boolean lineSep) 编码为Base64，URL安全的，lineSep 在76个char之后是CRLF还是EOF
     * String encodeUrlSafe(CharSequence source) base64编码，URL安全，默认 UTF-8 编码
     * String encodeUrlSafe(CharSequence source, Charset charset)
     * String encodeUrlSafe(CharSequence source, String charset)
     * String encodeUrlSafe(File file)
     * String encodeUrlSafe(InputStream in)
     */
    @Test
    public void base64Test3() {
        String url = "https://pan.baidu.com/disk/main?from=homeFlow#/index?category=all&path=%2FStudy_Data%2Fczbk%2FIT黑马_32_28期%2F黑马28期基础班%2B就业班%2F02_北京黑马28期就业班%2Bhadoop大数据%2F老师分享资料%2F数据库资料&r=1";

        String urlSafe = Base64.encodeUrlSafe(url);
        String encode = Base64.encode(url);

        // aHR0cHM6Ly9wYW4uYmFpZHUuY29tL2Rpc2svbWFpbj9mcm9tPWhvbWVGbG93Iy9pbmRleD9jYXRlZ29yeT1hbGwmcGF0aD0lMkZTdHVkeV9EYXRhJTJGY3piayUyRklU6buR6amsXzMyXzI45pyfJTJG6buR6amsMjjmnJ_ln7rnoYDnj60lMkLlsLHkuJrnj60lMkYwMl_ljJfkuqzpu5HpqawyOOacn-WwseS4muePrSUyQmhhZG9vcOWkp-aVsOaNriUyRuiAgeW4iOWIhuS6q-i1hOaWmSUyRuaVsOaNruW6k-i1hOaWmSZyPTE
        // aHR0cHM6Ly9wYW4uYmFpZHUuY29tL2Rpc2svbWFpbj9mcm9tPWhvbWVGbG93Iy9pbmRleD9jYXRlZ29yeT1hbGwmcGF0aD0lMkZTdHVkeV9EYXRhJTJGY3piayUyRklU6buR6amsXzMyXzI45pyfJTJG6buR6amsMjjmnJ/ln7rnoYDnj60lMkLlsLHkuJrnj60lMkYwMl/ljJfkuqzpu5HpqawyOOacn+WwseS4muePrSUyQmhhZG9vcOWkp+aVsOaNriUyRuiAgeW4iOWIhuS6q+i1hOaWmSUyRuaVsOaNruW6k+i1hOaWmSZyPTE=
        System.out.println(urlSafe);
        System.out.println(encode);

        // https://pan.baidu.com/disk/main?from=homeFlow#/index?category=all&path=%2FStudy_Data%2Fczbk%2FIT黑马_32_28期%2F黑马28期基础班%2B就业班%2F02_北京黑马28期就业班%2Bhadoop大数据%2F老师分享资料%2F数据库资料&r=1
        System.out.println(Base64.decodeStr(urlSafe));
        // https://pan.baidu.com/disk/main?from=homeFlow#/index?category=all&path=%2FStudy_Data%2Fczbk%2FIT黑马_32_28期%2F黑马28期基础班%2B就业班%2F02_北京黑马28期就业班%2Bhadoop大数据%2F老师分享资料%2F数据库资料&r=1
        System.out.println(Base64.decodeStr(encode));
    }


}
