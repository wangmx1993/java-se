package org.example.hutool.other;

import cn.hutool.db.sql.SqlFormatter;
import org.junit.jupiter.api.Test;

/**
 * 格式化SQL
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/12/31 下午5:15
 */
public class SqlFormatterTest {

    /**
     * 格式化SQL
     * 经过实测：SqlFormatter.format 只适合用于没有注释的纯SQL文本。如果有注释，则结果将无法保证正确。
     */
    @Test
    public void testSqlFormatter() {
        String sql = "SELECT T.Agency_Id, t.biz_key, t.agency_code, t.agency_name, t.mof_div_code, t.fiscal_year, t.version, t.is_leaf,t.level_no,t.parent_id,t.pid,t.audit_id, t.is_end, t.is_deleted, t.is_enabled, t.start_date, t.end_date, t.create_time, t.update_time, t.from_agency_id, rowid FROM bas_agency_info t where t.mof_div_code = '430000000' and t.agency_code = '001888' and t.version = '2024rcdtwh'";
        String format = SqlFormatter.format(sql);
        System.out.println(format);
        /*
        SELECT
        T.Agency_Id,
                t.biz_key,
                t.agency_code,
                t.agency_name,
                t.mof_div_code,
                t.fiscal_year,
                t.version,
                t.is_leaf,
                t.level_no,
                t.parent_id,
                t.pid,
                t.audit_id,
                t.is_end,
                t.is_deleted,
                t.is_enabled,
                t.start_date,
                t.end_date,
                t.create_time,
                t.update_time,
                t.from_agency_id,
                rowid
        FROM
        bas_agency_info t
        where
        t.mof_div_code = '430000000'
        and t.agency_code = '001888'
        and t.version = '2024rcdtwh'
        */
    }
}
