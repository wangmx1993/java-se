package org.example.hutool.clone;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Map;

/**
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/1/31 17:34
 */
public class Person4 implements Serializable {
    private String pid;
    private String name;
    private Integer age;
    private Float salary;
    private LocalDate birthday;
    private Boolean isMarry;
    private Map<String, Object> extMap;

    public Person4() {
    }

    public Person4(String pid, String name, Integer age, Float salary, LocalDate birthday, Boolean isMarry) {
        this.pid = pid;
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.birthday = birthday;
        this.isMarry = isMarry;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Boolean getMarry() {
        return isMarry;
    }

    public void setMarry(Boolean marry) {
        isMarry = marry;
    }

    public Map<String, Object> getExtMap() {
        return extMap;
    }

    public void setExtMap(Map<String, Object> extMap) {
        this.extMap = extMap;
    }


    @Override
    public String toString() {
        return "Person1{" +
                "pid='" + pid + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                ", birthday=" + birthday +
                ", isMarry=" + isMarry +
                ", extMap=" + extMap +
                '}';
    }
}
