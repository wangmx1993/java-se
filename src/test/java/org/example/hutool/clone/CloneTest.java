package org.example.hutool.clone;

import cn.hutool.core.clone.CloneSupport;
import cn.hutool.core.util.ObjectUtil;
import org.junit.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 * HuTool 对象克隆演示
 * 1、Apache BeanUtils 也是一个很好的方式，它对 java bean 没有任何要求，底层使用反射轻松实现对象的深克隆
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/1/31 17:37
 */
public class CloneTest {

    /**
     * 原生方式对象克隆。
     * 1、自定义 Java bean，必须实现{@link Cloneable} 接口
     * 2、重写 Object 的 {@link Object#clone()} 方法，调用 super.clone();
     * 3、后期 Java bean 调用 clone() 方法即可克隆对象
     * 4、此种方式属于浅克隆，即只对基本数据类型有效，对 list、Map、Set、java bean 等关联对象无效，即克隆出来的新对象仍然与源对象公用这些引用类型。
     *
     * @throws CloneNotSupportedException
     */
    @Test
    public void testClone1() throws CloneNotSupportedException {
        Person1 person = new Person1("1991", "华安", 22, 89890.8F, LocalDate.now(), false);
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("color", "#fff");
        person.setExtMap(hashMap);

        Person1 clone = (Person1) person.clone();
        clone.setAge(33);
        clone.getExtMap().put("left", "200px");
        //Person1{pid='1991', name='华安', age=22, salary=89890.8, birthday=2021-02-01, isMarry=false, extMap={color=#fff, left=200px}}
        System.out.println(person);
        //Person1{pid='1991', name='华安', age=33, salary=89890.8, birthday=2021-02-01, isMarry=false, extMap={color=#fff, left=200px}}
        System.out.println(clone);
    }

    /**
     * java bean 实现 {@link cn.hutool.core.clone.Cloneable} 接口克隆对象
     * 1、此接口继承{@link java.lang.Cloneable}，定义了一个返回泛型的成员方法，就是在原生方式的基础上稍微封装了一下
     * 2、实现此接口后必须实现 clone 方法，同样调用父类 clone 方法即可
     * 3、此种方式属于浅克隆，即只对基本数据类型有效，对 list、Map、Set、java bean 等关联对象无效，即克隆出来的新对象仍然与源对象公用这些引用类型。
     */
    @Test
    public void testClone2() {
        Person2 person = new Person2("1993", "华安", 22, 89890.8F, LocalDate.now(), false);
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("color", "#fff");
        person.setExtMap(hashMap);

        Person2 clone = person.clone();

        clone.setAge(33);
        clone.setMarry(true);
        clone.getExtMap().put("left", "200px");

        //Person1{pid='1993', name='华安', age=22, salary=89890.8, birthday=2021-02-01, isMarry=false, extMap={color=#fff, left=200px}}
        System.out.println(person);
        //Person1{pid='1993', name='华安', age=33, salary=89890.8, birthday=2021-02-01, isMarry=true, extMap={color=#fff, left=200px}}
        System.out.println(clone);
    }

    /**
     * java bean 继承{@link CloneSupport} 类克隆对象
     * 1、{@link cn.hutool.core.clone.Cloneable} 接口 封装了原生的 {@link java.lang.Cloneable}
     * 2、{@link CloneSupport} 类继续封装了，{@link cn.hutool.core.clone.Cloneable} 接口
     * 3、只要继承此类，不用写任何代码即可使用 clone()方法
     * 4、此种方式属于浅克隆，即只对基本数据类型属性有效，对 list、Map、Set、java bean 等关联对象无效，即克隆出来的新对象仍然与源对象公用这些引用类型。
     */
    @Test
    public void testClone3() {
        Person3 person = new Person3("1993", "华安", 22, 89890.8F, LocalDate.now(), false);
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("color", "#fff");
        person.setExtMap(hashMap);

        Person3 clone = person.clone();
        clone.setAge(33);
        clone.setMarry(true);
        clone.getExtMap().put("left", "200px");

        System.out.println(person);
        System.out.println(clone);
    }

    /**
     * 深克隆
     * 1、实现 Cloneable 接口后克隆的对象是浅克隆，只对基本数据类型属性有效
     * 2、实现深克隆，使用 {@link ObjectUtil#cloneByStream} 方式，Java bean 必须实现 Serializable 接口。
     * 3、这种方式底层其实也是使用序列化的方式进行的深度克隆
     */
    @Test
    public void testCloneByStream() {
        Person4 person = new Person4("1993", "华安", 22, 89890.8F, LocalDate.now(), false);
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("color", "#fff");
        person.setExtMap(hashMap);

        Person4 clone = ObjectUtil.cloneByStream(person);
        clone.setAge(33);
        clone.setMarry(true);
        clone.getExtMap().put("left", "200px");

        //Person1{pid='1993', name='华安', age=22, salary=89890.8, birthday=2021-02-01, isMarry=false, extMap={color=#fff}}
        System.out.println(person);
        //Person1{pid='1993', name='华安', age=33, salary=89890.8, birthday=2021-02-01, isMarry=true, extMap={color=#fff, left=200px}}
        System.out.println(clone);
    }


}
