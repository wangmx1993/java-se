package org.example.hutool.date;

import cn.hutool.core.date.ChineseDate;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Console;
import org.junit.Test;

/**
 * 农历日期-ChineseDate，提供了生肖、天干地支、传统节日等方法。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/30 9:22
 */
public class ChineseDateTest {

    /**
     * ChineseDate(int chineseYear, int chineseMonth, int chineseDay)：通过农历日期创建对象
     * * @param chineseYear  农历年
     * * @param chineseMonth 农历月，1表示一月（正月）
     * * @param chineseDay   农历日，1表示初一
     * ChineseDate(Date date)：通过公历日期构造对象
     */
    @Test
    public void testChineseDate() {
        DateTime dateTime = DateUtil.parse("1993-10-05");

        ChineseDate chineseDate1 = new ChineseDate(1993, 8, 20);
        ChineseDate chineseDate2 = new ChineseDate(dateTime);

        System.out.println(chineseDate1);//癸酉鸡年 八月二十
        System.out.println(chineseDate2);//癸酉鸡年 八月二十
    }

    /**
     * String getChineseMonth()：获得农历月份（中文，例如二月，十二月，或者润一月）
     * String getChineseMonthName()：获得农历月称呼（中文，例如二月，腊月，或者润正月）
     * String getChineseDay()：获得农历日
     * String getCyclical()：获得年的天干地支
     * String getChineseZodiac()：获得年份生肖
     * String getFestivals()：获得节日
     */
    @Test
    public void testGetXxx() {
        ChineseDate chineseDate = new ChineseDate(DateUtil.parse("1993-10-00"));
        String chineseMonth = chineseDate.getChineseMonth();
        String chineseMonthName = chineseDate.getChineseMonthName();
        String chineseDay = chineseDate.getChineseDay();
        String cyclical = chineseDate.getCyclical();
        String chineseZodiac = chineseDate.getChineseZodiac();
        String festivals = chineseDate.getFestivals();

        Console.log("chineseMonth={}", chineseMonth);//八月
        Console.log("chineseMonthName={}", chineseMonthName);//八月
        Console.log("chineseDay={}", chineseDay);//十五
        Console.log("cyclical={}", cyclical);//癸酉
        Console.log("chineseZodiac={}", chineseZodiac);//鸡
        Console.log("festivals={}", festivals);//中秋节
    }

    /**
     * boolean isLeapMonth()：当前农历月份是否为闰月
     * String toStringNormal()：转换为标准的日期格式来表示农历日期，例如2020-01-13
     */
    @Test
    public void testToStringNormal() {
        ChineseDate chineseDate = new ChineseDate(DateUtil.parse("1993-10-00"));

        boolean leapMonth = chineseDate.isLeapMonth();
        String string = chineseDate.toString();
        String stringNormal = chineseDate.toStringNormal();

        Console.log("leapMonth={}", leapMonth);//false
        Console.log("string={}", string);//癸酉鸡年 八月十五
        Console.log("stringNormal={}", stringNormal);//1993-08-15

    }


}
