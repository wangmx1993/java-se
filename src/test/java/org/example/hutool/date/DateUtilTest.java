package org.example.hutool.date;

import cn.hutool.core.date.*;
import org.junit.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 日期时间工具-DateUtil
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/2/7 14:39
 */
public class DateUtilTest {

    /**
     * DateTime date(): 当前时间，转换为{@link DateTime}对象
     * DateTime date(long date): Long类型时间转为{@link DateTime},只支持毫秒级别时间戳
     * DateTime date(Date date): {@link Date}类型时间转为{@link DateTime}，如果date本身为DateTime对象，则返回强转后的对象，否则新建一个DateTime对象
     * String now(): 当前时间，格式 yyyy-MM-dd HH:mm:ss
     * String today(): 当前日期，格式 yyyy-MM-dd
     * DateTime dateSecond(): 当前时间，转换为{@link DateTime}对象，忽略毫秒部分
     * <p>
     * String DateTime.toDateStr()：转为"yyyy-MM-dd" 格式字符串
     */
    @Test
    public void dateTest() {
        DateTime dateTime1 = DateUtil.date();
        DateTime dateTime2 = DateUtil.date(System.currentTimeMillis());
        System.out.println("dateTime1=" + dateTime1);//2021-02-07 14:42:33
        System.out.println("dateTime2=" + dateTime2);//2021-02-07 14:42:33

        String toDateStr = dateTime2.toDateStr();
        System.out.println("toDateStr=" + toDateStr);//toDateStr=2021-05-26

        String now = DateUtil.now();
        String today = DateUtil.today();
        DateTime dateSecond = DateUtil.dateSecond();

        System.out.println(now);//2021-02-07 14:43:24
        System.out.println(today);//2021-02-07
        System.out.println("dateSecond=" + dateSecond);//dateTime3=2021-02-07 14:48:18

        Timestamp timestamp = new Timestamp(new Date().getTime());
        System.out.println("timestamp -> date："+DateUtil.date(timestamp.getTime()));

    }

    /**
     * int year(Date date): 获得年的部分
     * int month(Date date): 获得月份，从0开始计数
     * int quarter(Date date): 获得指定日期所属季度，从1开始计数
     * int dayOfYear(Date date): 获得指定日期是这个日期所在年的第几天
     * int dayOfMonth(Date date)：获得指定日期是这个日期所在月份的第几天
     * int dayOfWeek(Date date) : 获得指定日期是星期几，1表示周日，2表示周一
     * Week dayOfWeekEnum(Date date) : 获得指定日期是星期几
     * int weekOfYear(Date date): 获得指定日期是所在年份的第几周
     * int weekOfMonth(Date date): 获得指定日期是所在月份的第几周
     * int hour(Date date, boolean is24HourClock):  获得指定日期的小时数部分,is24HourClock 是否24小时制
     * int minute(Date date): 获得指定日期的分钟数部分
     * int second(Date date):获得指定日期的秒数部分
     * int millisecond(Date date): 获得指定日期的毫秒数部分,没有时，为0
     * boolean isAM(Date date): 是否为上午
     * boolean isPM(Date date): 是否为下午
     * String yearAndQuarter(Date date): 获得指定日期年份和季节, 格式：[20131]表示2013年第一季度
     */
    @Test
    public void dayOfTest() {
        DateTime dateTime = DateUtil.parse("1993-08-28 12:36:45");

        int year = DateUtil.year(dateTime);
        int month = DateUtil.month(dateTime);
        System.out.println(DateUtil.quarter(dateTime));//3

        System.out.println(year + "、" + month);//1993、7
        System.out.println(DateUtil.weekOfYear(dateTime));//35
        System.out.println(DateUtil.weekOfMonth(dateTime));//5

        int dayOfYear = DateUtil.dayOfYear(dateTime);
        int dayOfMonth = DateUtil.dayOfMonth(dateTime);
        int dayOfWeek = DateUtil.dayOfWeek(dateTime);
        Week week = DateUtil.dayOfWeekEnum(dateTime);

        System.out.println("dayOfYear=" + dayOfYear);//dayOfYear=237
        System.out.println("dayOfMonth=" + dayOfMonth);//dayOfMonth=25
        System.out.println("dayOfWeek=" + dayOfWeek);//dayOfWeek=4
        System.out.println(week.toChinese() + ":" + week.getValue());//星期三:4

        System.out.println(DateUtil.hour(dateTime, true));//12
        System.out.println(DateUtil.minute(dateTime));//36
        System.out.println(DateUtil.second(dateTime));//45
        System.out.println(DateUtil.millisecond(dateTime));//0

        System.out.println(DateUtil.isAM(dateTime));//false
        System.out.println(DateUtil.isPM(dateTime));//true

        System.out.println(DateUtil.yearAndQuarter(dateTime));//19933
    }

    /**
     * DateTime parse(CharSequence dateCharSequence): 将日期字符串转换为{@link DateTime}对象，
     * 1、不支持只包含 年月 的格式，至少是 年月日。
     * 2、不支持long类型时间解析，比如 DateUtil.parse(String.valueOf(new Date().getTime())) 会报错无法解析。
     * 3、特别注意：只包含年月时，不会报错，但是解析的值是不对的：1970-01-01 20:33:02
     * 4、如果 dateCharSequence 为 null 或者为空白字符，则返回 null。
     * 5、如果解析失败，则抛出异常：cn.hutool.core.date.DateException: No format fit for date String [xxx] !
     * 6、默认支持的格式如下：
     * <ol>
     * <li>yyyy-MM-dd HH:mm:ss</li>
     * <li>yyyy/MM/dd HH:mm:ss</li>
     * <li>yyyy.MM.dd HH:mm:ss</li>
     * <li>yyyy年MM月dd日 HH时mm分ss秒</li>
     * <li>yyyy-MM-dd</li>
     * <li>yyyy/MM/dd</li>
     * <li>yyyy.MM.dd</li>
     * <li>HH:mm:ss</li>
     * <li>HH时mm分ss秒</li>
     * <li>yyyy-MM-dd HH:mm</li>
     * <li>yyyy-MM-dd HH:mm:ss.SSS</li>
     * <li>yyyyMMddHHmmss</li>
     * <li>yyyyMMddHHmmssSSS</li>
     * <li>yyyyMMdd</li>
     * <li>EEE, dd MMM yyyy HH:mm:ss z</li>
     * <li>EEE MMM dd HH:mm:ss zzz yyyy</li>
     * <li>yyyy-MM-dd'T'HH:mm:ss'Z'</li>
     * <li>yyyy-MM-dd'T'HH:mm:ss.SSS'Z'</li>
     * <li>yyyy-MM-dd'T'HH:mm:ssZ</li>
     * <li>yyyy-MM-dd'T'HH:mm:ss.SSSZ</li>
     * </ol>
     * DateTime parseCST(CharSequence cstString): 解析CST时间，格式：EEE MMM dd HH:mm:ss z yyyy（例如：Wed Aug 01 00:00:00 CST 2012）
     * <p>
     * DateTime parse(String str, String... parsePatterns): 通过给定的日期格式解析日期时间字符串。
     * * 传入的日期格式会逐个尝试，直到解析成功，返回{@link DateTime}对象，否则抛出{@link DateException}异常。
     * 这就是原生 java.util.Date 输出的格式，这都可以直接解析，牛B
     * <p>
     * DateTime parseDate(CharSequence dateString)
     * 1、解析日期字符串，忽略时分秒，支持的格式包括：
     * * <pre>
     * 	 * yyyy-MM-dd
     * 	 * yyyy/MM/dd
     * 	 * yyyy.MM.dd
     * 	 * yyyy年MM月dd日
     * 	 * </pre>
     * 2、对输入值要求为年月日时，推荐使用此方法，一旦格式不匹配，解析就会异常，因为 parse 方法对 yyyyMM 时会解析错误，但是不会报错。
     * 3、DateUtil.parseDate("1993/06/31") -> 1993-07-01 00:00:00
     */
    @Test
    public void parseTest1() {
        DateTime dateTime1 = DateUtil.parse("12:05:45");
        System.out.println("dateTime1=" + dateTime1);//dateTime1=2021-02-07 12:05:45
        System.out.println(DateUtil.parse("1993-08-25"));// 1993-08-25 00:00:00
        System.out.println(DateUtil.parse("1993/8/21"));// 1993-08-21 00:00:00
        System.out.println(DateUtil.parse("1993-08-25 12:05"));// 1993-08-25 12:05:00
        System.out.println(DateUtil.parse("1993-08-25 12:05:45"));// 1993-08-25 12:05:45
        System.out.println(DateUtil.parse("1993/08/25 12:05:45"));// 1993-08-25 12:05:45
        System.out.println(DateUtil.parse("19930825120545"));// 1993-08-25 12:05:45
        System.out.println(DateUtil.parse("2021-06-24 11:12:58.0"));// 2021-06-24 11:12:58
        System.out.println(DateUtil.parse("2021-7-1 9:5:7"));// 2021-07-01 09:05:07
        System.out.println(DateUtil.parse("2021年7月2日 4:35:35"));// 2021-07-02 04:35:35
        System.out.println(DateUtil.parse("1993-08-25T00:00:00"));// 1993-08-25 00:00:00
        System.out.println(DateUtil.parse("2021-09-08T01:40:05.953Z"));// 1993-08-25 00:00:00
        System.out.println(DateUtil.parse("2021年7月2日"));// 2021-07-02 00:00:00
        System.out.println(DateUtil.parse("2021年02月01日"));// 2021-02-01 00:00:00

        //默认的格式中基本已经涵盖了常用的格式，也可以自己指定格式
        DateTime parse = DateUtil.parse("1993#08#25", "yyyy#MM#dd", "yyyy+MM+dd");
        System.out.println(parse);//1993-08-25 00:00:00

        Date date = new Date();
        // Wed Jan 11 15:06:23 CST 2023
        System.out.println(date);
        // 2023-01-11 15:06:23
        System.out.println(DateUtil.parseCST(date.toString()));
        // 2023-01-11 15:06:23
        System.out.println(DateUtil.parse(date.toString()));

        // 对输入值要求为年月日时，推荐使用此方法，一旦格式不匹配，解析就会异常，
        // 1993-08-25 00:00:00
        System.out.println(DateUtil.parseDate("1993.08.25"));


        System.out.println("---------------特别注意：使用陷阱，一不留神就会意想不到--------------");
        // 只包含年月时，不会报错，但是值显然是不对的：1970-01-01 20:33:02
        System.out.println(DateUtil.parse("202201"));
        // 常理来说月份和日期都不合法时，直接报错更好理解，而它会默认相加，导致结果为：1993-07-01 00:00:00
        System.out.println(DateUtil.parse("19930631"));
        // 常理来说月份和日期都不合法时，直接报错更好理解，而它会默认相加，导致结果为：1993-07-01 00:00:00
        System.out.println(DateUtil.parse("19930631", "yyyyMMdd", "yyyy/MM/dd"));
        // 常理来说月份和日期都不合法时，直接报错更好理解，而它会默认相加，导致结果为：1993-07-01 00:00:00
        System.out.println(DateUtil.parseDate("1993/06/31"));

        // 4月没有31号，但是结果却是 1993-05-01 00:00:00
        System.out.println(DateUtil.parse("1993/04/31", "yyyy/MM/dd", ""));
    }

    /**
     * ===推荐日期解析方式===
     * DateTime parse(CharSequence dateStr, String format)
     * * 当明确日期格式时，使用本方法指定的格式进行解析，其它方式都有缺陷，比如：
     * * DateUtil.parse("19930631") -> 1993-07-01 00:00:00
     * * DateUtil.parseDate("1993/06/31") -> 1993-07-01 00:00:00
     * * DateUtil.parse("19930631", "yyyyMMdd", "yyyy/MM/dd") -> 1993-07-01 00:00:00
     * * DateUtil.parse("19930631", "yyyyMMdd") -> cn.hutool.core.date.DateException: Parse [19930631] with format [yyyyMMdd] error!
     */
    @Test
    public void parseTest2() {
        // 1993-06-30 00:00:00
        System.out.println(DateUtil.parse("19930630", "yyyyMMdd"));

        // 1993-06-30 16:50:00
        System.out.println(DateUtil.parse("199306301650", "yyyyMMddHHmm"));

        // 1993-06-30 12:25:59
        System.out.println(DateUtil.parse("19930630122559", "yyyyMMddHHmmss"));

        // 1993-06-05 00:00:00
        System.out.println(DateUtil.parse("1993/6/5", "yyyy/MM/dd"));

        // 1993-06-05 01:02:45
        System.out.println(DateUtil.parse("1993/6/5 1:2:45", "yyyy/MM/dd HH:mm:ss"));

        // 只取匹配的部分：1993-07-07 00:00:00
        System.out.println(DateUtil.parse("1993/7/7 11:12:45", "yyyy/MM/dd"));

        // 只取匹配的部分：1993-08-08 00:00:00
        System.out.println(DateUtil.parse("1993/8/8我爱中国", "yyyy/MM/dd"));

        // 报错：cn.hutool.core.date.DateException: Parse [1993/8/80爱中国] with format [yyyy/MM/dd] error!
        // System.out.println(DateUtil.parse("1993/8/80爱中国", "yyyy/MM/dd"));

        // 报错：没有0月
        // System.out.println(DateUtil.parse("1993/0/1", "yyyy/MM/dd"));
        // 报错：没有0号
        // System.out.println(DateUtil.parse("1993/8/0", "yyyy/MM/dd"));

        // 报错：cn.hutool.core.date.DateException: Parse [1993-08-08] with format [yyyy/MM/dd] error!
        // System.out.println(DateUtil.parse("1993-08-08", "yyyy/MM/dd"));

        // 报错：cn.hutool.core.date.DateException: Parse [A1993/8/8我爱中国] with format [yyyy/MM/dd] error!
        // System.out.println(DateUtil.parse("A1993/8/8我爱中国", "yyyy/MM/dd"));

        // 而当指定解析格式时，一旦日期不合法就会异常：cn.hutool.core.date.DateException: Parse [19930631] with format [yyyyMMdd] error!
        // 报错，6月没有31
        // System.out.println(DateUtil.parse("19930631", "yyyyMMdd"));

        // 报错 满24进1，正确应该是 1993-07-01 00:20:55
        // System.out.println(DateUtil.parse("1993-06-30 24:20:55", "yyyy-MM-dd HH:mm:ss"));

        // 报错，满60进1, 正确应该是 1993-06-27 00:00:59
        // System.out.println(DateUtil.parse("1993/06/26 23:60:59", "yyyy/MM/dd HH:mm:ss"));

        // 报错，满60进1，正确应该是 1993-06-27 00:00:00
        // System.out.println(DateUtil.parse("19930626235960", "yyyyMMddHHmmss"));
    }


    /**
     * String format(Date date, String format): 根据特定格式格式化日期。常用格式见： {@link DatePattern}
     * String format(LocalDateTime localDateTime, String format)： 根据特定格式格式化日期
     * String formatDate(Date date): 格式化日期部分（不包括时间）, 格式 yyyy-MM-dd
     * String formatDateTime(Date date): 格式化日期时间, 格式 yyyy-MM-dd HH:mm:ss
     * String formatTime(Date date)：格式化时间， 格式 HH:mm:ss
     * String formatLocalDateTime(LocalDateTime localDateTime)：格式化日期时间，格式 yyyy-MM-dd HH:mm:ss
     * String formatChineseDate(Date date, boolean isUppercase, boolean withTime)：
     * date        被格式化的日期
     * isUppercase 是否采用大写形式
     * withTime    是否包含时间部分
     * 格式化为中文日期格式，如果isUppercase为false，则返回类似：2018年10月24日，否则返回二〇一八年十月二十四日
     */
    @Test
    public void formatTest() {
        DateTime dateTime = DateUtil.parse("1993-08-28 12:36:45");
        String format1 = DateUtil.format(dateTime, "yyyy/MM/dd");
        String format2 = DateUtil.format(dateTime, "yyyy#MM#dd");
        String format3 = DateUtil.format(dateTime, "yyyyMMddHHmmss");
        String format4 = DateUtil.format(dateTime, "yyyy-MM");

        String formatDate = DateUtil.formatDate(dateTime);
        String formatDateTime = DateUtil.formatDateTime(dateTime);
        String formatTime = DateUtil.formatTime(dateTime);

        System.out.println("format1=" + format1);//format1=1993/08/28
        System.out.println("format2=" + format2);//format2=1993#08#28
        System.out.println("format3=" + format3);//format3=19930828123645
        System.out.println("format4=" + format4);//format4=1993-08

        System.out.println("formatDate=" + formatDate);//1993-08-28
        System.out.println("formatDateTime=" + formatDateTime);//1993-08-28 12:36:45
        System.out.println("formatTime=" + formatTime);//12:36:45

        System.out.println(DateUtil.formatChineseDate(dateTime, false, true));//1993年08月28日12时36分45秒
        System.out.println(DateUtil.formatChineseDate(dateTime, false, false));//1993年08月28日
        System.out.println(DateUtil.formatChineseDate(dateTime, true, false));//一九九三年八月二十八日
    }

    /**
     * int lengthOfYear(int year): 获得指定年份的总天数
     * int lengthOfMonth(int month, boolean isLeapYear)： 获得指定月份的总天数
     * boolean isLeapYear(int year): 是否闰年
     */
    @Test
    public void lengthTest() {
        System.out.println(DateUtil.lengthOfYear(2020));//366
        System.out.println(DateUtil.lengthOfMonth(2, DateUtil.isLeapYear(2020)));//29
    }

    /**
     * DateTime beginOfDay(Date date): 获取某天的开始时间
     * DateTime beginOfMonth(Date date): 获取某月的开始时间
     * DateTime beginOfQuarter(Date date)：获取某季度的开始时间
     * DateTime beginOfWeek(Date date): 获取某周的开始时间，周一定为一周的开始时间
     * DateTime beginOfWeek(Date date, boolean isMondayAsFirstDay): 获取某周的开始时间，
     * isMondayAsFirstDay 是否周一做为一周的第一天（false表示周日做为第一天）
     * <p>
     * DateTime endOfDay(Date date): 获取某天的结束时间
     * DateTime endOfMonth(Date date): 获取某月的结束时间
     * DateTime endOfQuarter(Date date)：获取某季度的结束时间
     * DateTime endOfWeek(Date date)： 获取某周的结束时间，周日定为一周的结束
     * DateTime endOfWeek(Date date, boolean isSundayAsLastDay)
     * isSundayAsLastDay 是否周日做为一周的最后一天（false表示周六做为最后一天）
     */
    @Test
    public void beginOfTest() {
        DateTime dateTime = DateUtil.parse("1993-08-25 12:36:45");

        System.out.println(DateUtil.beginOfDay(dateTime));//1993-08-25 00:00:00
        System.out.println(DateUtil.beginOfMonth(dateTime));//1993-08-01 00:00:00
        System.out.println(DateUtil.beginOfQuarter(dateTime));//1993-07-01 00:00:00
        System.out.println("beginOfWeek=" + DateUtil.beginOfWeek(dateTime));//1993-08-23 00:00:00
        System.out.println("beginOfYear=" + DateUtil.beginOfYear(dateTime));//1993-01-01 00:00:00

        System.out.println(DateUtil.endOfDay(dateTime));//1993-08-25 23:59:59
        System.out.println(DateUtil.endOfMonth(dateTime));////1993-08-31 23:59:59
        System.out.println(DateUtil.endOfQuarter(dateTime));//1993-09-30 23:59:59
        System.out.println(DateUtil.endOfYear(dateTime));//1993-12-31 23:59:59
    }

    /**
     * DateTime offsetMonth(Date date, int offset): 偏移月数，正数向未来偏移，负数向历史偏移，n * 12 个月表示偏移 n 年
     * DateTime offsetDay(Date date, int offset)： 偏移天数，正数向未来偏移，负数向历史偏移
     * DateTime offsetHour(Date date, int offset)： 偏移小时数，正数向未来偏移，负数向历史偏移
     * DateTime offsetMinute(Date date, int offset): 偏移分钟数，正数向未来偏移，负数向历史偏移
     * DateTime offsetSecond(Date date, int offset): 偏移秒数，正数向未来偏移，负数向历史偏移
     * DateTime offsetMillisecond(Date date, int offset)： 偏移毫秒数，正数向未来偏移，负数向历史偏移
     * DateTime offsetWeek(Date date, int offset)： 偏移周数，正数向未来偏移，负数向历史偏移
     * <p>
     * DateTime offset(Date date, DateField dateField, int offset):获取指定日期偏移指定时间后的时间，生成的偏移日期不影响原日期
     * date      基准日期
     * dateField 偏移的粒度大小（小时、天、月等）{@link DateField}
     * offset    偏移量，正数为向后偏移，负数为向前偏移
     */
    @Test
    public void offsetTest() {
        DateTime dateTime = DateUtil.parse("1993-08-25 12:36:45");

        System.out.println(DateUtil.offsetMonth(dateTime, 6));//1994-02-25 12:36:45
        System.out.println(DateUtil.offsetDay(dateTime, -15));//1993-08-10 12:36:45
        System.out.println(DateUtil.offsetHour(dateTime, 8));//1993-08-25 20:36:45
        System.out.println("offsetMinute=" + DateUtil.offsetMinute(dateTime, 30));//1993-08-25 13:06:45
        System.out.println(DateUtil.offsetSecond(dateTime, 3 * 60));//1993-08-25 12:39:45
        System.out.println(DateUtil.offsetMillisecond(dateTime, 10 * 1000));//1993-08-25 12:36:55
        System.out.println(DateUtil.offsetWeek(dateTime, 10));//1993-11-03 12:36:45

        System.out.println(DateUtil.offset(dateTime, DateField.YEAR, 27));//2020-08-25 12:36:45

        //针对当前时间，提供了简化的偏移方法（例如昨天、上周、上个月等）
        System.out.println(DateUtil.now());//当前时间 2021-02-07 16:25:37
        System.out.println(DateUtil.yesterday());//昨天 2021-02-06 16:25:37
        System.out.println(DateUtil.tomorrow());//明天 2021-02-08 16:25:37
        System.out.println(DateUtil.lastWeek());//上周 2021-01-31 16:25:37
        System.out.println(DateUtil.nextWeek());//下周 2021-02-14 16:25:37
        System.out.println(DateUtil.lastMonth());//上个月 2021-01-07 16:25:37
        System.out.println(DateUtil.nextMonth());//下个月 2021-03-07 16:25:37
    }

    /**
     * long between(Date beginDate, Date endDate, DateUnit unit):
     * beginDate 起始日期
     * endDate   结束日期
     * unit      相差的单位：相差 天{@link DateUnit#DAY}、小时{@link DateUnit#HOUR} 等
     * long between(Date beginDate, Date endDate, DateUnit unit, boolean isAbs)
     * isAbs     日期间隔是否只保留绝对值正数
     * long betweenMonth(Date beginDate, Date endDate, boolean isReset)：
     * 计算两个日期相差月数<br>
     * 在非重置情况下，如果起始日期的天大于结束日期的天，月数要少算1（不足1个月）
     * long betweenMs(Date beginDate, Date endDate)： 判断两个日期相差的毫秒数
     * long betweenWeek(Date beginDate, Date endDate, boolean isReset)：计算指定指定时间区间内的周数
     * long betweenYear(Date beginDate, Date endDate, boolean isReset)：计算两个日期相差年数
     * 在非重置情况下，如果起始日期的月大于结束日期的月，年数要少算1（不足1年）
     */
    @Test
    public void betweenTest1() {
        DateTime dateTime1 = DateUtil.parse("1993-08-25 12:36:45");
        DateTime dateTime2 = DateUtil.parse("2021-02-07 16:25:37");

        System.out.println(DateUtil.between(dateTime1, dateTime2, DateUnit.DAY));
        System.out.println(DateUtil.betweenYear(dateTime1, dateTime2, true));//28。
        System.out.println(DateUtil.betweenYear(dateTime1, dateTime2, false));//27。false 时表示精确到时分秒

        System.out.println(DateUtil.betweenMonth(dateTime1, dateTime2, true));//330
        System.out.println(DateUtil.betweenMonth(dateTime1, dateTime2, false));//329。false 时表示精确到时分秒

        /**
         * 有时候我们希望看到易读的时间差，比如XX天XX小时XX分XX秒，此时使用DateUtil.formatBetween方法
         * String formatBetween(Date beginDate, Date endDate, BetweenFormatter.Level level)：格式化日期间隔输出
         * beginDate 起始日期
         * endDate   结束日期
         * level     级别，按照天、小时、分、秒、毫秒分为5个等级
         * String formatBetween(Date beginDate, Date endDate):格式化日期间隔输出，精确到毫秒, XX天XX小时XX分XX秒
         * String formatBetween(long betweenMs)：格式化日期间隔输出，精确到毫秒，XX天XX小时XX分XX秒XX毫秒
         *  String formatBetween(long betweenMs, BetweenFormatter.Level level)
         */
        System.out.println(DateUtil.formatBetween(dateTime1, dateTime2, BetweenFormatter.Level.SECOND));//10028天3小时48分52秒
        System.out.println(DateUtil.formatBetween(dateTime1, dateTime2));//10028天3小时48分52秒
    }

    /**
     * long betweenDay(Date beginDate, Date endDate, boolean isReset): 判断两个日期相差的天数
     * 有时候我们计算相差天数的时候需要忽略时分秒。
     * 比如：2016-02-01 23:59:59和2016-02-02 00:00:00相差一秒
     * 如果isReset为{@code false}相差天数为0。
     * 如果isReset为{@code true}相差天数将被计算为1
     */
    @Test
    public void betweenDayTest1() {
        DateTime dateTime1 = DateUtil.parse("2021-08-25 23:36:45");
        DateTime dateTime2 = DateUtil.parse("2021-08-26 00:25:37");
        DateTime dateTime3 = DateUtil.parse("2021-08-26 23:36:45");

        // 0
        System.out.println(DateUtil.betweenDay(dateTime1, dateTime2, false));
        // 1
        System.out.println(DateUtil.betweenDay(dateTime1, dateTime2, true));
        // 1
        System.out.println(DateUtil.betweenDay(dateTime2, dateTime1, true));
        // 1
        System.out.println(DateUtil.betweenDay(dateTime1, dateTime3, false));
        // 1
        System.out.println(DateUtil.betweenDay(dateTime1, dateTime3, true));
        // 1
        System.out.println(DateUtil.betweenDay(dateTime3, dateTime1, true));
    }

    /**
     * TimeInterval timer(): 计时器, 计算某个过程花费的时间，精确到毫秒
     * long interval(): 从开始到当前的间隔时间（毫秒数），如果使用纳秒计时，返回纳秒差，否则返回毫秒差
     * long intervalSecond() ：从开始到当前的间隔秒数，取绝对值
     * long intervalMinute() ：从开始到当前的间隔分钟数，取绝对值
     * long intervalMs() ：从开始到当前的间隔时间（毫秒数）
     * String intervalPretty() ：从开始到当前的间隔时间（毫秒数），返回XX天XX小时XX分XX秒XX毫秒
     * long intervalRestart() : 重新计时并返回从开始到当前的持续时间
     * TimeInterval restart(): 重新开始计算时间（重置开始时间）:
     *
     * @throws InterruptedException
     */
    @Test
    public void timerTest() throws InterruptedException {
        TimeInterval timeInterval = DateUtil.timer();
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(3000));
        System.out.println(timeInterval.interval());
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(3000));
        System.out.println(timeInterval.intervalSecond());
        System.out.println(timeInterval.intervalPretty());//1秒824毫秒
    }

    /**
     * String getZodiac(int month, int day) :通过生日计算星座, month 月，从0开始计数，day 天，返回星座名
     * String getChineseZodiac(int year): 计算生肖，只计算1900年后出生的人
     * int compare(Date date1, Date date2) ：安全的日期比较，比较结果，如果date1 &lt; date2，返回数小于0，date1==date2返回0，date1 &gt; date2 大于0
     * long current(): 当前时间的时间戳,毫秒
     * long currentSeconds() ：当前时间的时间戳（秒）
     */
    @Test
    public void getZodiacTest() {
        String zodiac = DateUtil.getZodiac(Month.OCTOBER.getValue(), 19);
        String chineseZodiac = DateUtil.getChineseZodiac(1993);

        System.out.println(zodiac);//天秤座
        System.out.println(chineseZodiac);//鸡

        int compare = DateUtil.compare(DateUtil.parse("1993-08-25 14:12:12"), DateUtil.parse("1993-08-25 14:12:14"));
        System.out.println(compare);//1

        System.out.println(DateUtil.current());
        System.out.println(DateUtil.currentSeconds());
    }

    /**
     * 获取年龄时忽略时分秒进行计算
     * int age(Date birthday, Date dateToCompare)：计算相对于dateToCompare 的年龄，长用于计算指定生日在某年的年龄
     * int ageOfNow(String birthDay) ：计算相对当前时间的年龄，birthDay 生日，标准日期字符串
     * int ageOfNow(Date birthDay)：和上面同理
     */
    @Test
    public void ageTest1() {
        DateTime dateTime1 = DateUtil.parse("1993-08-25 12:36:45");
        System.out.println(DateUtil.age(dateTime1, DateUtil.date()));//27
        System.out.println(DateUtil.ageOfNow("1993-08-25"));//27

        // 0
        System.out.println(DateUtil.age(dateTime1, DateUtil.parse("1993-12-25")));
        // 0
        System.out.println(DateUtil.age(dateTime1, DateUtil.parse("1994-05-25")));
        // 0
        System.out.println(DateUtil.age(dateTime1, DateUtil.parse("1994-08-24")));
        // 1
        System.out.println(DateUtil.age(dateTime1, DateUtil.parse("1994-08-25")));
        // 1
        System.out.println(DateUtil.age(dateTime1, DateUtil.parse("1994-08-25 12:30:00")));
        // 1
        System.out.println(DateUtil.age(dateTime1, DateUtil.parse("1994-08-26")));
        // 1
        System.out.println(DateUtil.age(dateTime1, DateUtil.parse("1994-12-25")));
    }


    /**
     * boolean isIn(Date date, Date beginDate, Date endDate):当前日期是否在日期指定范围内，起始日期和结束日期可以互换
     * boolean isSameDay(final Date date1, final Date date2): 比较两个日期是否为同一天
     * boolean isSameMonth(final Date date1, final Date date2) ：比较两个日期是否为同一月
     * int thisDayOfMonth()：当前日期是这个日期所在月份的第几天
     * int thisDayOfWeek()：当前日期是星期几
     * int thisHour(boolean is24HourClock)：当前日期的小时数部分，is24HourClock 是否24小时制
     * int thisMinute()：当前日期的分钟数部分
     * int thisSecond()：当前日期的秒数部分
     * int thisMillisecond(): 当前日期的毫秒数部分
     * int thisMonth(): 当前月份
     * int thisWeekOfYear(): 当前日期所在年份的第几周
     * int thisYear() ：今年
     * Instant toInstant(Date date)： Date对象转换为{@link Instant}对象
     * LocalDateTime toLocalDateTime(Date date)：{@link Date} 转换为 {@link LocalDateTime}，使用系统默认时区
     * LocalDateTime toLocalDateTime(Instant instant)：{@link Instant} 转换为 {@link LocalDateTime}，使用系统默认时区
     */
    @Test
    public void testThisYear() {
        System.out.println(DateUtil.thisYear());//2021
        System.out.println(DateUtil.thisWeekOfYear());//6
    }

    /**
     * String toDateStr(): 转为"yyyy-MM-dd" 格式字符串
     * String toString(): 转为"yyyy-MM-dd HH:mm:ss" 格式字符串
     */
    @Test
    public void testDateTime() {
        DateTime dateTime1 = DateUtil.parse("1993-08-25 12:36:45.0");

        System.out.println(dateTime1.toDateStr());//1993-08-25
        System.out.println(dateTime1.toString());//1993-08-25 12:36:45
    }

    /**
     * int age(Date birthday, Date dateToCompare) 计算相对于dateToCompare的年龄，长用于计算指定生日在某年的年龄
     * : birthday      生日，不能为null，否则报错
     * : dateToCompare 需要对比的日期，为null时，默认为当前时间
     * <p>
     * int ageOfNow(Date birthDay): 生日转为年龄，计算法定年龄
     * : birthday      生日，不能为null，否则报错
     */
    @Test
    public void testAge() {
        int age = DateUtil.age(DateUtil.parse("19930517"), DateUtil.parse("20230518"));
        // 30
        System.out.println(age);

        int ageOfNow = DateUtil.ageOfNow(DateUtil.parse("19930518"));
        // 30
        System.out.println(ageOfNow);
    }

}
