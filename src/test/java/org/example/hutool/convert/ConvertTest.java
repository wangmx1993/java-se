package org.example.hutool.convert;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.text.UnicodeUtil;
import cn.hutool.core.util.CharsetUtil;
import org.junit.Test;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 类型转换工具类 - Convert
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/2/2 16:29
 */
public class ConvertTest {

    /**
     * Integer toInt(Object value)；转换为int<br>
     * 1、如果给定的值为{@code null}，或者转换失败，返回默认值{@code null}<br>
     * 2、支持正负数，只要 value 中含有数字，就会以第一部分的数字作为结果返回
     * 3、转换失败不会报错
     */
    @Test
    public void toIntTest() {
        Integer integer1 = Convert.toInt("8989");
        Integer integer2 = Convert.toInt("89FTP11");
        Integer integer3 = Convert.toInt("0FTP11UP");
        Integer integer4 = Convert.toInt("-0FTP11UP");
        Integer integer5 = Convert.toInt("-989");
        Integer integer6 = Convert.toInt("1993-08-25");
        Integer integer7 = Convert.toInt("YUH_YGF");
        Integer integer8 = Convert.toInt(null);
        Integer integer9 = Convert.toInt("-D989");

        System.out.println(integer1);//8989
        System.out.println(integer2);//89
        System.out.println(integer3);//0
        System.out.println(integer4);//0
        System.out.println(integer5);//-989
        System.out.println(integer6);//1993
        System.out.println(integer7);//null
        System.out.println(integer8);//null
        System.out.println(integer9);//null
    }

    /**
     * Integer toInt(Object value, Integer defaultValue)：转换为int<br>
     * 1、如果给定的值为空，或者转换失败，返回默认值<br>
     * 2、支持正负数，只要 value 中含有数字，就会以第一部分的数字作为结果返回
     * 3、转换失败不会报错
     */
    @Test
    public void toIntDefaultValueTest() {
        Integer integer1 = Convert.toInt("8989", 0);
        Integer integer2 = Convert.toInt("89FTP11", 0);
        Integer integer3 = Convert.toInt("0FTP11UP", 0);
        Integer integer4 = Convert.toInt("-1FTP11UP", 0);
        Integer integer5 = Convert.toInt("-989", 0);
        Integer integer6 = Convert.toInt("1993-08-25", 0);
        Integer integer7 = Convert.toInt("YUH_YGF", 0);
        Integer integer8 = Convert.toInt(null, 0);
        Integer integer9 = Convert.toInt("-D989", 0);

        System.out.println(integer1);//8989
        System.out.println(integer2);//89
        System.out.println(integer3);//0
        System.out.println(integer4);//-1
        System.out.println(integer5);//-989
        System.out.println(integer6);//1993
        System.out.println(integer7);//0
        System.out.println(integer8);//0
        System.out.println(integer9);//0
    }

    /**
     * Integer[] toIntArray(Object value)：转换为Integer数组<br>
     * 1、注意被转换的 value 必须能够转换，如给不能转换，则抛异常，比如 "abc"、null 都会转换异常
     */
    @Test
    public void toIntArrayTest() {
        Integer[] integer1 = Convert.toIntArray("8989");
        Integer[] integer2 = Convert.toIntArray("89FTP11");
        Integer[] integer3 = Convert.toIntArray("0FTP11UP");
        Integer[] integer4 = Convert.toIntArray("-0FTP11UP");
        Integer[] integer5 = Convert.toIntArray("-989");
        Integer[] integer6 = Convert.toIntArray("1993-08-25");
        Integer[] integer7 = Convert.toIntArray(new String[]{"1", "2", "3", "4"});
        Integer[] integer8 = Convert.toIntArray(new Long[]{11L, 22L, 33L, 44L});

        System.out.println(Arrays.asList(integer1));//8989
        System.out.println(Arrays.asList(integer2));//89
        System.out.println(Arrays.asList(integer3));//0
        System.out.println(Arrays.asList(integer4));//0
        System.out.println(Arrays.asList(integer5));//-989
        System.out.println(Arrays.asList(integer6));//1993
        System.out.println(Arrays.asList(integer7));//[1, 2, 3, 4]
        System.out.println(Arrays.asList(integer8));//[11, 22, 33, 44]
    }

    /**
     * String toStr(Object value)：转换为字符串
     * 如果给定的值为{@code null}，或者转换失败，返回默认值{@code null}<br>
     * 转换失败不会报错
     * String toStr(Object value, String defaultValue)
     * String[] toStrArray(Object value)
     */
    @Test
    public void toStrTest() {
        String str1 = Convert.toStr(8989);
        String str2 = Convert.toStr(10000L);
        String str3 = Convert.toStr(425.25F);
        String str4 = Convert.toStr(false);
        String str5 = Convert.toStr(null);
        String str6 = Convert.toStr(new int[]{1, 2, 3,});
        String str7 = Convert.toStr(new String[]{"a", "b", "c"});

        System.out.println(str1);//8989
        System.out.println(str2);//10000
        System.out.println(str3);//425.25
        System.out.println(str4);//false
        System.out.println(str5);//null（注意不是字符串）
        System.out.println(str6);//[1, 2, 3]
        System.out.println(str7);//[a, b, c]
    }

    /**
     * Date toDate(Object value):转换为Date<br>
     * 如果给定的值为空，或者转换失败，返回{@code null}<br>
     * 转换失败不会报错
     * Date toDate(Object value, Date defaultValue)
     */
    @Test
    public void toDateTest() {
        Date date1 = Convert.toDate("1993-08");
        Date date2 = Convert.toDate("1993/08");
        Date date3 = Convert.toDate("1993-08-25");
        Date date4 = Convert.toDate("1993/08/25");
        Date date5 = Convert.toDate("19930826");
        Date date6 = Convert.toDate("19930826");
        Date date7 = Convert.toDate("19930825 1211");
        Date date8 = Convert.toDate("1993-08-25 12:00");
        Date date9 = Convert.toDate("1993/08/25 12:00");
        Date date10 = Convert.toDate("1993-08-25 12:10:15");
        Date date11 = Convert.toDate("1993/08/25 12:10:15");
        Date date12 = Convert.toDate(null);
        Date date13 = Convert.toDate("xxx");

        System.out.println("1、" + date1);//null
        System.out.println("2、" + date2);//null
        System.out.println("3、" + date3);//Wed Aug 25 00:00:00 CST 1993
        System.out.println("4、" + date4);//Wed Aug 25 00:00:00 CST 1993
        System.out.println("5、" + date5);//Thu Aug 26 00:00:00 CST 1993
        System.out.println("6、" + date6);//Thu Aug 26 00:00:00 CST 1993
        System.out.println("7、" + date7);//null
        System.out.println("8、" + date8);//Wed Aug 25 12:00:00 CST 1993
        System.out.println("9、" + date9);//Wed Aug 25 12:00:00 CST 1993
        System.out.println("10、" + date10);//Wed Aug 25 12:10:15 CST 1993
        System.out.println("11、" + date11);//Wed Aug 25 12:10:15 CST 1993
        System.out.println("12、" + date12);//null
        System.out.println("13、" + date13);//null
    }

    /**
     * T convert(TypeReference<T> reference, Object value): 转换值为指定类型
     *
     * <T>   :    目标类型
     * reference ： 类型参考，用于持有转换后的泛型类型
     * value   ：  值
     * 转换器不存在时抛出 ConvertException 异常
     */
    @Test
    public void convertTest() {
        Object[] a1 = {"a", "你", "好", "", 1};
        Object[] a2 = {11, 22, 3300, 44, 1};

        List<String> list1 = Convert.convert(new TypeReference<List<String>>() {
        }, a1);
        List<Integer> list2 = Convert.convert(new TypeReference<List<Integer>>() {
        }, a2);

        list1.add("111");
        list2.add(123);

        System.out.println(list1);
        System.out.println(list2);
    }

    /**
     * String toSBC(String input): 半角转全角
     * String toDBC(String input): 全角转半角
     */
    @Test
    public void toSBCTest() {
        String str1 = "12345，6789。";
        String str2 = "abc";
        String str3 = "蚩尤，后裔。";

        String sbc1 = Convert.toSBC(str1);
        String sbc2 = Convert.toSBC(str2);
        String sbc3 = Convert.toSBC(str3);

        String dbc1 = Convert.toDBC(sbc1);
        String dbc2 = Convert.toDBC(sbc2);
        String dbc3 = Convert.toDBC(sbc3);

        System.out.println(sbc1);//１２３４５，６７８９。
        System.out.println(sbc2);//ａｂｃ
        System.out.println(sbc3);//蚩尤，后裔。

        System.out.println(dbc1);//12345,6789。
        System.out.println(dbc2);//abc
        System.out.println(dbc3);//蚩尤,后裔。
    }

    /**
     * String toHex(String str, Charset charset): 字符串转换成十六进制字符串，结果为小写
     * String hexToStr(String hexStr, Charset charset): 十六进制字符串转换字符串
     * String toHex(byte[] bytes)：byte 数组转 16 进制字符串
     * byte[] hexToBytes(String src)：Hex字符串转换为 Byte 值
     */
    @Test
    public void toHexTest() {
        String str1 = "万里长城今犹在，不见当年秦始皇";
        String hex1 = Convert.toHex(str1, CharsetUtil.CHARSET_UTF_8);
        //e4b887e9878ce995bfe59f8ee4bb8ae78ab9e59ca8efbc8ce4b88de8a781e5bd93e5b9b4e7a7a6e5a78be79a87
        System.out.println(hex1);

        String hexToStr1 = Convert.hexToStr(hex1, Charset.forName("UTF-8"));
        //万里长城今犹在，不见当年秦始皇
        System.out.println(hexToStr1);

        String hex2 = Convert.toHex(str1.getBytes());
        //hex2=e4b887e9878ce995bfe59f8ee4bb8ae78ab9e59ca8efbc8ce4b88de8a781e5bd93e5b9b4e7a7a6e5a78be79a87
        System.out.println("hex2=" + hex2);

        byte[] bytes = Convert.hexToBytes(hex2);
        String hexToStr2 = Convert.hexToStr(hex2, Charset.forName("UTF-8"));

        //万里长城今犹在，不见当年秦始皇
        System.out.println(new String(bytes));
        //万里长城今犹在，不见当年秦始皇
        System.out.println(hexToStr2);
    }

    /**
     * String strToUnicode(String strText): String 的字符串转换成 unicode 的 String
     * String unicodeToStr(String unicode)：解码 unicode 编码的字符串，其中非 unicode 编码的内容原样返回。
     */
    @Test
    public void unicodeTest() {
        String str1 = "万里长城今犹在，不见当年秦始皇";
        String unicode1 = Convert.strToUnicode(str1);
        //\u4e07\u91cc\u957f\u57ce\u4eca\u72b9\u5728\uff0c\u4e0d\u89c1\u5f53\u5e74\u79e6\u59cb\u7687
        System.out.println(unicode1);

        // 解码 unicode 编码的字符串，其中非 unicode 编码的内容原样返回。
        String str2 = Convert.unicodeToStr(unicode1 + "。蚩尤后裔");
        // 万里长城今犹在，不见当年秦始皇。蚩尤后裔
        System.out.println(str2);
    }

    /**
     * 提供Unicode字符串和普通字符串之间的转换
     * String toUnicode(String str): 字符串编码为Unicode形式
     * String toString(String unicode):
     * * Unicode字符串转为普通字符串<br>
     * * Unicode字符串的表现方式为：\\uXXXX
     */
    @Test
    public void testUnicodeUtil() {
        String str1 = "万里长城今犹在，不见当年秦始皇";

        String toUnicode = UnicodeUtil.toUnicode(str1);
        // \u4e07\u91cc\u957f\u57ce\u4eca\u72b9\u5728\uff0c\u4e0d\u89c1\u5f53\u5e74\u79e6\u59cb\u7687
        System.out.println(toUnicode);

        String toString = UnicodeUtil.toString(toUnicode + "。蚩尤后裔");
        // 万里长城今犹在，不见当年秦始皇。蚩尤后裔
        System.out.println(toString);

    }


    /**
     * String convertCharset(String str, String sourceCharset, String destCharset)
     * 给定字符串转换字符编码，如果参数为空，则返回原字符串，不报错。
     * str 被转码的字符串
     * sourceCharset 原字符集
     * destCharset 目标字符集
     * 经过实测：UTF-8 转为 GBK 后，再转换回  UTF-8，中文仍然有些乱码
     */
    @Test
    public void convertCharsetTest() {
        String str1 = "万里长城今犹在，不见当年秦始皇。";
        String result1 = Convert.convertCharset(str1, CharsetUtil.UTF_8, CharsetUtil.GBK);
        String result2 = Convert.convertCharset(str1, CharsetUtil.UTF_8, CharsetUtil.ISO_8859_1);
        String result3 = Convert.convertCharset(result1, CharsetUtil.GBK, CharsetUtil.UTF_8);
        String result4 = Convert.convertCharset(result2, CharsetUtil.ISO_8859_1, CharsetUtil.UTF_8);

        System.out.println("result1=" + result1);//result1=涓囬噷闀垮煄浠婄姽鍦紝涓嶈褰撳勾绉﹀鐨囥��
        System.out.println("result2=" + result2);//result2=ä¸éé¿åä»ç¹å¨ï¼ä¸è§å½å¹´ç§¦å§çã
        System.out.println("result3=" + result3);//result3=万里长城今犹在，不见当年秦始皇�??
        System.out.println("result4=" + result4);//result4=万里长城今犹在，不见当年秦始皇。
    }

    /**
     * long convertTime(long sourceDuration, TimeUnit sourceUnit, TimeUnit destUnit)
     * 1、转换时间单位
     * sourceDuration 时长
     * sourceUnit 源单位
     * destUnit 目标单位
     */
    @Test
    public void convertTimeTest() {
        long time = Convert.convertTime(3, TimeUnit.MINUTES, TimeUnit.MILLISECONDS);
        System.out.println(time);//180000
    }

    /**
     * String digitToChinese(Number n): 金额转为中文大写数字
     * 1、注意：它只精确到小数点后面两位
     */
    @Test
    public void digitToChineseTest() {
        String chinese1 = Convert.digitToChinese(18000.4567);
        String chinese2 = Convert.digitToChinese(null);
        String chinese3 = Convert.digitToChinese(0);
        String chinese4 = Convert.digitToChinese(1000);
        String chinese5 = Convert.digitToChinese(0.1080);

        System.out.println(chinese1);//壹万捌仟元肆角陆分
        System.out.println(chinese2);//零
        System.out.println(chinese3);//零元整
        System.out.println(chinese4);//壹仟元整
        System.out.println(chinese5);//零元壹角壹分
    }

}
