package org.example.hutool.captcha;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.captcha.generator.MathGenerator;
import cn.hutool.captcha.generator.RandomGenerator;
import cn.hutool.core.lang.Console;
import cn.hutool.core.math.Calculator;
import org.example.controller.LoginController;
import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.File;

/**
 * 图形验证码工具
 * <p>
 * web 应用使用可以参考 {@link LoginController}
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/2/23 9:30
 */
public class CaptchaUtilTest {

    /**
     * LineCaptcha createLineCaptcha(int width, int height)：创建线干扰的验证码，默认5位验证码，150条干扰线
     * LineCaptcha createLineCaptcha(int width, int height, int codeCount, int lineCount)
     * width：图片宽、height：图片高、codeCount：字符个数，默认5、lineCount：干扰线条数，默认150
     * <p>
     * write(File file)：验证码写出到文件
     * write(OutputStream out)：验证码写出到输出流
     * write(String path)：验证码写出到指定路径
     * boolean verify(String userInputCode)：验证用户输入的验证码是否有效
     * createCode()：重新生成验证码
     */
    @Test
    public void createCodeTest1() {
        //验证码输出到桌面 captcha 文件夹下
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();

        //定义图形验证码的长和宽(单位像素)，默认5位验证码，150条干扰线
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(200, 100);
        //图形验证码写出，可以写出到文件，也可以写出到流
        lineCaptcha.write(new File(homeDirectory, "captcha/line1.png"));
        //输出验证码
        Console.log("验证码：{}", lineCaptcha.getCode());
        //验证用户输入的验证码是否有效，返回 boolean 值
        String userInputCode = "0yfs7";
        Console.log("用户输入 {} 是否正确：{}", userInputCode, lineCaptcha.verify(userInputCode));

        //重新生成验证码，一个 ICaptcha 实例可以反复生成验证码，对应页面的刷新验证码重新生成功能。
        lineCaptcha.createCode();
        lineCaptcha.write(new File(homeDirectory, "captcha/line1.png"));
        Console.log("验证码：{}", lineCaptcha.getCode());
        //验证图形验证码的有效性，返回boolean值
        Console.log("用户输入 {} 是否正确：{}", userInputCode, lineCaptcha.verify(userInputCode));
    }

    /**
     * CircleCaptcha createCircleCaptcha(int width, int height)：创建圆圈干扰的验证码，默认5位验证码，15个干扰圈
     * CircleCaptcha createCircleCaptcha(int width, int height, int codeCount, int circleCount)
     */
    @Test
    public void createCodeTest2() {
        //验证码输出到桌面 captcha 文件夹下
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();

        //定义图形验证码的长、宽(单位像素)、验证码字符数、干扰元素个数
        CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(200, 100, 4, 20);
        //图形验证码写出，可以写出到文件，也可以写出到流
        captcha.write(new File(homeDirectory, "captcha/circle.png"));
        //输出验证码
        Console.log("验证码：{}", captcha.getCode());
        //验证用户输入的验证码是否有效，返回 boolean 值
        String userInputCode = "0yfs";
        Console.log("用户输入 {} 是否正确：{}", userInputCode, captcha.verify(userInputCode));
    }

    /**
     * ShearCaptcha createShearCaptcha(int width, int height, int codeCount, int thickness):创建扭曲干扰的验证码，默认5位验证码
     */
    @Test
    public void createCodeTest3() {
        //验证码输出到桌面 captcha 文件夹下
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();

        //定义图形验证码的长、宽、验证码字符数、干扰线宽度
        ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(200, 100, 4, 4);
        //图形验证码写出，可以写出到文件，也可以写出到流
        captcha.write(new File(homeDirectory, "captcha/shear.png"));
        //输出验证码
        Console.log("验证码：{}", captcha.getCode());
        //验证用户输入的验证码是否有效，返回 boolean 值
        String userInputCode = "0yfs";
        Console.log("用户输入 {} 是否正确：{}", userInputCode, captcha.verify(userInputCode));
    }

    /**
     * 自定义验证码 1
     * 1、可以指定输出的验证码内容，比如纯数字，纯中文，或者混合等等，输出的字符从指定的内容中随机输出。
     */
    @Test
    public void createCodeTest4() {
        // 验证码输出到桌面 captcha 文件夹下
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        String source1 = "0123456789";
        String source2 = "中华民族心中燃烧的激情永远不会熄灭成为一个有用的人报效我的祖国";

        // 自定义纯数字的验证码（随机4位，可重复）。
        RandomGenerator randomGenerator = new RandomGenerator(source2, 4);
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(300, 100);
        lineCaptcha.setGenerator(randomGenerator);

        // 图形验证码写出，可以写出到文件，也可以写出到流
        lineCaptcha.write(new File(homeDirectory, "captcha/random.png"));
        // 输出验证码
        Console.log("验证码：{}", lineCaptcha.getCode());
        // 验证用户输入的验证码是否有效，返回 boolean 值
        String userInputCode = lineCaptcha.getCode();
        Console.log("用户输入 {} 是否正确：{}", userInputCode, lineCaptcha.verify(userInputCode));
    }

    /**
     * 自定义验证码 2：四则运算方式
     * 1、输出的验证码如 38+23=，用户必须输入 61 之后，verify 验证方法才会为 true。
     */
    @Test
    public void createCodeTest5() {
        // 验证码输出到桌面 captcha 文件夹下
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();

        ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(200, 45, 2, 2);
        // 自定义验证码内容为四则运算方式
        captcha.setGenerator(new MathGenerator());
        // 图形验证码写出，可以写出到文件，也可以写出到流
        captcha.write(new File(homeDirectory, "captcha/mathGenerator.png"));

        // 获取输出到页面的验证码，如： 16-37=
        Console.log("验证码：{}", captcha.getCode());
        // 获取四则运算验证码的结果值，如： -21
        int calculateResult = (int) Calculator.conversion(captcha.getCode());
        System.out.println(calculateResult);

        // 验证用户输入的验证码是否有效，返回 boolean 值
        String userInputCode = String.valueOf(calculateResult);
        // 用户输入 -21 是否正确：true
        Console.log("用户输入 {} 是否正确：{}", userInputCode, captcha.verify(userInputCode));
    }
}
