package org.example.hutool.io;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.LineHandler;
import cn.hutool.core.lang.Console;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;
import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

/**
 * 文件工具类-FileUtil
 * FileUtil类包含以下几类操作工具：
 * 文件操作：包括文件目录的新建、删除、复制、移动、改名等
 * 文件判断：判断文件或目录是否非空，是否为目录，是否为文件等等。
 * 绝对路径：针对ClassPath中的文件转换为绝对路径文件。
 * 文件名：主文件名，扩展名的获取
 * 读操作：包括类似IoUtil中的getReader、readXXX操作
 * 写操作：包括getWriter和writeXXX操作
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/18 16:13
 */
public class FileUtilTest {

    /**
     * File file(File parent, String path): 根据的路径构建文件，在Win下直接构建，在Linux下拆分路径单独构建
     * File file(File directory, String... names): 通过多层目录参数创建文件
     * File file(String path):  创建File对象，自动识别相对或绝对路径，相对路径将自动从ClassPath下寻找
     * File file(String parent, String path)
     * File file(String... names): 通过多层目录创建文件
     * File file(URI uri): 创建File对象
     * File file(URL url): 创建File对象
     * <p>
     * String getAbsolutePath(String path)
     * String getAbsolutePath(File file): 获取标准的绝对路径
     */
    @Test
    public void testFile() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = FileUtil.file(homeDirectory, "a/b/c/1.txt");
        System.out.println(file);//C:\Users\A\Desktop\a\b\c\1.txt

        File file1 = FileUtil.file("data/config.properties");
        System.out.println(file1);//E:\IDEA_Projects\java-org.se\target\test-classes\data\config.properties

        //E:\IDEA_Projects\java-org.se\target\test-classes\data\config.properties
        System.out.println(FileUtil.getAbsolutePath(file1));

        // 通过多层目录参数创建文件
        File file2 = FileUtil.file(homeDirectory, "a", "b1", "c1", "readme.txt");
        // E:\桌面\a\b1\c1\readme.txt
        System.out.println(file2.getAbsolutePath());
        // 创建文件及其父目录，如果这个文件存在，直接返回这个文件
        FileUtil.touch(file2);
    }

    /**
     * 创建临时文件:
     * File createTempFile(String prefix, String suffix, File dir, boolean isReCreat)
     * * prefix    前缀，至少3个字符
     * * suffix    后缀，如果null则使用默认 .tmp
     * * dir       临时文件创建的所在目录
     * * isReCreat 是否重新创建文件（删掉原来的，创建新的）
     * <p>
     * File createTempFile(File dir): 创建后的文件名为 prefix[Randon].tmp
     * * prefix 默认为 hutool
     * * suffix 默认没有
     * * isReCreat 默认为 true
     * File createTempFile(File dir, boolean isReCreat)
     */
    @Test
    public void testCreateTempFile() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File file = FileUtil.file(homeDirectory, "a");
        File tempFile = FileUtil.createTempFile(file);
        // E:\桌面\a\hutool2250895531828170663.tmp
        System.out.println(tempFile.getAbsolutePath());

        File tempFile1 = FileUtil.createTempFile(file, true);
        // E:\桌面\a\hutool5361165797496047050.tmp
        System.out.println(tempFile1.getAbsolutePath());

        File tempFile2 = FileUtil.createTempFile("basic-server-", ".wmx", file, true);
        // E:\桌面\a\basic-server-2295776886275789992.wmx
        System.out.println(tempFile2.getAbsolutePath());

        FileUtil.writeString("蚩尤后裔，文成武德。", tempFile1, Charset.forName("UTF-8"));
        FileUtil.writeString("蚩尤后裔，一统天下。", tempFile2, Charset.forName("UTF-8"));
    }

    /**
     * String extName(File file) 获取文件扩展名（后缀名），扩展名不带“.”
     */
    @Test
    public void testExtName() {
        File file1 = new File("E:\\桌面\\a\\hutool8935434316658003958.tmp");
        File file2 = new File("/a/b.tmp");
        File file3 = new File("a/b.tmp");
        File file4 = new File("a/b");

        // tmp
        System.out.println(FileUtil.extName(file1));
        // tmp
        System.out.println(FileUtil.extName(file2));
        // tmp
        System.out.println(FileUtil.extName(file3));
        // 返回空
        System.out.println(FileUtil.extName(file4));
    }

    /**
     * String getLineSeparator(): 获取当前系统的换行分隔符
     * <pre>
     *   Windows: \r\n
     *   Mac: \r
     *   Linux: \n
     * </pre>
     * String getMimeType(String filePath) 根据文件扩展名获得MimeType
     * String getName(String filePath) 返回文件名，如 abc.txt
     * String getName(File file): 返回文件名，如 abc.txt
     * String getPrefix(File file)：返回主文件名,如 abc
     * String getSuffix(File file)：获取文件后缀名，扩展名不带“.”，如 txt
     */
    @Test
    public void testGetLineSeparator() {
        System.out.println("换行" + FileUtil.getLineSeparator() + "新行开始。");

        System.out.println(FileUtil.getMimeType("/a/b.mp4"));// null
        System.out.println(FileUtil.getMimeType("/a/b.mp3"));// null
        System.out.println(FileUtil.getMimeType("/a/b.js"));// application/x-javascript
        System.out.println(FileUtil.getMimeType("/a/b.html"));// text/html
        System.out.println(FileUtil.getMimeType("/a/b.css"));// text/css
        System.out.println(FileUtil.getMimeType("/a/b.java"));// text/plain
        System.out.println(FileUtil.getMimeType("/a/b.xls"));// null
        System.out.println(FileUtil.getMimeType("/a/b.xlsx"));// null
        System.out.println(FileUtil.getMimeType("/a/b.doc"));// null
        System.out.println(FileUtil.getMimeType("/a/b.docx"));// null
        System.out.println(FileUtil.getMimeType("/a/b.jpg"));// image/jpeg
        System.out.println(FileUtil.getMimeType("/a/b.jpeg"));// image/jpeg
        System.out.println(FileUtil.getMimeType("/a/b.png"));// image/png

        System.out.println("==================");
        // b.tmp
        System.out.println(FileUtil.getName("/a/b.tmp"));
    }

    /**
     * BufferedReader getReader(File file, Charset charset): 获得一个文件读取器
     * BufferedWriter getWriter(File file, Charset charset, boolean isAppend): 获得一个带缓存的写入对象
     * BufferedInputStream getInputStream(String path)
     * BufferedInputStream getInputStream(File file): 获得缓冲输入流，如果文件不存在，则抛异常
     * BufferedOutputStream getOutputStream(File file)
     * BufferedOutputStream getOutputStream(String path)：获得一个输出流对象，如果文件不存在，则自动创建
     */
    @Test
    public void testGetOutputStream() throws IOException {
        //1、获取文件输入流
        File file1 = FileUtil.file("data/config.properties");
        BufferedInputStream bufferedInputStream = FileUtil.getInputStream(file1);
        InputStreamReader inputStreamReader = new InputStreamReader(bufferedInputStream, "gbk");
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        //2、获取文件输出流
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File deskFile = FileUtil.file(homeDirectory, "a/b/c/1.txt");
        BufferedOutputStream outputStream = FileUtil.getOutputStream(deskFile);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
        BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

        //3、读写文件内容
        String readLine = null;
        for (readLine = bufferedReader.readLine(); readLine != null; ) {
            bufferedWriter.write(readLine);
            bufferedWriter.newLine();//换行
            readLine = bufferedReader.readLine();
        }
        bufferedWriter.flush();
        bufferedWriter.close();
        bufferedReader.close();

        // E:\桌面\a\b\c\1.txt 1.txt
        Console.log(deskFile, FileUtil.getName(deskFile));
    }

    /**
     * PrintWriter getPrintWriter(String path, String charset, boolean isAppend)
     * PrintWriter getPrintWriter(File file, Charset charset, boolean isAppend): 获得一个打印写入对象，可以有print
     */
    @Test
    public void testGetPrintWriter() {
        //1、获取文件输入流
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File deskFile = FileUtil.file(homeDirectory, "a/b/c/1.txt");
        PrintWriter printWriter = FileUtil.getPrintWriter(deskFile, CharsetUtil.UTF_8, true);
        printWriter.write("大秦赋\n");
        printWriter.write("过秦论\n");
        printWriter.flush();
        printWriter.close();
    }

    /**
     * File getTmpDir(): 获取临时文件目录，底层就是 System.getProperty("java.io.tmpdir")
     * String getTmpDirPath(): 获取临时文件路径（绝对路径），底层就是 System.getProperty("java.io.tmpdir")
     * File getUserHomeDir(): 获取用户目录，底层是 System.getProperty("user.home")
     * String getUserHomePath():获取用户路径（绝对路径），底层是 System.getProperty("user.home")
     * File getWebRoot(): 获取Web项目下的web root路径
     * 原理是首先获取ClassPath路径，由于在web项目中ClassPath位于 WEB-INF/classes/下，故向上获取两级目录即可。
     */
    @Test
    public void testGetTmpDirPath() {
        File tmpDir = FileUtil.getTmpDir();
        String tmpDirPath = FileUtil.getTmpDirPath();
        // C:\Users\Think\AppData\Local\Temp
        System.out.println(tmpDir);
        // C:\Users\Think\AppData\Local\Temp\
        System.out.println(tmpDirPath);

        File userHomeDir = FileUtil.getUserHomeDir();
        // C:\Users\Think
        System.out.println(userHomeDir.toPath());

        // C:\Users\Think
        System.out.println(FileUtil.getUserHomePath());

        String classPath = ClassUtil.getClassPath();
        // D:/project/IDEA_project/java-se/target/test-classes/
        System.out.println(classPath);

        File webRoot = FileUtil.getWebRoot();
        // D:\project\IDEA_project\java-se
        System.out.println(webRoot.toPath());
    }

    /**
     * boolean isAbsolutePath(String path)：判断给定路径已经是绝对路径
     * boolean isDirectory(File file)：判断是否为目录，如果file为null，则返回false
     * boolean isDirectory(String path)
     * boolean isDirEmpty(File dir): 判断目录是否为空
     * boolean isEmpty(File file)：目录：里面没有文件时为空 文件：文件大小为0时为空
     * boolean isFile(File file)：判断是否为文件，如果 file 为 null，则返回 false
     * boolean isFile(String path)
     * boolean isModifed(File file, long lastModifyTime)：判断文件是否被改动，如果文件对象为 null 或者文件不存在，被视为改动
     * boolean isSub(File parent, File sub)：判断给定的目录是否为给定文件或文件夹的子目录
     * boolean isSymlink(File file)：判断是否为符号链接文件
     * boolean isWindows(): 是否为 Windows 环境
     */
    @Test
    public void testIsAbsolutePath() {
        System.out.println(FileUtil.isAbsolutePath("/a/b/c/"));//true
        System.out.println(FileUtil.isAbsolutePath("a/b/c/"));//false
        System.out.println(FileUtil.isAbsolutePath("E:/a/b/c/"));//true

        File file1 = FileUtil.file("data/config.properties");
        boolean empty = FileUtil.isEmpty(file1);
        System.out.println(empty);//false
        System.out.println(FileUtil.isWindows());
    }

    /**
     * int lastIndexOfSeparator(String filePath)：获得最后一个文件路径分隔符的位置
     * Date lastModifiedTime(File file)：指定文件最后修改时间
     * Date lastModifiedTime(String path)
     * List<String> listFileNames(String path): 获得指定目录下所有文件(不含目录), 不会扫描子目录
     * * @param path 相对ClassPath的目录或者绝对路径目录
     * * @return 文件路径列表（如果是jar中的文件，则给定类似.jar!/xxx/xxx的路径）
     * List<File> loopFiles(File file)：递归遍历目录以及子目录中的所有文件
     * List<File> loopFiles(File file)：递归遍历目录以及子目录中的所有文件(不含目录)
     * List<File> loopFiles(String path)
     * File[] ls(String path): 列出目录文件(包含目录)，不能是压缩包中的路径
     * * @param path 目录绝对路径或者相对路径
     */
    @Test
    public void testLastModifiedTime() {
        File file1 = FileUtil.file("data/config.properties");
        //D:\project\IDEA_project\java-org.se\target\test-classes\data\config.properties:74
        System.out.println(file1 + ":" + file1.toString().length());
        //56
        System.out.println(FileUtil.lastIndexOfSeparator(file1.getPath()));

        Date lastModifiedTime = FileUtil.lastModifiedTime(file1);
        System.out.println(DateUtil.date(lastModifiedTime));//2021-04-08 16:21:20

        System.out.println("-----------------------listFileNames-----------------------------");
        String path = "E:\\study";
        List<String> listFileNames = FileUtil.listFileNames(path);
        listFileNames.stream().forEach(i -> System.out.println(i));

        System.out.println("-----------------------listFileNames-----------------------------");
        List<File> loopFiles = FileUtil.loopFiles(path);
        loopFiles.stream().forEach(item -> System.out.println(item));

        System.out.println("-----------------------ls-----------------------------");
        File[] files = FileUtil.ls(path);
        Arrays.asList(files).stream().forEach(i -> System.out.println(i));
    }

    /**
     * File mkdir(File dir): 创建文件夹，会递归自动创建其不存在的父文件夹，如果存在直接返回此文件夹
     * File mkdir(String dirPath)：级联创建文件夹，如果已经存在，则直接返回.
     * File mkParentDirs(String path)
     * File mkParentDirs(File file)：创建所给文件或目录的父目录
     */
    @Test
    public void testMkdir() throws IOException {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File deskDir = FileUtil.file(homeDirectory, "a/b/c/");
        File deskFile = FileUtil.file(homeDirectory, "a1/b1/c1/d1/e.txt");

        File mkdir = FileUtil.mkdir(deskDir);
        // 创建目录：E:\桌面\a\b\c
        Console.log("创建目录：{}", mkdir);

        FileUtil.mkParentDirs(deskFile);
        // 创建父目录了：E:\桌面\a1\b1\c1\d1
        Console.log("创建父目录了：{}", deskFile.getParentFile());

        deskFile.createNewFile();
    }

    /**
     * move(File src, File target, boolean isOverride): 移动文件或者目录
     * * @param src        源文件或者目录
     * * @param target     目标文件或者目录，目标目录下不为空时，移动报错.
     * * @param isOverride 是否覆盖目标，只有目标为文件才覆盖
     */
    @Test
    public void testMove() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File fromDir = FileUtil.file(homeDirectory, "a/b/c/");
        File fromFile = FileUtil.file(homeDirectory, "a1/b1/c1/d1/e.txt");

        File deskDir = FileUtil.file(homeDirectory, "x");
        File deskFile = FileUtil.file(homeDirectory, "y");

        FileUtil.mkdir(deskDir);
        FileUtil.mkdir(deskFile);

        FileUtil.move(fromDir, deskDir, true);
        FileUtil.move(fromFile, deskFile, true);
        Console.log("文件移动:{} -> {}", fromDir, deskDir);
    }

    /**
     * boolean newerThan(File file, File reference)：给定文件或目录(file)的最后修改时间是否晚于给定文件(reference)的时间
     * boolean newerThan(File file, long timeMillis)：给定文件或目录(file)的最后修改时间是否晚于给定时间(timeMillis)
     * boolean pathEndsWith(File file, String suffix): 判断文件路径是否有指定后缀，忽略大小写
     * boolean pathEquals(File file1, File file2)： 取两个文件的绝对路径比较路径是否相同，在Windows下忽略大小写，在Linux下不忽略。
     */
    @Test
    public void testNewerThan() {
        File file1 = FileUtil.file("data/config.properties");
        System.out.println(DateUtil.formatDateTime(FileUtil.lastModifiedTime(file1)));//2021-04-08 16:21:20

        DateTime dateTime = DateUtil.parse("2021-04-09 16:21:20");
        boolean newerThan = FileUtil.newerThan(file1, dateTime.getTime());
        System.out.println(newerThan);//false

        System.out.println(FileUtil.pathEndsWith(file1, ".doc"));//false
        System.out.println(FileUtil.pathEndsWith(file1, ".properties"));//true
    }

    /**
     * String readableFileSize(long size)
     * String readableFileSize(File file): 返回可读的文件大小，文件不存在时，返回 0
     * long size(File file): 计算目录或文件的总大小，单位字节，file 为 null 或者文件不存在返回 0
     * * 当给定对象为文件时，直接调用 {@link File#length()}<br>
     * * 当给定对象为目录时，遍历目录下的所有文件和目录，递归计算其大小，求和返回
     */
    @Test
    public void testReadableFileSize() {
        File file = FileUtil.file("data/config.properties");
        System.out.println(file.length());//254 文件不存在时，返回0

        String fileSize = FileUtil.readableFileSize(file);
        System.out.println(fileSize);//254 B

        long size = FileUtil.size(FileUtil.file("E:\\study"));
        Console.log(size, FileUtil.readableFileSize(size));//11255439454 10.48 GB

    }

    /**
     * byte[] readBytes(String filePath)
     * byte[] readBytes(File file)：读取文件所有数据，文件的长度不能超过 Integer.MAX_VALUE
     * List<String> readLines(File file, String charset)
     * List<String> readLines(String path, String charset)
     * List<String> readLines(String path, Charset charset)
     * List<String> readLines(File file, Charset charset):从文件中读取每一行数据
     * void readLines(File file, Charset charset, LineHandler lineHandler):按行处理文件内容
     * List<String> readLines(URL url, String charset)
     * List<String> readLines(URL url, Charset charset) :从 URL 网络文件或本地文件中读取每一行数据
     *
     * @throws UnsupportedEncodingException
     */
    @Test
    public void testReadLines() throws UnsupportedEncodingException, MalformedURLException {
        System.out.println("----------------------------------readBytes----------------------------------");
        File file = FileUtil.file("data/config.properties");
        byte[] bytes = FileUtil.readBytes(file);
        String content = new String(bytes, "GBK");
        System.out.println(content);

        System.out.println("----------------------------------readLines----------------------------------");
        List<String> readLines = FileUtil.readLines(file, "gbk");
        readLines.stream().forEach(i -> System.out.println(i));

        System.out.println("----------------------------------LineHandler----------------------------------");
        FileUtil.readLines(file, Charset.forName("gbk"), new LineHandler() {
            @Override
            public void handle(String line) {
                System.out.println(line);
            }
        });

        System.out.println("----------------------------------readLines ---URL-------------------------------");
        URL url = new URL("https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.js");
        List<String> lines = FileUtil.readLines(url, CharsetUtil.UTF_8);
        lines.stream().forEach(item -> System.out.println(item));
    }

    /**
     * String readString(File file, String charsetName)
     * String readString(String path, Charset charset)
     * String readString(URL url, String charset)
     * String readString(File file, Charset charset): 读取文件内容，适合字符流
     * <p>
     * List<String> readUtf8Lines(String path)
     * List<String> readUtf8Lines(File file)：使用 UTF-8 编码从文件中读取每一行数据
     * void readUtf8Lines(File file, LineHandler lineHandler)
     * List<String> readUtf8Lines(URL url)：使用 UTF-8 编码从 url 文件中读取内容
     * String readUtf8String(String path)
     * String readUtf8String(File file)：读取文件内容，以字符串形式返回
     */
    @Test
    public void testReadUtf8Lines() throws MalformedURLException {
        System.out.println("--------------------------readString--------------------------------------");
        File file = FileUtil.file("data/config.properties");
        String string = FileUtil.readString(file, "gbk");
        System.out.println(string);

        System.out.println("--------------------------readUtf8Lines--------------------------------------");
        URL url = new URL("https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.js");
        List<String> readUtf8Lines = FileUtil.readUtf8Lines(url);
        readUtf8Lines.stream().forEach(item -> System.out.println(item));
    }

    /**
     * File rename(File file, String newName, boolean isOverride): 修改文件或目录的文件名，不变更路径，只是简单修改文件名，不保留扩展名。
     * File rename(File file, String newName, boolean isRetainExt, boolean isOverride)
     * * @param file       被修改的文件，不存在时报错
     * * @param newName    新的文件名，如需扩展名，需自行在此参数加上，原文件名的扩展名不会被保留
     * * @param isOverride 是否覆盖目标文件
     * * @param isRetainExt 是否保留原文件的扩展名，如果保留，则newName不需要加扩展名
     */
    @Test
    public void testRename() {
        String path = "E:\\wmx\\2.txt";
        File rename = FileUtil.rename(FileUtil.file(path), "3.txt", true);
        System.out.println(rename);

        File rename1 = FileUtil.rename(rename, "34", true, true);
        System.out.println(rename1);
    }

    /**
     * File touch(String fullFilePath)
     * File touch(File file)：创建文件及其父目录，如果这个文件存在，直接返回这个文件.(只能创建文件)
     * File touch(String parent, String path)
     * File touch(File parent, String path)：创建文件及其父目录，如果这个文件存在，直接返回这个文件.(只能创建文件)
     * * @param parent 父文件对象
     * * @param path   文件路径
     */
    @Test
    public void testTouch() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = FileUtil.file(homeDirectory, "a/b/c/蚩尤后裔.json");

        File touch = FileUtil.touch(destFile);
        Console.log("新建文件：{}", touch);
    }

    /**
     * File writeBytes(byte[] data, String path)
     * File writeBytes(byte[] data, File dest): 将字节数据写入到文件中，覆盖模式
     * File writeBytes(byte[] data, File dest, int off, int len, boolean isAppend)
     * * @param data     数据
     * * @param dest     目标文件
     * * @param off      数据开始位置
     * * @param len      数据长度
     * * @param isAppend 是否追加模式
     */
    @Test
    public void testWriteBytes() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = FileUtil.file(homeDirectory, "a.txt");

        FileUtil.touch(destFile);
        File file = FileUtil.writeBytes("蚩尤后裔".getBytes(), destFile);
        Console.log("写入文件:{}", file);
    }

    /**
     * File writeFromStream(InputStream in, String fullFilePath)
     * File writeFromStream(InputStream in, File dest): 将流的内容写入文件, 此方法会自动关闭输入流
     * File writeFromStream(InputStream in, File dest, boolean isCloseIn)
     * * @param isCloseIn 是否关闭输入流
     */
    @Test
    public void testWriteFromStream() {
        InputStream resourceAsStream = FileUtilTest.class.getClassLoader().getResourceAsStream("data/config.properties");

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = FileUtil.file(homeDirectory, "b.txt");

        File writeFromStream = FileUtil.writeFromStream(resourceAsStream, destFile);
        Console.log("写入到文件：{}", writeFromStream);
    }

    /**
     * File writeLines(Collection<T> list, String path, String charset, boolean isAppend)
     * File writeLines(Collection<T> list, File file, Charset charset, boolean isAppend)：将列表数据写入文件，一条数据一行
     * * @param list     列表
     * * @param file     文件
     * * @param charset  字符集
     * * @param isAppend 是否追加
     * File writeUtf8Lines(Collection<T> list, File file):使用默认编码为UTF-8
     * File appendLines(Collection<T> list, File file, Charset charset)：将列表写入文件，追加模式
     * * * @param list    列表
     * * * @param file    文件，不存在时自动创建
     * * * @param charset 字符集
     * File appendUtf8Lines(Collection<T> list, File file)：追加内容，使用默认编码为UTF-8
     */
    @Test
    public void testWriteLines() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = FileUtil.file(homeDirectory, "c.txt");

        List<String> list = ListUtil.of("大唐", "强汉", "大明");

        File writeLines = FileUtil.writeLines(list, destFile, "UTF-8", true);
        Console.log("写入到文件：{}", writeLines);

        List<String> list2 = ListUtil.of("大唐2", "强汉2", "大明2");
        File appendLines = FileUtil.appendLines(list2, destFile, "utf-8");
        Console.log("追加到文件：{}", appendLines);
    }

    /**
     * File writeMap(Map<?, ?> map, File file, Charset charset, String kvSeparator, boolean isAppend)
     * 将 Map 写入文件，每个键值对为一行，一行中键与值之间使用 kvSeparator 分隔
     * File writeUtf8Map(Map<?, ?> map, File file, String kvSeparator, boolean isAppend) ：使用默认 utf-8 编码
     */
    @Test
    public void testWriteMap() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = FileUtil.file(homeDirectory, "d.txt");

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("id", 1001);
        dataMap.put("name", "华安");
        dataMap.put("age", 34);

        File writeMap = FileUtil.writeMap(dataMap, destFile, CharsetUtil.CHARSET_UTF_8, "=", true);
        Console.log("写入到文件：{}", writeMap);
    }

    /**
     * File writeString(String content, String path, String charset): 将 String 写入文件，覆盖模式
     * long writeToStream(String fullFilePath, OutputStream out)
     * long writeToStream(File file, OutputStream out): 将文件(file)写入到输出流中，此方法不会自动关闭输出流，覆盖模式
     * File writeUtf8String(String content, File file) ：使用默认 utf-8 编码
     * File appendString(String content, File file, Charset charset)：将 String 内容写入文件，追加模式
     * File appendUtf8String(String content, File file)：默认使用 UTF-8 编码追加内容
     */
    @Test
    public void testWriteString() throws IOException {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = FileUtil.file(homeDirectory, "d.txt");
        File destFile2 = FileUtil.file(homeDirectory, "e.txt");

        File writeString = FileUtil.writeString("蚩尤后裔", destFile, "utf-8");
        Console.log("写入到文件：{}", writeString);

        OutputStream outputStream = new FileOutputStream(destFile2);
        long writeToStream = FileUtil.writeToStream(destFile, outputStream);
        Console.log("写入到文件：{}，总共大小为 {} 字节", destFile2, writeToStream);
        outputStream.flush();
        outputStream.close();
    }

    /**
     * boolean clean(File directory)：递归删除文件夹下的所有内容，某个文件删除失败会终止删除操作
     * boolean cleanEmpty(File directory)：递归删除空的文件夹，不删除文件，即如果目录为空，则删除，否则不删除
     * boolean del(File file)：删除文件或者文件夹，删除文件夹时不会判断文件夹是否为空，如果不空则递归删除子文件或文件夹
     * 某个文件删除失败会终止删除操作
     * <p>
     * boolean containsInvalid(String fileName)：文件名中是否包含在Windows下不支持的非法字符，包括： \ / : * ? " &lt; &gt; |
     * String cleanInvalid(String fileName): 清除文件名中的在Windows下不支持的非法字符，包括： \ / : * ? " &lt; &gt; |
     * * fileName：文件名（必须不包括路径，否则路径符将被替换）
     */
    @Test
    public void testClean() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File deskDir = FileUtil.file(homeDirectory, "a");

        boolean clean = FileUtil.clean(deskDir);
        System.out.println(clean);
    }

    /**
     * contentEqualsIgnoreEOL(File file1, File file2, Charset charset):比较两个文件内容是否相同,首先比较长度，长度一致再比较内容，比较内容采用按行读取，每行比较<br>
     * boolean contentEquals(File file1, File file2)：比较两个文件内容是否相同，首先比较长度，长度一致再比较内容
     * File convertCharset(File file, Charset srcCharset, Charset destCharset):
     * 转换文件编码,读取的文件实际编码必须与指定的 srcCharset 编码一致，否则导致乱码
     * boolean equals(File file1, File file2)：检查两个文件是否是同一个文件，所谓文件相同，是指File对象是否指向同一个文件或文件夹
     */
    @Test
    public void testEquals() throws FileNotFoundException {
        File file = FileUtil.file("data/config.properties");
        InputStream resourceAsStream = new FileInputStream(file);

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = FileUtil.file(homeDirectory, "b.txt");

        File writeFromStream = FileUtil.writeFromStream(resourceAsStream, destFile);

        boolean equals = FileUtil.contentEquals(file, writeFromStream);
        //文件C:\Users\Think\Desktop\b.txt与文件C:\Users\Think\Desktop\b.txt内容是否相同？true
        Console.log("文件{}与文件{}内容是否相同？{}", destFile, writeFromStream, equals);
    }

    /**
     * File copy(File src, File dest, boolean isOverride): 复制文件或目录
     * * 1、src和dest都为目录，则将src目录及其目录下所有文件目录拷贝到dest下
     * * 2、src和dest都为文件，直接复制，名字为dest
     * * 3、src为文件，dest为目录，将src拷贝到dest目录下
     * * @param isOverride 是否覆盖目标文件
     * boolean exist(File file)：判断文件是否存在，如果file为null，则返回false
     */
    @Test
    public void testCopy() {
        File file = FileUtil.file("data/config.properties");

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = FileUtil.file(homeDirectory, "E.txt");

        File copy = FileUtil.copy(file, destFile, true);
        Console.log("文件复制：{}", copy);
    }

    /**
     * List<File> loopFiles(File file)
     * List<File> loopFiles(String path): 递归遍历目录以及子目录中的所有文件
     * File[] ls(String path): 列出当前目录下的文件(包括目录，但是不会递归),给定的绝对路径不能是压缩包中的路径
     */
    @Test
    public void testLoopFiles1() {
        File file = new File("E:\\shares");
        System.out.println("=====================loopFiles============================================");
        List<File> list = FileUtil.loopFiles(FileUtil.file(file));
        int i = 0;
        for (File file1 : list) {
            System.out.println((++i) + " ==== " + file1.getAbsoluteFile());
        }
        System.out.println("=====================ls============================================");
        i = 0;
        File[] files = FileUtil.ls("E:\\shares");
        for (File file1 : files) {
            System.out.println((++i) + " ==== " + file1.getAbsoluteFile());
        }
    }

    /**
     * List<File> loopFiles(String path, FileFilter fileFilter)
     * List<File> loopFiles(File file, FileFilter fileFilter): 递归遍历目录以及子目录中的所有文件,果提供file为文件，直接返回过滤结果
     * * @param path       当前遍历文件或目录的路径
     * * @param fileFilter 文件过滤规则对象，选择要保留的文件，只对文件有效，不过滤目录
     */
    @Test
    public void testLoopFiles2() {
        File file = new File("E:\\wmx\\基础库发版\\20210629\\脚本");
        List<File> fileList = FileUtil.loopFiles(file, pathname -> {
            if (pathname.toString().contains("视图")) {
                // 只查询文件名称中含有 "视图" 的文件
                return true;
            }
            return false;
        });
        int i = 0;
        for (File file1 : fileList) {
            System.out.println((++i) + " ==== " + file1.getAbsoluteFile().toString());
        }
    }

    /**
     * List<File> loopFiles(Path path, int maxDepth, FileFilter fileFilter):递归遍历目录以及子目录中的所有文件, 如果提供file为文件，直接返回过滤结果
     * * @param file       当前遍历文件或目录
     * * @param maxDepth   遍历最大深度，-1表示遍历到没有目录为止
     * * @param fileFilter 文件过滤规则对象，选择要保留的文件，只对文件有效，不过滤目录，null表示接收全部文件
     */
    @Test
    public void testLoopFiles3() {
        File file = new File("E:\\wmx\\基础库发版\\20210629\\脚本");
        List<File> list = FileUtil.loopFiles(FileUtil.file(file), 5, null);
        String root = file.getAbsoluteFile().toString() + "\\";
        int i = 0;
        for (File file1 : list) {
            System.out.println((++i) + " ==== " + file1.getAbsoluteFile().toString().replace(root, ""));
        }
    }


    /**
     * 测试将内容插入到文件开头 和 结尾
     */
    @Test
    public void testInsert2StartAndEnd() {
        String startLine = "--================蚩尤后裔===============";
        String endLine = "--================" + DateUtil.date() + "===============";

        File file = FileUtil.file("D:\\project\\IDEA_project\\java-se\\src\\test\\resources\\data\\备忘录-数据库常用表.sql");
        List<String> readLines = FileUtil.readLines(file, "UTF-8");

        FileUtil.writeString(startLine, file, Charset.forName("UTF-8"));
        FileUtil.appendString(FileUtil.getLineSeparator(), file, Charset.forName("UTF-8"));

        FileUtil.appendLines(readLines, file, Charset.forName("UTF-8"));

        FileUtil.appendString(FileUtil.getLineSeparator(), file, Charset.forName("UTF-8"));
        FileUtil.appendString(endLine, file, Charset.forName("UTF-8"));
        System.out.println("将内容插入到文件开头 和 结尾：" + FileUtil.getAbsolutePath(file));
    }

    /**
     * 获取 schema_version 表插入脚本
     * 支持自动在sql脚本文件的开头和结尾插入脚本执行记录日志。
     */
    @Test
    public void testWriteSchemaVersionSql() {
        // 修改点1：发版脚本地址（大版本下可能有多个日期，多轮发版日期）
        String sqlPath = "D:\\project\\budget3.2\\basic\\sql\\update\\V3.2.1-V3.2.2";
        // 修改点2：当前发版日期（sqlPath下面其它发版日期的脚本不会再取，只过滤当前发版日期的脚本）
        String releaseDate = "20240424";
        // 修改点3：当前发版的版本号
        String releaseVersion = "basic3.5.1（build" + releaseDate + "）";
        // 修改点4：是否自动写入到文件中
        boolean isAutoWrite2File = true;

        // 递归遍历目录以及子目录中的所有SQL文件（默认按原始顺序返回）
        List<File> files = FileUtil.loopFiles(sqlPath, pathname -> pathname.getAbsolutePath().contains(releaseDate) && StrUtil.equalsIgnoreCase(FileUtil.extName(pathname), "sql"));
        StringBuilder sBuilder = new StringBuilder();
        List<String> readLines = new ArrayList<>();
        List<String> startLines = new ArrayList<>(8);
        List<String> endLines = new ArrayList<>(8);
        for (File file : files) {
            readLines.clear();
            endLines.clear();
            startLines.clear();
            sBuilder.delete(0, sBuilder.length());
            // 先读取文件本身的内容
            readLines = FileUtil.readLines(file, "UTF-8");
            // 取相对路径，去掉前缀，并将路径的右斜杠调整为要求的左斜杠。
            String relativePath = file.getAbsolutePath().replace(sqlPath + "\\", "");
            relativePath = relativePath.replace("\\", "/");
            sBuilder.append("insert into schema_version (ID, VERSION, DESCRIPTION, TYPE, SCRIPT, SERVER_NAME, UPDATE_BY, UPDATE_ON, CREATE_TIME) ");
            sBuilder.append(FileUtil.getLineSeparator());
            sBuilder.append("values (sys_guid(),'").append(releaseVersion).append("', 'start', '功能编码', '").append(relativePath).append("', ");
            sBuilder.append("'basic', '脚本实际操作人姓名', sysdate, sysdate);");

            startLines.add(FileUtil.getLineSeparator());
            startLines.add("-- 开头执行（脚本执行记录）");
            startLines.add(sBuilder.toString());
            startLines.add(FileUtil.getLineSeparator());

            endLines.add(FileUtil.getLineSeparator());
            endLines.add("-- 结尾执行（脚本执行记录）");
            endLines.add(sBuilder.toString().replace("'start'", "'end'"));
            endLines.add(FileUtil.getLineSeparator());
            if (isAutoWrite2File) {
                // 先覆盖
                FileUtil.writeLines(startLines, file, CharsetUtil.UTF_8, false);
                // 再回写原内容
                FileUtil.appendLines(readLines, file, CharsetUtil.UTF_8);
                // 再追加
                FileUtil.appendLines(endLines, file, CharsetUtil.UTF_8);
                System.out.println("schema_version 脚本已写入：" + file.getAbsolutePath());
            } else {
                startLines.stream().forEach(item -> System.out.println(item));
                endLines.stream().forEach(item -> System.out.println(item));
            }
        }
    }

}
