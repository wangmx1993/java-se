package org.example.hutool.file;

import cn.hutool.core.io.file.FileAppender;
import cn.hutool.core.util.CharsetUtil;
import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * FileAppender - 文件追加器
 * 持有一个文件，在内存中积累一定量的数据后统一追加到文件<br>
 * 此类只有在写入文件时打开文件，并在写入结束后关闭之。因此此类不需要关闭<br>
 * 在调用append方法后会缓存于内存，只有超过容量后才会一次性写入文件，因此内存中随时有剩余未写入文件的内容，在最后必须调用flush方法将剩余内容刷入文件
 *
 * ==此类主要用于类似于日志写出这类需求所用==
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/18 12:08
 */
public class FileAppenderTest {

    /**
     * FileAppender(File destFile, Charset charset, int capacity, boolean isNewLineMode)
     * FileAppender(File destFile, int capacity, boolean isNewLineMode)
     * * @param destFile 目标文件
     * * @param charset 编码
     * * @param capacity 当行数积累多少条时刷入到文件
     * * @param isNewLineMode 追加内容是否为新行
     * FileAppender append(String line): 内容追加
     * FileAppender flush()：刷入到文件
     */
    @Test
    public void testFileAppender() throws InterruptedException {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = new File(homeDirectory,System.currentTimeMillis()+".txt");
        System.out.println(destFile);

        FileAppender fileAppender = new FileAppender(destFile, CharsetUtil.CHARSET_UTF_8,10,true);
        for(int i=0;i<100;i++){
            fileAppender.append(i+"");
            TimeUnit.MILLISECONDS.sleep(100);
        }
        fileAppender.flush();
    }
}
