package org.example.hutool.file;

import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.core.lang.Console;
import cn.hutool.core.util.CharsetUtil;
import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.PrintWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件写入-FileWriter
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/18 11:12
 */
public class FileWriterTest {

    /**
     * FileWriter(File file): 构建对象，使用默认编码：UTF-8
     * FileWriter(File file, Charset charset)：使用指定编码 {@link CharsetUtil}
     * FileWriter(File file, String charset) ：使用指定编码
     * FileWriter(String filePath)：相对路径会被转换为相对于 ClassPath 的路径
     * FileWriter(String filePath, Charset charset)
     * FileWriter(String filePath, String charset)
     */
    @Test
    public void testFileWriter() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = new File(homeDirectory, System.currentTimeMillis() + ".txt");

        FileWriter fileWriter = new FileWriter(destFile, "UTF-8");
        fileWriter.write("大风起兮云飞扬，", true);
        fileWriter.write("威加海内兮归故乡。\n", true);//需要自己控制换行
        fileWriter.write("安得猛士兮守四方。", true);

        System.out.println(destFile);
    }

    /**
     * File write(String content): 将 String 写入文件，覆盖模式。返回目标文件
     * File write(String content, boolean isAppend):  isAppend 是否追加。返回目标文件
     * File write(byte[] data, int off, int len): 写入数据到文件。返回目标文件。适用于字节流
     * File write(byte[] data, int off, int len, boolean isAppend)。适用于字节流
     * * @param data 数据
     * * @param off 数据开始位置
     * * @param len 数据长度
     * * @param isAppend 是否追加模式
     */
    @Test
    public void testWrite() {
        URL resource = FileWriterTest.class.getClassLoader().getResource("data/images/1.jpg");
        String file = resource.getFile();

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = new File(homeDirectory, "2.jpg");
        FileReader fileReader = FileReader.create(new File(file));
        byte[] readBytes = fileReader.readBytes();

        FileWriter fileWriter = FileWriter.create(destFile);
        fileWriter.write(readBytes, 0, readBytes.length);
        System.out.println(destFile);
    }

    /**
     * File writeFromStream(InputStream in): 将流的内容写入文件,此方法会自动关闭输入流
     * File writeFromStream(InputStream in, boolean isCloseIn): isCloseIn 是否关闭输入流
     * File writeLines(Collection<T> list)： 将列表写入文件，覆盖模式
     * File writeLines(Collection<T> list, boolean isAppend)：isAppend 是否追加
     */
    @Test
    public void testWriteFromStream() {
        URL resource = FileWriterTest.class.getClassLoader().getResource("data/images/1.jpg");
        String file = resource.getFile();

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = new File(homeDirectory, "3.jpg");
        FileReader fileReader = FileReader.create(new File(file));

        FileWriter fileWriter = FileWriter.create(destFile);
        fileWriter.writeFromStream(fileReader.getInputStream(), true);
        System.out.println(destFile);
    }

    /**
     * File writeMap(Map<?, ?> map, String kvSeparator, boolean isAppend):
     * 将 Map 内容写入文件，每个键值对为一行，一行中键与值之间使用 kvSeparator 分隔
     * File writeMap(Map<?, ?> map, LineSeparator lineSeparator, String kvSeparator, boolean isAppend)
     */
    @Test
    public void testWriteMap() {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("id", 1001);
        dataMap.put("name", "华安");
        dataMap.put("age", 34);

        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = new File(homeDirectory, "4.txt");
        FileWriter fileWriter = FileWriter.create(destFile);
        fileWriter.writeMap(dataMap, "=", true);

        System.out.println(destFile);
    }

    /**
     * BufferedWriter getWriter(boolean isAppend): 获得 JDK 原生的带缓存的写入对象
     * PrintWriter getPrintWriter(boolean isAppend)：获得 JDK 原生的打印写入对象，可以有 print
     * BufferedOutputStream getOutputStream()：获得 JDK 原生的输出流对象
     */
    @Test
    public void testGetPrintWriter() {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = new File(homeDirectory, System.currentTimeMillis() + ".txt");

        FileWriter fileWriter = FileWriter.create(destFile);
        PrintWriter printWriter = fileWriter.getPrintWriter(true);
        printWriter.write("蚩尤后裔！\n");
        printWriter.write("天之骄子！\n");

        printWriter.flush();
        printWriter.close();
        Console.log("输出文件：{}", destFile);
    }

}
