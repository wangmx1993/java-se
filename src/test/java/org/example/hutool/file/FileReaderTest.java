package org.example.hutool.file;

import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.util.CharsetUtil;
import org.junit.Before;
import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.net.URL;

/**
 * 文件读取-FileReader
 *
 * 在JDK中，同样有一个FileReader类，但是并不如想象中的那样好用，于是Hutool便提供了更加便捷FileReader类。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/18 10:34
 */
public class FileReaderTest {
    private String file;

    @Before
    public void init(){
        URL resource = FileReaderTest.class.getClassLoader().getResource("data/config.properties");
        file = resource.getFile();
        System.out.println(file);///E:/IDEA_Projects/java-org.se/target/test-classes/data/config.properties
    }

    /**
     *  FileReader(File file): 构建对象，使用默认编码：UTF-8
     *  FileReader(File file, Charset charset)：使用指定编码 {@link CharsetUtil}
     *  FileReader(File file, String charset) ：使用指定编码
     *  FileReader(String filePath)：相对路径会被转换为相对于 ClassPath 的路径
     *  FileReader(String filePath, Charset charset)
     *  FileReader(String filePath, String charset)
     */
    @Test
    public void testFileReader(){
        FileReader fileReader = new FileReader(file,"GBK");
        String readString = fileReader.readString();
        System.out.println(readString);
    }

    /**
     * byte[] readBytes(): 读取文件所有数据，适合图片、视频等字节流, 文件的长度不能超过 {@link Integer#MAX_VALUE}
     * List<String> readLines()： 从文件中读取每一行数据，适合字符流
     * String readString()： 读取文件内容，适合字符流
     * @throws UnsupportedEncodingException
     */
    @Test
    public void testRead() throws UnsupportedEncodingException {
        FileReader fileReader = new FileReader(file,CharsetUtil.GBK);
        byte[] bytes = fileReader.readBytes();
        System.out.println(new String(bytes,"GBK"));
    }

    /**
     * long writeToStream(OutputStream out): 将文件写入流中，此方法不会关闭比输出流,返回写出的字节数
     * long writeToStream(OutputStream out, boolean isCloseOut)：isCloseOut：结束后是否关闭输出流
     *
     * @throws FileNotFoundException
     */
    @Test
    public void testWriteToStream() throws FileNotFoundException {
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = new File(homeDirectory,file.substring(file.lastIndexOf("/")));
        FileOutputStream outputStream = new FileOutputStream(destFile);

        FileReader fileReader = new FileReader(file,CharsetUtil.GBK);
        fileReader.writeToStream(outputStream,true);
        System.out.println(destFile.toPath().toString());
    }

    /**
     * BufferedReader getReader() : 获得 JDK 原生文件读取器
     * BufferedInputStream getInputStream()：获得 JDK 原生缓冲输入流
     *
     * @throws IOException
     */
    @Test
    public void testGetReader() throws IOException {
        FileReader fileReader = new FileReader(file,CharsetUtil.GBK);
        BufferedReader bufferedReader = fileReader.getReader();
        BufferedInputStream bufferedInputStream = fileReader.getInputStream();

        String readLine = null;
        while ((readLine = bufferedReader.readLine()) !=null){
            System.out.println(readLine);
        }
        bufferedReader.close();
    }

}
