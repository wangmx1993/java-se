package org.example.hutool.file;

import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.file.Tailer;
import cn.hutool.core.util.CharsetUtil;
import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * 文件内容跟随器 - Tailer，实现类似Linux下"tail -f"命令功能
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/18 12:23
 */
public class TailerTest {

    /**
     * Tailer(File file, LineHandler lineHandler)
     * Tailer(File file, LineHandler lineHandler, int initReadLine): 构造跟踪器，默认UTF-8编码
     * Tailer(File file, Charset charset, LineHandler lineHandler)
     * Tailer(File file, Charset charset, LineHandler lineHandler, int initReadLine, long period)
     * * @param file 文件
     * * @param charset 编码，默认UTF-8编码
     * * @param lineHandler 行处理器，{@link Tailer#CONSOLE_HANDLER} 是命令行打印的行处理器，跟踪的内容会输出到控制台
     * * @param initReadLine 启动时预读取的行数，默认为0
     * * @param period 检查内容变化的时间间隔，默认为 1000，单位毫秒
     *
     * void start()：开始监听，同步阻塞监听
     * void start(boolean async)： async 是否异步执行
     *
     * @throws IOException
     */
    @Test
    public void testTailer() throws IOException {
        // 1、如果被跟踪的文件不存在，则新建
        File homeDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
        File destFile = new File(homeDirectory,"6.txt");
        if (!destFile.exists()){
            destFile.createNewFile();
        }

        // 2、确定跟踪器启动时预读取的行数
        int initReadLine =0;
        FileReader fileReader = FileReader.create(destFile, CharsetUtil.CHARSET_UTF_8);
        int size = fileReader.readLines().size();
        initReadLine = size >= 10 ? 10 : size;

        // 3、跟踪目标文件,默认使用 命令行打印的行处理器，跟踪的结果会打印到控制台
        Tailer tailer = new Tailer(destFile, Tailer.CONSOLE_HANDLER, initReadLine);
        tailer.start(true);
        System.out.println(destFile);

        // 4、因为上面异步跟踪，所以主线程故意让他阻塞
        Scanner scanner = new Scanner(System.in);
        scanner.next();
    }
}
