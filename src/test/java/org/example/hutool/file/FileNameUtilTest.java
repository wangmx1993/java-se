package org.example.hutool.file;

import cn.hutool.core.io.file.FileNameUtil;
import cn.hutool.core.lang.Console;
import org.junit.Test;

import java.io.File;
import java.net.URL;

/**
 * 文件名工具-FileNameUtil
 * 1、文件名操作工具类，主要针对文件名获取主文件名、扩展名等操作，同时针对Windows平台，清理无效字符。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/18 15:32
 */
public class FileNameUtilTest {

    /**
     * String extName(String fileName)
     * String extName(File file): 获取文件扩展名（后缀名），扩展名不带“.”，如 txt
     * String getName(String filePath)
     * String getName(File file): 返回文件名，如 abc.txt
     * String getPrefix(String fileName)
     * String getPrefix(File file): 返回主文件名,如 abc
     * String getSuffix(File file)
     * String getSuffix(String fileName): 获得文件后缀名，扩展名不带“.”，如 txt
     * String mainName(String fileName)
     * String mainName(File file): 等同 getPrefix
     *
     */
    @Test
    public void testExtName(){
        URL resource = FileReaderTest.class.getClassLoader().getResource("data/config.properties");
        File file = new File(resource.getPath());
        System.out.println(file);//E:\IDEA_Projects\java-org.se\target\test-classes\data\config.properties

        String extName = FileNameUtil.extName(file);
        String name = FileNameUtil.getName(file);
        String prefix = FileNameUtil.getPrefix(file);
        String suffix = FileNameUtil.getSuffix(file);
        String mainName = FileNameUtil.mainName(file);

        System.out.println(extName);//properties
        System.out.println(name);//config.properties
        System.out.println(prefix);//config
        System.out.println(suffix);//properties
        System.out.println(mainName);//config
    }

    /**
     * boolean isType(String fileName, String... extNames): 根据文件名检查文件类型，忽略大小写
     * 	* @param fileName 文件名，例如hutool.png
     * 	* @param extNames 被检查的扩展名数组，同一文件类型可能有多种扩展名，扩展名不带“.”
     * 	* @return 是否是指定扩展名的类型
     */
    @Test
    public void testIsType(){
        String path = "https://avatar.csdnimg.cn/6/D/4/3_wangmx1993328.jpg";
        boolean type1 = FileNameUtil.isType(path, "png", "gif", "jpeg");
        boolean type2 = FileNameUtil.isType(path, "png", "gif", "jpeg","JPG");
        Console.log(type1, type2);//false true
    }

}
