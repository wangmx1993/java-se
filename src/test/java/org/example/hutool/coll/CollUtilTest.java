package org.example.hutool.coll;

import cn.hutool.core.collection.CollUtil;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 集合相关工具类 - CollUtil
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/4/30 9:46
 */
public class CollUtilTest {

    /**
     * ArrayList<T> newArrayList：新建一个 ArrayList
     * LinkedList<T> newLinkedList(T... values)：新建 LinkedList
     * BlockingQueue<T> newBlockingQueue(int capacity, boolean isLinked): 新建{@link BlockingQueue}
     * BlockingQueue: 阻塞队列，在队列为空时，获取元素的线程会等待队列变为非空,当队列满时，存储元素的线程会等待队列可用。
     * * @param capacity 容量
     * * @param isLinked 是否为链表形式
     * CopyOnWriteArrayList<T> newCopyOnWriteArrayList(Collection<T> collection)：新建一个线程安全的 CopyOnWriteArrayList
     */
    @Test
    public void testNewArrayList() {
        ArrayList<Integer> arrayList = CollUtil.newArrayList(1, 2, 3, 4, 5, 6);
        LinkedList<Integer> linkedList = CollUtil.newLinkedList(11, 22, 33, 44);

        CopyOnWriteArrayList<Integer> copyOnWriteArrayList = CollUtil.newCopyOnWriteArrayList(arrayList);
        copyOnWriteArrayList.addAll(linkedList);

        System.out.println(arrayList);//[1, 2, 3, 4, 5, 6]
        System.out.println(linkedList);//[11, 22, 33, 44]
        System.out.println(copyOnWriteArrayList);//[1, 2, 3, 4, 5, 6, 11, 22, 33, 44]
    }

    /**
     * <K, V> HashMap<K, V> newHashMap()
     * <K, V> HashMap<K, V> newHashMap(int size)
     * <K, V> HashMap<K, V> newHashMap(int size, boolean isOrder)：新建 HashMap 对象
     * * @param size 初始大小，由于默认负载因子0.75，传入的size会实际初始大小为size / 0.75
     * * @param isOrder Map的Key是否有序，有序返回 {@link LinkedHashMap}，否则返回 {@link HashMap}
     */
    @Test
    public void testNewHashMap() {
        HashMap<Object, Object> hashMap1 = new HashMap<>();
        HashMap<Object, Object> hashMap2 = new HashMap<>();
        HashMap<Object, Object> hashMap3 = new HashMap<>();

        hashMap1.put("id", 1000);
        hashMap1.put("name", "华安");
        hashMap1.put("age", 25);

        hashMap2.put("id", 1000);
        hashMap2.put("name", "华安");
        hashMap2.put("age", 25);

        hashMap3.put("id", 1000);
        hashMap3.put("name", "华安");
        hashMap3.put("age", 25);

        System.out.println(hashMap1);//{name=华安, id=1000, age=25}
        System.out.println(hashMap2);//{name=华安, id=1000, age=25}
        System.out.println(hashMap3);//{id=1000, name=华安, age=25}
    }

    /**
     * HashSet<T> newHashSet(boolean isSorted, Collection<T> collection)
     * HashSet<T> newHashSet(Collection<T> collection)
     * HashSet<T> newHashSet(T... ts)： 新建 HashSet 对象，元素无序
     * * @param isSorted   是否有序，有序返回 {@link LinkedHashSet}，否则返回{@link HashSet}
     * LinkedHashSet<T> newLinkedHashSet(T... ts)
     */
    @Test
    public void testNewHashSet() {
        HashSet<Integer> hashSet = CollUtil.newHashSet(3, 4, 5, 1, 2, 22, 6, 45, 456);
        LinkedHashSet<Integer> linkedHashSet = CollUtil.newLinkedHashSet(3, 4, 5, 1, 2, 22, 6, 45, 456);
        System.out.println(hashSet);//[1, 2, 3, 4, 5, 22, 6, 456, 45]
        System.out.println(linkedHashSet);//[3, 4, 5, 1, 2, 22, 6, 45, 456]
    }

    /**
     * Collection<T> addAll(Collection<T> collection, Iterator<T> iterator)：将 iterator 加入到 collection 中
     * Collection<T> addAll(Collection<T> collection, Object value)：将指定对象全部加入到集合(collection)中
     * * @param value      对象，可能为Iterator、Iterable、Enumeration、Array
     * Collection<T> addAll(Collection<T> collection, T[] values)：将数组元素加入到集合（collection）中
     * List<T> addAllIfNotContains(List<T> list, List<T> otherList):将另一个列表中的元素加入到列表中，如果列表中已经存在此元素则忽略之
     */
    @Test
    public void testAddAll() {
        ArrayList<Integer> arrayList1 = CollUtil.newArrayList(1, 2, 3);
        ArrayList<Integer> arrayList2 = CollUtil.newArrayList(22, 3, 44);

        CollUtil.addAll(arrayList1, arrayList2);
        System.out.println(arrayList1);//[1, 2, 3, 22, 3, 44]

        CollUtil.addAll(arrayList2, 55);
        System.out.println(arrayList2);//[22, 3, 44, 55]

        CollUtil.addAllIfNotContains(arrayList1, arrayList2);
        System.out.println(arrayList1);//[1, 2, 3, 22, 3, 44, 55]
    }

    /**
     * boolean contains(Collection<?> collection, Object value): 判断指定集合是否包含指定值，如果集合为空（null或者空），返回{@code false}，否则找到元素返回{@code true}
     * boolean containsAll(Collection<?> coll1, Collection<?> coll2):集合1中是否包含集合2中所有的元素，即集合2是否为集合1的子集
     * boolean containsAny(Collection<?> coll1, Collection<?> coll2) 其中一个集合在另一个集合中是否至少包含一个元素，即是两个集合是否至少有一个共同的元素
     * clear(Collection<?>... collections)：清除一个或多个集合内的元素，每个集合调用clear()方法
     */
    @Test
    public void testContains() {
        ArrayList<Integer> arrayList1 = CollUtil.newArrayList(1, 2, 3);
        ArrayList<Integer> arrayList2 = CollUtil.newArrayList(22, 3, 44);

        System.out.println(CollUtil.contains(arrayList1, 3));//true
        System.out.println(CollUtil.containsAny(arrayList1, arrayList2));//true
        System.out.println(CollUtil.containsAll(arrayList1, arrayList2));//false

        CollUtil.clear(arrayList1);
        System.out.println(arrayList1);//[]
    }

    /**
     * ArrayList<T> distinct(Collection<T> collection)：对集合元素进行去重
     * List<T> emptyIfNull(List<T> set): 如果提供的集合为{@code null}，返回一个【不可变】的默认空集合，否则返回原集合
     * Set<T> emptyIfNull(Set<T> set)：如果提供的集合为{@code null}，返回一个不可变的默认空集合，否则返回原集合<br>
     */
    @Test
    public void testDistinct1() {
        ArrayList<Integer> arrayList1 = CollUtil.newArrayList(1, 2, 3, 2);

        Collection<Integer> distinct = CollUtil.distinct(arrayList1);
        System.out.println(distinct);//[1, 2, 3]

        Set<String> set = null;
        Set<String> set1 = CollUtil.emptyIfNull(set);
        System.out.println(set1);// []
    }

    @Test
    public void testDistinct2() {
        List<String> arrayList = CollUtil.newArrayList("001001", "001002", "001001", null, null);
        List<String> distinct = CollUtil.distinct(arrayList);
        // [001001, 001002, 001001, null, null]
        System.out.println(arrayList);
        // [001001, 001002, null]
        System.out.println(distinct);
    }

    /**
     * T removeNull(T collection):去除{@code null} 元素，此方法直接修改原集合
     * 如果集合本身是 null，则原样返回。
     */
    @Test
    public void testRemoveNull() {
        List<String> dataList = new ArrayList<>();
        dataList.add("1");
        dataList.add("2");
        dataList.add(null);
        dataList.add("");
        dataList.add(null);
        dataList.add(null);

        // [1, 2, null, , null, null]
        System.out.println(dataList);
        CollUtil.removeNull(dataList);
        // [1, 2, ]
        System.out.println(dataList);
    }

    /**
     * T removeBlank(T collection) 去除{@code null}或者""或者空白字符串 元素，此方法直接修改原集合
     * 如果集合本身是 null，则原样返回。
     */
    @Test
    public void testRemoveBlank() {
        List<String> dataList = new ArrayList<>();
        dataList.add("1");
        dataList.add("2");
        dataList.add(null);
        dataList.add("");
        dataList.add("  ");
        dataList.add(null);

        CollUtil.removeBlank(dataList);
        // [1, 2]
        System.out.println(dataList);
    }

    /**
     * T removeEmpty(T collection) : 去除{@code null}或者"" 元素，此方法直接修改原集合
     * 如果集合本身是 null，则原样返回。
     */
    @Test
    public void testRemoveEmpty() {
        List<String> dataList = new ArrayList<>();
        dataList.add("1");
        dataList.add("2");
        dataList.add(null);
        dataList.add("");
        dataList.add("  ");
        dataList.add(null);

        CollUtil.removeEmpty(dataList);
        // [1, 2,   ]
        System.out.println(dataList);
    }

    /**
     * Collection<T> disjunction(Collection<T> coll1, Collection<T> coll2)
     * 获取两个集合的双向差集。
     * 针对一个集合中存在多个相同元素的情况，计算两个集合中此元素的个数，保留两个集合中此元素个数差的个数<br>
     * 例如：
     * <pre>
     *     disjunction([a, b, c, c, c], [a, b, c, c]) -》 [c]
     *     disjunction([a, b], [])                    -》 [a, b]
     *     disjunction([a, b, c], [b, c, d])          -》 [a, d]
     * </pre>
     * 任意一个集合为空，返回另一个集合<br>
     * 两个集合无差集则返回空集合。
     * 特别提醒：这个方法不是常规意义上的 coll1 减去 coll2，而是 coll1 - coll2=差集1，coll2 - coll1=差集2，然后两个插件求并集得到最终的结果，
     */
    @Test
    public void testDisjunction() {
        List<Integer> list1 = CollUtil.newArrayList(1, 3, 4, 4, 4, 6, null, null);
        List<Integer> list2 = CollUtil.newArrayList(1, 2, 3, 4, 5, 5, 7, null);
        List<Integer> list3 = CollUtil.newArrayList();

        Collection<Integer> disjunction = CollUtil.disjunction(list1, list2);
        // [1, 3, 4, 4, 4, 6, null, null]
        System.out.println(list1);
        // [1, 2, 3, 4, 5, 5, 7, null]
        System.out.println(list2);
        // [2, 4, 4, 5, 5, 6, 7]
        System.out.println(disjunction);

        Collection<Integer> disjunction2 = CollUtil.disjunction(list3, list2);
        // [1, 2, 3, 4, 5, 5, 7, null]
        System.out.println(disjunction2);
    }

    /**
     * Collection<T> subtract(Collection<T> coll1, Collection<T> coll2)
     * 计算集合的单向差集，即只返回【集合1】中有，但是【集合2】中没有的元素，
     * 对应重复元素会自动去重。
     * 例如：subtract([1,2,3,4,4,4], [2,3,4,5]) -》 [1]
     */
    @Test
    public void testSubtract() {
        List<Integer> list1 = CollUtil.newArrayList(1, 3, 4, 4, 4, 6);
        List<Integer> list2 = CollUtil.newArrayList(1, 2, 3, 4, 5, 5, 7);
        List<Integer> list3 = CollUtil.newArrayList();

        Collection<Integer> disjunction = CollUtil.subtract(list1, list2);
        // [1, 3, 4, 4, 4, 6, null]
        System.out.println(list1);
        // [1, 2, 3, 4, 5, 5, 7]
        System.out.println(list2);
        // [6, null]
        System.out.println(disjunction);

        Collection<Integer> disjunction2 = CollUtil.subtract(list3, list2);
        // []
        System.out.println(disjunction2);
    }

    /**
     * List<T> subtractToList(Collection<T> coll1, Collection<T> coll2)
     * 计算集合的单差集，即只返回【集合1】中有，但是【集合2】中没有的元素.
     * 双方都有且重复元素会自动去重。
     * 例如：subtractToList([1,2,3,4,4,4] ,[2,3,4,5]) -》 [1]
     */
    @Test
    public void testSubtractToList() {
        List<Integer> list1 = CollUtil.newArrayList(1, 3, 4, 4, 4, 6, null);
        List<Integer> list2 = CollUtil.newArrayList(1, 2, 3, 4, 5, 5, 7);
        List<Integer> list3 = CollUtil.newArrayList();

        List<Integer> disjunction = CollUtil.subtractToList(list1, list2);
        // [1, 3, 4, 4, 4, 6, null]
        System.out.println(list1);
        // [1, 2, 3, 4, 5, 5, 7]
        System.out.println(list2);
        // [6, null]
        System.out.println(disjunction);

        List<Integer> disjunction2 = CollUtil.subtractToList(list3, list2);
        // []
        System.out.println(disjunction2);

        // [2, 5, 5, 7]
        System.out.println(CollUtil.subtractToList(list2, list1));
    }

    @Test
    public void testSubtractToList2() {
        List<String> list1 = CollUtil.newArrayList("001001", "001001", "001002", "001002", "001003", null);
        List<String> list2 = CollUtil.newArrayList("001001", "001002", "001002", "001002", "001004", "001005");
        // [001003, null]
        System.out.println(CollUtil.subtractToList(list1, list2));
        // [001004, 001005]
        System.out.println(CollUtil.subtractToList(list2, list1));
    }

    /**
     * Collection<T> intersection(Collection<T> coll1, Collection<T> coll2)
     * 两个集合的交集<br>
     * 针对一个集合中存在多个相同元素的情况，计算两个集合中此元素的个数，保留最少的个数<br>
     * 例如：集合1：[a, b, c, c, c]，集合2：[a, b, c, c]<br>
     * 结果：[a, b, c, c]，此结果中只保留了两个 c。
     * coll1、coll2 任意一个为空，则返回空集合。
     */
    @Test
    public void testIntersection1() {
        List<String> names1 = new ArrayList<>();
        List<String> names2 = new ArrayList<>();
        names1.add("张三");
        names1.add("张三");
        names1.add("李四");
        names1.add("李四");

        names2.add("王五");
        names2.add("张三");
        names2.add("张三");
        names2.add("张三");
        Collection<String> intersection = CollUtil.intersection(names1, names2);
        // [张三, 张三, 李四, 李四]
        System.out.println(names1);
        // [王五, 张三, 张三, 张三]
        System.out.println(names2);
        // [张三, 张三]
        System.out.println(intersection);
    }

    /**
     * Collection<T> intersection(Collection<T> coll1, Collection<T> coll2, Collection<T>... otherColls)
     * 多个集合的交集<br>
     * 针对一个集合中存在多个相同元素的情况，计算两个集合中此元素的个数，保留最少的个数<br>
     * 例如：集合1：[a, b, c, c, c]，集合2：[a, b, c, c]<br>
     * 结果：[a, b, c, c]，此结果中只保留了两个c。
     * 任意一个集合为空，则返回空集合。
     */
    @Test
    public void testIntersection2() {
        List<Integer> list1 = CollUtil.newArrayList(1, 4, 3, 4, 4, 6);
        List<Integer> list2 = CollUtil.newArrayList(1, 2, 3, 4, 4, 5);
        List<Integer> list3 = CollUtil.newArrayList(2, 2, 4, 4, 5, 5);

        Collection<Integer> intersection = CollUtil.intersection(list1, list2, list3);
        // [1, 4, 3, 4, 4, 6]
        System.out.println(list1);
        // [1, 2, 3, 4, 4, 5]
        System.out.println(list2);
        // [2, 2, 4, 4, 5, 5]
        System.out.println(list3);
        // [4, 4]
        System.out.println(intersection);
    }

    /**
     * Set<T> intersectionDistinct(Collection<T> coll1, Collection<T> coll2, Collection<T>... otherColls)
     * 多个集合的交集，对结果去重<br>
     * 针对一个集合中存在多个相同元素的情况，只保留一个<br>
     * 例如：集合1：[a, b, c, c, c]，集合2：[a, b, c, c]<br>
     * 结果：[a, b, c]，此结果中只保留了一个c
     */
    @Test
    public void testIntersectionDistinct() {
        List<Integer> list1 = CollUtil.newArrayList(1, 4, 3, 4, 4, 6);
        List<Integer> list2 = CollUtil.newArrayList(1, 2, 3, 4, 4, 5);
        List<Integer> list3 = CollUtil.newArrayList(2, 2, 4, 4, 5, 5);

        Set<Integer> intersection = CollUtil.intersectionDistinct(list1, list2, list3);
        // [1, 4, 3, 4, 4, 6]
        System.out.println(list1);
        // [1, 2, 3, 4, 4, 5]
        System.out.println(list2);
        // [2, 2, 4, 4, 5, 5]
        System.out.println(list3);
        // [4]
        System.out.println(intersection);
    }

    /**
     * String join(Iterable<T> iterable, CharSequence conjunction)
     * 以 conjunction 为分隔符将集合转换为字符串<br>
     * 如果集合元素为数组、{@link Iterable}或{@link Iterator}，则递归组合其为字符串
     * <p>
     * String join(Iterable<T> iterable, CharSequence conjunction, String prefix, String suffix)
     * 以 conjunction 为分隔符将集合转换为字符串
     * prefix：每个元素添加的前缀，null表示不添加
     * suffix：每个元素添加的后缀，null表示不添加
     */
    @Test
    public void testJoin1() {
        List<Integer> list1 = CollUtil.newArrayList(1, 4, 3, 4, 4, 6);
        String join = CollUtil.join(list1, ",", "'", "'");
        // '1','4','3','4','4','6'
        System.out.println(join);
    }

    @Test
    public void testJoin2() {
        List<Integer> list1 = CollUtil.newArrayList(1, 4, 3, 4, 4, 6);
        List<Integer> list2 = CollUtil.newArrayList(11, 41, 31, 41, 41, 61);
        List<List<Integer>> list = CollUtil.newArrayList(list1, list2);
        String join = CollUtil.join(list, ",");
        // 1,4,3,4,4,6,11,41,31,41,41,61
        System.out.println(join);
    }

    /**
     * 比如将集合中的消息转为 html 文本
     */
    @Test
    public void testJoin3() {
        List<String> list1 = CollUtil.newArrayList("中国", "美国", "英国", "法国", "日本", "朝鲜");
        String join = CollUtil.join(list1, "", "&nbsp;", "<br/>");
        // &nbsp;中国<br/>&nbsp;美国<br/>&nbsp;英国<br/>&nbsp;法国<br/>&nbsp;日本<br/>&nbsp;朝鲜<br/>
        System.out.println(join);
    }

    /**
     * Collection<T> union(Collection<T> coll1, Collection<T> coll2)
     * * 两个集合的并集<br>
     * Collection<T> union(Collection<T> coll1, Collection<T> coll2, Collection<T>... otherColls)
     * * 多个集合的并集
     * * 针对一个集合中存在多个相同元素的情况，计算两个集合中此元素的个数，保留最多的个数<br>
     * * 例如：集合1：[a, b, c, c, c]，集合2：[a, b, c, c]<br>
     * * 结果：[a, b, c, c, c]，此结果中只保留了三个c
     * List<T> unionAll(Collection<T> coll1, Collection<T> coll2, Collection<T>... otherColls)
     * * 多个集合的完全并集，类似于SQL中的“UNION ALL”<br>
     * * 针对一个集合中存在多个相同元素的情况，保留全部元素<br>
     * * 例如：集合1：[a, b, c, c, c]，集合2：[a, b, c, c]<br>
     * * 结果：[a, b, c, c, c, a, b, c, c]
     * Set<T> unionDistinct(Collection<T> coll1, Collection<T> coll2, Collection<T>... otherColls)
     * * 多个集合的非重复并集，类似于SQL中的“UNION DISTINCT”<br>
     * * 针对一个集合中存在多个相同元素的情况，只保留一个<br>
     * * 例如：集合1：[a, b, c, c, c]，集合2：[a, b, c, c]<br>
     * * 结果：[a, b, c]，此结果中只保留了一个c
     */
    @Test
    public void testUnion() {
        List<Integer> list1 = CollUtil.newArrayList(1, 4, 3, 4, 4, 6);
        List<Integer> list2 = CollUtil.newArrayList(1, 2, 3, 4, 4, 5);

        // [1, 2, 3, 4, 4, 4, 5, 6]
        System.out.println(CollUtil.union(list1, list2));
        // [1, 4, 3, 4, 4, 6, 1, 2, 3, 4, 4, 5]
        System.out.println(CollUtil.unionAll(list1, list2));
        // [1, 4, 3, 6, 2, 5]
        System.out.println(CollUtil.unionDistinct(list1, list2));
    }

}
