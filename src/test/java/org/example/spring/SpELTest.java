package org.example.spring;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ArrayUtil;
import org.example.QLExpress.Person;
import org.junit.Test;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * SpEL(Spring EL) 全称为 Spring Expression Language（即 Spring 表达式语言）是一种强大的表达式语言
 * 在 Spring 产品组合中，它是表达式计算的基础。它支持在运行时查询和操作对象图，它可以与基于 XML 和基于注解的 Spring 配置还有 bean 定义一起使用。由于它能够在运行时动态分配值，因此可以为我们节省大量 Java 代码。
 * 官方文档：https://docs.spring.io/spring-framework/reference/core/expressions.html。
 * 官方示例：https://github.com/spring-projects/spring-framework/tree/main/spring-expression。
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2024/12/29 10:34
 */
public class SpELTest {

    @Test
    public void testHelloWorld() {
        // ExpressionParser 接口负责解析表达式字符串
        ExpressionParser parser = new SpelExpressionParser();
        // Expression 接口负责计算定义的表达式字符串。
        // 调用 parser.parseExpression（...） 和 exp.getValue（...） 时可以引发的两种类型的异常分别是 ParseException 和 EvaluationException。
        Expression exp = parser.parseExpression("'Hello World'");// 计算文本字符串表达式，单引号必须有，表示它是一个完整的文本字符串表达式.
        String message = (String) exp.getValue();
        System.out.println(message);// Hello World
    }

    @Test
    public void testTextExpressionConcat1() {
        // SpEL 支持广泛的功能，例如调用方法、访问属性和调用构造函数。
        ExpressionParser parser = new SpelExpressionParser();
        Expression exp = parser.parseExpression("'Hello World'.concat('!')");// 对字符串文本 Hello World 调用 concat 方法。
        String message = (String) exp.getValue();
        System.out.println(message);// Hello World!
    }

    @Test
    public void testBytes() {
        ExpressionParser parser = new SpelExpressionParser();
        // invokes 'getBytes()'
        Expression exp = parser.parseExpression("'Hello World'.bytes");// 访问字符串文本 Hello World 的 Bytes JavaBean 属性。
        byte[] bytes = (byte[]) exp.getValue();
        System.out.println(ArrayUtil.toString(bytes));// [72, 101, 108, 108, 111, 32, 87, 111, 114, 108, 100]
        System.out.println(new String(bytes));// Hello World
    }

    @Test
    public void testLength() {
        // SpEL 还通过使用标准点表示法（例如 prop1.prop2.prop3）以及相应的属性值设置来支持嵌套属性。还可以访问 Public 字段。
        ExpressionParser parser = new SpelExpressionParser();
        // invokes 'getBytes().length'
        Expression exp = parser.parseExpression("'Hello World'.bytes.length");// 使用点表示法获取字符串文本的长度
        int length = (Integer) exp.getValue();
        System.out.println(length);// 11
    }

    @Test
    public void testToUpperCase() {
        ExpressionParser parser = new SpelExpressionParser();
        // 可以调用 String 的构造函数，而不是使用字符串文本。从文本构造一个新的 String 并将其转换为大写。
        Expression exp = parser.parseExpression("new String('hello world').toUpperCase()");
        // 泛型方法 public <T> T getValue(Class<T> desiredResultType). 无需将表达式的值强制转换为所需的结果类型。
        // 如果值无法强制转换为类型 T 或使用已注册的类型转换器进行转换，则会引发 EvaluationException。
        String message = exp.getValue(String.class);
        System.out.println(message);// HELLO WORLD
    }

    /**
     * SpEL 更常见的用法是提供一个表达式字符串，该字符串针对特定对象实例（称为根对象）进行评估。
     * 以下示例说明如何从 Inventor 类的实例中检索 name 特性，以及如何在布尔表达式中引用 name 特性。
     */
    @Test
    public void testJavBean() {
        // Java Bean对象
        Person person = new Person(1001, "张三", DateUtil.parse("1993/08/25"), 13500.88F);
        ExpressionParser parser = new SpelExpressionParser();

        Expression exp = parser.parseExpression("name"); // 解析表达式中name属性;如果目标对象没有此属性，则报错：org.springframework.expression.spel.SpelEvaluationException: EL1008E: Property or field 'xxx' cannot be found on object of type 'org.example.QLExpress.Person' - maybe not public or not valid?
        String name = (String) exp.getValue(person);
        System.out.println(name);// 张三

        exp = parser.parseExpression("name == 'Nikola Tesla'");// name属性值是否等于后者
        System.out.println(exp.getValue(person, Boolean.class));// false

        exp = parser.parseExpression("name == '张三'");// name属性值是否等于后者
        System.out.println(exp.getValue(person, Boolean.class));// true
    }

    /**
     * 文本表达式
     * 1、字符串可以用单引号 （'） 或双引号 （“） 分隔。要在用单引号括起来的字符串文本中包含单引号，请使用两个相邻的单引号字符。
     * 同样，要在用双引号括起来的字符串文本中包含双引号，请使用两个相邻的双引号字符。
     * 2、数字支持使用负号、指数表示法和小数点。
     */
    @Test
    public void testLiteralExpressions() {
        ExpressionParser parser = new SpelExpressionParser();

        String helloWorld = (String) parser.parseExpression("'Hello World'").getValue();
        System.out.println(helloWorld);//

        String pizzaParlor = (String) parser.parseExpression("'Tony''s Pizza'").getValue();
        System.out.println(pizzaParlor);// Tony's Pizza

        double avogadrosNumber = (Double) parser.parseExpression("6.0221415E+23").getValue();
        System.out.println(avogadrosNumber);// 6.0221415E23

        int maxValue = (Integer) parser.parseExpression("0x7FFFFFFF").getValue();
        System.out.println(maxValue);// 2147483647

        boolean trueValue = (Boolean) parser.parseExpression("true").getValue();
        System.out.println(trueValue);// true

        Object nullValue = parser.parseExpression("null").getValue();
        System.out.println(nullValue);// null

        System.out.println(parser.parseExpression("15 + 4 - 3.5").getValue());// 15.5
        System.out.println(parser.parseExpression("15-4.5+3.5").getValue());// 14.0
    }

}
