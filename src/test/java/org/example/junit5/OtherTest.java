package org.example.junit5;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * junit5 重复性测试、参数化测试、内嵌测试
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/9/12 11:17
 */
public class OtherTest {

    /**
     * 重复性测试
     * 1、在许多场景中需要对同一个接口方法进行重复测试，例如对幂等性接口的测试。
     * 2、JUnit Jupiter 通过使用 @RepeatedTest(n) 指定需要重复的次数
     */
    @RepeatedTest(10)
    @DisplayName("重复测试")
    public void repeatedTest() {
        System.out.println("调用：" + System.nanoTime());
    }

    /**
     * 参数化测试
     * 1、参数化测试可以按照多个参数分别运行多次单元测试。
     * 2、类似于重复性测试，只不过每次运行传入的参数不用。
     * 3、@ParameterizedTest：表示参数测试。
     * 4、@ValueSource：提供一组数据，表示参数值，支持八种基本类型以及 String 和自定义对象类型，使用极其方便。
     * 5、@DisplayName：定义类或方法的自定义显示名称。
     *
     * @param a
     */
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3})
    @DisplayName("参数化测试")
    public void paramTest(int a) {
        System.out.println(a);
        assertTrue(a > 0 && a < 4);
    }

    /**
     * 内嵌测试
     * 1、@Nested：表示被注解的类是一个嵌套的非静态测试类.
     */
    @Nested
    @DisplayName("内嵌Class测试")
    class OrderTestClazz {
        @Test
        @DisplayName("取消订单")
        public void cancelOrder() {
            int status = 200;
            System.out.println("取消订单成功,订单状态为:" + status);
        }
    }
}
