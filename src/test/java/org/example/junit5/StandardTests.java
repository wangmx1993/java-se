package org.example.junit5;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * junit5 单元测试基本使用
 *
 * @author wangMaoXiong
 * @version 1.0
 * @date 2021/9/12 9:31
 */
public class StandardTests {
    /**
     * @BeforeAll 最先运行，方法必须是 static 修饰
     * @BeforeEach 第二个运行
     * @Test 测试方法运行
     * @AfterEach 倒数第二个运行
     * @AfterAll 最后运行，方法必须是 static 修饰
     */

    @BeforeAll
    static void initAll() {
        System.out.println("BeforeAll");
    }

    @BeforeEach
    void init() {
        System.out.println("BeforeEach");
    }

    @Test
    void succeedingTest() {
        assertNotNull(100);
    }

    @Test
    void failingTest() {
        fail("a failing test");
    }

    @Test
    @Disabled("for demonstration purposes")
    void skippedTest() {
        // not executed
    }

    @AfterEach
    void tearDown() {
        System.out.println("AfterEach");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("AfterAll");
    }
}