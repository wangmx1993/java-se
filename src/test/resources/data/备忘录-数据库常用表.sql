-- ####### ysframe 库 start ####### --

select * from gap_module t where t.web_server = 'basic-web' order by t.name; -- web 功能表,其中的 web_server 字段对应微服务名称..
select * from gap_menu;-- 用户功能表。关联 module_id。平台配置的菜单就是存放在这里。parameter 自动可以为菜单配置参数，后台 org.se.getCustomParam().get("pos_type") 可以获取。id 、MODULE_ID 值保持与上面 gap_module 的 id 值一致
select * from gap_user  ; -- ysframe 用户表


select * from gap_virtual_database t; -- 后台微服务对应的 "逻辑数据库"
select * from gap_virtual_table t ; -- "逻辑数据库" 下的 "表". 用于平台做一些配置
select * from gap_data_field t ; -- "表" 中的 "字段"。 用于平台做一些配置

--1.1 老版 ui 视图版
select * from ptframe_1.gap_sys_module t order by t.module_id ;--ui视图菜单功能表
select * from ptframe_1.gap_sys_uiview t order by t.ui_code  ; -- 视图表
select * from ptframe_1.gap_sys_button t ; --按钮表
select * from ptframe_1.gap_sys_uidetail t order by t.field_index ; -- 字段表
select * from ptframe_1.gap_sys_uidetail_widget_attr ; --视图、字段高级属性表
select * from  ptframe_1.gap_sys_uiwidget_attr t --控件属性表，用于辅助控件.
select * from ptframe_1.gap_sys_uiwidget ; --控件表.


--2.1 新版 ui 视图使用的 表
select * from GAP_UI_VIEW_GROUP t;
select * from GAP_UI_VIEW_GROUP t where t.view_cate_id = 21 order by code; -- 视图分组表

select * from GAP_UI_VIEW t ;
select * from GAP_UI_VIEW t where t.group_id = 1601; -- 视图表

select * from GAP_UI_COLUMN t ;
select * from GAP_UI_COLUMN t where t.view_id = 2440 order by t.disp_order; -- 视图字段表

select * from GAP_UI_BUTTON t ;
select * from GAP_UI_BUTTON t where t.view_id = 2440; -- 视图按钮表
select * from GAP_UI_BUTTON_WF_STATUS; -- 按钮流程状态关系表

select * from GAP_UI_VIEW_TYPE; -- 视图类型表,平台使用，业务系统不需要关心
select * from GAP_UI_WIDGET; -- 视图字段显示控件表
select * from GAP_UI_WIDGET_ATTR; -- 视图字段显示控件表
select * from GAP_UI_WIDGET_ATTR_FIELD;

-- ####### ysframe 库 end ####### --


-- ####### yselement 库 start ####### --

select * from gap_element t; -- yselement 库下要素关联关系表，其中的 ele_source 为关联的具体要素表
select * from ptelement_1.gap_ele_agency; --财政单位/机构表。单位属于要素。
select * from GFM_ELE_RY_ZW; --职务（职称）
select * from gap_no_seq ; --查找指定序列的值（以前老版是在 frame库里面的，现在2.1版本迁移导了 element） 库中）
-- ####### yselement 库 end ####### --


-- ####### hnbs 库 start ####### --

select * from gbm_audit_bill;	-- 审核单表  .audit_type > 1:单位-人事 2：单位-财务 3：人员 ;is_end > 0:录入中 -1：送审中 1：送审完成

select * from gbm_bb_jgb;	--编办表 jgid 是主键
select * from gap_ele_agency; --财政单位/机构表
select * from gbm_bb_mof_org; --编办机构与财政机构关联关系表 jgid 与 agency_id 做联合主键

select * from gbm_bs_affix ; --附件表

select * from gfm_bs_asset_facility ; --资产信息表 ，没有流程 
select * from gfm_bs_car ; --交通工具 ，没有流程

select * from gap_wf_task;   --工作流程数据,流程走完。数据会删除掉，1.1 版本是在 hnbs_3 库中，2.1 版本是在 workflow 库中.
select * from gap_wf_tasklog;  --工作流程日志,流程走完，数据会删除掉。其中退回填写的退回意见会存储在 TASK_OPINION 字段中。
-- gap_wf_tasklog 数据会删除掉，1.1 版本是在 hnbs_3 库中，2.1 版本是在 workflow 库中.




--人事和财务共用一个接口表，只有人事有编办接收，确认接收后会同时插入人事和财务当前表 ，会创建两个审核单
select * from Gbm_Bs_Ageninfo_interface ;--接口表，用于编办接收 
select * from Gbm_Bs_Ageninfo;--人事当前表 ，接收之后，数据就会放入此表 
select * from Gbm_Bs_Ageninfo_lib;--人事正式表，送审->审核通过之后，数据就会放入此表 

select * from Gbm_Bs_Ageninfo_Detail ; --财务当前表，Gbm_Bs_Ageninfo_interface 接收之后，会同时插入一条数据到此表
select * from Gbm_Bs_Ageninfo_Detail_Lib ; --财务正式表,送审->审核通过之后，数据就会放入此表 


--人员信息库,数据会从另外一个 编办系统 自动同步过来。只有 "其他" 可以手动新增

select * FROM gbm_bs_zzry_xz_interface;--行政 接收表
select * from gbm_bs_zzry_sy_interface;--事业 接收表
select * from gbm_bs_ltx_xz_interface;--离休 接收表
select * from gbm_bs_tx_sy_interface;--退休 接收表
select * from gbm_bs_qtry_interface;--其他 接收表

select * FROM gbm_bs_zzry_xz;--行政当前表
select * from gbm_bs_zzry_sy;--事业当前表
select * from gbm_bs_ltx_xz;--离休当前表
select * from gbm_bs_tx_sy;--退休当前表
select * from gbm_bs_qtry;--其他当前表，页面点击 "人员增加" 的数据，直接存入此表

select * FROM gbm_bs_zzry_xz_lib;--行政正式表
select * from gbm_bs_zzry_sy_lib;--事业正式表
select * from gbm_bs_ltx_xz_lib;--离休正式表
select * from gbm_bs_tx_sy_lib;--退休正式表
select * from gbm_bs_qtry_lib;--其它人员正式表，审核通过后存入此表


select * from gfm_rp_datasource; -- 报表数据源。其中d DS_SAVE_TABLE 存储表名.
select * from gbm_import_batch; -- 基础信息库变动导入批次表

select * from gfm_rp_master; --基础信息库菜单对应的表格
select * from gfm_rp_cell;--表格字段表，其中的 data_source 字段关联 ptelement_1 下 gap_element 中要素 code
select * from gfm_rp_file ;-- 表单文件表，report_id 是主键


select * from gfm_repbill_reg ; -- 表单注册表，ui_code 是主键
select * from gfm_ps_uv_btn ; --菜单按钮表. MENU_ID 值与上面 gap_module 主键 id 保持一致
select * from gfm_repbill_menu ; -- 表单菜单表。MENU_ID 值与上面 gap_module 主键 id 保持一致. url 与 gap_module 表中的 url 一致。
select * from gfm_repbill_menu_report ; -- 菜单、ui_code、report_id 关联表. MENU_ID 值与上面 gap_module 主键 id 保持一致

select * from gfm_ps_uv_column; -- 用户功能视图控件
select * from gfm_ps_uv_view; -- 用户视图字段

select * from  gap_ele_agency; -- 单位表，upload_agency_code 为上级单位编码。业务库和 yselement 库都有一份.

--老版：sys_id关联 frame.gap_system_server 微服务表的 id
select * from gap_wf_process t; -- 工作流流程
select * from gap_wf_node t ; -- 工作流流程节点
select * from gap_wf_transfer ;  -- 工作流流程线
select * from gap_node_menu t ; -- 工作流程、工作流节点、用户功能关联表。menu_id 与 gap_module 主键 id 保持一致
select t.*,t.rowid from workflow2_1.gap_node_role t where proc_id in(20901,2045706002);--工作流节点、角色（gap_role）关联表


-- ####### hnbs 库 end ####### --


--001101 中共湖南省委办公厅机关    汪茂雄		where agency_id = 19156791
--001102101 湖南省委机要局机关     张星宇		where agency_id = 19157507
--001102301 湖南省密钥管理中心     高彩琼		where agency_id = 19157508
--001201 湖南省委办公厅机关图书馆  张蕊			where agency_id = 19156688
--001202 湖南省委办公厅文件管理中心   蒋洪		where agency_id = 19156691  
--001203 湖南省委办公厅机关房产维修队  郑建		where agency_id = 19156690  
--001205 《湖南工作》编辑部			公用		where agency_id = 19156684
--001204 湖南省委办公厅汽车队		公用		where agency_id = 19156687

--003001 湖南省人民政府办公厅本级				where agency_id = 202411
--003002 湖南省人民政府驻上海办事处				where agency_id = 202412


1 加密 后 password: 5FB208117C8111E1
录入岗： 登陆用户：001001	密码： 1	单位 id:	agency_id = 20792		单位名称：中共湖南省委办公厅本级	13812345678
录入岗： 登陆用户：002002  	密码： 1	单位 id：	agency_id = 20802     	单位名称：中共湖南省委机关医院		17752851626
审核岗： 登陆用户: 100100	密码： 1	单位 id:	agency_id = 20793		单位名称：中共湖南省委办公厅		13707317308		赵晓华


change_type_id 操作类型 ：
    {
        '17': '在职转离退休房补（增）',
        '18': '在职转离退休房补（减）',
        '19': '离退休转离退休房补（增）',
        '20': '离退休转离退休房补（减）',
        '1':'信息增加',
        '2':'人员调入',
        '3':'在职转退休',
        '4':'信息删除',
        '5':'人员调出',
        '6':'人员退休',
        '7':'信息调整',
        '8':'批量调整',
        '9':'行政转事业（增）',
        '10':'事业转行政（增）',
        '11':'行政转事业（减）',
        '12':'事业转行政（减）',
        '13':'离退行政转事业（增）',
        '14':'离退事业转行政（减）',
        '15':'离退行政转事业（减）',
        '16':'离退事业转行政（增）'
    };